﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface ICentralizeHHistoryRepository : IBaseRepository<CentralizeHHistory>
    {
        CentralizeHHistory getCentralizeHHistory(string id);
        CentralizeHHistory getCentralizeHHistoryCreate(string id);
        IList<CentralizeHHistory> getCentralizeHHistorys(IList<string> signatureHIds);
        bool IsDuplicate(string nik);
        PagedResult<CentralizeHHistory> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<CentralizeHHistory> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, DateTime start, DateTime end, User user, string[] subBranch);
        PagedResult<CentralizeHHistory> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        IList<CentralizeHHistory> FindforExport(string searchColumn, string searchValue, DateTime start, DateTime end);
    }
}
