﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;
using UOB.SVS.Model;
using UOB.SVS.Model.Helper;


namespace UOB.SVS.Interface
{
    public interface ICentralizeUnitHReqRepository : IBaseRepository<CentralizeUnitHRequest>
    {
        CentralizeUnitHRequest getCentralizeUnitHReq(string id);
        IList<CentralizeUnitHRequest> getCentralizeUnitHReqList(IList<string> CentralizeUnitHReqIds);
        bool IsDuplicate(string nik);
        PagedResult<CentralizeUnitHRequest> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<CentralizeUnitHRequest> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, CentralizeReqFilter filter);
        PagedResult<CentralizeUnitHRequest> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, CentralizeReqFilter filter, User user);
        PagedResult<CentralizeUnitHRequest> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<CentralizeUnitHRequest> FindAllPagedCreate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, CentralizeReqFilter filter);

        void DeleteCentralizeUnitHReq(string ID);

        IList<CentralizeDSignReq> getSignReqD(string nik);
        void AddSignReqD(CentralizeDSignReq item);
        void DeleteSignReqD(string Id);
        void DeleteSignReqD(long Id);


        IList<CentralizeDDocReq> getDocReqD(string nik);
        CentralizeDDocReq getDocReqD(long ID);
        void AddDocReqD(CentralizeDDocReq item);
        void DeleteDocReqD(string Id);
        void DeleteDocReqD(long Id);
    }
}
