﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface IAccountPrefixRepository : IBaseRepository<AccountPrefix>
    {
        AccountPrefix getAccountPrefix(string PrefixCode);
        AccountPrefix getAccountPrefix(long id);
        bool IsDuplicate(string PrefixCode);
        IList<AccountPrefix> getAccountPrefixes();
        PagedResult<AccountPrefix> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<AccountPrefix> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<AccountPrefix> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string branchCode);
    }
}
