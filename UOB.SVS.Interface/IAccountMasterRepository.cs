﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface IAccountMasterRepository : IBaseRepository<AccountMaster>
    {
        AccountMaster getAccount(string AccountNo);
        PagedResult<AccountMaster> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, SVSReqFilter filter, User user);
        PagedResult<AccountMaster> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, SVSReqFilter filter, User user,string[] subBranch);
        AccountMaster getAccountbyCIF(string CIFno);
    }
}
