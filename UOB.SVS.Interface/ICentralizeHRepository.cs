﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Model;
using UOB.SVS.Model.Helper;

namespace UOB.SVS.Interface
{
    public interface ICentralizeHRepository : IBaseRepository<CentralizeHeader>
    {
        CentralizeDDoc getDocDet(long Id);
        PagedResult<CentralizeHeader> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<CentralizeHeader> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, CentralizeReqFilter filter);
        CentralizeHeader getCentralizeHeader(string id, string name, string titles, string region, string division, string departement);
        CentralizeHeader getCentralizeHeaderNik(string nik);
        IList<CentralizeDDoc> getDocDet(string nik);
        IList<CentralizeDSign> getSignDet(string nik);
        void DeleteCentralizeH(string ID);
        void DeleteSignDet(string ID);
        void DeleteDocDet(string ID);
        void AddSignDet(CentralizeDSign item);
        void AddDocDet(CentralizeDDoc item);
        CentralizeHeader getCentralizeHeader(string id);
        void UpdateCHeader(CentralizeHeader centH);
        void AddDocDet(SignatureDDoc item);
        void AddSignDet(SignatureDSign item);
    }
}
