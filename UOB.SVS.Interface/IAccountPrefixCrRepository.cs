﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface IAccountPrefixCrRepository : IBaseRepository<AccountPrefixCr>
    {
        AccountPrefixCr getAccountPrefixCr(string PrefixCode);
        AccountPrefixCr getAccountPrefixCr(long id);
        bool IsDuplicate(string PrefixCode);
        IList<AccountPrefixCr> getAccountPrefixCres();
        //PagedResult<AccountPrefixCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        //PagedResult<AccountPrefixCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        //PagedResult<AccountPrefixCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string branchCode);
        AccountPrefixCr getExistingRequest(string ApprovalType, string PrefixCode);
    }
}
