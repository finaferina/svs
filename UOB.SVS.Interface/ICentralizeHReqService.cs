﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Treemas.Credential.Model;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface ICentralizeHReqService
    {
        string SaveCentralizeReq(CentralizeUnitHRequest centHReq);
        string SaveDocument(CentralizeDDocReq document);
        string SaveSign(CentralizeDSignReq sign);
        string ApproveReq(CentralizeUnitHRequest centHReq, CentralizeHeader centH, CentralizeHHistory centHistory, User user);
        string RejectReq(CentralizeUnitHRequest centHReq, User user);
        string CancelCentralizeReq(CentralizeUnitHRequest centHReq);
        string UpdateCentralizeReq(CentralizeUnitHRequest centHReq);
    }
}
