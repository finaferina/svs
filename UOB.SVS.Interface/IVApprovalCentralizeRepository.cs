﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface IVApprovalCentralizeRepository : IBaseRepository<VApprovalCentralize>
    {
        VApprovalCentralize GetApprovalCentralize(string id);
        PagedResult<VApprovalCentralize> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<VApprovalCentralize> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter);
        PagedResult<VApprovalCentralize> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter, User user, string[] arrFeature);
        PagedResult<VApprovalCentralize> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter, User user, string[] arrFeature, string searchColumn, string searchValue);
        PagedResult<VApprovalCentralize> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
    }
}
