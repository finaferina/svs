﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface IExceptionReportRepository: IBaseRepository<ExceptionReport>
    {
        PagedResult<ExceptionReport> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string reportType, DateTime start, DateTime end, User user, string[] subBranch);
        IList<ExceptionReport> FindAllExport(string searchColumn, string searchValue, string reportType, DateTime start, DateTime end, User user, string[] subBranch);
    }
}
