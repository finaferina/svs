﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface IBranchCrRepository : IBaseRepository<BranchCr>
    {
        BranchCr getBranch(string branchCode);
        BranchCr getBranch(long id);
        BranchCr getExistingRequest(string ApprovalType, string branchCode);
        IList<BranchCr> getBranchs(IList<string> branchCode);
        bool IsDuplicate(string branchCode);
        PagedResult<BranchCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<BranchCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey,  ApplicationCRFilter filter);
        PagedResult<BranchCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
    }
}
