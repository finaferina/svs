﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface IBranchAccountPrefixRepository : IBaseRepository<BranchAccountPrefix>
    {
        BranchAccountPrefix getAccountPrefix(string PrefixCode);
        BranchAccountPrefix getAccountPrefix(string BranchCode, string PrefixCode);
        BranchAccountPrefix getAccountPrefix(long id);
        int DeleteAccountPrefix(string BranchCode, string PrefixCode);
        bool IsDuplicate(string PrefixCode);
        IList<BranchAccountPrefix> getAccountPrefixes();
        IList<BranchAccountPrefix> getAccountPrefixes(List<string> Branches);
        IList<BranchAccountPrefix> getAccountPrefixes(string BranchCode);
        PagedResult<BranchAccountPrefix> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<BranchAccountPrefix> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string branchCode);
    }
}
