﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface IBranchAccountPrefixCrRepository : IBaseRepository<BranchAccountPrefixCr>
    {
        BranchAccountPrefixCr getAccountPrefixExisting(string PrefixCode);
        BranchAccountPrefixCr getAccountPrefix(string PrefixCode);
        BranchAccountPrefixCr getAccountPrefix(string BranchCode, string PrefixCode);
        BranchAccountPrefixCr getAccountPrefix(long id);
        bool IsDuplicate(string PrefixCode);
        int deleteRequest(string BranchCode, string PrefixCode);
        IList<BranchAccountPrefixCr> getAccountPrefixesRequest(string PrefixCode);
        IList<BranchAccountPrefixCr> getAccountPrefixesRequest(List<string> Branches);
        IList<BranchAccountPrefixCr> getAccountPrefixes();
        IList<BranchAccountPrefixCr> getAccountPrefixes(List<string> Branches);
        IList<BranchAccountPrefixCr> getAccountPrefixes(string BranchCode, string CreatedUser);
        PagedResult<BranchAccountPrefixCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<BranchAccountPrefixCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string branchCode);
    }
}
