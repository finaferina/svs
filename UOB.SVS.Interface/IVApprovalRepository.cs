﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface IVApprovalRepository : IBaseRepository<VApproval>
    {
        VApproval GetApproval(string id);
        PagedResult<VApproval> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<VApproval> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey,  ApplicationCRFilter filter);
        PagedResult<VApproval> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter, User user,string[] arrFeature);
        PagedResult<VApproval> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
    }
}
