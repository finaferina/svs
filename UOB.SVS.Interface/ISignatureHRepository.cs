﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Model;

namespace UOB.SVS.Interface
{
    public interface ISignatureHRepository : IBaseRepository<SignatureHeader>
    {
        SignatureHeader getSignatureHeader(string id);
        IList<SignatureHeader> getSignatureHeaders(IList<string> signatureHIds);
        bool IsDuplicate(string accno);
        PagedResult<SignatureHeader> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, SVSReqFilter filter);
        PagedResult<SignatureHeader> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);

        void DeleteSignatureH(string ID);

        void AddKTPDet(SignatureDKTP item);
        IList<SignatureDKTP> getKTPDet(string accountNo);
        void DeleteKTPDet(string ID);

        void AddSignDet(SignatureDSign item);
        IList<SignatureDSign> getSignDet(string accountNo);
        void DeleteSignDet(string ID);

        void AddDocDet(SignatureDDoc item);
        void UpdateSgHeader(SignatureHeader item);
        void UpdateDocListDet(IList<SignatureDDoc> item);
        void UpdateKTPListDet(IList<SignatureDKTP> item);
        void UpdateSignListDet(IList<SignatureDSign> item);
        void UpdateDocDet(SignatureDDoc item);
        void UpdateKTPDet(SignatureDKTP item);
        void UpdateSignDet(SignatureDSign item);
        void UpdateSignatureDetail(string AccountNo
            , IList<SignatureDDoc> docs
            , IList<SignatureDKTP> ktps
            , IList<SignatureDSign> signs);
        IList<SignatureDDoc> getDocDet(string accountNo);
        SignatureDDoc getDocDet(long Id);
        void DeleteDocDet(string ID);
    }
}
