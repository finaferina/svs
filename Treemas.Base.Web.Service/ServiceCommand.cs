﻿using System;

namespace Treemas.Base.Web.Service
{
    public abstract class ServiceCommand : IServiceCommand
    {
        private string name;

        public ServiceCommand(string name)
        {
            this.name = name;
        }

        public abstract ServiceResult Execute(ServiceParameter parameter);

        public string Name =>
            this.name;
    }
}

