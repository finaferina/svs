﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Treemas.Base.Web.Service
{
    
    public class ServiceClientFactory<T> : IDisposable
    {
        public ServiceClientFactory(string url) : this(url, null)
        {
        }

        public ServiceClientFactory(string url, Binding binding)
        {
            this.Endpoint = new EndpointAddress(url);
            if (binding == null)
            {
                this.ServiceBinding = ServiceBindings.CreateBasicBinding();
            }
            else
            {
                this.ServiceBinding = binding;
            }
            this.Factory = new ChannelFactory<T>(this.ServiceBinding, this.Endpoint);
        }

        public T Create() => 
            this.Factory.CreateChannel();

        public virtual void Dispose()
        {
            this.Factory.Close();
        }

        protected EndpointAddress Endpoint { get; set; }

        protected ChannelFactory<T> Factory { get; set; }

        protected Binding ServiceBinding { get; private set; }
    }
}

