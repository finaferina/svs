﻿using System;
using System.ServiceModel.Channels;
using Treemas.Base.Utilities;

namespace Treemas.Base.Web.Service
{
    public class BaseServiceClient<T> : IDisposable
    {
        public BaseServiceClient(T client)
        {
            this.Client = client;
        }

        public virtual void Dispose()
        {
            if (!this.Client.IsNull())
            {
                ((IChannel) this.Client).Close();
            }
        }

        public T Client { get; private set; }
    }
}

