﻿using System;
using System.Runtime.Serialization;

namespace Treemas.Base.Web.Service
{
    [DataContract(Namespace="Treemas.Base.Web.Service")]
    public class ServiceRuntimeParameter
    {
        [DataMember]
        public string Command { get; set; }

        [DataMember]
        public string ParameterString { get; set; }
    }
}

