﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.Base.Web.Service
{
    public class ServiceCommandPool
    {
        public ServiceCommandPool()
        {
            this.CommandMap = new Dictionary<string, IServiceCommand>();
        }

        public void AddCommand(IServiceCommand command)
        {
            if (command != null)
            {
                string name = command.Name;
                if (this.CommandMap.ContainsKey(name))
                {
                    this.CommandMap[name] = command;
                }
                else
                {
                    this.CommandMap.Add(name, command);
                }
            }
        }

        public void Clear()
        {
            if (!this.CommandMap.IsNull())
            {
                this.CommandMap.Clear();
            }
        }

        public IServiceCommand GetCommand(string name)
        {
            if (this.CommandMap.ContainsKey(name))
            {
                return this.CommandMap[name];
            }
            return null;
        }

        public void RemoveCommand(string name)
        {
            if (this.CommandMap.ContainsKey(name))
            {
                this.CommandMap.Remove(name);
            }
        }

        private IDictionary<string, IServiceCommand> CommandMap { get; set; }

        public ICollection<IServiceCommand> Commands =>
            this.CommandMap.Values;
    }
}

