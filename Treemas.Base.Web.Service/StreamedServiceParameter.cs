﻿using System;
using System.IO;
using Treemas.Base.Utilities;

namespace Treemas.Base.Web.Service
{
    public class StreamedServiceParameter : BaseServiceParameter
    {
        public StreamedServiceParameter() : this(null, delegate (JsonDataMap data) {
        })
        {
        }

        public StreamedServiceParameter(string command) : this(command, delegate (JsonDataMap param) {
        })
        {
        }

        public StreamedServiceParameter(string command, Action<JsonDataMap> paramAction) : base(command, paramAction)
        {
        }

        public static StreamedServiceParameter Create(StreamedServiceRuntimeParameter runtimeParam)
        {
            StreamedServiceParameter parameter = new StreamedServiceParameter();
            if (!runtimeParam.IsNull())
            {
                parameter.Data = runtimeParam.Data;
                parameter.Command = runtimeParam.Command;
                parameter.Parameters.Clear();
                parameter.Parameters.FromString(runtimeParam.ParameterString);
            }
            return parameter;
        }

        public void FromRuntime(StreamedServiceRuntimeParameter param)
        {
            this.Command = null;
            base.Parameters.Clear();
            if (!param.IsNull())
            {
                this.Data = param.Data;
                this.Command = param.Command;
                base.Parameters.Clear();
                base.Parameters.FromString(param.ParameterString);
            }
        }

        public StreamedServiceRuntimeParameter ToRuntime() => 
            new StreamedServiceRuntimeParameter { 
                Data = this.Data,
                Command = this.Command,
                ParameterString = base.Parameters.ToString()
            };

        public Stream Data { get; set; }
    }
}

