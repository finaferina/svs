﻿using System;
using Treemas.Base.Utilities;

namespace Treemas.Base.Web.Service
{
    public class ServiceResult : BaseServiceResult
    {
        public ServiceResult()
        {
        }

        public ServiceResult(Action<JsonDataMap> dataAction) : base(dataAction)
        {
        }

        public static ServiceResult Create(ServiceRuntimeResult runtimeResult)
        {
            if (!runtimeResult.IsNull())
            {
                ServiceResult result = new ServiceResult();
                result.Data.Clear();
                result.Data.FromString(runtimeResult.DataString);
                return result;
            }
            return null;
        }

        public void FromRuntime(ServiceRuntimeResult result)
        {
            base.Status = ServiceStatus.OK;
            base.Data.Clear();
            if (!result.IsNull())
            {
                base.Data.Clear();
                base.Data.FromString(result.DataString);
            }
        }

        public ServiceRuntimeResult ToRuntime()
        {
            ServiceRuntimeResult result = new ServiceRuntimeResult();
            if (!result.IsNull())
            {
                result.DataString = base.Data.ToString();
            }
            return result;
        }
    }
}

