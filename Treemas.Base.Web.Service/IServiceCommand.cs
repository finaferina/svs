﻿using System;

namespace Treemas.Base.Web.Service
{
    public interface IServiceCommand
    {
        string Name { get; }
    }
}

