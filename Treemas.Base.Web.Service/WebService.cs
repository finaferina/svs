﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;

namespace Treemas.Base.Web.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode=AspNetCompatibilityRequirementsMode.Allowed)]
    public class WebService : IWebService, IDisposable
    {
        public WebService()
        {
            this.Commands = new ServiceCommandPool();
        }

        public virtual void Dispose()
        {
        }

        public virtual ServiceRuntimeResult Execute(ServiceRuntimeParameter parameter)
        {
            if (this.Commands != null)
            {
                try
                {
                    IServiceCommand command = this.Commands.GetCommand(parameter.Command);
                    if (command != null)
                    {
                        ServiceResult result = ((ServiceCommand) command).Execute(ServiceParameter.Create(parameter));
                        if (result != null)
                        {
                            return result.ToRuntime();
                        }
                    }
                }
                catch (Exception exception)
                {
                    string message;
                    if (exception.InnerException != null)
                    {
                        message = $"{exception.Message}. {exception.InnerException.Message}";
                    }
                    else
                    {
                        message = exception.Message;
                    }
                    throw new FaultException(message);
                }
            }
            return null;
        }

        protected ServiceCommandPool Commands { get; set; }
    }
}

