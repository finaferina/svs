﻿using System;

namespace Treemas.Base.Web.Service
{
    public abstract class StreamedServiceCommand : IServiceCommand
    {
        private string name;

        public StreamedServiceCommand(string name)
        {
            this.name = name;
        }

        public abstract StreamedServiceResult Execute(StreamedServiceParameter parameter);

        public string Name =>
            this.name;
    }
}

