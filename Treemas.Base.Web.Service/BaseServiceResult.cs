﻿using System;
using Treemas.Base.Utilities;

namespace Treemas.Base.Web.Service
{
    public class BaseServiceResult
    {
        private const string DATA_KEY_MESSAGE = "_tms_srv_res_message";
        private const string DATA_KEY_STATUS = "_tms_srv_res_status";

        public BaseServiceResult() : this(delegate (JsonDataMap data) {
        })
        {
        }

        public BaseServiceResult(Action<JsonDataMap> dataAction)
        {
            this.Data = new JsonDataMap();
            if (dataAction != null)
            {
                dataAction(this.Data);
            }
            this.Status = ServiceStatus.OK;
            this.Message = null;
        }

        public JsonDataMap Data { get; private set; }

        public string Message
        {
            get
            {
                return this.Data.Get<string>(DATA_KEY_MESSAGE);
            }
            set
            {
                this.Data.Add<string>(DATA_KEY_MESSAGE, value);
            }
        }

        public ServiceStatus Status
        {
            get
            {
                return this.Data.Get<ServiceStatus>(DATA_KEY_STATUS);
            }
            set
            {
                this.Data.Add<int>(DATA_KEY_STATUS, (int) value);
            }
        }
    }
}

