﻿using System;
using Treemas.Base.Utilities;

namespace Treemas.Base.Web.Service
{
    public class ServiceParameter : BaseServiceParameter
    {
        public ServiceParameter() : this(null, delegate (JsonDataMap data) {
        })
        {
        }

        public ServiceParameter(string command) : this(command, delegate (JsonDataMap param) {
        })
        {
        }

        public ServiceParameter(string command, Action<JsonDataMap> paramAction) : base(command, paramAction)
        {
        }

        public static ServiceParameter Create(ServiceRuntimeParameter runtimeParam)
        {
            ServiceParameter parameter = new ServiceParameter();
            if (!runtimeParam.IsNull())
            {
                parameter.Command = runtimeParam.Command;
                parameter.Parameters.Clear();
                parameter.Parameters.FromString(runtimeParam.ParameterString);
            }
            return parameter;
        }

        public void FromRuntime(ServiceRuntimeParameter param)
        {
            this.Command = null;
            base.Parameters.Clear();
            if (!param.IsNull())
            {
                this.Command = param.Command;
                base.Parameters.Clear();
                base.Parameters.FromString(param.ParameterString);
            }
        }

        public ServiceRuntimeParameter ToRuntime() => 
            new ServiceRuntimeParameter { 
                Command = this.Command,
                ParameterString = base.Parameters.ToString()
            };
    }
}

