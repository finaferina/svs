﻿using System;
using System.ServiceModel;

namespace Treemas.Base.Web.Service
{
    public class ServiceBindings
    {
        private ServiceBindings()
        {
        }

        public static BasicHttpBinding CreateBasicBinding() => 
            new BasicHttpBinding();

        public static WSHttpBinding CreateHttpBinding() => 
            new WSHttpBinding();

        public static NetNamedPipeBinding CreateNetNamedPipeBinding() => 
            new NetNamedPipeBinding();

        public static NetTcpBinding CreateNetTcpBinding() => 
            new NetTcpBinding();

        public static WebHttpBinding CreateWebBinding() => 
            new WebHttpBinding();
    }
}

