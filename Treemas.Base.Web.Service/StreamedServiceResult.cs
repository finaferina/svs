﻿using System;
using System.IO;
using Treemas.Base.Utilities;

namespace Treemas.Base.Web.Service
{
    public class StreamedServiceResult : BaseServiceResult
    {
        public StreamedServiceResult() : this(delegate (JsonDataMap data) {
        })
        {
        }

        public StreamedServiceResult(Action<JsonDataMap> dataAction) : base(dataAction)
        {
        }

        public static StreamedServiceResult Create(StreamedServiceRuntimeResult runtimeResult)
        {
            if (!runtimeResult.IsNull())
            {
                StreamedServiceResult result = new StreamedServiceResult();
                result.Data.Clear();
                result.Data.FromString(runtimeResult.DataString);
                result.DataStream = runtimeResult.Data;
                return result;
            }
            return null;
        }

        public void FromRuntime(StreamedServiceRuntimeResult result)
        {
            base.Status = ServiceStatus.OK;
            base.Data.Clear();
            if (!result.IsNull())
            {
                base.Data.Clear();
                base.Data.FromString(result.DataString);
                this.DataStream = result.Data;
            }
        }

        public StreamedServiceRuntimeResult ToRuntime()
        {
            StreamedServiceRuntimeResult result = new StreamedServiceRuntimeResult();
            if (!result.IsNull())
            {
                result.Data = this.DataStream;
                if (!base.Data.IsNull())
                {
                    result.DataString = base.Data.ToString();
                }
            }
            return result;
        }

        public Stream DataStream { get; set; }
    }
}

