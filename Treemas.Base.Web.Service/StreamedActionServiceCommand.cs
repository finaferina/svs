﻿using System;

namespace Treemas.Base.Web.Service
{
    public class StreamedActionServiceCommand : StreamedServiceCommand
    {
        public StreamedActionServiceCommand(string name, Func<StreamedServiceParameter, StreamedServiceResult> _Action) : base(name)
        {
            this._Action = _Action;
        }

        public override StreamedServiceResult Execute(StreamedServiceParameter parameter) => 
            this._Action(parameter);

        private Func<StreamedServiceParameter, StreamedServiceResult> _Action { get; set; }
    }
}

