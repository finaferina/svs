﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class ExceptionReportView
    {
        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public string CIFNumber { get; set; }
        public string OpenDate { get; set; }
        public string CloseDate { get; set; }
        public string Reason { get; set; }
        public string RequestType { get; set; }
        public string BranchCode { get; set; }
        public string ActionDate { get; set; }
    }
}