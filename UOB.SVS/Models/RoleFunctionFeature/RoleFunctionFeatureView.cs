﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class RoleFunctionFeatureView
    {
        public long Id { get; set; }
        public long RoleId { get; set; }
        public long FunctionId { get; set; }
        public long RoleFunctionId { get; set; }
        public long FeatureId { get; set; }
        public bool selected { get; set; }
        public string Name { get; set; }
    }
}