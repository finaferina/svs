﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class RoleFunctionFeatureCrView
    {
        public string Id { get; set; }
        public string RoleId { get; set; }
        public string FunctionId { get; set; }
        public string RoleFunctionId { get; set; }
        public string FeatureId { get; set; }
        public bool selected { get; set; }
        public string Name { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedDate { get; set; }
        public string ApprovalType { get; set; }
        public string ApprovalStatus { get; set; }
        public string MakerUser { get; set; }
        public string MakerDate { get; set; }
        public List<FunctionCrView> NewFeatureList { get; set; }
        public List<FunctionCrView> OldFeatureList { get; set; }
    }
}