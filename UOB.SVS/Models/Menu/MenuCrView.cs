﻿
using System.Collections.Generic;
using Treemas.Credential.Model;

namespace UOB.SVS.Models
{
    public class MenuCrView
    {
        public string Id { get; set; }
        public string MenuId { get; set; }
        public string MenuName { get; set; }
        public string ApplicationID { get; set; }
        public string Application { get; set; }
        public string FunctionId { get; set; }
        public string FunctionName { get; set; }
        public string MenuUrl { get; set; }
        public int MenuLevel { get; set; }
        public int MenuOrder { get; set; }
        public string MenuIcon { get; set; }
        public bool Active { get; set; }

        public string MenuIdBefore { get; set; }
        public string MenuNameBefore { get; set; }
        public string ApplicationIDBefore { get; set; }
        public string ApplicationBefore { get; set; }
        public string FunctionIdBefore { get; set; }
        public string FunctionNameBefore { get; set; }
        public string MenuUrlBefore { get; set; }
        public int MenuLevelBefore { get; set; }
        public int MenuOrderBefore { get; set; }
        public string MenuIconBefore { get; set; }
        public bool ActiveBefore { get; set; }
        public string ParentMenuBefore { get; set; }
        public string ParentMenu { get; set; }
        public IList<MenuCr> ChildMenu { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedDate { get; set; }
        public string ApprovalType { get; set; }
        public string ApprovalStatus { get; set; }
        public string MakerUser { get; set; }
        public string MakerDate { get; set; }
    }
}