﻿using System;
using System.Web;
using System.Collections.Generic;
using Treemas.Credential.Model;

namespace UOB.SVS.Models
{
    [Serializable]
    public class CentralizeUnitHReqView
    {
        public string NIK { get; set; }
        public string CopiedNIK { get; set; }
        public string Name { get; set; }
        public string NameOld { get; set; }
        public string DocType { get; set; }
        public string DocTypeOld { get; set; }
        public string Region { get; set; }
        public string RegionOld { get; set; }
        public IList<Division> Divisions { set; get; }
        public string Division { get; set; }
        public string DivisionOld { get; set; }
        public IList<Departement> Departements { set; get; }
        public string Departement { get; set; }
        public string DepartementOld { get; set; }
        public string Titles { get; set; }
        public string TitlesOld { get; set; }
        public string Note { get; set; }
        public string NoteOld { get; set; }
        public string RequestType { get; set; }
        public string RequestTypeString
        {
            get
            {
                if (RequestType == "C")
                {
                    return "Create";
                }
                if (RequestType == "U")
                {
                    return "Update";
                }
                if (RequestType == "D")
                {
                    return "Delete";
                }
                return "";
            }
        }
        public DateTime? RequestDate { get; set; }
        public string RequestDateString { get; set; }
        public string RequestUser { get; set; }
        public string RequestReason { get; set; }
        public string RejectNote { get; set; }
        public bool IsRejected { get; set; }
        public List<CentralizeSignature> Signatures { get; set; }
        public List<CentralizeDocument> Documents { get; set; }
        public List<CentralizeDeleteFormat> DeletedSignatures { get; set; }
        public List<CentralizeDeleteFormat> DeletedDocuments { get; set; }
    }

    public class CentralizeSignature
    {
        public long ID { get; set; }

        public int Index { get; set; }
        public string ImageType { get; set; }
        public string CroppedBlob { get; set; }
        public string ImageTypeOld { get; set; }
        public string CroppedBlobOld { get; set; }
        public string NIK { get; set; }
        public Boolean IsNew { get; set; }
        public string FileName { get; set; }
        public int FileSeq { get; set; }
        public bool IsValidImage { get; set; }
    }

    public class CentralizeDocument
    {
        public long ID { get; set; }
        public int Index { get; set; }
        public string FileType { get; set; }
        public string FileBlob { get; set; }
        public string DocumentType { get; set; }
        public string FileTypeOld { get; set; }
        public string FileBlobOld { get; set; }
        public string DocumentTypeOld { get; set; }
        public string NIK { get; set; }
        public Boolean IsNew { get; set; }
        public long FileSize { get; set; }
        public string FileName { get; set; }
        public int FileSeq { get; set; }
        public bool IsValidDoc { get; set; }
        //public byte[] FileByte { get; set; }
    }

    public class CentralizeDeleteFormat
    {
        public long ID { get; set; }
    }

    public class CentralizeDocumentForm
    {
        public long ID { get; set; }
        public int Index { get; set; }
        public string FileType { get; set; }
        public string FileBlob { get; set; }
        public string DocumentType { get; set; }
        public string FileTypeOld { get; set; }
        public string FileBlobOld { get; set; }
        public string DocumentTypeOld { get; set; }
        public string NIK { get; set; }
        public Boolean IsNew { get; set; }

    }
}