﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class AuthorizationView
    {
        public long Id { get; set; }
        public string Application { get; set; }
        public string Feature { get; set; }
        public string Function { get; set; }
        public string QualifierKey { get; set; }
        public string QualifierValue { get; set; }
        public string Role { get; set; }
        public string Username { get; set; }
        public bool Selected { get; set; }
    }
}