﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class UserView
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string RegNo { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string OldPassword { get; set; }
        public string PasswordExpirationDate { get; set; }
        public string AccountValidityDate { get; set; }
        public string FullName { get; set; }
        public string FullNameNew { get; set; }
        //public string LastName { get; set; }
        public bool InActiveDirectory { get; set; }
        public bool InActiveDirectoryNew { get; set; }
        public string sInActiveDirectory { get; set; }
        public string sInActiveDirectoryNew { get; set; }
        public int SessionTimeout { get; set; }
        public int LockTimeout { get; set; }
        public int MaxConLogin { get; set; }
        public bool IsActive { get; set; }
        public bool IsPrinted { get; set; }
        public bool Approved { get; set; }
        //public string Name { get; set; }

        public int passExp { get; set; }
        public int AccValidity { get; set; }
        public string Applications { get; set; }
        public string ApplicationsNew { get; set; }
        public string Roles { get; set; }
        public string RolesNew { get; set; }

        public string CreatedDate { get; set; }
        public string LastLogin { get; set; }

        public string ApprovedBy { get; set; }
        public string ApprovedDate { get; set; }
        public string ApprovalType { get; set; }
        public string ApprovalStatus { get; set; }

        public string CreatedBy { get; set; }
        public string ChangedBy { get; set; }
        public string ChangedDate { get; set; }
        public string YesNo { get; set; }
        public string BranchCode { get; set; }
        public string BranchCodeNew { get; set; }
        public long UserAppID { get; set; }

    }
}