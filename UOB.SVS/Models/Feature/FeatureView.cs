﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class FeatureView
    {
        public long Id { get; set; }
        public string Application { get; set; }
        public string FeatureId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}