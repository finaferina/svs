﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class DepartementCrView
    {
        public string Id { get; set; }
        public string ApplicationID { get; set; }
        public string Application { get; set; }
        public string DepartementId { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Region { get; set; }
        public string Division { get; set; }
        public string RequestStatus { get; set; }
        public string ApprovalFuntion { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedDate { get; set; }
        public string ApprovalType { get; set; }
        public string ApprovalStatus { get; set; }
        public string MakerUser { get; set; }
        public string MakerDate { get; set; }

        public string ApplicationIDBefore { get; set; }
        public string ApplicationBefore { get; set; }
        public string DepartementIdBefore { get; set; }
        public string NameBefore { get; set; }
        public string AliasBefore { get; set; }
        public string RegionBefore { get; set; }
        public string DivisionBefore { get; set; }
    }
}