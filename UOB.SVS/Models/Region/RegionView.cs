﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class RegionView
    {
        public long Id { get; set; }
        public string Application { get; set; }
        public string RegionId { get; set; }
        public string TransId { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
    }
}