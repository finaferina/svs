﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class RoleDocTypeView
    {
        public long Id { get; set; }
        public long RoleId { get; set; }
        public long DocumentTypeId { get; set; }
        public string DocType { get; set; }
        public string Description { get; set; }
        public bool selected { get; set; }
    }
}