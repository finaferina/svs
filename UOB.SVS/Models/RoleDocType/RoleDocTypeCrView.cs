﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class RoleDocTypeCrView
    {
        public string Id { get; set; }
        public string RoleId { get; set; }
        public string DocumentTypeId { get; set; }
        public string Name { get; set; }
        public bool selected { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedDate { get; set; }
        public string ApprovalType { get; set; }
        public string ApprovalStatus { get; set; }
        public string MakerUser { get; set; }
        public string MakerDate { get; set; }
        //public List<DocTypeView> NewDocTypeList { get; set; }
        //public List<DocTypeView> OldDocTypeList { get; set; }
        public List<DocumentTypeView> NewFunctionList { get; set; }
        public List<DocumentTypeView> OldFunctionList { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ChangedBy { get; set; }
        public string ChangedDate { get; set; }
    }
}