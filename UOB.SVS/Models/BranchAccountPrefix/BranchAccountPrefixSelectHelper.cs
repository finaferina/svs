﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class BranchAccountPrefixSelectHelper
    {
        public bool selected { get; set; }
        public string PrefixCode { get; set; }
    }
}