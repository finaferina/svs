﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class BranchAccountPrefixView
    {
        public string PrefixCode { get; set; }
        public string BranchCode { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ChangedBy { get; set; }
        public string ChangedDate { get; set; }
        public bool selected { get; set; }
    }
}