﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class BranchAccountPrefixCrView
    {
        public string Id { get; set; }
        public string PrefixCode { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ChangedBy { get; set; }
        public string ChangedDate { get; set; }
        public bool selected { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedDate { get; set; }
        public string ApprovalType { get; set; }
        public string ApprovalStatus { get; set; }
        public string MakerUser { get; set; }
        public string MakerDate { get; set; }
        public List<AccountPrefixView> NewAccountPrefixList { get; set; }
        public List<AccountPrefixView> OldAccountPrefixList { get; set; }
    }
}