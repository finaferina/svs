﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class SignatureHHistoryView
    {
        public long RecordId { get; set; }
        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public string AccountType { get; set; }
        public string CIFNumber { get; set; }
        public string BranchCode { get; set; }
        public string Note { get; set; }
        public string RequestType { get; set; }
        public string RequestDate { get; set; }
        public string RequestUser { get; set; }
        public string RequestReason { get; set; }
        public string ApproveBy { get; set; }
        public string ApproveDate { get; set; }
        public string RejectDate { get; set; }
        public string RejectBy { get; set; }
        public string RejectReason { get; set; }
        public string OpenDate { get; set; }
        public string CloseDate { get; set; }
    }
}