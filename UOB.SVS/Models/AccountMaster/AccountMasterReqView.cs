﻿using System;
using System.Web;
using System.Collections.Generic;

namespace UOB.SVS.Models
{
    public class AccountMasterReqView
    {
        public virtual string AccountNo { get; set; }
        public virtual string AccountName { get; set; }
        public virtual string AccountType { get; set; }
        public virtual string CIFNo { get; set; }
        public virtual string BranchCode { get; set; }
        public virtual string Status { get; set; }
        public virtual DateTime? DateOpened { get; set; }
    }
    
}