﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class DivisionView
    {
        public long Id { get; set; }
        public string Application { get; set; }
        public string DivisionId { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Region { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
    }
}