﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class FunctionCrView
    {
        public string Id { get; set; }
        public string ApplicationID { get; set; }
        public string Application { get; set; }
        public string FunctionId { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string RequestStatus { get; set; }
        public string ApprovalFuntion { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedDate { get; set; }
        public string ApprovalType { get; set; }
        public string ApprovalStatus { get; set; }
        public string MakerUser { get; set; }
        public string MakerDate { get; set; }

        public string ApplicationIDBefore { get; set; }
        public string ApplicationBefore { get; set; }
        public string FunctionIdBefore { get; set; }
        public string DescriptionBefore { get; set; }
        public string NameBefore { get; set; }
    }
}