﻿
using System.Collections.Generic;

namespace UOB.SVS.Models
{
    public class FunctionView
    {
        public long Id { get; set; }
        public string Application { get; set; }
        public string FunctionId { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public List<FeatureView> NewFeatureList { get; set; }
        public List<FeatureView> OldFeatureList { get; set; }
    }
}