﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class RoleFunctionView
    {
        public long Id { get; set; }
        public long RoleId { get; set; }
        public long FunctionId { get; set; }
        public string FunctionName { get; set; }
        public string Name { get; set; }
        public bool selected { get; set; }
    }
}