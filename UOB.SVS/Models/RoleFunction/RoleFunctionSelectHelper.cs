﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    [Serializable]
    public class RoleFunctionSelectHelper
    {
        public bool selected { get; set; }
        public long FunctionId { get; set; }
    }
}