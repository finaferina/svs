﻿using System.Collections.Generic;

namespace UOB.SVS.Models
{
    public class PushFileDataView
    {
        public string FOLDER { get; set; }
        public List<PushFileDataObject> FILES { get; set; }
    }

    public class PushFileDataObject
    {
        public string FILE_NAME { get; set; }
        public string CREATE_DATE { get; set; }
        public string FILE_SIZE { get; set; }
        public string FILE_PATH { get; set; }
    }
}