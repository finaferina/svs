﻿using System;
using System.Web;

namespace UOB.SVS.Models
{
    public class SignatureSCView
    {

        public long ID { get; set; }
        public string AccountNo { get; set; }
        public string CustomerName { get; set; }
        public string Note { get; set; }
        public string Signature { get; set; }
        public string KTP { get; set; }
        public string NoteKTP { get; set; }
        public string Pengenal { get; set; }
        public string AddBy { get; set; }
        public DateTime? DateAdd { get; set; }
        public string UpdBy { get; set; }
        public DateTime? DateUpd { get; set; }

        public string DelBy { get; set; }
        public DateTime? DateDel { get; set; }
        public string AppAddBy { get; set; }
        public DateTime? DateAppAdd { get; set; }
        public string AppUpBy { get; set; }
        public DateTime? DateAppUpd { get; set; }
        public string AppDelBy { get; set; }
        public DateTime? DateAppDel { get; set; }
        public string NotAppAddBy { get; set; }
        public DateTime? DateNotAppAdd { get; set; }
        public string NotAppUpdBy { get; set; }
        public DateTime? DateNotAppUpd { get; set; }

        public string NotAppDelBy { get; set; }
        public DateTime? DateNotAppDell { get; set; }
        public string StatusFlag { get; set; }
        public string StatusAktif { get; set; }
        public string OldNote { get; set; }
        public string OldNoteKTP { get; set; }
        public string FSignature { get; set; }
        public string FKTP { get; set; }
        public string FNote { get; set; }
        public string ZSignature { get; set; }
        public string ZOldSignature { get; set; }
        public string ZKTP { get; set; }

        public string ZOldKTP { get; set; }
        public string OldSignature { get; set; }
        public string OldKTP { get; set; }
        public string OldAccNumber { get; set; }


        public  string ApprovedBy { get; set; }
        public  string ApprovalType { get; set; }
        public string RequestType { get; set; }
        public string RequestTypeString
        {
            get
            {
                if (RequestType == "C")
                {
                    return "Create";
                }
                if (RequestType == "U")
                {
                    return "Update";
                }
                if (RequestType == "D")
                {
                    return "Delete";
                }
                return "";
            }
        }

        public DateTime? ApprovedDate { get; set; }
        public string ApprovalStatus { get; set; }

        
    }
}