﻿
using System.Collections.Generic;

namespace UOB.SVS.Models
{
    public class FunctionFeatureCrView
    {
        public string Id { get; set; }        
        public string FunctionId { get; set; }
        public string FunctionName { get; set; }
        public string FeatureId { get; set; }
        public string FeatureName { get; set; }
        public string Description { get; set; }
        public bool Selected { get; set; }
        public string FunctionIdBefore { get; set; }
        public string FeatureIdBefore { get; set; }
        public string FeatureNameBefore { get; set; }
        public string DescriptionBefore { get; set; }
        public string RequestStatus { get; set; }
        public string ApprovalFuntion { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedDate { get; set; }
        public string ApprovalType { get; set; }
        public string ApprovalStatus { get; set; }
        public string MakerUser { get; set; }
        public string MakerDate { get; set; }
        public List<FeatureView> NewFeatureList { get; set; }
        public List<FeatureView> OldFeatureList { get; set; }

    }
}