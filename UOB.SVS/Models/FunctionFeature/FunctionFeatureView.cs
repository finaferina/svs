﻿
namespace UOB.SVS.Models
{
    public class FunctionFeatureView
    {
        public long Id { get; set; }        
        public long FunctionId { get; set; }
        public long FeatureId { get; set; }
        public string FeatureName { get; set; }
        public string Description { get; set; }
        public bool Selected { get; set; }
    }
}