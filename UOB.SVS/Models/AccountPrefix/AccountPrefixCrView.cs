﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UOB.SVS.Models
{
    public class AccountPrefixCrView
    {
        public string Id { get; set; }
        public string PrefixCode { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ChangedBy { get; set; }
        public string ChangedDate { get; set; }

        public string PrefixCodeBefore { get; set; }
        public string DescriptionBefore { get; set; }
        public bool Approved { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedDate { get; set; }
        public string ApprovalType { get; set; }
        public string ApprovalStatus { get; set; }
        public string MakerUser { get; set; }
        public string MakerDate { get; set; }
    }
}