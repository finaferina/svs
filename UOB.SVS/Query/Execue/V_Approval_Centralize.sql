USE [SVS]
GO

/****** Object:  View [dbo].[V_Approval_Centralize]    Script Date: 2022-04-14 9:47:29 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[V_Approval_Centralize] AS
SELECT CAST ('R' + CAST (ID AS VARCHAR) AS VARCHAR) AS Id
    , 'Region' AS ApprovalFuntion
    , CREATED_BY AS MakerUser
    , CONVERT (DATETIME, CONVERT (VARCHAR (10), CREATED_DATE, 110)) AS MakerDate
    , APPROVAL_TYPE AS RequestType
    , APPROVAL_STATUS AS ApprovalStatus
    , '' ROLE_ID
FROM dbo.SEC_REGION_CR
WHERE APPROVAL_STATUS = 'N'
union
SELECT CAST ('D' + CAST (ID AS VARCHAR) AS VARCHAR) AS Id
    , 'Division' AS ApprovalFuntion
    , CREATED_BY AS MakerUser
    , CONVERT (DATETIME, CONVERT (VARCHAR (10), CREATED_DATE, 110)) AS MakerDate
    , APPROVAL_TYPE AS RequestType
    , APPROVAL_STATUS AS ApprovalStatus
    , '' ROLE_ID
FROM dbo.SEC_DIVISION_CR
WHERE APPROVAL_STATUS = 'N'
union
SELECT CAST ('E' + CAST (ID AS VARCHAR) AS VARCHAR) AS Id
    , 'Departement' AS ApprovalFuntion
    , CREATED_BY AS MakerUser
    , CONVERT (DATETIME, CONVERT (VARCHAR (10), CREATED_DATE, 110)) AS MakerDate
    , APPROVAL_TYPE AS RequestType
    , APPROVAL_STATUS AS ApprovalStatus
    , '' ROLE_ID
FROM dbo.SEC_DEPARTEMENT_CR
WHERE APPROVAL_STATUS = 'N'
union
SELECT CAST ('C' + CAST (ID AS VARCHAR) AS VARCHAR) AS Id
    , 'Centralize Document Type' AS ApprovalFuntion
    , CREATED_BY AS MakerUser
    , CONVERT (DATETIME, CONVERT (VARCHAR (10), CREATED_DATE, 110)) AS MakerDate
    , APPROVAL_TYPE AS RequestType
    , APPROVAL_STATUS AS ApprovalStatus
    , '' ROLE_ID
FROM dbo.SEC_CENTRALUNITDOCTYPE_CR
WHERE APPROVAL_STATUS = 'N'
GO


