﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using UOB.SVS.Resources;
using UOB.SVS.Model;
using UOB.SVS.Model.Helper;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System.Linq;
using Treemas.Credential.Model;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;

namespace UOB.SVS.Controllers
{
    public class ApproveSVSCentralUnitController : PageController
    {
        private ICentralizeUnitHReqRepository _hReqRepository;
        private ICentralizeHRepository _hRepository;
        private ICentralizeHReqService _centralizeHReqService;
        private ICentralizeHHistoryRepository _centHHistoryRepo;
        //private IAuditTrailLog _auditRepository;
        private IAuditTrailCentalizeLog _auditRepository;
        private ICentralUnitDocTypeRepository _doctypeRepository;
        private IRegionRepository _regionRepository;
        private IDivisionRepository _divisionRepository;
        private IDepartementRepository _departementRepository;
        //private IBranchRepository _branchRepository;
        //private IDocumentTypeRepository _docTypeRepository;
        private IUserRepository _userRepository;

        public ApproveSVSCentralUnitController(ISessionAuthentication sessionAuthentication, ICentralizeUnitHReqRepository hReqRepository,
            //IBranchRepository branchRepository, IDocumentTypeRepository docTypeRepository, 
            ICentralizeHRepository hRepository, ICentralizeHReqService centralizeHReqService, IAuditTrailCentalizeLog auditRepository, IUserRepository userRepository,
            ICentralizeHHistoryRepository centHHistoryRepo, ICentralUnitDocTypeRepository centralUnitDocTypeRepository, IRegionRepository regionRepository,
            IDivisionRepository divisionRepository, IDepartementRepository departementRepository) : base(sessionAuthentication)
        {
            this._hReqRepository = hReqRepository;
            this._auditRepository = auditRepository;
            //this._branchRepository = branchRepository;
            this._centralizeHReqService = centralizeHReqService;
            //this._docTypeRepository = docTypeRepository;
            this._hRepository = hRepository;
            this._userRepository = userRepository;
            this._centHHistoryRepo = centHHistoryRepo;
            this._doctypeRepository = centralUnitDocTypeRepository;
            this._regionRepository = regionRepository;
            this._divisionRepository = divisionRepository;
            this._departementRepository = departementRepository;
            Settings.ModuleName = "ApproveSVSCentralUnit";
            Settings.Title = "Approval Officer";
        }

        protected override void Startup()
        {

        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                CentralizeReqFilter filter = new CentralizeReqFilter();
                User user = Lookup.Get<User>();
                //filter.Branches = getBranches();
                filter.ApprovalType = "ApproveCentralize";
                filter.RequestType = Request.QueryString.GetValues("reqType")[0];
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<CentralizeUnitHRequest> data;
                if (!searchValue.IsNullOrEmpty())
                {
                    if (searchColumn == "NIK")
                        filter.NIK = searchValue;
                    else if (searchColumn == "Name")
                        filter.Name = searchValue;
                }

                data = _hReqRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter, user);
                var items = data.Items.ToList().ConvertAll<CentralizeUnitHReqView>(new Converter<CentralizeUnitHRequest, CentralizeUnitHReqView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public CentralizeUnitHReqView ConvertFrom(CentralizeUnitHRequest item)
        {
            CentralizeUnitHReqView returnItem = new CentralizeUnitHReqView();
            returnItem.NIK = item.NIK;
            returnItem.Name = HttpUtility.HtmlEncode(item.Name);
            returnItem.Region = item.Region;
            returnItem.Division = item.Division;
            returnItem.Departement = item.Departement;
            returnItem.Titles = item.Titles;
            returnItem.Note = HttpUtility.HtmlEncode(item.Note);
            returnItem.RequestType = item.RequestType;
            returnItem.RequestDate = item.RequestDate;
            returnItem.RequestUser = item.RequestUser;
            returnItem.RequestReason = item.RequestReason;
            returnItem.RequestDateString = item.RequestDate.Value.ToString("dd/MM/yyyy");

            int index = 1;
            if (item.Documents != null)
            {
                IList<CentralizeDocument> docs = new List<CentralizeDocument>();
                foreach (CentralizeDDocReq row in item.Documents)
                {
                    CentralizeDocument doc = new CentralizeDocument();
                    doc.Index = index++;
                    doc.DocumentType = row.DocumentType;
                    doc.FileBlob = row.FileBlob;
                    doc.FileType = row.FileType;
                    doc.FileSeq = row.FileSeq;
                    doc.ID = row.ID;
                    doc.IsValidDoc = true;
                    docs.Add(doc);
                }
                returnItem.Documents = docs.ToList();
            }

            if (item.Signatures != null)
            {
                index = 1;
                IList<CentralizeSignature> signs = new List<CentralizeSignature>();
                foreach (CentralizeDSignReq row in item.Signatures)
                {
                    CentralizeSignature sign = new CentralizeSignature();
                    sign.Index = index++;
                    sign.CroppedBlob = HttpUtility.HtmlEncode(row.Signature);
                    sign.ImageType = HttpUtility.HtmlEncode(row.ImageType);
                    sign.IsValidImage = row.Signature.Length % 4 == 0 ? true : false;
                    sign.FileSeq = row.FileSeq;
                    signs.Add(sign);
                }
                returnItem.Signatures = signs.ToList();
            }

            return returnItem;
        }
        public CentralizeUnitHReqView ConvertFromHeader(CentralizeHeader header)
        {
            CentralizeUnitHReqView returnItem = new CentralizeUnitHReqView();
            returnItem.NIK = header.NIK;
            returnItem.NameOld = header.Name;
            returnItem.RegionOld = header.Region;
            returnItem.DivisionOld = header.Division;
            returnItem.DepartementOld = header.Departement;
            returnItem.TitlesOld = header.Titles;
            returnItem.NoteOld = header.Note;

            int index = 1;

            if (header.Documents != null)
            {
                IList<CentralizeDocument> docs = new List<CentralizeDocument>();
                foreach (CentralizeDDoc row in header.Documents)
                {
                    CentralizeDocument doc = new CentralizeDocument();
                    doc.Index = index++;
                    doc.DocumentTypeOld = row.DocumentType;
                    doc.FileBlobOld = row.FileBlob;
                    doc.FileTypeOld = row.FileType;
                    doc.FileSeq = row.Seq;
                    doc.ID = row.Id;
                    doc.IsValidDoc = true;
                    docs.Add(doc);
                }
                returnItem.Documents = docs.ToList();
            }

            if (header.Signatures != null)
            {
                index = 1;
                IList<CentralizeSignature> signs = new List<CentralizeSignature>();
                foreach (CentralizeDSign row in header.Signatures)
                {
                    CentralizeSignature sign = new CentralizeSignature();
                    sign.Index = index++;
                    sign.CroppedBlobOld = HttpUtility.HtmlEncode(row.Signature);
                    sign.ImageTypeOld = HttpUtility.HtmlEncode(row.ImageType);
                    sign.IsValidImage = row.Signature.Length % 4 == 0 ? true : false;
                    sign.FileSeq = row.Seq;
                    signs.Add(sign);
                }
                returnItem.Signatures = signs.ToList();
            }

            return returnItem;
        }

        public ActionResult Edit(string id)
        {
            Parameter param = Lookup.Get<Parameter>();
            ViewData["Parameter"] = param;
            CentralizeUnitHRequest reqH = _hReqRepository.getCentralizeUnitHReq(id);
            if (reqH.IsNull())
            {
                ScreenMessages.Submit(ScreenMessage.Error("NIK does not exist!"));
                return RedirectToAction("", "ApproveCentralize");
            }

            reqH.Documents = _hReqRepository.getDocReqD(reqH.NIK);
            reqH.Signatures = _hReqRepository.getSignReqD(reqH.NIK);

            User userReq = _userRepository.GetUser(reqH.RequestUser);
            CentralizeUnitHReqView data = ConvertFrom(reqH);

            //CentralizeHeader reqHeader = _hRepository.getCentralizeHeader(id, name, titles, region, division, departement);
            CentralizeHeader reqHeader = _hRepository.getCentralizeHeader(id);
            if (reqHeader != null)
            {
                reqHeader.Documents = _hRepository.getDocDet(reqH.NIK);
                reqHeader.Signatures = _hRepository.getSignDet(reqH.NIK);

                CentralizeUnitHReqView dataheader = ConvertFromHeader(reqHeader);
                ViewData["DataHeader"] = dataheader;
            }

            ViewData["centralizeData"] = data;
            ViewData["UserReq"] = userReq;
            Session["centralizeData"] = data;

            return CreateView(data);
        }

        private ViewResult CreateView(CentralizeUnitHReqView data)
        {
            User user = Lookup.Get<User>();

            //List<RoleDocType> authDocs = Lookup.Get<List<RoleDocType>>();
            //ViewData["DocCount"] = authDocs.Count;

            //ViewData["UserData"] = user;
            //ViewData["BranchMaster"] = createBranchSelect("");
            //ViewData["DocTypeMaster"] = createDocTypeSelect("");
            //ViewData["AccTypeList"] = createAccountTypeSelect("");
            ViewData["AllDocTypeMaster"] = createAllDocTypeSelect();

            if (data.RequestType == "U")
            {
                return View("DetailEdit", data);
            }
            else
            {
                return View("Detail", data);
            }
        }


        [HttpPost]
        public ActionResult Approve(CentralizeUnitHReqView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            CentralizeUnitHRequest reqH = _hReqRepository.getCentralizeUnitHReq(data.NIK);
            User userReq = _userRepository.GetUser(reqH.RequestUser);

            try
            {
                //if (user.BranchCode != "000")// && userReq.BranchCode != user.BranchCode)
                //{
                //    List<Branch> uBranches = getBranches();
                //    Branch selectedBranch = uBranches.Find(b => b.BranchCode.Trim() == data.BranchCode.Trim());
                //    if (selectedBranch.IsNull())
                //        message = SvsResources.Validation_BranchCodeNotMatch;
                //}
                CentralizeUnitHReqView currData = (CentralizeUnitHReqView)Session["centralizeData"];
                if (currData.NIK.Trim() != data.NIK.Trim())
                {
                    message = "approve failed, invalid or not exist NIK!";
                }

                //if (currData.CIFNumber.Trim() != data.CIFNumber.Trim())
                //{
                //    message = "approve failed, invalid or not exist CIF number!";
                //}

                if (user.Username.Trim() == userReq.Username.Trim())
                {
                    message = "Cannot approve your own request data!";
                }

                if (!(data.RequestDate == reqH.RequestDate))
                {
                    message = "Already change";
                }
                if (message.IsNullOrEmpty())
                {

                    reqH.Documents = _hReqRepository.getDocReqD(reqH.NIK);
                    reqH.Signatures = _hReqRepository.getSignReqD(reqH.NIK);

                    CentralizeUnitHReqView dataReq = ConvertFrom(reqH);
                    CentralizeHeaderAudit ObjectBefore = new CentralizeHeaderAudit();
                    CentralizeHeaderAudit ObjectAfter = new CentralizeHeaderAudit();
                    CentralizeHHistory oldcentHistory = _centHHistoryRepo.getCentralizeHHistoryCreate(data.NIK);

                    if (reqH.RequestType.Trim() != "C")
                    {
                        //CentralizeHeader oldcentH = _hRepository.getCentralizeHeader(data.NIK, data.Name, data.Titles, data.Region, data.Division, data.Departement);
                        CentralizeHeader oldcentH = _hRepository.getCentralizeHeaderNik(data.NIK);
                        oldcentH.Documents = _hRepository.getDocDet(oldcentH.NIK);
                        oldcentH.Signatures = _hRepository.getSignDet(oldcentH.NIK);

                        ObjectBefore.Id = oldcentH.NIK;
                        ObjectBefore.NIK = oldcentH.NIK;
                        ObjectBefore.Name = oldcentH.Name;
                        ObjectBefore.Region = oldcentH.Region;
                        ObjectBefore.Division = oldcentH.Division;
                        ObjectBefore.Departement = oldcentH.Departement;
                        ObjectBefore.Titles = oldcentH.Titles;
                        //ObjectBefore.BranchCode = oldcentH.BranchCode;
                        ObjectBefore.Note = oldcentH.Note;
                        ObjectBefore.Signatures = oldcentH.Signatures.Count > 0 ? oldcentH.Signatures.Count.ToString() + " item(s)" : null;
                        ObjectBefore.Documents = oldcentH.Documents.Count > 0 ? oldcentH.Documents.Count.ToString() + " item(s)" : null;
                        ObjectBefore.CreatedBy = !oldcentHistory.IsNull() ? oldcentHistory.RequestUser : null;
                        ObjectBefore.CreatedDate = !oldcentHistory.IsNull() ? oldcentHistory.RequestDate : null;
                    }

                    CentralizeUnitHRequest newData = new CentralizeUnitHRequest("");
                    newData.NIK = dataReq.NIK;
                    newData.Name = dataReq.Name;
                    newData.Region = dataReq.Region;
                    newData.Division = dataReq.Division;
                    newData.Departement = dataReq.Departement;
                    newData.Titles = dataReq.Titles;
                    //newData.BranchCode = dataReq.BranchCode;
                    newData.Note = dataReq.Note;
                    newData.RequestType = dataReq.RequestType;
                    newData.RequestDate = dataReq.RequestDate;
                    newData.RequestUser = dataReq.RequestUser;
                    newData.RequestReason = dataReq.RequestReason;

                    CentralizeHeader centH = new CentralizeHeader("");

                    centH.NIK = dataReq.NIK;
                    centH.Name = HttpUtility.HtmlDecode(dataReq.Name);
                    centH.Region = dataReq.Region;
                    centH.Division = dataReq.Division;
                    centH.Departement = dataReq.Departement;
                    centH.Titles = dataReq.Titles;
                    //centH.BranchCode = dataReq.BranchCode;
                    centH.Note = HttpUtility.HtmlDecode(dataReq.Note);
                    centH.CreateUser = dataReq.RequestUser;
                    centH.CreateDate = dataReq.RequestDate;

                    ObjectAfter.Id = dataReq.NIK;
                    ObjectAfter.NIK = dataReq.NIK;
                    ObjectAfter.Name = HttpUtility.HtmlDecode(dataReq.Name);
                    ObjectAfter.Region = dataReq.Region;
                    ObjectAfter.Division = dataReq.Division;
                    ObjectAfter.Departement = dataReq.Departement;
                    ObjectAfter.Titles = dataReq.Titles;
                    //ObjectAfter.BranchCode = dataReq.BranchCode;
                    ObjectAfter.Note = HttpUtility.HtmlDecode(dataReq.Note);
                    ObjectAfter.Signatures = dataReq.Signatures.Count > 0 ? dataReq.Signatures.Count.ToString() + " item(s)" : null;
                    ObjectAfter.Documents = dataReq.Documents.Count > 0 ? dataReq.Documents.Count.ToString() + " item(s)" : null;
                    ObjectAfter.CreatedBy = dataReq.RequestUser;
                    if (reqH.RequestType.Trim() != "C")
                        ObjectAfter.CreatedDate = !oldcentHistory.IsNull() ? oldcentHistory.RequestDate : null;
                    else
                        ObjectAfter.CreatedDate = dataReq.RequestDate;


                    int index = 1;
                    IList<CentralizeDDoc> docs = new List<CentralizeDDoc>();

                    if (dataReq.Documents != null)
                    {
                        
                        foreach (CentralizeDocument item in dataReq.Documents)
                        {
                            CentralizeDDocReq rDoc = _hReqRepository.getDocReqD(item.ID);

                            CentralizeDDoc doc = new CentralizeDDoc(0L);
                            doc.NIK = dataReq.NIK;
                            doc.DocumentType = item.DocumentType;
                            doc.FileBlob = rDoc.IsNull() ? item.FileBlob : rDoc.FileBlob;
                            doc.FileType = item.FileType;
                            doc.Seq = index++;//item.FileSeq; //
                            docs.Add(doc);
                        }
                    }
                    centH.Documents = docs;

                    IList<CentralizeDSign> signs = new List<CentralizeDSign>();
                    if (dataReq.Documents != null)
                    {
                        index = 1;
                        foreach (CentralizeSignature item in dataReq.Signatures)
                        {
                            CentralizeDSign sign = new CentralizeDSign(0L);
                            sign.NIK = dataReq.NIK;
                            sign.Signature = item.CroppedBlob;
                            sign.ImageType = item.ImageType;
                            sign.Seq = index++;//item.FileSeq;//
                            signs.Add(sign);
                        }
                    }
                    centH.Signatures = signs;

                    CentralizeHHistory centHistory = new CentralizeHHistory(0L);
                    centHistory.NIK = dataReq.NIK;
                    centHistory.Name = dataReq.Name;
                    centHistory.Region = dataReq.Region;
                    centHistory.Division = dataReq.Division;
                    centHistory.Departement = dataReq.Departement;
                    centHistory.Titles = dataReq.Titles;
                    //centHistory.BranchCode = dataReq.BranchCode;
                    centHistory.Note = dataReq.Note;
                    centHistory.RequestType = dataReq.RequestType;
                    centHistory.RequestDate = dataReq.RequestDate;
                    centHistory.RequestUser = dataReq.RequestUser;
                    centHistory.RequestReason = dataReq.RequestReason;
                    centHistory.ApproveBy = user.Username;
                    centHistory.ApproveDate = DateTime.Now;

                    message = _centralizeHReqService.ApproveReq(newData, centH, centHistory, user);

                    // Hapus detail data, biar audit trailnya ngak penuh
                    //svsH.Documents = null;
                    //svsH.Signatures = null;
                    //svsH.IdentityCards = null;

                    //oldsvsH.Documents = null;
                    //oldsvsH.Signatures = null;
                    //oldsvsH.IdentityCards = null;

                    //_auditRepository.SaveAuditTrail(Settings.ModuleName, svsH, user.Username, "Approve");
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, ObjectBefore, ObjectAfter, dataReq.RequestUser, dataReq.RequestDate, user.Username, dataReq.RequestType);

                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approve));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            //ini kalau ga ke index
            //CollectScreenMessages();
            //return CreateView(data); //ini akan problem gara2 si dropdown di disable, harus kasih hidden valuenya si dropdown di detail

            //ini kalau ke index
            //CollectScreenMessages(); ga perlu karena ke index otomatis collect, kalo ada ini malah ga keluar messagenya by Ivan
            return RedirectToAction("Index");
        }

        //[HttpPost]
        public ActionResult Reject(string Id)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            CentralizeUnitHRequest requestH = _hReqRepository.getCentralizeUnitHReq(Id);
            User userReq = _userRepository.GetUser(requestH.RequestUser);

            try
            {
                //if (user.BranchCode != "000")// && userReq.BranchCode != user.BranchCode)
                //{
                //    List<Branch> uBranches = getBranches();
                //    Branch selectedBranch = uBranches.Find(b => b.BranchCode.Trim() == requestH.BranchCode.Trim());
                //    if (selectedBranch.IsNull())
                //        message = SvsResources.Validation_BranchCodeNotMatch;
                //}

                if (message.IsNullOrEmpty())
                {
                    CentralizeUnitHRequest reqH = _hReqRepository.getCentralizeUnitHReq(Id);
                    reqH.IsRejected = true;
                    reqH.Documents = _hReqRepository.getDocReqD(reqH.NIK);
                    reqH.Signatures = _hReqRepository.getSignReqD(reqH.NIK);

                    CentralizeHeader oldcentH = _hRepository.getCentralizeHeader(reqH.NIK);
                    if (oldcentH.IsNull())
                        oldcentH = new CentralizeHeader(reqH.NIK);

                    message = _centralizeHReqService.RejectReq(reqH, user);

                    CentralizeHeaderAudit ObjectBefore = new CentralizeHeaderAudit();
                    CentralizeHeaderAudit ObjectAfter = new CentralizeHeaderAudit();

                    CentralizeHHistory oldcentHistory = _centHHistoryRepo.getCentralizeHHistoryCreate(Id);
                    CentralizeHeader SignObjectBefore = _hRepository.getCentralizeHeader(Id);
                    if (!SignObjectBefore.IsNull())
                    {
                        SignObjectBefore.Documents = _hRepository.getDocDet(SignObjectBefore.NIK);
                        SignObjectBefore.Signatures = _hRepository.getSignDet(SignObjectBefore.NIK);
                        ObjectBefore.Id = SignObjectBefore.NIK;
                        ObjectBefore.NIK = SignObjectBefore.NIK;
                        ObjectBefore.Name = SignObjectBefore.Name;
                        ObjectBefore.Region = SignObjectBefore.Region;
                        ObjectBefore.Division = SignObjectBefore.Division;
                        ObjectBefore.Departement = SignObjectBefore.Departement;
                        ObjectBefore.Titles = SignObjectBefore.Titles;
                        //ObjectBefore.BranchCode = SignObjectBefore.BranchCode;
                        ObjectBefore.Note = SignObjectBefore.Note;
                        ObjectBefore.Signatures = SignObjectBefore.Signatures.Count > 0 ? SignObjectBefore.Signatures.Count.ToString() + " item(s)" : null;
                        ObjectBefore.Documents = SignObjectBefore.Documents.Count > 0 ? SignObjectBefore.Documents.Count.ToString() + " item(s)" : null;
                        ObjectBefore.CreatedBy = !oldcentHistory.IsNull() ? oldcentHistory.RequestUser : null;
                        ObjectBefore.CreatedDate = !oldcentHistory.IsNull() ? oldcentHistory.RequestDate : null;
                    }

                    ObjectAfter.Id = reqH.NIK;
                    ObjectAfter.NIK = reqH.NIK;
                    ObjectAfter.Name = reqH.Name;
                    ObjectAfter.Region = reqH.Region;
                    ObjectAfter.Division = reqH.Division;
                    ObjectAfter.Departement = reqH.Departement;
                    ObjectAfter.Titles = reqH.Titles;
                    //ObjectAfter.BranchCode = reqH.BranchCode;
                    ObjectAfter.Note = reqH.Note;
                    ObjectAfter.Signatures = reqH.Signatures.Count > 0 ? reqH.Signatures.Count.ToString() + " item(s)" : null;
                    ObjectAfter.Documents = reqH.Documents.Count > 0 ? reqH.Documents.Count.ToString() + " item(s)" : null;
                    ObjectAfter.CreatedBy = reqH.RequestUser;
                    if (reqH.RequestType.Trim() != "C")
                        ObjectAfter.CreatedDate = !oldcentHistory.IsNull() ? oldcentHistory.RequestDate : null;
                    else
                        ObjectAfter.CreatedDate = reqH.RequestDate;
                    //// Hapus detail data, biar audit trailnya ngak penuh
                    //reqH.Documents = null;
                    //reqH.Signatures = null;
                    //reqH.IdentityCards = null;

                    //oldsvsH.Documents = null;
                    //oldsvsH.Signatures = null;
                    //oldsvsH.IdentityCards = null;

                    //_auditRepository.SaveAuditTrail(Settings.ModuleName, reqH, user.Username, "Reject");
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, ObjectBefore, ObjectAfter, reqH.RequestUser, reqH.RequestDate, user.Username, "Reject");

                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {

                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Reject));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            //ini kalau ga ke index
            //CollectScreenMessages();
            //return CreateView(data); //ini akan problem gara2 si dropdown di disable, harus kasih hidden valuenya si dropdown di detail

            //ini kalau ke index
            //CollectScreenMessages(); ga perlu karena ke index otomatis collect, kalo ada ini malah ga keluar messagenya by Ivan
            return RedirectToAction("Index");
        }

        //public List<Branch> getBranches()
        //{
        //    User user = Lookup.Get<User>();
        //    IList<string> branchs = new List<string>();
        //    if (user.BranchCode == "000")
        //    {
        //        return _branchRepository.getBranchs().ToList();
        //    }
        //    else
        //    {
        //        branchs.Add(user.BranchCode);
        //        return _branchRepository.getBranchs(branchs).ToList();
        //    }
        //}

        #region Dropdownlist

        //private IList<SelectListItem> createBranchSelect(string selected)
        //{
        //    IList<SelectListItem> dataList = _branchRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Branch, SelectListItem>(ConvertFrom));
        //    if (!selected.IsNullOrEmpty())
        //    {
        //        dataList.FindElement(item => item.Value == selected).Selected = true;
        //    }
        //    return dataList;
        //}

        private IList<SelectListItem> createAccountTypeSelect(string selected)
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = "CS", Text = "CA/SA" });
            dataList.Insert(1, new SelectListItem() { Value = "TD", Text = "TD" });
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString().Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(Branch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.BranchCode;
            returnItem.Value = item.BranchCode;
            return returnItem;
        }

        //private IList<SelectListItem> createDocTypeSelect(string selected)
        //{
        //    //List<RoleDocType> authDocs = Lookup.Get<List<RoleDocType>>();
        //    List<DocumentType> docList = _doctypeRepository.FindAll().ToList();
        //    List<DocumentType> finalDocList = new List<DocumentType>();
        //    foreach (DocumentType item in docList)
        //    {
        //        var found = authDocs.FindElement(x => x.DocumentTypeId == item.Id);
        //        if (!found.IsNull())
        //        {
        //            finalDocList.Add(item);
        //        }
        //    }

        //    IList<SelectListItem> dataList = finalDocList.ConvertAll<SelectListItem>(new Converter<DocumentType, SelectListItem>(ConvertFrom));

        //    IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
        //    IList<SelectListItem> sortedList = sortedEnum.ToList();

        //    if (!selected.IsNullOrEmpty())
        //    {
        //        sortedList.FindElement(item => item.Value == selected).Selected = true;
        //    }
        //    return sortedList;
        //}

        private IList<SelectListItem> createAllDocTypeSelect()
        {
            List<CentralUnitDocType> docList = _doctypeRepository.FindAll().ToList();

            IList<SelectListItem> dataList = docList.ConvertAll<SelectListItem>(new Converter<CentralUnitDocType, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
            IList<SelectListItem> sortedList = sortedEnum.ToList();
            return sortedList;
        }

        public SelectListItem ConvertFrom(CentralUnitDocType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }

        #endregion

        [HttpGet]
        public ActionResult NoAuthCheckViewDocument(string State, long ID)
        {
            JsonResult result = new JsonResult();

            if (State == "New")
            {
                CentralizeDDocReq doc = _hReqRepository.getDocReqD(ID);
                if (doc.IsNull())
                {
                    return Content("Not exist or invalid document");
                }

                byte[] blob = Convert.FromBase64String(doc.FileBlob);

                string filetype = doc.FileType.Split(';')[0].Split(':')[1];
                if (filetype.Trim() == "application/pdf")
                {
                    return File(blob, filetype);
                }
                else
                {
                    Stream strm = new MemoryStream(Convert.FromBase64String(doc.FileBlob));
                    Bitmap bmp = (Bitmap)Bitmap.FromStream(strm);

                    ImageFormatConverter imgConverter = new ImageFormatConverter();
                    var png = imgConverter.ConvertFromString("PNG");

                    MemoryStream ms = new MemoryStream();
                    bmp.Save(ms, (ImageFormat)png);

                    byte[] bytImg = ms.ToArray();
                    string strImg = Convert.ToBase64String(bytImg);
                    return Content("<img src='data:image/PNG;base64," + strImg + "' alt='" + doc.DocumentType + "'/>");
                }

            }
            else
            {
                CentralizeDDoc doc = _hRepository.getDocDet(ID);
                if (doc.IsNull())
                {
                    return Content("Not exist or invalid document");
                }

                byte[] blob = Convert.FromBase64String(doc.FileBlob);

                string filetype = doc.FileType.Split(';')[0].Split(':')[1];
                if (filetype.Trim() == "application/pdf")
                {
                    return File(blob, filetype);
                }
                else
                {
                    return Content("<img src='" + doc.FileType + doc.FileBlob + "' alt='" + doc.DocumentType + "'/>");
                }

            }

        }
    }
}