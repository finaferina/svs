﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;
using System.Globalization;

namespace UOB.SVS.Controllers
{
    public class ParameterApprovalCentralUnitController : PageController
    {
        private IParameterCentralUnitRepository _parameterRepository;
        private IParameterAuditTrailCentralUnit _auditRepository;
        public ParameterApprovalCentralUnitController(ISessionAuthentication sessionAuthentication, IParameterCentralUnitRepository parameterRepository, IParameterAuditTrailCentralUnit auditRepository)
            : base(sessionAuthentication)
        {
            this._parameterRepository = parameterRepository;
            this._auditRepository = auditRepository;
            Settings.Title = ParameterResources.ApprovalPageTitle;

            Settings.ModuleName = "ParameterApprovalCentralUnit";
            Settings.Title = "Parameter Approval Centralize";
        }

        protected override void Startup()
        {
            User user = Lookup.Get<User>();
            ParameterHistoryCentralUnit param = _parameterRepository.GetChangesRequest();
            bool isSameUser = false;
            if (param != null)
            {
                if (user.Username.Trim() == param.ChangedBy.Trim())
                {
                    isSameUser = true;
                    //isSameUser = false;
                    ScreenMessages.Submit(ScreenMessage.Error("Cannot approve your own requested data!"));
                }

                //param.UserId = Encryption.Instance.DecryptText(param.UserId);
                //param.Password = Encryption.Instance.DecryptText(param.Password);
                //param.NewUserId = Encryption.Instance.DecryptText(param.NewUserId);
                //param.NewPassword = Encryption.Instance.DecryptText(param.NewPassword);
                ViewData["ParameterHistory"] = param;
            }
            ViewData["isSameUser"] = isSameUser;

        }

        // POST: Function/Edit/5
        [HttpPost]
        public ActionResult Approve(string ApprovalStatus)
        {
            ViewData["ActionName"] = "Approve";
            User user = Lookup.Get<User>();
            string message = "";
            //string approveDate = Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy HH:mm:ss");
            try
            {
                ParameterCentralUnit paramDataOld = _parameterRepository.getParameter();
                ParameterCentralUnit paramData = _parameterRepository.getParameter();
                ParameterHistoryCentralUnit histData = _parameterRepository.GetChangesRequest();
                histData.ApprovedBy = user.Username;
                //histData.ApprovedDate = DateTime.ParseExact(approveDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);//dibuat seperti ini karena pakai datetime.now formatnya tidak sesuai
                histData.ApprovedDate = DateTime.Now;

                if (histData.ApprovalStatus.Trim() == "A")
                {
                    ScreenMessages.Submit(ScreenMessage.Error("Cannot approve, data already approved!"));
                    CollectScreenMessages();
                    return CreateView();
                }
                else if (histData.ApprovalStatus.Trim() == "D")
                {
                    ScreenMessages.Submit(ScreenMessage.Error("Cannot approve, data already rejected!"));
                    CollectScreenMessages();
                    return CreateView();
                }

                //_auditRepository.SaveAuditTrail(Settings.ModuleName, paramData, user.Username, "BeforeEdit");
                if (ApprovalStatus == ApprovalStatusState.D.ToString())
                {
                    histData.ApprovalStatus = ApprovalStatusState.D.ToString();
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, paramData, histData, histData.ChangedBy, histData.ChangedDate, user.Username, "Reject");

                }
                else if (ApprovalStatus == ApprovalStatusState.A.ToString())
                {
                    histData.ApprovalStatus = ApprovalStatusState.A.ToString();

                    //paramData.UserId = histData.NewUserId;
                    //paramData.Password = histData.NewPassword;
                    //paramData.DukcapilUrl = histData.NewDukcapilUrl;
                    //paramData.StaggingUrl = histData.NewStaggingUrl;
                    //paramData.ClientIPAddress = histData.NewClientIPAddress;
                    //paramData.SessionTimeout = (histData.NewSessionTimeout <= 0) ? 30 : histData.NewSessionTimeout;
                    //paramData.LoginAttempt = (histData.NewLoginAttempt <= 0) ? 3 : histData.NewLoginAttempt;
                    //paramData.PasswordExp = histData.NewPasswordExp;
                    //Penambahan Rifki

                    //paramData.SizeKTP = histData.NewSizeKTP;
                    paramData.SizeSignature = histData.NewSizeSignature;
                    paramData.SizeDocument = histData.NewSizeDocument;
                    //paramData.KeepHistory = histData.NewKeepHistory;
                    paramData.KeepAuditTrail = histData.NewKeepAuditTrail;
                    paramData.KeepPendingAppr = histData.NewKeepPendingAppr;
                    //paramData.MaxDiffAccount = histData.NewMaxDiffAccount;
                    //paramData.KeepCloseAccount = histData.NewKeepCloseAccount;
                    paramData.ChangedBy = histData.ChangedBy;
                    paramData.ChangedDate = histData.ApprovedDate;
                    //paramData.Validity = histData.NewValidity;
                    //paramData.NIKExpiredDuration = histData.NewNIKExpiredDuration;
                    _parameterRepository.Save(paramData);

                    _auditRepository.SaveAuditTrail(Settings.ModuleName, paramDataOld, histData, histData.ChangedBy, histData.ChangedDate, user.Username, "Edit");

                }

                _parameterRepository.SaveHistory(histData);

            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                if (ApprovalStatus == ApprovalStatusState.D.ToString())
                {
                    ScreenMessages.Submit(ScreenMessage.Success(ParameterResources.Validation_Reject));
                }
                else if (ApprovalStatus == ApprovalStatusState.A.ToString())
                {
                    ScreenMessages.Submit(ScreenMessage.Success(ParameterResources.Validation_Approve));
                }
                return RedirectToRoute(new { controller = "ParameterApprovalCentralUnit", action = "Index" });
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView();

        }

        private ViewResult CreateView()
        {
            ParameterHistoryCentralUnit param = _parameterRepository.GetChangesRequest();
            User user = Lookup.Get<User>();
            bool isSameUser = false;
            //param.UserId = Encryption.Instance.DecryptText(param.UserId);
            //param.Password = Encryption.Instance.DecryptText(param.Password);
            //param.NewUserId = Encryption.Instance.DecryptText(param.NewUserId);
            //param.NewPassword = Encryption.Instance.DecryptText(param.NewPassword);
            if (user.Username.Trim() == param.ChangedBy.Trim())
            {
                isSameUser = true;
            }

            ViewData["ParameterHistory"] = param;
            ViewData["isSameUser"] = isSameUser;

            return View("ParameterApproval");
        }

    }
}