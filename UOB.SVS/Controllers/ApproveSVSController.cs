﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using UOB.SVS.Resources;
using UOB.SVS.Model;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System.Linq;
using Treemas.Credential.Model;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;

namespace UOB.SVS.Controllers
{
    public class ApproveSVSController : PageController
    {
        private ISignatureHReqRepository _hReqRepository;
        private ISignatureHRepository _hRepository;
        private ISignatureHReqService _signatureHReqService;
        private ISignatureHHistoryRepository _signHHistoryRepo;
        private IAuditTrailLog _auditRepository;
        private IBranchRepository _branchRepository;
        private IDocumentTypeRepository _docTypeRepository;
        private IUserRepository _userRepository;

        public ApproveSVSController(ISessionAuthentication sessionAuthentication, ISignatureHReqRepository hReqRepository,
            IBranchRepository branchRepository, IDocumentTypeRepository docTypeRepository, ISignatureHRepository hRepository,
            ISignatureHReqService signatureHReqService, IAuditTrailLog auditRepository, IUserRepository userRepository, ISignatureHHistoryRepository signHHistoryRepo) : base(sessionAuthentication)
        {
            this._hReqRepository = hReqRepository;
            this._auditRepository = auditRepository;
            this._branchRepository = branchRepository;
            this._signatureHReqService = signatureHReqService;
            this._docTypeRepository = docTypeRepository;
            this._hRepository = hRepository;
            this._userRepository = userRepository;
            this._signHHistoryRepo = signHHistoryRepo;
            Settings.ModuleName = "ApproveSVS";
            Settings.Title = "Approval SVS";
        }

        protected override void Startup()
        {

        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                SVSReqFilter filter = new SVSReqFilter();
                User user = Lookup.Get<User>();
                filter.Branches = getBranches();
                filter.ApprovalType = "ApproveSVS";
                filter.RequestType = Request.QueryString.GetValues("reqType")[0];
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<SignatureHRequest> data;
                if (!searchValue.IsNullOrEmpty())
                {
                    if (searchColumn == "AccountNumber")
                        filter.AccountNo = searchValue;
                    else if (searchColumn == "AccountName")
                        filter.AccountName = searchValue;
                }

                data = _hReqRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter, user);
                var items = data.Items.ToList().ConvertAll<SignatureHReqView>(new Converter<SignatureHRequest, SignatureHReqView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public SignatureHReqView ConvertFrom(SignatureHRequest item)
        {
            SignatureHReqView returnItem = new SignatureHReqView();
            returnItem.AccountNo = item.AccountNo;
            returnItem.AccountName = HttpUtility.HtmlEncode(item.AccountName);
            returnItem.AccountType = item.AccountType;
            returnItem.CIFNumber = item.CIFNumber;
            returnItem.BranchCode = item.BranchCode;
            returnItem.Note = HttpUtility.HtmlEncode(item.Note);
            returnItem.RequestType = item.RequestType;
            returnItem.RequestDate = item.RequestDate;
            returnItem.RequestUser = item.RequestUser;
            returnItem.RequestReason = item.RequestReason;
            returnItem.RequestDateString = item.RequestDate.Value.ToString("dd/MM/yyyy");

            int index = 1;
            if (item.Documents != null)
            {
                IList<Document> docs = new List<Document>();
                foreach (SignatureDDocReq row in item.Documents)
                {
                    Document doc = new Document();
                    doc.Index = index++;
                    doc.DocumentType = row.DocumentType;
                    doc.FileBlob = row.FileBlob;
                    doc.FileType = row.FileType;
                    doc.FileSeq = row.FileSeq;
                    doc.ID = row.ID;
                    doc.IsValidDoc = true;
                    docs.Add(doc);
                }
                returnItem.Documents = docs.ToList();
            }

            if (item.IdentityCards != null)
            {
                index = 1;
                IList<IdentityCard> idcards = new List<IdentityCard>();
                foreach (SignatureDKTPReq row in item.IdentityCards)
                {
                    IdentityCard idcard = new IdentityCard();
                    idcard.Index = index++;
                    idcard.CroppedBlob = HttpUtility.HtmlEncode(row.KTP);
                    idcard.ImageType = HttpUtility.HtmlEncode(row.ImageType);
                    idcard.IsValidImage = row.KTP.Length % 4 == 0 ? true : false;
                    idcard.FileSeq = row.FileSeq;
                    idcards.Add(idcard);
                }
                returnItem.IdentityCards = idcards.ToList();
            }

            if (item.Signatures != null)
            {
                index = 1;
                IList<Signature> signs = new List<Signature>();
                foreach (SignatureDSignReq row in item.Signatures)
                {
                    Signature sign = new Signature();
                    sign.Index = index++;
                    sign.CroppedBlob = HttpUtility.HtmlEncode(row.Signature);
                    sign.ImageType = HttpUtility.HtmlEncode(row.ImageType);
                    sign.IsValidImage = row.Signature.Length % 4 == 0 ? true : false;
                    sign.FileSeq = row.FileSeq;
                    signs.Add(sign);
                }
                returnItem.Signatures = signs.ToList();
            }

            return returnItem;
        }
        public SignatureHReqView ConvertFromHeader(SignatureHeader header)
        {
            SignatureHReqView returnItem = new SignatureHReqView();
            returnItem.AccountNo = header.AccountNo;
            returnItem.AccountNameOld = header.AccountName;
            returnItem.AccountTypeOld = header.AccountType;
            returnItem.CIFNumberOld = header.CIFNumber;
            returnItem.BranchCodeOld = header.BranchCode;
            returnItem.NoteOld = header.Note;

            int index = 1;

            if (header.Documents != null)
            {
                IList<Document> docs = new List<Document>();
                foreach (SignatureDDoc row in header.Documents)
                {
                    Document doc = new Document();
                    doc.Index = index++;
                    doc.DocumentTypeOld = row.DocumentType;
                    doc.FileBlobOld = row.FileBlob;
                    doc.FileTypeOld = row.FileType;
                    doc.FileSeq = row.Seq;
                    doc.ID = row.Id;
                    doc.IsValidDoc = true;
                    docs.Add(doc);
                }
                returnItem.Documents = docs.ToList();
            }

            if (header.IdentityCards != null)
            {
                index = 1;
                IList<IdentityCard> idcards = new List<IdentityCard>();
                foreach (SignatureDKTP row in header.IdentityCards)
                {
                    IdentityCard idcard = new IdentityCard();
                    idcard.Index = index++;
                    idcard.CroppedBlobOld = HttpUtility.HtmlEncode(row.KTP);
                    idcard.ImageTypeOld = HttpUtility.HtmlEncode(row.ImageType);
                    idcard.IsValidImage = row.KTP.Length % 4 == 0 ? true : false;
                    idcard.FileSeq = row.Seq;
                    idcards.Add(idcard);
                }
                returnItem.IdentityCards = idcards.ToList();
            }

            if (header.Signatures != null)
            {
                index = 1;
                IList<Signature> signs = new List<Signature>();
                foreach (SignatureDSign row in header.Signatures)
                {
                    Signature sign = new Signature();
                    sign.Index = index++;
                    sign.CroppedBlobOld = HttpUtility.HtmlEncode(row.Signature);
                    sign.ImageTypeOld = HttpUtility.HtmlEncode(row.ImageType);
                    sign.IsValidImage = row.Signature.Length % 4 == 0 ? true : false;
                    sign.FileSeq = row.Seq;
                    signs.Add(sign);
                }
                returnItem.Signatures = signs.ToList();
            }

            return returnItem;
        }

        public ActionResult Edit(string id)
        {
            Parameter param = Lookup.Get<Parameter>();
            ViewData["Parameter"] = param;
            SignatureHRequest reqH = _hReqRepository.getSignatureHReq(id);
            if (reqH.IsNull())
            {
                ScreenMessages.Submit(ScreenMessage.Error("Account number does not exist!"));
                return RedirectToAction("", "ApproveSVS");
            }

            reqH.IdentityCards = _hReqRepository.getKTPReqD(reqH.AccountNo);
            reqH.Documents = _hReqRepository.getDocReqD(reqH.AccountNo);
            reqH.Signatures = _hReqRepository.getSignReqD(reqH.AccountNo);

            User userReq = _userRepository.GetUser(reqH.RequestUser);
            SignatureHReqView data = ConvertFrom(reqH);

            SignatureHeader reqHeader = _hRepository.getSignatureHeader(id);
            if (reqHeader != null)
            {
                reqHeader.IdentityCards = _hRepository.getKTPDet(reqH.AccountNo);
                reqHeader.Documents = _hRepository.getDocDet(reqH.AccountNo);
                reqHeader.Signatures = _hRepository.getSignDet(reqH.AccountNo);

                SignatureHReqView dataheader = ConvertFromHeader(reqHeader);
                ViewData["DataHeader"] = dataheader;
            }

            ViewData["SVSData"] = data;
            ViewData["UserReq"] = userReq;
            Session["SVSData"] = data;

            return CreateView(data);
        }

        private ViewResult CreateView(SignatureHReqView data)
        {
            User user = Lookup.Get<User>();

            List<RoleDocType> authDocs = Lookup.Get<List<RoleDocType>>();
            ViewData["DocCount"] = authDocs.Count;

            ViewData["UserData"] = user;
            ViewData["BranchMaster"] = createBranchSelect("");
            ViewData["DocTypeMaster"] = createDocTypeSelect("");
            ViewData["AccTypeList"] = createAccountTypeSelect("");
            ViewData["AllDocTypeMaster"] = createAllDocTypeSelect();

            if (data.RequestType == "U")
            {
                return View("DetailEdit", data);
            }
            else
            {
                return View("Detail", data);
            }
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Approve(SignatureHReqView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            SignatureHRequest reqH = _hReqRepository.getSignatureHReq(data.AccountNo);
            User userReq = _userRepository.GetUser(reqH.RequestUser);

            try
            {
                if (user.BranchCode != "000")// && userReq.BranchCode != user.BranchCode)
                {
                    List<Branch> uBranches = getBranches();
                    Branch selectedBranch = uBranches.Find(b => b.BranchCode.Trim() == data.BranchCode.Trim());
                    if (selectedBranch.IsNull())
                        message = SvsResources.Validation_BranchCodeNotMatch;
                }
                SignatureHReqView currData = (SignatureHReqView)Session["SVSData"];
                if (currData.AccountNo.Trim() != data.AccountNo.Trim())
                {
                    message = "approve failed, invalid or not exist account number!";
                }

                if (currData.CIFNumber.Trim() != data.CIFNumber.Trim())
                {
                    message = "approve failed, invalid or not exist CIF number!";
                }

                if (user.Username.Trim() == userReq.Username.Trim())
                {
                    message = "Cannot approve your own request data!";
                }

                if (!(data.RequestDate == reqH.RequestDate))
                {
                    message = "Already change";
                }
                if (message.IsNullOrEmpty())
                {

                    reqH.IdentityCards = _hReqRepository.getKTPReqD(reqH.AccountNo);
                    reqH.Documents = _hReqRepository.getDocReqD(reqH.AccountNo);
                    reqH.Signatures = _hReqRepository.getSignReqD(reqH.AccountNo);

                    SignatureHReqView dataReq = ConvertFrom(reqH);
                    SignatureHeaderAudit ObjectBefore = new SignatureHeaderAudit();
                    SignatureHeaderAudit ObjectAfter = new SignatureHeaderAudit();
                    SignatureHHistory oldsvsHistory = _signHHistoryRepo.getSignatureHHistoryCreate(data.AccountNo);

                    if (reqH.RequestType.Trim() != "C")
                    {
                        SignatureHeader oldsvsH = _hRepository.getSignatureHeader(data.AccountNo);
                        oldsvsH.IdentityCards = _hRepository.getKTPDet(oldsvsH.AccountNo);
                        oldsvsH.Documents = _hRepository.getDocDet(oldsvsH.AccountNo);
                        oldsvsH.Signatures = _hRepository.getSignDet(oldsvsH.AccountNo);

                        ObjectBefore.Id = oldsvsH.AccountNo;
                        ObjectBefore.AccountNo = oldsvsH.AccountNo;
                        ObjectBefore.AccountName = oldsvsH.AccountName;
                        ObjectBefore.AccountType = oldsvsH.AccountType;
                        ObjectBefore.CIFNumber = oldsvsH.CIFNumber;
                        ObjectBefore.BranchCode = oldsvsH.BranchCode;
                        ObjectBefore.Note = oldsvsH.Note;
                        ObjectBefore.Signatures = oldsvsH.Signatures.Count > 0 ? oldsvsH.Signatures.Count.ToString() + " item(s)" : null;
                        ObjectBefore.IdentityCards = oldsvsH.IdentityCards.Count > 0 ? oldsvsH.IdentityCards.Count.ToString() + " item(s)" : null;
                        ObjectBefore.Documents = oldsvsH.Documents.Count > 0 ? oldsvsH.Documents.Count.ToString() + " item(s)" : null;
                        ObjectBefore.CreatedBy = !oldsvsHistory.IsNull() ? oldsvsHistory.RequestUser : null;
                        ObjectBefore.CreatedDate = !oldsvsHistory.IsNull() ? oldsvsHistory.RequestDate : null;
                    }

                    SignatureHRequest newData = new SignatureHRequest("");
                    newData.AccountNo = dataReq.AccountNo;
                    newData.AccountName = dataReq.AccountName;
                    newData.AccountType = dataReq.AccountType;
                    newData.CIFNumber = dataReq.CIFNumber;
                    newData.BranchCode = dataReq.BranchCode;
                    newData.Note = dataReq.Note;
                    newData.RequestType = dataReq.RequestType;
                    newData.RequestDate = dataReq.RequestDate;
                    newData.RequestUser = dataReq.RequestUser;
                    newData.RequestReason = dataReq.RequestReason;

                    SignatureHeader svsH = new SignatureHeader("");

                    svsH.AccountNo = dataReq.AccountNo;
                    svsH.AccountName = HttpUtility.HtmlDecode(dataReq.AccountName);
                    svsH.AccountType = dataReq.AccountType;
                    svsH.CIFNumber = dataReq.CIFNumber;
                    svsH.BranchCode = dataReq.BranchCode;
                    svsH.Note = HttpUtility.HtmlDecode(dataReq.Note);

                    ObjectAfter.Id = dataReq.AccountNo;
                    ObjectAfter.AccountNo = dataReq.AccountNo;
                    ObjectAfter.AccountName = HttpUtility.HtmlDecode(dataReq.AccountName);
                    ObjectAfter.AccountType = dataReq.AccountType;
                    ObjectAfter.CIFNumber = dataReq.CIFNumber;
                    ObjectAfter.BranchCode = dataReq.BranchCode;
                    ObjectAfter.Note = HttpUtility.HtmlDecode(dataReq.Note);
                    ObjectAfter.Signatures = dataReq.Signatures.Count > 0 ? dataReq.Signatures.Count.ToString() + " item(s)" : null;
                    ObjectAfter.IdentityCards = dataReq.IdentityCards.Count > 0 ? dataReq.IdentityCards.Count.ToString() + " item(s)" : null;
                    ObjectAfter.Documents = dataReq.Documents.Count > 0 ? dataReq.Documents.Count.ToString() + " item(s)" : null;
                    ObjectAfter.CreatedBy = dataReq.RequestUser;
                    if (reqH.RequestType.Trim() != "C")
                        ObjectAfter.CreatedDate = !oldsvsHistory.IsNull() ? oldsvsHistory.RequestDate : null;
                    else
                        ObjectAfter.CreatedDate = dataReq.RequestDate;


                    int index = 1;
                    IList<SignatureDDoc> docs = new List<SignatureDDoc>();

                    if (dataReq.Documents != null)
                    {
                        
                        foreach (Document item in dataReq.Documents)
                        {
                            SignatureDDocReq rDoc = _hReqRepository.getDocReqD(item.ID);

                            SignatureDDoc doc = new SignatureDDoc(0L);
                            doc.AccountNo = dataReq.AccountNo;
                            doc.DocumentType = item.DocumentType;
                            doc.FileBlob = rDoc.IsNull() ? item.FileBlob : rDoc.FileBlob;
                            doc.FileType = item.FileType;
                            doc.Seq = index++;//item.FileSeq; //
                            docs.Add(doc);
                        }
                    }
                    svsH.Documents = docs;

                    IList<SignatureDKTP> idcards = new List<SignatureDKTP>();
                    if (dataReq.Documents != null)
                    {
                        index = 1;
                        foreach (IdentityCard item in dataReq.IdentityCards)
                        {
                            SignatureDKTP idcard = new SignatureDKTP(0L);
                            idcard.AccountNo = dataReq.AccountNo;
                            idcard.KTP = item.CroppedBlob;
                            idcard.ImageType = item.ImageType;
                            idcard.Seq = index++;//item.FileSeq;//
                            idcards.Add(idcard);
                        }
                    }
                    svsH.IdentityCards = idcards;

                    IList<SignatureDSign> signs = new List<SignatureDSign>();
                    if (dataReq.Documents != null)
                    {
                        index = 1;
                        foreach (Signature item in dataReq.Signatures)
                        {
                            SignatureDSign sign = new SignatureDSign(0L);
                            sign.AccountNo = dataReq.AccountNo;
                            sign.Signature = item.CroppedBlob;
                            sign.ImageType = item.ImageType;
                            sign.Seq = index++;//item.FileSeq;//
                            signs.Add(sign);
                        }
                    }
                    svsH.Signatures = signs;

                    SignatureHHistory svsHistory = new SignatureHHistory(0L);
                    svsHistory.AccountNo = dataReq.AccountNo;
                    svsHistory.AccountName = dataReq.AccountName;
                    svsHistory.AccountType = dataReq.AccountType;
                    svsHistory.CIFNumber = dataReq.CIFNumber;
                    svsHistory.BranchCode = dataReq.BranchCode;
                    svsHistory.Note = dataReq.Note;
                    svsHistory.RequestType = dataReq.RequestType;
                    svsHistory.RequestDate = dataReq.RequestDate;
                    svsHistory.RequestUser = dataReq.RequestUser;
                    svsHistory.RequestReason = dataReq.RequestReason;
                    svsHistory.ApproveBy = user.Username;
                    svsHistory.ApproveDate = DateTime.Now;

                    message = _signatureHReqService.ApproveReq(newData, svsH, svsHistory, user);

                    // Hapus detail data, biar audit trailnya ngak penuh
                    //svsH.Documents = null;
                    //svsH.Signatures = null;
                    //svsH.IdentityCards = null;

                    //oldsvsH.Documents = null;
                    //oldsvsH.Signatures = null;
                    //oldsvsH.IdentityCards = null;

                    //_auditRepository.SaveAuditTrail(Settings.ModuleName, svsH, user.Username, "Approve");
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, ObjectBefore, ObjectAfter, dataReq.RequestUser, dataReq.RequestDate, user.Username, dataReq.RequestType);

                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approve));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            //ini kalau ga ke index
            //CollectScreenMessages();
            //return CreateView(data); //ini akan problem gara2 si dropdown di disable, harus kasih hidden valuenya si dropdown di detail

            //ini kalau ke index
            //CollectScreenMessages(); ga perlu karena ke index otomatis collect, kalo ada ini malah ga keluar messagenya by Ivan
            return RedirectToAction("Index");
        }

        //[HttpPost]
        public ActionResult Reject(string Id)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            SignatureHRequest requestH = _hReqRepository.getSignatureHReq(Id);
            User userReq = _userRepository.GetUser(requestH.RequestUser);

            try
            {
                if (user.BranchCode != "000")// && userReq.BranchCode != user.BranchCode)
                {
                    List<Branch> uBranches = getBranches();
                    Branch selectedBranch = uBranches.Find(b => b.BranchCode.Trim() == requestH.BranchCode.Trim());
                    if (selectedBranch.IsNull())
                        message = SvsResources.Validation_BranchCodeNotMatch;
                }

                if (message.IsNullOrEmpty())
                {
                    SignatureHRequest reqH = _hReqRepository.getSignatureHReq(Id);
                    reqH.IsRejected = true;
                    reqH.IdentityCards = _hReqRepository.getKTPReqD(reqH.AccountNo);
                    reqH.Documents = _hReqRepository.getDocReqD(reqH.AccountNo);
                    reqH.Signatures = _hReqRepository.getSignReqD(reqH.AccountNo);

                    SignatureHeader oldsvsH = _hRepository.getSignatureHeader(reqH.AccountNo);
                    if (oldsvsH.IsNull())
                        oldsvsH = new SignatureHeader(reqH.AccountNo);

                    message = _signatureHReqService.RejectReq(reqH, user);

                    SignatureHeaderAudit ObjectBefore = new SignatureHeaderAudit();
                    SignatureHeaderAudit ObjectAfter = new SignatureHeaderAudit();

                    SignatureHHistory oldsvsHistory = _signHHistoryRepo.getSignatureHHistoryCreate(Id);
                    SignatureHeader SignObjectBefore = _hRepository.getSignatureHeader(Id);
                    if (!SignObjectBefore.IsNull())
                    {
                        SignObjectBefore.IdentityCards = _hRepository.getKTPDet(SignObjectBefore.AccountNo);
                        SignObjectBefore.Documents = _hRepository.getDocDet(SignObjectBefore.AccountNo);
                        SignObjectBefore.Signatures = _hRepository.getSignDet(SignObjectBefore.AccountNo);
                        ObjectBefore.Id = SignObjectBefore.AccountNo;
                        ObjectBefore.AccountNo = SignObjectBefore.AccountNo;
                        ObjectBefore.AccountName = SignObjectBefore.AccountName;
                        ObjectBefore.AccountType = SignObjectBefore.AccountType;
                        ObjectBefore.CIFNumber = SignObjectBefore.CIFNumber;
                        ObjectBefore.BranchCode = SignObjectBefore.BranchCode;
                        ObjectBefore.Note = SignObjectBefore.Note;
                        ObjectBefore.Signatures = SignObjectBefore.Signatures.Count > 0 ? SignObjectBefore.Signatures.Count.ToString() + " item(s)" : null;
                        ObjectBefore.IdentityCards = SignObjectBefore.IdentityCards.Count > 0 ? SignObjectBefore.IdentityCards.Count.ToString() + " item(s)" : null;
                        ObjectBefore.Documents = SignObjectBefore.Documents.Count > 0 ? SignObjectBefore.Documents.Count.ToString() + " item(s)" : null;
                        ObjectBefore.CreatedBy = !oldsvsHistory.IsNull() ? oldsvsHistory.RequestUser : null;
                        ObjectBefore.CreatedDate = !oldsvsHistory.IsNull() ? oldsvsHistory.RequestDate : null;
                    }

                    ObjectAfter.Id = reqH.AccountNo;
                    ObjectAfter.AccountNo = reqH.AccountNo;
                    ObjectAfter.AccountName = reqH.AccountName;
                    ObjectAfter.AccountType = reqH.AccountType;
                    ObjectAfter.CIFNumber = reqH.CIFNumber;
                    ObjectAfter.BranchCode = reqH.BranchCode;
                    ObjectAfter.Note = reqH.Note;
                    ObjectAfter.Signatures = reqH.Signatures.Count > 0 ? reqH.Signatures.Count.ToString() + " item(s)" : null;
                    ObjectAfter.IdentityCards = reqH.IdentityCards.Count > 0 ? reqH.IdentityCards.Count.ToString() + " item(s)" : null;
                    ObjectAfter.Documents = reqH.Documents.Count > 0 ? reqH.Documents.Count.ToString() + " item(s)" : null;
                    ObjectAfter.CreatedBy = reqH.RequestUser;
                    if (reqH.RequestType.Trim() != "C")
                        ObjectAfter.CreatedDate = !oldsvsHistory.IsNull() ? oldsvsHistory.RequestDate : null;
                    else
                        ObjectAfter.CreatedDate = reqH.RequestDate;
                    //// Hapus detail data, biar audit trailnya ngak penuh
                    //reqH.Documents = null;
                    //reqH.Signatures = null;
                    //reqH.IdentityCards = null;

                    //oldsvsH.Documents = null;
                    //oldsvsH.Signatures = null;
                    //oldsvsH.IdentityCards = null;

                    //_auditRepository.SaveAuditTrail(Settings.ModuleName, reqH, user.Username, "Reject");
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, ObjectBefore, ObjectAfter, reqH.RequestUser, reqH.RequestDate, user.Username, "Reject");

                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {

                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Reject));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            //ini kalau ga ke index
            //CollectScreenMessages();
            //return CreateView(data); //ini akan problem gara2 si dropdown di disable, harus kasih hidden valuenya si dropdown di detail

            //ini kalau ke index
            //CollectScreenMessages(); ga perlu karena ke index otomatis collect, kalo ada ini malah ga keluar messagenya by Ivan
            return RedirectToAction("Index");
        }

        public List<Branch> getBranches()
        {
            User user = Lookup.Get<User>();
            IList<string> branchs = new List<string>();
            if (user.BranchCode == "000")
            {
                return _branchRepository.getBranchs().ToList();
            }
            else
            {
                branchs.Add(user.BranchCode);
                return _branchRepository.getBranchs(branchs).ToList();
            }
        }

        #region Dropdownlist

        private IList<SelectListItem> createBranchSelect(string selected)
        {
            IList<SelectListItem> dataList = _branchRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Branch, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        private IList<SelectListItem> createAccountTypeSelect(string selected)
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = "CS", Text = "CA/SA" });
            dataList.Insert(1, new SelectListItem() { Value = "TD", Text = "TD" });
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString().Trim()).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFrom(Branch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.BranchCode;
            returnItem.Value = item.BranchCode;
            return returnItem;
        }

        private IList<SelectListItem> createDocTypeSelect(string selected)
        {
            List<RoleDocType> authDocs = Lookup.Get<List<RoleDocType>>();
            List<DocumentType> docList = _docTypeRepository.FindAll().ToList();
            List<DocumentType> finalDocList = new List<DocumentType>();
            foreach (DocumentType item in docList)
            {
                var found = authDocs.FindElement(x => x.DocumentTypeId == item.Id);
                if (!found.IsNull())
                {
                    finalDocList.Add(item);
                }
            }

            IList<SelectListItem> dataList = finalDocList.ConvertAll<SelectListItem>(new Converter<DocumentType, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (!selected.IsNullOrEmpty())
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }

        private IList<SelectListItem> createAllDocTypeSelect()
        {
            List<DocumentType> docList = _docTypeRepository.FindAll().ToList();

            IList<SelectListItem> dataList = docList.ConvertAll<SelectListItem>(new Converter<DocumentType, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
            IList<SelectListItem> sortedList = sortedEnum.ToList();
            return sortedList;
        }

        public SelectListItem ConvertFrom(DocumentType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Description;
            returnItem.Value = item.DocType;
            return returnItem;
        }

        #endregion

        [HttpGet]
        public ActionResult NoAuthCheckViewDocument(string State, long ID)
        {
            JsonResult result = new JsonResult();

            if (State == "New")
            {
                SignatureDDocReq doc = _hReqRepository.getDocReqD(ID);
                if (doc.IsNull())
                {
                    return Content("Not exist or invalid document");
                }

                byte[] blob = Convert.FromBase64String(doc.FileBlob);

                string filetype = doc.FileType.Split(';')[0].Split(':')[1];
                if (filetype.Trim() == "application/pdf")
                {
                    return File(blob, filetype);
                }
                else
                {
                    Stream strm = new MemoryStream(Convert.FromBase64String(doc.FileBlob));
                    Bitmap bmp = (Bitmap)Bitmap.FromStream(strm);

                    ImageFormatConverter imgConverter = new ImageFormatConverter();
                    var png = imgConverter.ConvertFromString("PNG");

                    MemoryStream ms = new MemoryStream();
                    bmp.Save(ms, (ImageFormat)png);

                    byte[] bytImg = ms.ToArray();
                    string strImg = Convert.ToBase64String(bytImg);
                    return Content("<img src='data:image/PNG;base64," + strImg + "' alt='" + doc.DocumentType + "'/>");
                }

            }
            else
            {
                SignatureDDoc doc = _hRepository.getDocDet(ID);
                if (doc.IsNull())
                {
                    return Content("Not exist or invalid document");
                }

                byte[] blob = Convert.FromBase64String(doc.FileBlob);

                string filetype = doc.FileType.Split(';')[0].Split(':')[1];
                if (filetype.Trim() == "application/pdf")
                {
                    return File(blob, filetype);
                }
                else
                {
                    return Content("<img src='" + doc.FileType + doc.FileBlob + "' alt='" + doc.DocumentType + "'/>");
                }

            }

        }
    }
}