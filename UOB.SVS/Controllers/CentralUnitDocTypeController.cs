﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;

namespace UOB.SVS.Controllers
{
    public class CentralUnitDocTypeController : PageController
    {
        private string _application = "SVS";
        private ICentralUnitDocTypeRepository _doctypeRepository;
        private ICentralUnitDocTypeCrRepository _doctypeCrRepository;
        private IApplicationRepository _appRepository;
        private IAppAuditTrailLog _auditRepository;
        public CentralUnitDocTypeController(ISessionAuthentication sessionAuthentication, ICentralUnitDocTypeRepository doctypeRepository,
            ICentralUnitDocTypeCrRepository doctypeCrRepository, IApplicationRepository appRepository, IAppAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._doctypeRepository = doctypeRepository;
            this._doctypeCrRepository = doctypeCrRepository;
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;
            Settings.ModuleName = "CentralUnitDocType";
            Settings.Title = "Master Doc Type";
        }

        protected override void Startup()
        {
            ViewData["loadState"] = (Request.QueryString.GetValues("loadState") == null) ? "0" : Request.QueryString.GetValues("loadState").FirstOrDefault();
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<CentralUnitDocType> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _doctypeRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _doctypeRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<CentralUnitDocTypeView>(new Converter<CentralUnitDocType, CentralUnitDocTypeView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public CentralUnitDocTypeView ConvertFrom(CentralUnitDocType item)
        {
            CentralUnitDocTypeView returnItem = new CentralUnitDocTypeView();
            returnItem.Application = item._Application;
            returnItem.Alias = item.Alias;
            returnItem.DocId = item.DocId;
            returnItem.Id = item.Id;
            returnItem.Name = item.Name;
            return returnItem;
        }

        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            CentralUnitDocTypeView data = new CentralUnitDocTypeView();
            return CreateView(data);
        }

        private ViewResult CreateView(CentralUnitDocTypeView data)
        {
            //ViewData["ApplicationMaster"] = createApplicationSelect(data.Application);
            return View("Detail", data);
        }

        // POST: Function/Create
        [HttpPost]
        public ActionResult Create(CentralUnitDocTypeView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    CentralUnitDocTypeCr newData = new CentralUnitDocTypeCr(0L);
                    newData._Application = _application;
                    newData.DocId = data.DocId;
                    newData.Name = data.Name;
                    newData.Alias = data.Alias;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    newData.ApprovalType = "C";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;
                    //_regionRepository.Add(newData);
                    _doctypeCrRepository.Add(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success("Data Berhasil di Tambahkan"));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(CentralUnitDocTypeView data, string mode)
        {
            if (data.Name == "")
                return "Nama Tidak Boleh Kosong";

            if (data.DocId == "")
                return "Doc ID Tidak Boleh Kosong";

            if (mode == "Create")
            {
                if (_doctypeRepository.IsDuplicate(_application, data.DocId))
                    return "Doc ID dan Nama Sudah Pernah Ada";
                else if (_doctypeRepository.IsDuplicateCR(_application, data.DocId, "N"))
                    return "Doc ID dan Nama Sudah Ada di Approval";

                if (_doctypeRepository.IsDuplicateByAlias(_application, data.DocId))
                    return "Alias Sudah Pernah Ada";
                else if (_doctypeRepository.IsDuplicateCRByAlias(_application, data.DocId, "N"))
                    return "Alias Sudah Ada di Approval";
            }
            return "";
        }

        // GET: Function/Edit/5
        public ActionResult Edit(long id)
        {
            ViewData["ActionName"] = "Edit";
            CentralUnitDocType autoCentralUnitDocType = _doctypeRepository.getCentralUnitDocType(id);
            CentralUnitDocTypeView data = ConvertFrom(autoCentralUnitDocType);

            return CreateView(data);
        }

        // POST: Function/Edit/5
        [HttpPost]
        public ActionResult Edit(CentralUnitDocTypeView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    //new additional by fahmi
                    CentralUnitDocType app = _doctypeRepository.getCentralUnitDocType(data.Id);
                    //if (!app.IsNull())
                    //{
                    //    _doctypeRepository.UpdateCentralUnitDocType(data.Id, data.DocId, data.Name, data.Alias, user.Username, DateTime.Now);
                    //}
                    CentralUnitDocTypeCr newData = new CentralUnitDocTypeCr(0L);
                    newData._Application = _application;
                    newData.DocId = data.DocId;
                    newData.Name = data.Name;
                    newData.Alias = data.Alias;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    newData.ApprovalType = "U";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;
                    _doctypeCrRepository.Add(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success("Data Berhasil di Ubah"));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }
        public ActionResult Delete(long id)
        {
            ViewData["ActionName"] = "Delete";
            CentralUnitDocType autoDoctype = _doctypeRepository.getCentralUnitDocType(id);
            CentralUnitDocTypeView data = ConvertFrom(autoDoctype);

            return CreateView(data);
        }

        // POST: Function/Delete/5
        [HttpPost]
        public ActionResult Delete(CentralUnitDocTypeView data)
        {
            User user = Lookup.Get<User>();
            CentralUnitDocType centralunitdoctypedata = _doctypeRepository.getCentralUnitDocType(data.Id);
            string message = "";
            try
            {
                //_directorateRepository.deleteRegion(Id);
                //_doctypeRepository.Remove(centralunitdoctypedata);
                CentralUnitDocTypeCr newData = new CentralUnitDocTypeCr(0L);
                newData._Application = centralunitdoctypedata._Application;
                newData.DocId = centralunitdoctypedata.DocId;
                newData.Name = centralunitdoctypedata.Name;
                newData.Alias = centralunitdoctypedata.Alias;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                newData.ApprovalType = "D";
                newData.ApprovalStatus = "N";
                newData.Approved = false;

                _doctypeCrRepository.Save(newData);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success("Data Berhasil di Hapus"));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(data.Id);
        }
    }
}