﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;
using System.Globalization;
using System.Text.RegularExpressions;
using Microsoft.Reporting.WebForms;
using System.IO;
using UOB.SVS.Model;
using UOB.SVS.Interface;
using SpreadsheetLight;
using System.Data;

namespace UOB.SVS.Controllers
{
    public class UserController : PageController
    {
        private IList<UserRole> userRoles;
        private IUserAccount _userAccRepository;
        private IUserRepository _userRepository;
        private IUserCrRepository _userCrRepository;
        private IParameterRepository _parameterRepository;
        private IApplicationRepository _appRepository;
        private IBranchRepository _branchRepository;
        private IAuthorizationRepository _authRepository;
        private IUserAuditTrailLog _auditRepository;
        private IUserApplicationRepository _userAppRepository;

        public UserController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository, IUserCrRepository userCrRepository,
            IApplicationRepository appRepository, IUserAuditTrailLog auditRepository, IAuthorizationRepository authRepository, IParameterRepository parameterRepository,
            IUserAccount userAccRepository, IBranchRepository branchRepository, IUserApplicationRepository userAppRepository, IRoleRepository roleRepository) : base(sessionAuthentication)
        {
            this._userRepository = userRepository;
            this._userCrRepository = userCrRepository;
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;
            this._parameterRepository = parameterRepository;
            this._userAccRepository = userAccRepository;
            this._branchRepository = branchRepository;
            this._authRepository = authRepository;
            this._userAppRepository = userAppRepository;

            Settings.ModuleName = "User";
            Settings.Title = UserResources.PageTitle;
        }
        protected override void Startup()
        {
            ViewData["loadState"] = (Request.QueryString.GetValues("loadState") == null) ? "0" : Request.QueryString.GetValues("loadState").FirstOrDefault();
            long pid = Convert.ToInt64(Request.RequestContext.RouteData.Values["Id"]);
            if (pid > 0)
            {
                User printUser = _userRepository.GetUserForPrint(pid);
                ViewData["PrintId"] = printUser.Id;
                if (printUser.IsPrinted == true)
                {
                    ViewData["PrintStatus"] = (long)1;
                }
                else
                {
                    ViewData["PrintStatus"] = (long)0;
                }
            }
            else
            {
                ViewData["PrintId"] = (long)0;
                ViewData["PrintStatus"] = (long)1;
            }
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {

                //var a = Encryption.Instance.DecryptText("rjpmaapCT5KXyt485bikZQ==");
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<User> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _userRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    if (searchColumn.ToLower().Contains("date"))
                    {
                        DateTime searchDate = DateTime.ParseExact(searchValue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        data = _userRepository.FindAllPagedDate(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchDate);
                    }
                    else
                    {
                        data = _userRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                    }

                }

                var items = data.Items.ToList().ConvertAll<UserView>(new Converter<User, UserView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public UserView ConvertFrom(User item)
        {
            UserView returnItem = new UserView();
            returnItem.Id = item.Id;
            returnItem.Username = item.Username;
            //if (!item.InActiveDirectory && item.Password != null)
            //{
            //    returnItem.Password = Encryption.Instance.DecryptText(item.Password);
            //}
            //returnItem.Password = item.Password;
            returnItem.PasswordExpirationDate = item.PasswordExpirationDate.ToString("dd/MM/yyyy");
            returnItem.AccountValidityDate = item.AccountValidityDate.ToString("dd/MM/yyyy");
            returnItem.FullName = item.FullName;
            //returnItem.LastName = item.LastName;
            returnItem.RegNo = item.RegNo;
            returnItem.SessionTimeout = SystemSettings.Instance.Security.SessionTimeout; //item.SessionTimeout;
            returnItem.LockTimeout = 10;//item.LockTimeout;
            returnItem.MaxConLogin = 1; //item.MaximumConcurrentLogin;
            returnItem.InActiveDirectory = item.InActiveDirectory;
            //returnItem.Name = item.Name;
            returnItem.IsActive = item.IsActive;
            returnItem.IsPrinted = item.IsPrinted;
            returnItem.Approved = item.Approved;
            returnItem.CreatedDate = item.CreatedDate.Value.ToString("dd/MM/yyyy");
            returnItem.CreatedBy = item.CreatedBy;
            //returnItem.ApprovalType = "CREATION";
            returnItem.BranchCode = item.BranchCode;
            // karena yang merubah data master pasti selalu approver
            returnItem.ApprovedBy = item.ChangedBy == null ? "" : item.ChangedBy;
            returnItem.ApprovedDate = item.ChangedDate == null ? "" : item.ChangedDate.Value.ToString("dd/MM/yyyy");

            string auth = _authRepository.getDistinctUserAuthorizationByUsername(item.Username);
            returnItem.Roles = auth;

            IList<UserApplication> userApplication = _userAppRepository.getUserApplication(item.Username);
            string userapp = "";

            foreach (UserApplication ua in userApplication)
            {
                //userapp += ua.Application + ",";
                userapp = ua.Application + ",";
            }

            returnItem.Applications = userapp.Length > 1 ? userapp.Substring(0, userapp.Length - 1) : "";


            //UserApplication userApp = _userAppRepository.getUserApplication(item.Username, "SVS");
            UserApplication userApp = _userAppRepository.getUserApp(item.Username, "SVS");
            if (!userApp.IsNull())
            returnItem.UserAppID = userApp.Id;

            return returnItem;
        }

        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            UserView data = new UserView();

            Parameter Param = _parameterRepository.getParameter();
            //data.AccountValidityDate = Param.Validity;
            data.PasswordExpirationDate = Param.PasswordExp;
            data.InActiveDirectory = true;
            return CreateView(data);
        }

        private ViewResult CreateView(UserView data)
        {
            ViewData["BranchMaster"] = createBranchSelect(data.BranchCode);
            return View("Detail", data);
        }


        // POST: Feature/Create
        [HttpPost]
        public ActionResult Create(UserView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            Parameter Param = _parameterRepository.getParameter();

            string message = "";
            long printId = 0;
            try
            {
                message = validateData(data, "Create", user);
                if (message.IsNullOrEmpty())
                {

                    UserCr newData = new UserCr(0L);
                    newData.Username = data.Username;

                    if (data.InActiveDirectory)
                    {
                        newData.Password = null;

                        if (_userAccRepository.IsUserExistInActiveDirectory(data.Username, SystemSettings.Instance.Security.DomainServer))
                        {
                            newData.PasswordExpirationDate = DateTime.Now.AddDays(double.Parse(Param.PasswordExp));

                            newData.AccountValidityDate = DateTime.Now.AddYears(99);
                            newData.FullName = data.FullName;
                            newData.RegNo = data.RegNo;
                            newData.SessionTimeout = SystemSettings.Instance.Security.SessionTimeout; //item.SessionTimeout;
                            newData.LockTimeout = 10;//item.LockTimeout;
                            newData.MaximumConcurrentLogin = 1; //item.MaximumConcurrentLogin;
                            newData.IsActive = user.Username == SystemSettings.Instance.Runtime.SuperUser ? true : false;
                            newData.InActiveDirectory = data.InActiveDirectory;
                            newData.IsPrinted = false;
                            newData.Approved = user.Username == SystemSettings.Instance.Runtime.SuperUser ? true : false;
                            newData.CreatedBy = user.Username;
                            newData.CreatedDate = DateTime.Now;
                            //newData.Roles = 
                            newData.ApprovalType = ApprovalType.C.ToString();

                            newData.ChangedBy = user.Username;
                            newData.BranchCode = data.BranchCode;

                            _userCrRepository.Add(newData);

                            if (user.Username == SystemSettings.Instance.Runtime.SuperUser)
                            {
                                _userCrRepository.setApproved(newData.Id, user.Username, DateTime.Now, ApprovalStatusState.A.ToString());

                                User newData2 = new User(0L);

                                newData2.Username = data.Username;
                                if (!data.InActiveDirectory)
                                {
                                    newData2.Password = Encryption.Instance.EncryptText(data.Password);
                                }
                                //else
                                //{
                                //    newData2.Password = Encryption.Instance.EncryptText(data.Password); ;
                                //}
                                newData2.PasswordExpirationDate = DateTime.Now.AddDays(double.Parse(Param.PasswordExp));
                                newData2.AccountValidityDate = DateTime.Now.AddYears(99);
                                newData2.FullName = data.FullName;
                                newData2.RegNo = data.RegNo;
                                newData2.SessionTimeout = data.SessionTimeout;
                                newData2.LockTimeout = data.LockTimeout;
                                newData2.MaximumConcurrentLogin = 1;
                                newData2.IsActive = true;
                                newData2.InActiveDirectory = data.InActiveDirectory;
                                newData2.IsPrinted = data.IsPrinted;
                                newData2.Approved = data.Approved;
                                newData2.CreatedBy = user.CreatedBy;
                                newData2.CreatedDate = DateTime.Now;
                                newData2.BranchCode = data.BranchCode;

                                _userRepository.Add(newData2);
                            }

                            //new modify 16092020
                            if(_userAppRepository.IsDuplicate(data.Username))
                                _userAppRepository.DeleteUserApplication(data.Username);

                            UserApplication newUAppData = new UserApplication(0L);
                            newUAppData.Username = data.Username;
                            newUAppData.Application = "SVS";
                            newUAppData.IsDefault = false;
                            newUAppData.ChangedBy = user.Username;
                            newUAppData.ChangedDate = DateTime.Now;
                            newUAppData.CreatedBy = user.Username;
                            newUAppData.CreatedDate = DateTime.Now;
                            _userAppRepository.Add(newUAppData);

                            printId = newData.Id;
                            data.Id = newData.Id;
                            data.UserAppID = newUAppData.Id;
                        }
                        else
                        {
                            message = "User not found in Active Directory";
                        }
                    }
                    else
                    {
                        //newData.Password = Encryption.Instance.EncryptText("12345678");//buat testing aja
                        newData.Password = Encryption.Instance.EncryptText(data.Password);//buat testing aja

                        newData.PasswordExpirationDate = DateTime.Now.AddDays(double.Parse(Param.PasswordExp));

                        newData.AccountValidityDate = DateTime.Now.AddYears(99);
                        newData.FullName = data.FullName;
                        newData.RegNo = data.RegNo;
                        newData.SessionTimeout = SystemSettings.Instance.Security.SessionTimeout; //item.SessionTimeout;
                        newData.LockTimeout = 10;//item.LockTimeout;
                        newData.MaximumConcurrentLogin = 1; //item.MaximumConcurrentLogin;
                        newData.IsActive = user.Username == SystemSettings.Instance.Runtime.SuperUser ? true : false;
                        newData.InActiveDirectory = data.InActiveDirectory;
                        newData.IsPrinted = false;
                        newData.Approved = user.Username == SystemSettings.Instance.Runtime.SuperUser ? true : false;
                        newData.CreatedBy = user.Username;
                        newData.CreatedDate = DateTime.Now;
                        newData.ApprovalType = ApprovalType.C.ToString();

                        newData.ChangedBy = user.Username;
                        newData.BranchCode = data.BranchCode;

                        _userCrRepository.Add(newData);

                        if (user.Username == SystemSettings.Instance.Runtime.SuperUser)
                        {
                            _userCrRepository.setApproved(newData.Id, user.Username, DateTime.Now, ApprovalStatusState.A.ToString());

                            User newData2 = new User(0L);

                            newData2.Username = data.Username;
                            //if (!data.InActiveDirectory)
                            //{
                            //    newData2.Password = Encryption.Instance.EncryptText(data.Password);
                            //}
                            //else
                            //{
                            //newData2.Password = Encryption.Instance.EncryptText("12345678");//buat testing aja
                            newData2.Password = Encryption.Instance.EncryptText(data.Password);//buat testing aja
                            //}
                            newData2.PasswordExpirationDate = DateTime.Now.AddDays(double.Parse(Param.PasswordExp));
                            newData2.AccountValidityDate = DateTime.Now.AddYears(99);
                            newData2.FullName = data.FullName;
                            newData2.RegNo = data.RegNo;
                            newData2.SessionTimeout = data.SessionTimeout;
                            newData2.LockTimeout = data.LockTimeout;
                            newData2.MaximumConcurrentLogin = 1;
                            newData2.IsActive = true;
                            newData2.InActiveDirectory = data.InActiveDirectory;
                            newData2.IsPrinted = data.IsPrinted;
                            newData2.Approved = data.Approved;
                            newData2.CreatedBy = user.CreatedBy;
                            newData2.CreatedDate = DateTime.Now;
                            newData2.BranchCode = data.BranchCode;

                            _userRepository.Add(newData2);
                        }

                        //new modify 16092020
                        if (_userAppRepository.IsDuplicate(data.Username))
                            _userAppRepository.DeleteUserApplication(data.Username);

                        UserApplication newUAppData = new UserApplication(0L);
                        newUAppData.Username = data.Username;
                        newUAppData.Application = "SVS";
                        newUAppData.IsDefault = false;
                        newUAppData.ChangedBy = user.Username;
                        newUAppData.ChangedDate = DateTime.Now;
                        newUAppData.CreatedBy = user.Username;
                        newUAppData.CreatedDate = DateTime.Now;
                        _userAppRepository.Add(newUAppData);

                        printId = newData.Id;
                        data.Id = newData.Id;
                        data.UserAppID = newUAppData.Id;
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            
            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(UserResources.Validation_Success));
                //return RedirectToAction("Edit\\" + data.Id);
                CollectScreenMessages();
                return CreateView(data); //RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
            //return Create();
        }

        private string validateData(UserView data, string mode, User user)
        {
            if (mode == "Edit")
            {
                UserCr usrcr = _userCrRepository.GetUser(data.Username, ApprovalType.U);

                if (user.Username != SystemSettings.Instance.Runtime.SuperUser)
                {
                    if (usrcr != null)
                        if (usrcr.ApprovalType == ApprovalType.U.ToString())
                            return UserResources.Validation_AlreadyRequest;
                }

                if (data.Username == "")
                    return UserResources.Validation_FillUsername;

                //if (data.RegNo.IsNull())
                //    return UserResources.Validation_FillRegNo;

                if (data.FullName.IsNull())
                    return UserResources.Validation_FillFirstName;

                //if (data.LastName.IsNull())
                //    return UserResources.Validation_FillLastName;

                if (data.SessionTimeout.IsNull())
                    return UserResources.Validation_FillSessionTimeout;

                if (data.LockTimeout.IsNull())
                    return UserResources.Validation_FillLockTimeout;

                //if (data.LastName.IsNull())
                //    return UserResources.Validation_FillMaxConLogin;           

            }

            if (mode == "Reset")
            {
                UserCr usrcr = _userCrRepository.GetUser(data.Username, ApprovalType.R);

                if (usrcr != null)
                    if (user.Username != SystemSettings.Instance.Runtime.SuperUser)
                    {
                        if (usrcr.ApprovalType == ApprovalType.R.ToString())
                            return UserResources.Validation_AlreadyRequest;
                    }

                if (!data.InActiveDirectory)
                {
                    if (data.Password == "")
                        return UserResources.Validation_FillPassword;

                    if (data.Password != data.ConfirmPassword)
                        return UserResources.Validation_ConfirmPass;

                    Regex regex = new Regex("^(?!.*[\\s])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-zA-Z]).{0,}|(?!.*[\\s])(?=.*[0-9])(?=.*[a-zA-Z]).{0,}|(?!.*[\\s])(?=.*[!@#$%^&*])(?=.*[a-zA-Z]).{0,}");
                    var match = regex.Match(data.Password).Success;
                    if (match == false)
                        return UserResources.Validation_Pass;
                }
            }

            if (mode == "Create")
            {
                UserCr usrcr = _userCrRepository.GetUser(data.Username, ApprovalType.C);

                if (usrcr != null)
                    if (user.Username != SystemSettings.Instance.Runtime.SuperUser)
                    {
                        if (usrcr.ApprovalType == ApprovalType.C.ToString())
                            return UserResources.Validation_AlreadyRequest;
                    }

                if (_userRepository.IsDuplicate(data.Username))
                    return UserResources.Validation_DuplicateData;

                //if (!data.InActiveDirectory)
                //{
                //    if (data.Password == "")
                //        return UserResources.Validation_FillPassword;

                //    if (data.Password != data.ConfirmPassword)
                //        return UserResources.Validation_ConfirmPass;

                //    Regex regex = new Regex("^(?!.*[\\s])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-zA-Z]).{0,}|(?!.*[\\s])(?=.*[0-9])(?=.*[a-zA-Z]).{0,}|(?!.*[\\s])(?=.*[!@#$%^&*])(?=.*[a-zA-Z]).{0,}");
                //    var match = regex.Match(data.Password).Success;
                //    if (match == false)
                //        return UserResources.Validation_Pass;
                //}
            }
            return "";
        }

        // GET: Feature/Edit/5
        public ActionResult Edit(long id)
        {
            ViewData["ActionName"] = "Edit";
            User user = _userRepository.GetUserById(id);
            if (user == null)
            {
                UserCr usercr = _userCrRepository.GetUserById(id);
                if (usercr != null)
                {
                    user = new User(usercr.Id);
                    user.Username = usercr.Username;
                    //if (!item.InActiveDirectory && item.Password != null)
                    //{
                    //    returnItem.Password = Encryption.Instance.DecryptText(item.Password);
                    //}
                    //returnItem.Password = item.Password;
                    user.PasswordExpirationDate = usercr.PasswordExpirationDate;//.ToString("dd/MM/yyyy");
                    user.AccountValidityDate = usercr.AccountValidityDate;//.ToString("dd/MM/yyyy");
                    user.FullName = usercr.FullName;
                    //returnItem.LastName = item.LastName;
                    user.RegNo = usercr.RegNo;
                    user.SessionTimeout = SystemSettings.Instance.Security.SessionTimeout; //item.SessionTimeout;
                    user.LockTimeout = 10;//item.LockTimeout;
                    //user.MaxConLogin = 1; //item.MaximumConcurrentLogin;
                    user.InActiveDirectory = usercr.InActiveDirectory;
                    //returnItem.Name = item.Name;
                    user.IsActive = usercr.IsActive;
                    user.IsPrinted = usercr.IsPrinted;
                    user.Approved = usercr.Approved;
                    user.CreatedDate = usercr.CreatedDate;//.Value.ToString("dd/MM/yyyy");
                    user.CreatedBy = usercr.CreatedBy;
                    //returnItem.ApprovalType = "CREATION";
                    user.BranchCode = usercr.BranchCode;

                }
            }

            UserView data = ConvertFrom(user);
           

            return CreateView(data);
        }

        // POST: Feature/Edit/5
        [HttpPost]
        public ActionResult Edit(UserView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit", user);
                if (message.IsNullOrEmpty())
                {

                    if (data.InActiveDirectory)
                    {
                        if (_userAccRepository.IsUserExistInActiveDirectory(data.Username, SystemSettings.Instance.Security.DomainServer))
                        {
                            if (user.Username == SystemSettings.Instance.Runtime.SuperUser)
                            {
                                User newDataold = _userRepository.GetUser(data.Username);

                                User newData2 = _userRepository.GetUser(data.Username);

                                newData2.FullName = data.FullName;
                                newData2.InActiveDirectory = data.InActiveDirectory;
                                newData2.BranchCode = data.BranchCode;
                                newData2.ChangedBy = data.ChangedBy;
                                newData2.ChangedDate = DateTime.Now; //apakah changed date = approval date

                                _userRepository.Save(newData2);
                                _auditRepository.SaveAuditTrail(Settings.ModuleName, newDataold, newData2, newData2.CreatedBy, newData2.CreatedDate, user.Username, "Edit");
                            }
                            else
                            {
                                User oldData = _userRepository.GetUserById(data.Id);

                                string auth = _authRepository.getDistinctUserAuthorizationByUsername(oldData.Username);
                                UserCr newData = new UserCr(0L);
                                newData.Username = oldData.Username;
                                newData.FullName = oldData.FullName;
                                newData.FullNameNew = data.FullName;
                                newData.RegNo = data.RegNo;
                                newData.SessionTimeout = oldData.SessionTimeout;
                                newData.LockTimeout = oldData.LockTimeout;
                                newData.MaximumConcurrentLogin = oldData.MaximumConcurrentLogin;

                                newData.LastLogin = oldData.LastLogin;
                                newData.LoginAttempt = oldData.LoginAttempt;

                                newData.InActiveDirectory = oldData.InActiveDirectory;
                                newData.InActiveDirectoryNew = data.InActiveDirectory;
                                newData.IsActive = oldData.IsActive;

                                newData.IsPrinted = oldData.IsPrinted;
                                newData.Approved = false;

                                newData.ApprovalType = ApprovalType.U.ToString();

                                newData.Password = oldData.Password;
                                newData.PasswordExpirationDate = oldData.PasswordExpirationDate;
                                newData.AccountValidityDate = oldData.AccountValidityDate;

                                newData.CreatedDate = oldData.CreatedDate;
                                newData.CreatedBy = oldData.CreatedBy;
                                newData.ChangedBy = user.Username;
                                newData.ChangedDate = DateTime.Now;

                                newData.BranchCode = oldData.BranchCode;
                                newData.BranchCodeNew = data.BranchCode;

                                newData.AuthorizedApplicationIdsOld = "SVS";
                                newData.AuthorizedApplicationIdsNew = "SVS";

                                newData.AuthorizedRoleIdsOld = auth;
                                newData.AuthorizedRoleIdsNew = auth;

                                _userCrRepository.Add(newData);
                            }
                        }
                        else
                        {
                            message = "Invalid Username in Active Directory";
                        }
                    }
                    else
                    {
                        if (user.Username == SystemSettings.Instance.Runtime.SuperUser)
                        {
                            User newDataold = _userRepository.GetUser(data.Username);

                            User newData2 = _userRepository.GetUser(data.Username);

                            newData2.FullName = data.FullNameNew;
                            newData2.InActiveDirectory = data.InActiveDirectory;
                            newData2.BranchCode = data.BranchCode;
                            newData2.ChangedBy = data.ChangedBy;
                            newData2.ChangedDate = DateTime.Now; //apakah changed date = approval date

                            _userRepository.Save(newData2);
                            _auditRepository.SaveAuditTrail(Settings.ModuleName, newDataold, newData2, newData2.CreatedBy, newData2.CreatedDate, user.Username, "Edit");
                        }
                        else
                        {

                            User oldData = _userRepository.GetUserById(data.Id);
                            string auth = _authRepository.getDistinctUserAuthorizationByUsername(oldData.Username);
                            UserCr newData = new UserCr(0L);
                            newData.Username = oldData.Username;
                            newData.FullName = oldData.FullName;
                            newData.FullNameNew = data.FullName;
                            newData.RegNo = data.RegNo;
                            newData.SessionTimeout = oldData.SessionTimeout;
                            newData.LockTimeout = oldData.LockTimeout;
                            newData.MaximumConcurrentLogin = oldData.MaximumConcurrentLogin;

                            newData.LastLogin = oldData.LastLogin;
                            newData.LoginAttempt = oldData.LoginAttempt;

                            newData.InActiveDirectory = oldData.InActiveDirectory;
                            newData.InActiveDirectoryNew = data.InActiveDirectory;
                            newData.IsActive = oldData.IsActive;

                            newData.IsPrinted = oldData.IsPrinted;
                            newData.Approved = false;

                            newData.ApprovalType = ApprovalType.U.ToString();

                            newData.Password = oldData.Password;
                            newData.PasswordExpirationDate = oldData.PasswordExpirationDate;
                            newData.AccountValidityDate = oldData.AccountValidityDate;

                            newData.CreatedDate = oldData.CreatedDate;
                            newData.CreatedBy = oldData.CreatedBy;
                            newData.ChangedBy = user.Username;
                            newData.ChangedDate = DateTime.Now;

                            newData.BranchCode = oldData.BranchCode;
                            newData.BranchCodeNew = data.BranchCode;

                            newData.AuthorizedApplicationIdsOld = "SVS";
                            newData.AuthorizedApplicationIdsNew = "SVS";

                            newData.AuthorizedRoleIdsOld = auth;
                            newData.AuthorizedRoleIdsNew = auth;

                            _userCrRepository.Add(newData);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(UserResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        public ActionResult Reset(long id)
        {
            ViewData["ActionName"] = "Reset";
            User userdata = _userRepository.GetUserById(id);
            UserView data = ConvertFrom(userdata);
            return ResetView(data);
        }

        private ViewResult ResetView(UserView data)
        {
            return View("Reset", data);
        }

        [HttpPost]
        public ActionResult Reset(UserView data)
        {
            ViewData["ActionName"] = "Reset";
            User user = Lookup.Get<User>();
            Parameter Param = _parameterRepository.getParameter();

            string message = "";
            try
            {
                message = validateData(data, "Reset", user);
                if (message.IsNullOrEmpty())
                {
                    User newData = _userRepository.GetUserById(data.Id);
                    if (user.Username == SystemSettings.Instance.Runtime.SuperUser)
                    {
                        _userRepository.resetPassword(data.Id, data.Password);
                    }
                    else
                    {
                        UserCr usrcr = new UserCr(0L);
                        string auth = _authRepository.getDistinctUserAuthorizationByUsername(newData.Username);
                        usrcr.Username = newData.Username;
                        usrcr.Password = Encryption.Instance.EncryptText(data.Password);
                        usrcr.PasswordExpirationDate = DateTime.Now.AddDays(double.Parse(Param.PasswordExp));
                        usrcr.AccountValidityDate = DateTime.Now.AddYears(99);
                        usrcr.FullName = newData.FullName;
                        usrcr.RegNo = newData.RegNo;
                        usrcr.SessionTimeout = newData.SessionTimeout;
                        usrcr.LockTimeout = newData.LockTimeout;
                        usrcr.MaximumConcurrentLogin = newData.MaximumConcurrentLogin;
                        usrcr.IsActive = true;
                        usrcr.InActiveDirectory = data.InActiveDirectoryNew;
                        //usrcr.InActiveDirectoryNew = data.InActiveDirectoryNew;
                        usrcr.IsPrinted = newData.IsPrinted;
                        usrcr.Approved = user.Username == SystemSettings.Instance.Runtime.SuperUser ? true : false;
                        //newData.ChangedBy = user.Username;
                        //newData.ChangedDate = DateTime.Now;
                        usrcr.CreatedBy = newData.CreatedBy;
                        usrcr.CreatedDate = newData.CreatedDate;
                        //newData.ApprovedBy = null;
                        //newData.ApprovedDate = null;
                        usrcr.ApprovalType = ApprovalType.R.ToString();
                        //newData.ApprovalStatus = null;
                        usrcr.LastLogin = null;

                        usrcr.ChangedBy = user.Username;
                        usrcr.BranchCode = newData.BranchCode;

                        usrcr.AuthorizedApplicationIdsOld = "SVS";
                        usrcr.AuthorizedApplicationIdsNew = "SVS";
                        if (!auth.IsNullOrEmpty())
                        {
                            usrcr.AuthorizedRoleIdsOld = auth;
                            usrcr.AuthorizedRoleIdsNew = auth;
                        }
                        _userCrRepository.Add(usrcr);
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(UserResources.Validation_Reset));
                return RedirectToAction("Index"); ;
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return ResetView(data);
        }

        // POST: Feature/Delete/5
        [HttpPost]
        public ActionResult Delete(long Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {

                User userdata = _userRepository.GetUserById(Id);
                string auth = _authRepository.getDistinctUserAuthorizationByUsername(userdata.Username);
                UserCr newData = new UserCr(0L);

                newData.Username = userdata.Username;
                newData.FullName = userdata.FullName;
                newData.RegNo = userdata.RegNo;
                newData.SessionTimeout = userdata.SessionTimeout;
                newData.LockTimeout = userdata.LockTimeout;
                newData.MaximumConcurrentLogin = userdata.MaximumConcurrentLogin;

                newData.LastLogin = userdata.LastLogin;
                newData.LoginAttempt = userdata.LoginAttempt;

                newData.InActiveDirectory = userdata.InActiveDirectory;
                newData.InActiveDirectoryNew = userdata.InActiveDirectory;
                newData.IsActive = userdata.IsActive;

                newData.IsPrinted = userdata.IsPrinted;
                newData.Approved = user.Username == SystemSettings.Instance.Runtime.SuperUser ? true : false;
                newData.ApprovalType = ApprovalType.D.ToString();

                newData.ApprovedBy = user.Username == SystemSettings.Instance.Runtime.SuperUser ? user.Username : null;

                DateTime? ApprovedDate = DateTime.Now;
                newData.ApprovedDate = user.Username == SystemSettings.Instance.Runtime.SuperUser ? ApprovedDate : null;

                newData.PasswordExpirationDate = userdata.PasswordExpirationDate;
                newData.AccountValidityDate = userdata.AccountValidityDate;

                newData.CreatedDate = userdata.CreatedDate;
                newData.CreatedBy = userdata.CreatedBy;
                //newData.ChangedBy = userdata.ChangedBy;
                newData.ChangedDate = userdata.ChangedDate;

                newData.ChangedBy = user.Username;
                newData.BranchCode = userdata.BranchCode;
                newData.AuthorizedApplicationIdsOld = "SVS";
                newData.AuthorizedRoleIdsOld = auth;

                _userCrRepository.Add(newData);


                if (user.Username == SystemSettings.Instance.Runtime.SuperUser)
                {
                    User usrdataold = new User(0L);

                    User usrdata = _userRepository.GetUserById(Id);
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, usrdataold, usrdata, usrdata.CreatedBy, usrdata.CreatedDate, user.Username, "Delete");
                    _userRepository.deleteAll(usrdata.Username);
                    _userRepository.Remove(usrdata);
                }

            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(UserResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }

        public ActionResult Activate(long id)
        {
            ViewData["ActionName"] = "Activate";
            User user = _userRepository.GetUserById(id);
            UserView data = ConvertFrom(user);
            ViewData["UserData"] = data;

            return View("Activate", data);
        }

        [HttpPost]
        public ActionResult Activate(UserView data)
        {
            ViewData["ActionName"] = "Activate";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                if (message.IsNullOrEmpty())
                {
                    Parameter Param = _parameterRepository.getParameter();
                    DateTime passexpdate = DateTime.Now.AddDays(double.Parse(Param.PasswordExp));
                    DateTime accvaldate = DateTime.Now.AddYears(99);

                    User userdata = _userRepository.GetUserById(data.Id);
                    //_auditRepository.SaveAuditTrail(Settings.ModuleName, userdata, user.Username, "Activate");

                    //actiave user on db
                    _userRepository.setActive(data.Id, passexpdate, accvaldate, user.Username);

                    ////actiave user LDAP, because get credential on user login AD is can make user locked
                    //if (userdata.InActiveDirectory)
                    //    _userRepository.setActiveAD(user.Username, user.Password, userdata.Username.Trim(), SystemSettings.Instance.Security.DomainServer);

                    UserCr newData = new UserCr(0L);
                    userdata = _userRepository.GetUserById(data.Id);

                    newData.Username = userdata.Username;
                    newData.FullName = userdata.FullName;
                    newData.RegNo = userdata.RegNo;
                    newData.SessionTimeout = userdata.SessionTimeout;
                    newData.LockTimeout = userdata.LockTimeout;
                    newData.MaximumConcurrentLogin = userdata.MaximumConcurrentLogin;

                    newData.LastLogin = userdata.LastLogin;
                    newData.LoginAttempt = userdata.LoginAttempt;

                    newData.InActiveDirectory = userdata.InActiveDirectory;
                    newData.InActiveDirectoryNew = userdata.InActiveDirectory;
                    newData.IsActive = userdata.IsActive;

                    newData.IsPrinted = userdata.IsPrinted;
                    newData.Approved = true;
                    newData.ApprovalType = ApprovalType.A.ToString();

                    newData.PasswordExpirationDate = userdata.PasswordExpirationDate;
                    newData.AccountValidityDate = userdata.AccountValidityDate;
                    newData.BranchCode = userdata.BranchCode;

                    newData.CreatedDate = userdata.CreatedDate;
                    newData.CreatedBy = userdata.CreatedBy;
                    newData.ChangedBy = userdata.ChangedBy;
                    newData.ChangedDate = userdata.ChangedDate;

                    _userCrRepository.Add(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(UserResources.Validation_SuccessActivate));
                return RedirectToRoute(new { controller = "User", action = "Index" });
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return RedirectToRoute(new { controller = "User", action = "Index" });
        }

        private IList<SelectListItem> createBranchSelect(string selected)
        {
            IList<SelectListItem> dataList = _branchRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Branch, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Value);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (!selected.IsNullOrEmpty())
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }

        public SelectListItem ConvertFrom(Branch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.BranchCode;
            returnItem.Value = item.BranchCode;
            return returnItem;
        }

        public ActionResult Export(string searchColumn, string searchValue)
        {
            var userList = _userRepository.FindAllPagedForExport(searchColumn, searchValue);
            IList<UserView> items = userList.ToList().ConvertAll<UserView>(new Converter<User, UserView>(ConvertFrom));

            DataTable dt = items.ListToDataTable();
            List<UserView> list = new List<UserView>();

            dt.Columns.Remove("Id");
            dt.Columns.Remove("RegNo");
            dt.Columns.Remove("Password");
            dt.Columns.Remove("OldPassword");
            dt.Columns.Remove("ConfirmPassword");
            dt.Columns.Remove("PasswordExpirationDate");
            dt.Columns.Remove("AccountValidityDate");
            dt.Columns.Remove("InActiveDirectory");
            dt.Columns.Remove("InActiveDirectoryNew");
            dt.Columns.Remove("sInActiveDirectory");
            dt.Columns.Remove("sInActiveDirectoryNew");
            dt.Columns.Remove("SessionTimeout");
            dt.Columns.Remove("LockTimeout");
            dt.Columns.Remove("MaxConLogin");
            dt.Columns.Remove("IsPrinted");
            dt.Columns.Remove("Approved");
            dt.Columns.Remove("passExp");
            dt.Columns.Remove("AccValidity");
            dt.Columns.Remove("LastLogin");
            dt.Columns.Remove("YesNo");
            dt.Columns.Remove("ApprovalStatus");
            dt.Columns.Remove("FullNameNew");
            dt.Columns.Remove("UserAppID");
            dt.Columns.Remove("ApplicationsNew");
            //dt.Columns.Remove("Roles");
            dt.Columns.Remove("RolesNew");
            //dt.Columns.Remove("ApprovedBy");
            //dt.Columns.Remove("ApprovedDate");
            dt.Columns.Remove("ApprovalType");
            dt.Columns.Remove("ChangedBy");
            dt.Columns.Remove("ChangedDate");
            dt.Columns.Remove("BranchCodeNew");

            MemoryStream ms = new MemoryStream();
            using (SLDocument sl = new SLDocument())
            {
                sl.ImportDataTable(1, 1, dt, true);
                sl.SelectWorksheet("Worksheet");
                sl.AutoFitColumn(1, 6);
                sl.SaveAs(ms);
            }

            ms.Position = 0;
            string fName = "SVS_Master_User.xls";
            return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fName);
        }

        //public ActionResult Print(long id)
        //{
        //    LocalReport lr = new LocalReport();
        //    string path = Path.Combine(Server.MapPath("~/Reports/PasswordPrint"), "PasswordPrint.rdlc");
        //    if (System.IO.File.Exists(path))
        //    {
        //        lr.ReportPath = path;
        //    }
        //    else
        //    {
        //        return View("Index");
        //    }

        //    User user = _userRepository.GetUserForPrint(id);
        //    if (user.IsNull())
        //    {
        //        ScreenMessages.Submit(ScreenMessage.Success(UserResources.Validation_Print));
        //        return RedirectToRoute(new { controller = "User", action = "Index" });
        //    }
        //    user.Password = Encryption.Instance.DecryptText(user.Password);

        //    List<User> dataModelList = new List<User>();
        //    dataModelList.Add(user);
        //    //IEnumerable<User> dataModelList = (IEnumerable<User>)user;

        //    ReportDataSource rd = new ReportDataSource("PasswordPrint", dataModelList);
        //    lr.DataSources.Add(rd);
        //    string reportType = "pdf";
        //    string mimeType;
        //    string encoding;
        //    string fileNameExtension;

        //    string deviceInfo =

        //    "<DeviceInfo>" +
        //    "  <OutputFormat>pdf</OutputFormat>" +
        //    "  <PageWidth>4in</PageWidth>" +
        //    "  <PageHeight>2in</PageHeight>" +
        //    "  <MarginTop>1cm</MarginTop>" +
        //    "  <MarginLeft>1cm</MarginLeft>" +
        //    "  <MarginRight>1cm</MarginRight>" +
        //    "  <MarginBottom>1cm</MarginBottom>" +
        //    "</DeviceInfo>";

        //    Warning[] warnings;
        //    string[] streams;
        //    byte[] renderedBytes;

        //    renderedBytes = lr.Render(
        //        reportType,
        //        deviceInfo,
        //        out mimeType,
        //        out encoding,
        //        out fileNameExtension,
        //        out streams,
        //        out warnings);

        //    _userRepository.setPrinted(id);
        //    ScreenMessages.Submit(ScreenMessage.Success(UserResources.Validation_Print));
        //    return File(renderedBytes, mimeType);
        //}

    }
}