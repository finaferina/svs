﻿using System;
using System.IO;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using UOB.SVS.Resources;
using UOB.SVS.Model;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System.Linq;
using Treemas.Credential.Model;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Drawing;
using System.Web;
using UOBISecurity;
using Treemas.Base.Configuration;
using Treemas.Credential.Repository;
using System.Text;
using System.Security.Cryptography;
using System.Xml.Serialization;

namespace UOB.SVS.Controllers
{
    public class CreateSVSController : PageController
    {
        private ISvsRepository _SvsRepo;
        private IParameterRepository _InquiryParameter;
        private ISignatureHReqRepository _signatureHReqRepository;
        private ISignatureHReqService _signatureHReqService;
        private IAccountNoViewRepository _accNoVwRepo;
        private IAuditTrailLog _auditRepository;
        private IBranchRepository _branchRepository;
        private IDocumentTypeRepository _docTypeRepository;
        private IAccountMasterRepository _accountMasterRepository;
        private IBranchAccountPrefixRepository _branchAccountPrefixRepository;
        private ISignatureHRepository _hRepository;

        private IList<RoleDocType> roleDocType;

        public CreateSVSController(ISessionAuthentication sessionAuthentication, ISvsRepository SvsRepo,
            IParameterRepository InquiryParameter, ISignatureHReqRepository signatureHReqRepository,
            ISignatureHReqService signatureHReqService, IAccountNoViewRepository accNoVwRepo, IAuditTrailLog auditRepository,
            IBranchRepository branchRepository, IDocumentTypeRepository docTypeRepository, IAccountMasterRepository accountMasterRepository,
            ISignatureHRepository signatureHRepository,
            IBranchAccountPrefixRepository branchAccountPrefixRepository) : base(sessionAuthentication)
        {
            this._SvsRepo = SvsRepo;
            this._InquiryParameter = InquiryParameter;
            this._signatureHReqRepository = signatureHReqRepository;
            this._signatureHReqService = signatureHReqService;
            this._accNoVwRepo = accNoVwRepo;
            this._branchRepository = branchRepository;
            this._docTypeRepository = docTypeRepository;
            this._accountMasterRepository = accountMasterRepository;
            this._branchAccountPrefixRepository = branchAccountPrefixRepository;
            this._auditRepository = auditRepository;
            this._hRepository = signatureHRepository;
            Settings.ModuleName = "CreateSVS";
            Settings.Title = "Create SVS";
        }

        protected override void Startup()
        {

            Create();
            double BlobSize = 0;
            Session.Add("BlobSize", BlobSize);
            //ViewData["BranchMaster"] = createBranchSelect("");
            //ViewData["DocTypeMaster"] = createDocTypeSelect("");

            // RedirectToAction("Create", "Create");
            //Response.Redirect("/Create");



            //this.Authentication.RedirectUrl = "CreateSVS/Create";

            //string controllerName = "Create";
            //System.Web.Routing.RequestContext ctx = new System.Web.Routing.RequestContext();

            //var controllerBuilder = ControllerBuilder.Current;
            //IControllerFactory factory = controllerBuilder.GetControllerFactory();
            //IController controller = factory.CreateController(ctx, controllerName);

            //try
            //{
            //    controller.Execute(ctx);
            //}
            //finally
            //{
            //    factory.ReleaseController(controller);
            //}
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.
                User user = Lookup.Get<User>();

                SVSReqFilter filter = new SVSReqFilter();
                filter.RequestType = "C";
                filter.Branches = getBranches();
                filter.RequestUser = user.Username;
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<SignatureHRequest> data;
                if (!searchValue.IsNullOrEmpty())
                {
                    if (searchColumn == "AccountNumber")
                        filter.AccountNo = searchValue;
                    else if (searchColumn == "AccountName")
                        filter.AccountName = searchValue;
                }

                data = _signatureHReqRepository.FindAllPagedCreate(pageNumber, pageSize, sortColumn, sortColumnDir, filter);
                var items = data.Items.ToList().ConvertAll<SignatureHReqView>(new Converter<SignatureHRequest, SignatureHReqView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public SignatureHReqView ConvertFrom(SignatureHRequest item)
        {
            SignatureHReqView returnItem = new SignatureHReqView();
            returnItem.AccountNo = item.AccountNo;
            returnItem.AccountName = item.AccountName;
            returnItem.AccountType = item.AccountType;
            returnItem.CIFNumber = item.CIFNumber;
            returnItem.BranchCode = item.BranchCode;
            returnItem.Note = item.Note;
            returnItem.RequestType = item.RequestType;
            returnItem.RequestDate = item.RequestDate;
            returnItem.RequestUser = item.RequestUser;
            returnItem.RequestReason = item.RequestReason;

            int index = 1;
            if (item.Documents != null)
            {
                IList<Document> docs = new List<Document>();
                foreach (SignatureDDocReq row in item.Documents)
                {
                    Document doc = new Document();
                    doc.ID = row.Id;
                    doc.Index = index++;
                    doc.DocumentType = row.DocumentType;
                    doc.FileBlob = row.FileBlob;
                    doc.FileType = row.FileType;
                    doc.AccountNo = row.AccountNo;
                    doc.IsNew = false;
                    docs.Add(doc);
                }
                returnItem.Documents = docs.ToList();
            }

            if (item.IdentityCards != null)
            {
                index = 1;
                IList<IdentityCard> idcards = new List<IdentityCard>();
                foreach (SignatureDKTPReq row in item.IdentityCards)
                {
                    IdentityCard idcard = new IdentityCard();
                    idcard.ID = row.Id;
                    idcard.Index = index++;
                    idcard.CroppedBlob = HttpUtility.HtmlEncode(row.KTP);
                    idcard.ImageType = HttpUtility.HtmlEncode(row.ImageType);
                    idcard.AccountNo = row.AccountNo;
                    idcard.IsNew = false;
                    idcards.Add(idcard);
                   
                }
                returnItem.IdentityCards = idcards.ToList();
            }

            if (item.Signatures != null)
            {
                index = 1;
                IList<Signature> signs = new List<Signature>();
                foreach (SignatureDSignReq row in item.Signatures)
                {
                    Signature sign = new Signature();
                    sign.ID = row.Id;
                    sign.Index = index++;
                    sign.CroppedBlob = HttpUtility.HtmlEncode(row.Signature);
                    sign.ImageType = HttpUtility.HtmlEncode(row.ImageType);
                    sign.AccountNo = row.AccountNo;
                    sign.IsNew = false;
                    signs.Add(sign);
                }
                returnItem.Signatures = signs.ToList();
            }
            return returnItem;
        }

        // GET: Function/Create
        public ActionResult Create()
        {
            List<RoleDocType> authDocs = Lookup.Get<List<RoleDocType>>();
            ViewData["DocCount"] = authDocs.Count;
            Response.Clear();

            Parameter param = Lookup.Get<Parameter>();

            ViewData["ActionName"] = "Create";
            ViewData["Parameter"] = param;

            SignatureHReqView data = new SignatureHReqView();
            return CreateView(data);
        }

        private ViewResult CreateView(SignatureHReqView data)
        {
            List<RoleDocType> authDocs = Lookup.Get<List<RoleDocType>>();
            ViewData["DocCount"] = authDocs.Count;

            ViewData["BranchMaster"] = createBranchSelect("");
            ViewData["DocTypeMaster"] = createDocTypeSelect("");
            ViewData["AllDocTypeMaster"] = createAllDocTypeSelect();
            ViewData["AccTypeMaster"] = createAccountTypeSelect("");
            return View("Detail", data);
        }

        // POST: Create new Data
        [HttpPost]
        public ActionResult Create(SignatureHReqView data)
        {
            Response.ClearContent();

            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            string returnCode = "";
            string warningmessage = "";
            string AccountNo = "";
            try
            {
                AccountMaster accMaster = _accountMasterRepository.getAccount(data.AccountNo);

                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    SignatureHRequest newData = new SignatureHRequest("");
                    AccountNo = data.AccountNo;
                    newData.AccountNo = data.AccountNo;
                    newData.AccountName = data.AccountName;
                    newData.AccountType = data.AccountType;
                    newData.CIFNumber = data.CIFNumber;
                    newData.BranchCode = data.BranchCode;
                    newData.Note = data.Note;
                    newData.RequestType = "C";
                    newData.RequestDate = DateTime.Now;
                    newData.RequestUser = user.Username;
                    newData.RequestReason = "Create New";
                    newData.IsRejected = false;

                    warningmessage = validateAccount(data, accMaster);
                    if (warningmessage == SvsResources.Validation_AccountMasterNotFound || warningmessage == "")
                    {
                        message = _signatureHReqService.SaveSignatureReq(newData);
                    }
                    else
                    {
                        message = warningmessage;
                    }


                    // Hapus detail data, biar audit trailnya ngak penuh
                    newData.Documents = null;
                    newData.Signatures = null;
                    newData.IdentityCards = null;
                }
            }
            catch (Exception e)
            {
                message = e.Message;
                returnCode = "ERR";
                //ScreenMessages.Submit(ScreenMessage.Error(message));
            }

            if (message.IsNullOrEmpty())
            {
                if (warningmessage.IsNullOrEmpty())
                {
                    returnCode = "OK";
                    ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                }
                else
                {
                    returnCode = "OK";
                    ScreenMessages.Submit(ScreenMessage.Success(string.Concat(warningmessage, "")));
                }

            }
            else
            {
                returnCode = "ERR";
                //sScreenMessages.Submit(ScreenMessage.Error(message));
            }

            JsonResult result = new JsonResult();
            result = this.Json(new
            {
                message = message,
                returnCode = returnCode,
                warningMessage = warningmessage,
                AccountNo = AccountNo
            });

            return result;
        }

        // POST: SaveDocument
        [HttpPost]
        public ActionResult NoAuthCheckDocSave(Document data)
        {
            User user = Lookup.Get<User>();
            string message = "";
            string returnCode = "";
            string NewFileBlob = "";
            //RsaEncryption rsa = new RsaEncryption();
            try
            {
                Parameter param = Lookup.Get<Parameter>();
                if (data.FileBlob.Length % 4 != 0)
                {
                    message = "Invalid uploaded document format!";
                }
                if (data.FileBlob.Length > param.GetSizeDocument)
                {
                    message = "Document " + data.FileName + " exceed maximum file size (" + (data.FileBlob.Length * 1365) + " KB) and cannot be uploaded!";
                }
                
                if(data.FileBlob == "null")
                {
                    SignatureDDoc dDoc = _hRepository.getDocDet(data.ID);
                    NewFileBlob = dDoc.FileBlob;
                    //NewFileBlob = rsa.Encrypt(NewFileBlob);
                    ////------------------------------------------------------------------------------
                    //Encryptor enc = new Encryptor();
                    //ApplicationRegistryHandler appReg = new ApplicationRegistryHandler();
                    ////tambah enkrip pake uobi security
                    //IConfigurationBinder binder = SystemConfigurationCabinet.Instance.GetBinder("Database");
                    //CompositeConfigurationItem items = (CompositeConfigurationItem)binder.GetConfiguration("System");
                    //string appName = items.GetItem("AppName").Value;
                    //string uobikey = items.GetItem("UOBIKey").Value;
                    //string keyReg = appReg.ReadFromRegistry(@"Software\" + appName, "Key");
                    //NewFileBlob = enc.Encrypt(NewFileBlob, keyReg, uobikey);
                }

                if (message.IsNullOrEmpty())
                {
                    SignatureHRequest reqH = _signatureHReqRepository.getSignatureHReq(data.AccountNo);
                    

                    if (reqH == null)
                    {
                        message = SvsResources.Validation_AccountNotFound;
                    }
                    else if ( NewFileBlob == "null") 
                    {
                        message = "Invalid Document " + data.DocumentType;
                    }
                    else
                    {
                        SignatureDDocReq doc = new SignatureDDocReq(0);
                        doc.AccountNo = data.AccountNo;
                        doc.DocumentType = data.DocumentType;
                        doc.FileBlob = data.FileBlob == "null" ? NewFileBlob : data.FileBlob; // data.FileBlob; //
                        doc.FileType = data.FileType;
                        doc.FileSeq = data.Index;
                        doc.RequestType = "C";
                        doc.RequestDate = DateTime.Now;
                        doc.RequestUser = user.Username;
                        doc.RequestReason = "Create New";
                        message = _signatureHReqService.SaveDocument(doc);
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
                returnCode = "ERR";
            }

            if (message.IsNullOrEmpty())
            {
                message = "";
                returnCode = "OK";
            }
            else
            {
                ScreenMessages.Submit(ScreenMessage.Error(message));
                returnCode = "ERR";
            }

            JsonResult result = new JsonResult();
            result = this.Json(new
            {
                message = message,
                returnCode = returnCode
            });
            return result;
        }

        //public class RsaEncryption
        //{
        //    private static RSACryptoServiceProvider csp = new RSACryptoServiceProvider(2048);
        //    private RSAParameters _privateKey;
        //    private RSAParameters _publicKey;

        //    public RsaEncryption()
        //    {
        //        _privateKey = csp.ExportParameters(true);
        //        _publicKey = csp.ExportParameters(false);

        //    }
        //    public string GetPublicKey()
        //    {
        //        var sw = new StringWriter();
        //        var xs = new XmlSerializer(typeof(RSAParameters));
        //        xs.Serialize(sw, _publicKey);
        //        return sw.ToString();
        //    }
        //    public string Encrypt(string plainText)
        //    {
        //        csp = new RSACryptoServiceProvider();
        //        csp.ImportParameters(_publicKey);
        //        var data = Encoding.Unicode.GetBytes(plainText);
        //        var cypher = csp.Encrypt(data, false);
        //        return Convert.ToBase64String(cypher);
        //    }
        //    public string Decrypt(string cypherText)
        //    {
        //        var dataBytes = Convert.FromBase64String(cypherText);
        //        csp.ImportParameters(_privateKey);
        //        var plainText = csp.Decrypt(dataBytes, false);
        //        return Encoding.Unicode.GetString(plainText);
        //    }
        //}

        //static string encrypt(string plainText)
        //{
        //    byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

        //    PemReader pr = new PemReader(
        //        (StreamReader)File.OpenText("./pem_public.pem")
        //    );
        //    RsaKeyParameters keys = (RsaKeyParameters)pr.ReadObject();

        //    // Pure mathematical RSA implementation
        //    // RsaEngine eng = new RsaEngine();

        //    // PKCS1 v1.5 paddings
        //    // Pkcs1Encoding eng = new Pkcs1Encoding(new RsaEngine());

        //    // PKCS1 OAEP paddings
        //    OaepEncoding eng = new OaepEncoding(new RsaEngine());
        //    eng.Init(true, keys);

        //    int length = plainTextBytes.Length;
        //    int blockSize = eng.GetInputBlockSize();
        //    List<byte> cipherTextBytes = new List<byte>();
        //    for (int chunkPosition = 0;
        //        chunkPosition < length;
        //        chunkPosition += blockSize)
        //    {
        //        int chunkSize = Math.Min(blockSize, length - chunkPosition);
        //        cipherTextBytes.AddRange(eng.ProcessBlock(
        //            plainTextBytes, chunkPosition, chunkSize
        //        ));
        //    }
        //    return Convert.ToBase64String(cipherTextBytes.ToArray());
        //}


        // POST: Save KTP/ID Card
        [HttpPost]
        public ActionResult NoAuthCheckIDCardSave(IdentityCard data)
        {
            User user = Lookup.Get<User>();
            string message = "";
            string returnCode = "";
            try
            {
                Parameter param = Lookup.Get<Parameter>();
                if (data.CroppedBlob.Length % 4 != 0)
                {
                    message = "Invalid uploaded image format!";
                }

                if (data.CroppedBlob.Length > param.GetSizeKTP)
                {
                    message = "ID Card image " + data.FileName + " exceed maximum file size (" + (data.CroppedBlob.Length * 1365) + " KB) and cannot be uploaded!";
                    //message = SvsResources.Validation_MaxSizeExceeded;
                }

                if (message.IsNullOrEmpty())
                {
                    SignatureHRequest reqH = _signatureHReqRepository.getSignatureHReq(data.AccountNo);
                    if (reqH == null)
                    {
                        message = SvsResources.Validation_AccountNotFound;
                    }
                    else
                    {
                        SignatureDKTPReq idcard = new SignatureDKTPReq(0);
                        idcard.AccountNo = data.AccountNo;
                        idcard.KTP = data.CroppedBlob;
                        idcard.ImageType = data.ImageType;
                        idcard.RequestType = "C";
                        idcard.FileSeq = data.Index;
                        idcard.RequestDate = DateTime.Now;
                        idcard.RequestUser = user.Username;
                        idcard.RequestReason = "Create New";
                        message = _signatureHReqService.SaveIDCard(idcard);
                    }

                }
            }
            catch (Exception e)
            {
                message = e.Message;
                returnCode = "ERR";
            }

            if (message.IsNullOrEmpty())
            {
                message = "";
                returnCode = "OK";
            }
            else
            {
                ScreenMessages.Submit(ScreenMessage.Error(message));
                returnCode = "ERR";
            }

            JsonResult result = new JsonResult();
            result = this.Json(new
            {
                message = message,
                returnCode = returnCode
            });
            return result;
        }

        // POST: Save KTP/ID Card
        [HttpPost]
        public ActionResult NoAuthCheckSignSave(Signature data)
        {
            User user = Lookup.Get<User>();
            string message = "";
            string returnCode = "";
            try
            {
                Parameter param = Lookup.Get<Parameter>();
                if (data.CroppedBlob.Length % 4 != 0)
                {
                    message = "Invalid uploaded image format!";
                }
                if (data.CroppedBlob.Length > param.GetSizeSignature)
                {
                    message = "Signature image " + data.FileName + " exceed maximum file size (" + (data.CroppedBlob.Length * 1365) + " KB) and cannot be uploaded!";
                    //message = SvsResources.Validation_MaxSizeExceeded;
                }

                if (message.IsNullOrEmpty())
                {
                    SignatureHRequest reqH = _signatureHReqRepository.getSignatureHReq(data.AccountNo);
                    if (reqH == null)
                    {
                        message = SvsResources.Validation_AccountNotFound;
                    }
                    else
                    {
                        SignatureDSignReq sign = new SignatureDSignReq(0);
                        sign.AccountNo = data.AccountNo;
                        sign.Signature = data.CroppedBlob;
                        sign.ImageType = data.ImageType;
                        sign.RequestType = "C";
                        sign.FileSeq = data.Index;
                        sign.RequestDate = DateTime.Now;
                        sign.RequestUser = user.Username;
                        sign.RequestReason = "Create New";
                        message = _signatureHReqService.SaveSign(sign);
                    }

                }
            }
            catch (Exception e)
            {
                message = e.Message;
                returnCode = "ERR";
            }

            if (message.IsNullOrEmpty())
            {
                message = "";
                returnCode = "OK";
            }
            else
            {
                ScreenMessages.Submit(ScreenMessage.Error(message));
                returnCode = "ERR";
            }

            JsonResult result = new JsonResult();
            result = this.Json(new
            {
                message = message,
                returnCode = returnCode
            });
            return result;
        }


        private string validateData(SignatureHReqView data, string mode)
        {
            AccountMaster acm = _accountMasterRepository.getAccount(data.AccountNo);
            User user = Lookup.Get<User>();

            if (data.AccountNo == "")
                return SvsResources.Validation_FillAccountNo;

            if (data.AccountName == "")
                return SvsResources.Validation_FillCustName;

            if (user.BranchCode != "000")
            {
                List<Branch> uBranches = getBranches();
                List<BranchAccountPrefix> uPrefix = getPrefixes(uBranches);

                Branch selectedBranch = uBranches.Find(b => b.BranchCode.Trim() == data.BranchCode.Trim());
                BranchAccountPrefix logicalBranches = uPrefix.Find(b => b.PrefixCode.Trim() == data.AccountNo.Trim().Substring(0, 3));

                if (selectedBranch.IsNull())
                {
                    return SvsResources.Validation_BranchCodeNotMatch;
                }

                if (acm.IsNull())
                {
                    if (logicalBranches.IsNull())
                    {
                        return SvsResources.Validation_AccountPrefixNotMatch;
                    }
                }
            }

            if (!acm.IsNull())
            {
                if (acm.AccountType.Trim() != data.AccountType.Trim())
                {
                    return SvsResources.Validation_AccountTypeNotMatch;
                }
            }
            //if(CIFExist == null)
            //    return "CIF is not exists in Account Master!";

            if (mode == "Create")
            {
                if (_accNoVwRepo.IsDuplicate(data.AccountNo))
                    return SvsResources.Validation_DuplicateData;
            }

            return "";
        }

        private string validateAccount(SignatureHReqView data, AccountMaster accMaster)
        {
            //if (accMaster != null)
            //{
            //    string msg = "";

            //    if (accMaster.AccountNo == "")
            //    {
            //        msg = SvsResources.Validation_AccountMasterNotFound;
            //    }

            //    if (accMaster.CIFNo == "")
            //    {
            //        msg =  "CIF is not exists in Account Master!";
            //    }

            //    return msg;
            //}

            if (accMaster == null)
            {
                return SvsResources.Validation_AccountMasterNotFound;
            }


            string message = "";

            if (accMaster.AccountType.Trim() != data.AccountType)
                message = SvsResources.Validation_AccountTypeNotMatch;

            if (accMaster.BranchCode != data.BranchCode)
                message = message == "" ? SvsResources.Validation_BranchCodeNotMatch : ", " + SvsResources.Validation_BranchCodeNotMatch;

            if (accMaster.CIFNo != data.CIFNumber)
                message = message == "" ? SvsResources.Validation_CIFNumberNotMatch : ", " + SvsResources.Validation_CIFNumberNotMatch;

            return message;
        }
        #region Dropdownlist

        private IList<SelectListItem> createAccountTypeSelect(string selected)
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = "CS", Text = "CA/SA" });
            dataList.Insert(1, new SelectListItem() { Value = "TD", Text = "TD" });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString().Trim()).Selected = true;
            }
            return dataList;
        }

        private IList<SelectListItem> createBranchSelect(string selected)
        {
            IList<SelectListItem> dataList = getBranches().ConvertAll<SelectListItem>(new Converter<Branch, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Value);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (!selected.IsNullOrEmpty())
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }

        public List<Branch> getBranches()
        {
            User user = Lookup.Get<User>();
            IList<string> branchs = new List<string>();
            if (user.BranchCode == "000")
            {
                return _branchRepository.getBranchs().ToList();
            }
            else
            {
                branchs.Add(user.BranchCode);
                return _branchRepository.getBranchs(branchs).ToList();
            }
        }

        public List<BranchAccountPrefix> getPrefixes(List<Branch> branches)
        {
            User user = Lookup.Get<User>();
            if (user.BranchCode == "000")
            {
                return _branchAccountPrefixRepository.getAccountPrefixes().ToList();
            }
            else
            {
                List<string> lstBranches = new List<string>();
                foreach (Branch branch in branches)
                {
                    lstBranches.Add(branch.BranchCode);
                }

                return _branchAccountPrefixRepository.getAccountPrefixes(lstBranches).ToList();
            }
        }


        public SelectListItem ConvertFrom(Branch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.BranchCode;
            returnItem.Value = item.BranchCode;
            return returnItem;
        }

        private IList<SelectListItem> createDocTypeSelect(string selected)
        {
            List<RoleDocType> authDocs = Lookup.Get<List<RoleDocType>>();
            List<DocumentType> docList = _docTypeRepository.FindAll().ToList();
            List<DocumentType> finalDocList = new List<DocumentType>();
            foreach (DocumentType item in docList)
            {
                var found = authDocs.FindElement(x => x.DocumentTypeId == item.Id);
                if (!found.IsNull())
                {
                    finalDocList.Add(item);
                }
            }

            IList<SelectListItem> dataList = finalDocList.ConvertAll<SelectListItem>(new Converter<DocumentType, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (!selected.IsNullOrEmpty())
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }

        private IList<SelectListItem> createAllDocTypeSelect()
        {
            List<DocumentType> docList = _docTypeRepository.FindAll().ToList();

            IList<SelectListItem> dataList = docList.ConvertAll<SelectListItem>(new Converter<DocumentType, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
            IList<SelectListItem> sortedList = sortedEnum.ToList();
            return sortedList;
        }

        public SelectListItem ConvertFrom(DocumentType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Description;
            returnItem.Value = item.DocType;
            return returnItem;
        }
        #endregion

        public ActionResult Edit(string id)
        {
            Parameter param = Lookup.Get<Parameter>();

            ViewData["Parameter"] = param;
            ViewData["ActionName"] = "Edit";
            SignatureHRequest reqH = _signatureHReqRepository.getSignatureHReq(id);
            if (reqH.IsNull())
            {
                ScreenMessages.Submit(ScreenMessage.Error("Account number does not exist!"));
                return RedirectToAction("", "CreateSVS");
            }
            reqH.IdentityCards = _signatureHReqRepository.getKTPReqD(reqH.AccountNo);
            reqH.Documents = _signatureHReqRepository.getDocReqD(reqH.AccountNo);
            reqH.Signatures = _signatureHReqRepository.getSignReqD(reqH.AccountNo);
            SignatureHReqView data = ConvertFrom(reqH);

            return CreateView(data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Edit(SignatureHReqView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            string returnCode = "";
            string warningmessage = "";
            string AccountNo = "";
            try
            {
                AccountMaster accMaster = _accountMasterRepository.getAccount(data.AccountNo);

                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    SignatureHRequest newData = _signatureHReqRepository.getSignatureHReq(data.AccountNo);
                    if (newData.IsNull())
                    {
                        message = "NF";
                        returnCode = "NF";
                        ScreenMessages.Submit(ScreenMessage.Error("Data Has Been Approved!"));
                    }
                    else
                    {
                        AccountNo = data.AccountNo;
                        newData.AccountNo = data.AccountNo;
                        newData.AccountName = data.AccountName;
                        newData.AccountType = data.AccountType;
                        newData.CIFNumber = data.CIFNumber;
                        newData.BranchCode = data.BranchCode;
                        newData.Note = data.Note;
                        newData.RequestType = "C";
                        newData.RequestDate = DateTime.Now;
                        newData.RequestUser = user.Username;
                        newData.RequestReason = "Create New";

                        IList<SignatureDDocReq> docs = new List<SignatureDDocReq>();
                        if (data.DeletedDocuments != null)
                        {
                            foreach (DeleteFormat item in data.DeletedDocuments)
                            {
                                SignatureDDocReq doc = new SignatureDDocReq(item.ID);
                                docs.Add(doc);
                            }
                        }
                        newData.Documents = docs;

                        IList<SignatureDKTPReq> idcards = new List<SignatureDKTPReq>();
                        if (data.DeletedIdentityCards != null)
                        {
                            foreach (DeleteFormat item in data.DeletedIdentityCards)
                            {
                                SignatureDKTPReq idcard = new SignatureDKTPReq(item.ID);
                                idcards.Add(idcard);
                            }
                        }
                        newData.IdentityCards = idcards;

                        IList<SignatureDSignReq> signs = new List<SignatureDSignReq>();
                        if (data.DeletedSignatures != null)
                        {
                            foreach (DeleteFormat item in data.DeletedSignatures)
                            {
                                SignatureDSignReq sign = new SignatureDSignReq(item.ID);
                                signs.Add(sign);
                            }
                        }
                        newData.Signatures = signs;

                        warningmessage = validateAccount(data, accMaster);
                        message = _signatureHReqService.UpdateSignatureReq(newData);

                        // Hapus detail data, biar audit trailnya ngak penuh
                        newData.Documents = null;
                        newData.Signatures = null;
                        newData.IdentityCards = null;

                    }

                }
            }
            catch (Exception e)
            {
                message = e.Message;
                returnCode = "ERR";
                //ScreenMessages.Submit(ScreenMessage.Error(message));
            }

            if (message.IsNullOrEmpty())
            {
                returnCode = "OK";
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
            }
            else
            {
                if (returnCode != "NF")
                {
                    returnCode = "ERR";
                }
                //ScreenMessages.Submit(ScreenMessage.Error(message));
            }

            JsonResult result = new JsonResult();
            result = this.Json(new
            {
                message = message,
                returnCode = returnCode,
                warningMessage = warningmessage,
                AccountNo = AccountNo
            });

            return result;
        }

        [HttpGet]
        public ActionResult NoAuthCheckViewDocument(int ID)
        {
            JsonResult result = new JsonResult();
            SignatureDDocReq doc = _signatureHReqRepository.getDocReqD(ID);
            if (doc.IsNull())
            {
                return Content("Not exist or invalid document");
            }
            byte[] blob = Convert.FromBase64String(doc.FileBlob);

            string filetype = doc.FileType.Split(';')[0].Split(':')[1];
            if (filetype.Trim() == "application/pdf")
            {
                return File(blob, filetype);
            }
            else
            {
                Stream strm = new MemoryStream(Convert.FromBase64String(doc.FileBlob));
                Bitmap bmp = (Bitmap)Bitmap.FromStream(strm);

                ImageFormatConverter imgConverter = new ImageFormatConverter();
                var png = imgConverter.ConvertFromString("PNG");

                MemoryStream ms = new MemoryStream();
                bmp.Save(ms, (ImageFormat)png);

                byte[] bytImg = ms.ToArray();
                string strImg = Convert.ToBase64String(bytImg);
                return Content("<img src='data:image/PNG;base64," + strImg + "' alt='" + doc.DocumentType + "'/>");
            }

        }

    }
}