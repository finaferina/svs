﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;
using System.Globalization;
using System.Text.RegularExpressions;
using Microsoft.Reporting.WebForms;
using System.IO;
using SpreadsheetLight;
using System.Data;

namespace UOB.SVS.Controllers
{
    public class UserInquiryController : PageController
    {
        private IList<UserRole> userRoles;
        private IUserRepository _userRepository;
        private IApplicationRepository _appRepository;
        private IAuditTrailLog _auditRepository;
        public UserInquiryController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository,
            IApplicationRepository appRepository, IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._userRepository = userRepository;
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;

            Settings.ModuleName = "UserInquiry";
            Settings.Title = UserResources.PageTitle;
        }
        protected override void Startup()
        {

        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<User> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _userRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    if (searchColumn.ToLower().Contains("date"))
                    {
                        DateTime searchDate = DateTime.ParseExact(searchValue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        data = _userRepository.FindAllPagedDate(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchDate);
                    }
                    else
                    {
                        data = _userRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                    }

                }

                userRoles = _userRepository.getDistinctUserRoles(data.Items.ToList());
                var items = data.Items.ToList().ConvertAll<UserView>(new Converter<User, UserView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public UserView ConvertFrom(User item)
        {
            UserView returnItem = new UserView();
            returnItem.Id = item.Id;
            returnItem.Username = item.Username;
            //returnItem.Password = Encryption.Instance.DecryptText(item.Password);
            returnItem.PasswordExpirationDate = item.PasswordExpirationDate.ToString("dd/MM/yyyy");
            returnItem.AccountValidityDate = item.AccountValidityDate.ToString("dd/MM/yyyy");
            returnItem.FullName = item.FullName;
            //returnItem.LastName = item.LastName;
            returnItem.RegNo = item.RegNo;
            returnItem.SessionTimeout = SystemSettings.Instance.Security.SessionTimeout; //item.SessionTimeout;
            returnItem.LockTimeout = 10;//item.LockTimeout;
            returnItem.MaxConLogin = 1; //item.MaximumConcurrentLogin;
            returnItem.InActiveDirectory = item.InActiveDirectory;
            //returnItem.Name = item.Name;
            returnItem.IsActive = item.IsActive;
            returnItem.IsPrinted = item.IsPrinted;
            returnItem.Approved = item.Approved;
            returnItem.CreatedDate = item.CreatedDate.Value.ToString("dd/MM/yyyy");
            returnItem.LastLogin = item.LastLogin.HasValue ? item.LastLogin.Value.ToString("dd/MM/yyyy") : "";
            IEnumerable<UserRole> roles = from ur in userRoles where ur.Username == item.Username select ur;
            returnItem.Roles = ConvertRoles(roles.ToList());

            return returnItem;
        }

        private string ConvertRoles(IList<UserRole> roles)
        {
            string rtnRoles = "";
            foreach (UserRole row in roles)
            {
                if (rtnRoles == "")
                    rtnRoles = row.Role.Trim();
                else
                    rtnRoles = String.Format("{0}, {1}", rtnRoles, row.Role.Trim());
            }
            return rtnRoles;
        }

        public ActionResult Export()
        {
            var userList = _userRepository.FindAll();
            userRoles = _userRepository.getDistinctUserRoles(userList.ToList());
            IList<UserView> items = userList.ToList().ConvertAll<UserView>(new Converter<User, UserView>(ConvertFrom));

            DataTable dt = items.ListToDataTable();
            List<UserView> list = new List<UserView>();

            MemoryStream ms = new MemoryStream();
            using (SLDocument sl = new SLDocument())
            {
                sl.ImportDataTable(1, 1, dt, true);
                sl.SelectWorksheet("Worksheet");
                sl.SaveAs(ms);
            }

            ms.Position = 0;
            string fName = "Dukcapil_User.xls";
            return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fName);
        }
    }
}