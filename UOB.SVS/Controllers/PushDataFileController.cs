﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using Treemas.Credential.Repository;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Configuration;
using UOB.SVS.Resources;
using SpreadsheetLight;
using System.Data;
using System.IO;

namespace UOB.SVS.Controllers
{
    public class PushDataFileController : PageController
    {
        private IList<UserRole> userRoles;
        private IUserRepository _userRepository;
        private IApplicationRepository _appRepository;
        private IAuditTrailLog _auditRepository;
        public PushDataFileController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository,
            IApplicationRepository appRepository, IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._userRepository = userRepository;
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;

            Settings.ModuleName = "PushDataFile";
            Settings.Title = "Push Data Master Account";
        }
        protected override void Startup()
        {
            IConfigurationBinder binder = SystemConfigurationCabinet.Instance.GetBinder("Database");
            CompositeConfigurationItem items = (CompositeConfigurationItem)binder.GetConfiguration("System");
            string strFileLocation = items.GetItem("FileLocation").Value;

            PushFileDataView fileData = new PushFileDataView();
            fileData.FILES = new List<PushFileDataObject>();
            fileData.FOLDER = strFileLocation;
            
            DirectoryInfo di = new DirectoryInfo(strFileLocation);

            foreach (FileInfo fi in di.GetFiles())
            {
                if (System.IO.File.Exists(fi.FullName))
                {
                    PushFileDataObject file = new PushFileDataObject();
                    file.FILE_NAME = fi.Name;
                    file.CREATE_DATE = fi.CreationTime.ToString("dd-MM-yyyy hh:mm:ss");
                    file.FILE_SIZE = fi.Length.ToString();
                    file.FILE_PATH = fi.FullName;

                    fileData.FILES.Add(file);
                }
            }

            this.Model = fileData;
            ViewData["ActionName"] = "List of files";
        }

        
        [HttpPost]
        public ActionResult Delete(string filename)
        {
            WriteToFile("start log " + DateTime.Now);
            string message = "";
            try
            {
                IConfigurationBinder binder = SystemConfigurationCabinet.Instance.GetBinder("Database");
                CompositeConfigurationItem items = (CompositeConfigurationItem)binder.GetConfiguration("System");

                string strFileLocation = items.GetItem("FileLocation").Value;

                string fullpath = strFileLocation+"\\" + filename;
                WriteToFile("path " + fullpath + " " + DateTime.Now);
                if (System.IO.File.Exists(fullpath))
                {
                    WriteToFile("berhasil akses path " + DateTime.Now);
                    System.IO.File.Delete(fullpath);
                    WriteToFile("file berhasil terdelete " + DateTime.Now);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
                WriteToFile("Error: " + message + " " + DateTime.Now);
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Delete_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            return RedirectToAction("Index");
        }
        public static void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\PushDataFile" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!System.IO.File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = System.IO.File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = System.IO.File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }

    }
}