﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;

namespace UOB.SVS.Controllers
{
    public class FeatureController : PageController
    {
        private IFeatureRepository _featRepository;
        private IFeatureCrRepository _featCrRepository;
        private IApplicationRepository _appRepository;
        private IAppAuditTrailLog _auditRepository;
        public FeatureController(ISessionAuthentication sessionAuthentication, IFeatureRepository featRepository,
            IApplicationRepository appRepository, IAppAuditTrailLog auditRepository, IFeatureCrRepository featCrRepository) : base(sessionAuthentication)
        {
            this._featRepository = featRepository;
            this._featCrRepository = featCrRepository;
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;
            Settings.ModuleName = "Feature";
            Settings.Title = FeatureResources.PageTitle;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<AuthorizationFeature> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _featRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _featRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<FeatureView>(new Converter<AuthorizationFeature, FeatureView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public FeatureView ConvertFrom(AuthorizationFeature item)
        {
            FeatureView returnItem = new FeatureView();
            returnItem.Application = item.Application;
            returnItem.Description = item.Description;
            returnItem.FeatureId = item.FeatureId;
            returnItem.Id = item.Id;
            returnItem.Name = item.Name;
            return returnItem;
        }

        public SelectListItem ConvertFrom(Application item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Name;
            returnItem.Value = item.ApplicationId;
            return returnItem;
        }

        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            FeatureView data = new FeatureView();
            return CreateView(data);
        }

        private ViewResult CreateView(FeatureView data)
        {
            ViewData["ApplicationMaster"] = createApplicationSelect(data.Application);
            return View("Detail", data);
        }

        private IList<SelectListItem> createApplicationSelect(string selected)
        {
            IList<SelectListItem> dataList = _appRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Application, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        // POST: Feature/Create
        [HttpPost]
        public ActionResult Create(FeatureView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    //AuthorizationFeature newData = new AuthorizationFeature(0L);
                    AuthorizationFeatureCr newData = new AuthorizationFeatureCr(0L);
                    newData.Application = data.Application;
                    newData.FeatureId = data.FeatureId;
                    newData.Name = data.Name;
                    newData.Description = data.Description;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;

                    newData.ApprovalType = "C";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;

                    //_featRepository.Add(newData);
                    _featCrRepository.Add(newData);
                    //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "CreateRequest");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(FeatureView data, string mode)
        {
            if (data.Application == "")
                return FeatureResources.Validation_SelectApplication;

            if (data.Name == "")
                return FeatureResources.Validation_FillFeatureName;

            if (data.FeatureId == "")
                return FeatureResources.Validation_FillFeatureId;

            if (mode == "Create")
            {
                if (_featRepository.IsDuplicate(data.Application, data.FeatureId))
                    return FeatureResources.Validation_DuplicateData;
            }
            return "";
        }

        // GET: Feature/Edit/5
        public ActionResult Edit(long id)
        {
            ViewData["ActionName"] = "Edit";
            AuthorizationFeature autoFeat = _featRepository.getFeature(id);
            FeatureView data = ConvertFrom(autoFeat);

            return CreateView(data);
        }

        // POST: Feature/Edit/5
        [HttpPost]
        public ActionResult Edit(FeatureView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    //AuthorizationFeature newData = new AuthorizationFeature(data.Id);
                    //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");
                    AuthorizationFeatureCr newData = new AuthorizationFeatureCr(0L);

                    newData.Application = data.Application;
                    newData.FeatureId = data.FeatureId;
                    newData.Name = data.Name;
                    newData.Description = data.Description;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;

                    newData.ApprovalType = "U";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;

                    //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "UpdateRequest");
                    _featCrRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }


        // POST: Feature/Delete/5
        [HttpPost]
        public ActionResult Delete(long Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                //AuthorizationFeature newData = new AuthorizationFeature(Id);
                AuthorizationFeatureCr newData = new AuthorizationFeatureCr(Id);
                AuthorizationFeature delData = _featRepository.getFeature(Id);
                newData.Application = delData.Application;
                newData.FeatureId = delData.FeatureId;
                newData.Name = delData.Name;
                newData.Description = delData.Description;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;

                newData.ApprovalType = "D";
                newData.ApprovalStatus = "N";
                newData.Approved = false;

                _featCrRepository.Add(newData);
                //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "DeleteRequest");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
    }
}