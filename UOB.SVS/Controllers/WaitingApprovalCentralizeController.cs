﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using UOB.SVS.Resources;
using UOB.SVS.Model;
using UOB.SVS.Model.Helper;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System.Linq;
using Treemas.Credential.Model;
using System.Drawing.Imaging;
using System.Drawing;
using System.Web;

namespace UOB.SVS.Controllers
{
    public class WaitingApprovalCentralizeController : PageController
    {
        private ICentralizeHRepository _hRepository;
        private ICentralizeUnitHReqRepository _hReqRepository;
        private ICentralizeHReqService _centralizeHReqService;
        private IAuditTrailLog _auditRepository;
        //private IBranchRepository _branchRepository;
        private ICentralUnitDocTypeRepository _doctypeRepository;
        private IRegionRepository _regionRepository;
        private IDivisionRepository _divisionRepository;
        private IDepartementRepository _departementRepository;
        private ICentralizeHHistoryRepository _hReqHistoryRepository;
        //private IBranchAccountPrefixRepository _branchAccountPrefixRepository;
        private IParameterCentralUnitRepository _parameterRepository;

        public WaitingApprovalCentralizeController(IParameterCentralUnitRepository parameterRepository, ISessionAuthentication sessionAuthentication, ICentralizeUnitHReqRepository hReqRepository, 
            ICentralizeHReqService centralizeHReqService, IAuditTrailLog auditRepository, ICentralUnitDocTypeRepository doctypeRepository,
            ICentralizeHRepository hRepository,
            IRegionRepository regionRepository, IDivisionRepository divisionRepository, IDepartementRepository departementRepository,
             ICentralizeHHistoryRepository hReqHistoryRepository
            //IBranchRepository branchRepository, 
            //IAccountNoViewRepository accNoVwRepo, IAccountMasterRepository accountMasterRepository,
            //IBranchAccountPrefixRepository branchAccountPrefixRepository
            ) : base(sessionAuthentication)
        {
            this._hRepository = hRepository;
            this._parameterRepository = parameterRepository;
            this._hReqRepository = hReqRepository;
            this._centralizeHReqService = centralizeHReqService;
            this._auditRepository = auditRepository;
            this._doctypeRepository = doctypeRepository;
            this._regionRepository = regionRepository;
            this._divisionRepository = divisionRepository;
            this._departementRepository = departementRepository;
            this._hReqHistoryRepository = hReqHistoryRepository;

            Settings.ModuleName = "WaitingApprovalCentralize";
            Settings.Title = "Waiting Approval";
        }

        protected override void Startup()
        {

        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                User user = Lookup.Get<User>();

                // Initialization.   
                CentralizeReqFilter filter = new CentralizeReqFilter();
                //filter.Branches = getBranches();
                filter.RequestUser = user.Username;
                filter.RequestType = Request.QueryString.GetValues("reqType")[0];
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<CentralizeUnitHRequest> data;
                if (!searchValue.IsNullOrEmpty())
                {
                    if (searchColumn == "NIK")
                        filter.NIK = searchValue;
                    else if (searchColumn == "Name")
                        filter.Name = searchValue;
                }

                data = _hReqRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);
                var items = data.Items.ToList().ConvertAll<CentralizeUnitHReqView>(new Converter<CentralizeUnitHRequest, CentralizeUnitHReqView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public CentralizeUnitHReqView ConvertFrom(CentralizeUnitHRequest item)
        {
            CentralizeUnitHReqView returnItem = new CentralizeUnitHReqView();
            returnItem.NIK = item.NIK;
            returnItem.Name = HttpUtility.HtmlEncode(item.Name);
            returnItem.Region = item.Region;
            returnItem.Division = item.Division;
            returnItem.Departement = item.Departement;
            returnItem.Titles = item.Titles;
            returnItem.Note = HttpUtility.HtmlEncode(item.Note);
            returnItem.RequestType = item.RequestType;
            returnItem.RequestDate = item.RequestDate;
            returnItem.RequestDateString = item.RequestDate.Value.ToString("dd/MM/yyyy");
            returnItem.RequestUser = item.RequestUser;
            returnItem.RequestReason = item.RequestReason;
            returnItem.IsRejected = item.IsRejected;

            int index = 1;
            if (item.Documents != null)
            {
                IList<CentralizeDocument> docs = new List<CentralizeDocument>();
                foreach (CentralizeDDocReq row in item.Documents)
                {
                    CentralizeDocument doc = new CentralizeDocument();
                    doc.ID = row.ID;
                    doc.Index = index++;
                    doc.DocumentType = row.DocumentType;
                    doc.FileBlob = row.FileBlob;
                    doc.FileType = HttpUtility.HtmlEncode(row.FileType);
                    doc.NIK = row.NIK;
                    doc.IsNew = false;
                    doc.FileSeq = row.FileSeq;
                    doc.IsValidDoc = true;

                    docs.Add(doc);
                }
                returnItem.Documents = docs.ToList();
            }

            if (item.Signatures != null)
            {
                index = 1;
                IList<CentralizeSignature> signs = new List<CentralizeSignature>();
                foreach (CentralizeDSignReq row in item.Signatures)
                {
                    CentralizeSignature sign = new CentralizeSignature();
                    sign.ID = row.Id;
                    sign.Index = index++;
                    sign.CroppedBlob = HttpUtility.HtmlEncode(row.Signature);
                    sign.ImageType = HttpUtility.HtmlEncode(row.ImageType);
                    sign.NIK = row.NIK;
                    sign.IsNew = false;
                    sign.IsValidImage = row.Signature.Length % 4 == 0 ? true : false;
                    signs.Add(sign);
                }
                returnItem.Signatures = signs.ToList();
            }
            return returnItem;
        }


        public ActionResult Edit(string id)
        {
            Parameter param = Lookup.Get<Parameter>();

            ViewData["Parameter"] = param;
            ViewData["ActionName"] = "Edit";
            CentralizeUnitHRequest reqH = _hReqRepository.getCentralizeUnitHReq(id);
            if (reqH.IsNull())
            {
                ScreenMessages.Submit(ScreenMessage.Error("NIK does not exist!"));
                return RedirectToAction("", "WaitingApprovalCentralize");
            }
            reqH.Documents = _hReqRepository.getDocReqD(reqH.NIK);
            reqH.Signatures = _hReqRepository.getSignReqD(reqH.NIK);
            CentralizeUnitHReqView data = ConvertFrom(reqH);

            return CreateView(data);
        }

        private ViewResult CreateView(CentralizeUnitHReqView data)
        {
            //List<RoleDocType> authDocs = Lookup.Get<List<RoleDocType>>();
            //ViewData["DocCount"] = authDocs.Count;

            ViewData["DocTypeMaster"] = createDocTypeSelect("");
            ViewData["RegionMaster"] = createRegionSelect("");
            ViewData["DivisionMaster"] = createDivisionSelect(data.Region.IsNullOrEmpty() ? "-" : data.Region, data.Division.IsNullOrEmpty() ? "-" : data.Division);
            ViewData["DepartementMaster"] = createDepartementSelect(data.Region.IsNullOrEmpty() ? "-" : data.Region, data.Division.IsNullOrEmpty() ? "-" : data.Division, data.Departement.IsNullOrEmpty() ? "-" : data.Departement);
            ViewData["AllDocTypeMaster"] = createAllDocTypeSelect();
            if (data.RequestType == "D")
            {
                return View("View", data);
            }
            else
            {
                return View("Detail", data);
            }
        }

        // POST: Role/Edit
        [HttpPost]
        public ActionResult Edit(CentralizeUnitHReqView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            string returnCode = "";
            string warningmessage = "";
            string NIK = "";
            try
            {
                //AccountMaster accMaster = _accountMasterRepository.getAccount(data.NIK);

                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {

                    CentralizeUnitHRequest reqH = _hReqRepository.getCentralizeUnitHReq(data.NIK);
                    if (reqH.IsNull())
                    {
                        //message = "NF";
                        //returnCode = "NF";
                        //ScreenMessages.Submit(ScreenMessage.Error("Data Has Been Approved or Rejected!"));

                        ScreenMessages.Submit(ScreenMessage.Error("Account number does not exist!"));
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        NIK = data.NIK;
                        reqH.Name = data.Name;
                        reqH.Region = data.Region;
                        reqH.Division = data.Division;
                        reqH.Departement = data.Departement;
                        reqH.Titles = data.Titles;
                        reqH.Note = data.Note;
                        reqH.RequestDate = DateTime.Now;

                        IList<CentralizeDDocReq> docs = new List<CentralizeDDocReq>();
                        if (data.Documents != null)
                        {
                            foreach (CentralizeDocument item in data.Documents)
                            {
                                CentralizeDDocReq doc = new CentralizeDDocReq(item.ID);
                                docs.Add(doc);
                            }
                        }
                        reqH.Documents = docs;

                        IList<CentralizeDSignReq> signs = new List<CentralizeDSignReq>();
                        if (data.Signatures != null)
                        {
                            foreach (CentralizeSignature item in data.Signatures)
                            {
                                CentralizeDSignReq sign = new CentralizeDSignReq(item.ID);
                                signs.Add(sign);
                            }
                        }
                        reqH.Signatures = signs;

                        //IList<CentralizeDDocReq> docs = new List<CentralizeDDocReq>();
                        //if (data.DeletedDocuments != null)
                        //{
                        //    foreach (CentralizeDeleteFormat item in data.DeletedDocuments)
                        //    {
                        //        CentralizeDDocReq doc = new CentralizeDDocReq(item.ID);
                        //        docs.Add(doc);
                        //    }
                        //}
                        //reqH.Documents = docs;

                        //IList<CentralizeDSignReq> signs = new List<CentralizeDSignReq>();
                        //if (data.DeletedSignatures != null)
                        //{
                        //    foreach (CentralizeDeleteFormat item in data.DeletedSignatures)
                        //    {
                        //        CentralizeDSignReq sign = new CentralizeDSignReq(item.ID);
                        //        signs.Add(sign);
                        //    }
                        //}
                        //reqH.Signatures = signs;

                        //warningmessage = validateAccount(data, accMaster);
                        if (warningmessage == "" || warningmessage == SvsResources.Validation_AccountMasterNotFound)
                        {
                            message = _centralizeHReqService.UpdateCentralizeReq(reqH);


                            if (message == "")
                            {
                                if (data.Signatures != null)
                                {
                                    foreach (var item in data.Signatures)
                                    {
                                        item.NIK = data.NIK;
                                        message = SignatureSave(item);
                                        if (!message.IsNullOrEmpty())
                                            throw new Exception(message);
                                    }
                                }
                                /*Save Doc*/

                                if (data.Documents != null)
                                {
                                    foreach (var item in data.Documents)
                                    {
                                        item.NIK = data.NIK;
                                        message = DocSave(item);
                                        if (!message.IsNullOrEmpty())
                                            throw new Exception(message);
                                    }
                                }
                            }
                        }
                        else
                        {
                            message = warningmessage;
                        }

                        // Hapus detail data, biar audit trailnya ngak penuh
                        reqH.Documents = null;
                        reqH.Signatures = null;
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
                returnCode = "ERR";
                //ScreenMessages.Submit(ScreenMessage.Error(message));
            }

            if (message.IsNullOrEmpty())
            {
                returnCode = "OK";
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
            }
            else
            {
                //if (returnCode != "NF")
                //{
                //    returnCode = "ERR";
                //}
                //ScreenMessages.Submit(ScreenMessage.Error(message));
                returnCode = "ERR";
            }

            JsonResult result = new JsonResult();
            result = this.Json(new
            {
                message = message,
                returnCode = returnCode,
                warningMessage = warningmessage,
                NIK = NIK
            });

            return result;
        }

        public string DocSave(CentralizeDocument data)
        {
            User user = Lookup.Get<User>();
            string message = "";
            string NewFileBlob = "";
            try
            {
                //ParameterCentralUnit param = Lookup.Get<ParameterCentralUnit>();
                ParameterCentralUnit param = _parameterRepository.getParameter();
                if (data.FileBlob.Length % 4 != 0)
                {
                    message = "Invalid uploaded document format!";
                }
                if (data.FileBlob.Length > param.GetSizeDocument)
                {
                    message = "Document " + data.FileName + " exceed maximum file size (" + (data.FileBlob.Length * 1365) + " KB) and cannot be uploaded!";
                }

                if (data.FileBlob == "null")
                {
                    CentralizeDDoc dDoc = _hRepository.getDocDet(data.ID);
                    NewFileBlob = dDoc.FileBlob;
                }

                if (message.IsNullOrEmpty())
                {
                    //CentralizeUnitHRequest reqH = _unithreqRepository.getCentralizeUnitHReq(data.NIK);

                    //if (reqH == null)
                    //{
                    //    message = SvsResources.Validation_AccountNotFound;
                    //}
                    //else if (NewFileBlob == "null")
                    if (NewFileBlob == "null")
                    {
                        message = "Invalid Document " + data.DocumentType;
                    }
                    else
                    {
                        CentralizeDDocReq doc = new CentralizeDDocReq(0);
                        doc.NIK = data.NIK;
                        doc.DocumentType = data.DocumentType;
                        doc.FileBlob = data.FileBlob == "null" ? NewFileBlob : data.FileBlob;
                        doc.FileType = data.FileType;
                        doc.FileSeq = data.Index;
                        doc.RequestType = "C";
                        doc.RequestDate = DateTime.Now;
                        doc.RequestUser = user.Username;
                        doc.RequestReason = "Create New";
                        message = _centralizeHReqService.SaveDocument(doc);
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
                //returnCode = "ERR";
            }

            if (message.IsNullOrEmpty())
            {
                message = "";
                //returnCode = "OK";
            }
            else
            {
                ScreenMessages.Submit(ScreenMessage.Error(message));
                //returnCode = "ERR";
            }

            return message;
        }

        public string SignatureSave(CentralizeSignature data)
        {
            User user = Lookup.Get<User>();
            //_unithreqRepository.IsDuplicate(data.NIK);
            //_doctypeRepository
            string message = "";
            try
            {
                //ParameterCentralUnit param = Lookup.Get<ParameterCentralUnit>();
                ParameterCentralUnit param = _parameterRepository.getParameter();

                if (data.CroppedBlob.Length % 4 != 0)
                {
                    message = "Invalid uploaded image format!";
                    throw new Exception(message);
                }
                if (data.CroppedBlob.Length > param.GetSizeSignature)
                {
                    message = "Signature image " + data.FileName + " exceed maximum file size (" + (data.CroppedBlob.Length * 1365) + " KB) and cannot be uploaded!, (JPG/JPEG Recommended format)";
                    //message = SvsResources.Validation_MaxSizeExceeded;
                    throw new Exception(message);
                }

                if (message.IsNullOrEmpty())
                {
                    //CentralizeUnitHRequest reqH = _unithreqRepository.getCentralizeUnitHReq(data.NIK);
                    //if (reqH == null)
                    //{
                    //    message = SvsResources.Validation_AccountNotFound;
                    //    throw new Exception(message);
                    //}
                    //else
                    //{
                    CentralizeDSignReq sign = new CentralizeDSignReq(0);
                    sign.NIK = data.NIK;
                    sign.Signature = data.CroppedBlob;
                    sign.ImageType = data.ImageType;
                    sign.RequestType = "C";
                    sign.FileSeq = data.Index;
                    sign.RequestDate = DateTime.Now;
                    sign.RequestUser = user.Username;
                    sign.RequestReason = "Create New";
                    message = _centralizeHReqService.SaveSign(sign);
                    //}

                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }

        public JsonResult ChangeDropdownRegion(string Region)
        {
            CentralizeUnitHReqView item = new CentralizeUnitHReqView();
            item.Divisions = _divisionRepository.getDivisionbyRegion(Region);

            return Json(item, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult ChangeDropdownDepartements(string region, string division = "")
        {
            CentralizeUnitHReqView item = new CentralizeUnitHReqView();
            item.Departements = _departementRepository.getDepartementbyRegion(region, division);

            return Json(item, JsonRequestBehavior.AllowGet);
        }

        private string validateData(CentralizeUnitHReqView data, string mode)
        {
            //AccountMaster acm = _accountMasterRepository.getAccount(data.NIK);

            if (data.NIK == "")
                return SvsResources.Validation_FillAccountNo;

            if (data.Name == "")
                return SvsResources.Validation_FillCustName;

            User user = Lookup.Get<User>();

            //if (user.BranchCode != "000")
            //{
            //    List<Branch> uBranches = getBranches();
            //    List<BranchAccountPrefix> uPrefix = getPrefixes(uBranches);

            //    Branch selectedBranch = uBranches.Find(b => b.BranchCode.Trim() == data.BranchCode.Trim());
            //    BranchAccountPrefix logicalBranches = uPrefix.Find(b => b.PrefixCode.Trim() == data.NIK.Trim().Substring(0, 3));

            //    if (selectedBranch.IsNull())
            //    {
            //        return SvsResources.Validation_BranchCodeNotMatch;
            //    }

            //    if (acm.IsNull())
            //    {
            //        if (logicalBranches.IsNull())
            //        {
            //            return SvsResources.Validation_AccountPrefixNotMatch;
            //        }
            //    }
            //}

            //if (!acm.IsNull())
            //{
            //    if (acm.AccountType.Trim() != data.AccountType.Trim())
            //    {
            //        return SvsResources.Validation_AccountTypeNotMatch;
            //    }
            //}

            //ini kalo tipe requestnya Update bakal masuk terus karna dia baca yg Create, terus ada error sequence lebih dari 1
            //SignatureHHistory reqData = _hReqHistoryRepository.getSignatureHHistory(data.AccountNo);
            //if (!reqData.IsNull())
            //{
            //    if (!reqData.ApproveBy.IsNull())
            //    {
            //        return "Cannot update, data already approved!";
            //    }
            //    else if (!reqData.RejectBy.IsNull())
            //    {
            //        return "Cannot update, data already rejected!";
            //    }
            //}

            return "";
        }

        //private string validateAccount(CentralizeUnitHReqView data, AccountMaster accMaster)
        //{
        //    if (accMaster == null)
        //    {
        //        return SvsResources.Validation_AccountMasterNotFound;
        //    }

        //    string message = "";

        //    if (accMaster.AccountType.Trim() != data.AccountType)
        //        message = SvsResources.Validation_AccountTypeNotMatch;

        //    if (accMaster.BranchCode.Trim() != data.BranchCode)
        //        message = message == "" ? SvsResources.Validation_BranchCodeNotMatch : ", " + SvsResources.Validation_BranchCodeNotMatch;

        //    if (accMaster.CIFNo.Trim() != data.CIFNumber.Trim())
        //        message = message == "" ? SvsResources.Validation_CIFNumberNotMatch : ", " + SvsResources.Validation_CIFNumberNotMatch;

        //    return message;
        //}

        //Cancel submitted data
        [HttpPost]
        public ActionResult Reject(string Id)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            string returnCode = "";

            try
            {
                //message = validateData(FixedAssetMaster, "Edit");
                CentralizeUnitHRequest reqH = _hReqRepository.getCentralizeUnitHReq(Id);
                if (reqH.IsNull())
                {
                    message = "Data Has Been Approved or Rejected!";
                }

                if (message.IsNullOrEmpty())
                {
                    message = _centralizeHReqService.CancelCentralizeReq(reqH);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Cancel));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            //ini kalau ga ke index
            //CollectScreenMessages();
            //return CreateView(data); //ini akan problem gara2 si dropdown di disable, harus kasih hidden valuenya si dropdown di detail

            //ini kalau ke index
            //CollectScreenMessages(); ga perlu karena ke index otomatis collect, kalo ada ini malah ga keluar messagenya by Ivan
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult NoAuthCheckViewDocument(int ID)
        {
            JsonResult result = new JsonResult();
            CentralizeDDocReq doc = _hReqRepository.getDocReqD(ID);
            if (doc.IsNull())
            {
                return Content("Not exist or invalid document");
            }
            byte[] blob = Convert.FromBase64String(doc.FileBlob);

            string filetype = doc.FileType.Split(';')[0].Split(':')[1];
            if (filetype.Trim() == "application/pdf")
            {
                return File(blob, filetype);
            }
            else
            {
                Stream strm = new MemoryStream(Convert.FromBase64String(doc.FileBlob));
                Bitmap bmp = (Bitmap)Bitmap.FromStream(strm);

                ImageFormatConverter imgConverter = new ImageFormatConverter();
                var png = imgConverter.ConvertFromString("PNG");

                MemoryStream ms = new MemoryStream();
                bmp.Save(ms, (ImageFormat)png);

                byte[] bytImg = ms.ToArray();
                string strImg = Convert.ToBase64String(bytImg);
                return Content("<img src='data:image/PNG;base64," + strImg + "' alt='" + doc.DocumentType + "'/>");
            }
        }

        #region Dropdownlist
        //private IList<SelectListItem> createBranchSelect(string selected)
        //{
        //    IList<SelectListItem> dataList = getBranches().ConvertAll<SelectListItem>(new Converter<Branch, SelectListItem>(ConvertFrom));

        //    IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Value);
        //    IList<SelectListItem> sortedList = sortedEnum.ToList();

        //    if (!selected.IsNullOrEmpty())
        //    {
        //        sortedList.FindElement(item => item.Value == selected).Selected = true;
        //    }
        //    return sortedList;
        //}

        //public List<Branch> getBranches()
        //{
        //    User user = Lookup.Get<User>();
        //    IList<string> branchs = new List<string>();
        //    if (user.BranchCode == "000")
        //    {
        //        return _branchRepository.getBranchs().ToList();
        //    }
        //    else
        //    {
        //        branchs.Add(user.BranchCode);
        //        return _branchRepository.getBranchs(branchs).ToList();
        //    }
        //}
        //public List<BranchAccountPrefix> getPrefixes(List<Branch> branches)
        //{
        //    User user = Lookup.Get<User>();
        //    if (user.BranchCode == "000")
        //    {
        //        return _branchAccountPrefixRepository.getAccountPrefixes().ToList();
        //    }
        //    else
        //    {
        //        List<string> lstBranches = new List<string>();
        //        foreach (Branch branch in branches)
        //        {
        //            lstBranches.Add(branch.BranchCode);
        //        }

        //        return _branchAccountPrefixRepository.getAccountPrefixes(lstBranches).ToList();
        //    }
        //}
        //public SelectListItem ConvertFrom(Branch item)
        //{
        //    SelectListItem returnItem = new SelectListItem();
        //    returnItem.Text = item.BranchCode;
        //    returnItem.Value = item.BranchCode;
        //    return returnItem;
        //}

        //private IList<SelectListItem> createAccountTypeSelect(string selected)
        //{
        //    IList<SelectListItem> dataList = new List<SelectListItem>();
        //    dataList.Insert(0, new SelectListItem() { Value = "CS", Text = "CA/SA" });
        //    dataList.Insert(1, new SelectListItem() { Value = "TD", Text = "TD" });
        //    if (!selected.IsNullOrEmpty())
        //    {
        //        dataList.FindElement(item => item.Value == selected.ToString().Trim()).Selected = true;
        //    }
        //    return dataList;
        //}


        //private IList<SelectListItem> createDocTypeSelect(string selected)
        //{
        //    List<RoleDocType> authDocs = Lookup.Get<List<RoleDocType>>();
        //    List<DocumentType> docList = _docTypeRepository.FindAll().ToList();
        //    List<DocumentType> finalDocList = new List<DocumentType>();
        //    foreach (DocumentType item in docList)
        //    {
        //        var found = authDocs.FindElement(x => x.DocumentTypeId == item.Id);
        //        if (!found.IsNull())
        //        {
        //            finalDocList.Add(item);
        //        }
        //    }

        //    IList<SelectListItem> dataList = finalDocList.ConvertAll<SelectListItem>(new Converter<DocumentType, SelectListItem>(ConvertFrom));

        //    IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
        //    IList<SelectListItem> sortedList = sortedEnum.ToList();

        //    if (!selected.IsNullOrEmpty())
        //    {
        //        sortedList.FindElement(item => item.Value == selected).Selected = true;
        //    }
        //    return sortedList;
        //}
        private IList<SelectListItem> createDocTypeSelect(string selected)
        {
            List<CentralUnitDocType> docList = _doctypeRepository.FindAll().ToList();
            List<CentralUnitDocType> finalDocList = new List<CentralUnitDocType>();
            foreach (CentralUnitDocType item in docList)
            {
                //var found = authDocs.FindElement(x => x.DocumentTypeId == item.Id);
                //if (!found.IsNull())
                //{
                finalDocList.Add(item);
                //}
            }

            IList<SelectListItem> dataList = finalDocList.ConvertAll<SelectListItem>(new Converter<CentralUnitDocType, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (!selected.IsNullOrEmpty())
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }

        private IList<SelectListItem> createRegionSelect(string selected)
        {
            IList<SelectListItem> dataList = _regionRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Treemas.Credential.Model.Region, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Value);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (!selected.IsNullOrEmpty())
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }

        private IList<SelectListItem> createDivisionSelect(string Region, string selected)
        {
            IList<SelectListItem> dataList = _divisionRepository.getDivisionbyRegion(Region).ToList().ConvertAll<SelectListItem>(new Converter<Division, SelectListItem>(ConvertFrom));
            IEnumerable<SelectListItem> sortedEnum = null;
            IList<SelectListItem> sortedList = null;

            if ((selected != "-"))
            {
                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                if (dataList.Count > 0)
                {
                    sortedList.FindElement(item => item.Value == selected).Selected = true;
                }
            }
            else
            {
                SelectListItem item = new SelectListItem();
                item.Text = "Select";
                item.Value = "";
                dataList.Add(item);

                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                sortedList.FindElement(x => x.Value == "").Selected = true;
            }

            return sortedList;
        }
        private IList<SelectListItem> createDepartementSelect(string Region, string Division, string selected)
        {
            IList<SelectListItem> dataList = _departementRepository.getDepartementbyRegion(Region, Division).ToList().ConvertAll<SelectListItem>(new Converter<Departement, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = null;
            IList<SelectListItem> sortedList = null;

            if ((selected != "-"))
            {
                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                if (dataList.Count > 0)
                {
                    sortedList.FindElement(item => item.Value == selected).Selected = true;
                }
            }
            else
            {
                SelectListItem item = new SelectListItem();
                item.Text = "Select";
                item.Value = "";
                dataList.Add(item);

                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                sortedList.FindElement(x => x.Value == "").Selected = true;
            }

            return sortedList;
        }

        private IList<SelectListItem> createAllDocTypeSelect()
        {
            List<CentralUnitDocType> docList = _doctypeRepository.FindAll().ToList();

            IList<SelectListItem> dataList = docList.ConvertAll<SelectListItem>(new Converter<CentralUnitDocType, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
            IList<SelectListItem> sortedList = sortedEnum.ToList();
            return sortedList;
        }
        public SelectListItem ConvertFrom(Treemas.Credential.Model.Region item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }
        public SelectListItem ConvertFrom(Division item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }
        public SelectListItem ConvertFrom(Departement item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }
        public SelectListItem ConvertFrom(CentralUnitDocType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }
        #endregion

    }
}