﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using UOB.SVS.Resources;
using UOB.SVS.Model;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System.Linq;
using Treemas.Credential.Model;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;

namespace UOB.SVS.Controllers
{
    public class InquirySVSController : PageController
    {
        private ISignatureHRepository _hRepository;
        private IAuditTrailLog _auditRepository;
        private IBranchRepository _branchRepository;
        private IDocumentTypeRepository _docTypeRepository;

        public InquirySVSController(ISessionAuthentication sessionAuthentication, ISignatureHRepository hRepository,
            IBranchRepository branchRepository, IDocumentTypeRepository docTypeRepository,
            IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._hRepository = hRepository;
            this._auditRepository = auditRepository;
            this._branchRepository = branchRepository;
            this._docTypeRepository = docTypeRepository;
            Settings.ModuleName = "InquirySVS";
            Settings.Title = "Inquiry SVS";
        }

        protected override void Startup()
        {

        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.
                User user = Lookup.Get<User>();

                SVSReqFilter filter = new SVSReqFilter();
                // filter.Branches = getBranches();
                string CIFNo = Request.QueryString.GetValues("CIFNo")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                filter.CIFNumber = CIFNo;
                // Loading.   
                PagedResult<SignatureHeader> data;

                data = _hRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);
                var items = data.Items.ToList().ConvertAll<SignatureHView>(new Converter<SignatureHeader, SignatureHView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }


        public SignatureHView ConvertFrom(SignatureHeader item)
        {
            User user = Lookup.Get<User>();

            SignatureHView returnItem = new SignatureHView();
            returnItem.AccountNo = item.AccountNo;
            returnItem.AccountName = HttpUtility.HtmlEncode(item.AccountName);
            returnItem.AccountType = item.AccountType;
            returnItem.CIFNumber = item.CIFNumber;
            returnItem.BranchCode = item.BranchCode;
            returnItem.Note = HttpUtility.HtmlEncode(item.Note);

            int index = 1;
            if (item.Documents != null)
            {
                IList<Document> docs = new List<Document>();
                foreach (SignatureDDoc row in item.Documents)
                {
                    Document doc = new Document();
                    doc.ID = row.ID;
                    doc.Index = index++;
                    doc.DocumentType = row.DocumentType;
                    doc.FileBlob = row.FileBlob;
                    doc.FileType = HttpUtility.HtmlEncode(row.FileType);
                    doc.AccountNo = row.AccountNo;
                    doc.IsNew = false;
                    doc.IsValidDoc = true;
                    docs.Add(doc);
                }
                returnItem.Documents = docs.ToList();
            }

            if (item.IdentityCards != null)
            {
                index = 1;
                IList<IdentityCard> idcards = new List<IdentityCard>();
                foreach (SignatureDKTP row in item.IdentityCards)
                {
                    IdentityCard idcard = new IdentityCard();
                    idcard.ID = row.Id;
                    idcard.Index = index++;
                    idcard.CroppedBlob = HttpUtility.HtmlEncode(row.KTP);
                    idcard.ImageType = HttpUtility.HtmlEncode(row.ImageType);
                    idcard.AccountNo = row.AccountNo;
                    idcard.IsNew = false;
                    idcard.IsValidImage = row.KTP.Length % 4 == 0 ? true : false;
                    idcards.Add(idcard);
                }
                returnItem.IdentityCards = idcards.ToList();
            }

            if (item.Signatures != null)
            {
                index = 1;
                IList<Signature> signs = new List<Signature>();
                foreach (SignatureDSign row in item.Signatures)
                {
                    Signature sign = new Signature();
                    sign.ID = row.Id;
                    sign.Index = index++;
                    sign.CroppedBlob = HttpUtility.HtmlEncode(row.Signature);
                     sign.ImageType = HttpUtility.HtmlEncode(row.ImageType);
                    sign.AccountNo = row.AccountNo;
                    sign.IsNew = false;
                    sign.IsValidImage = row.Signature.Length % 4 == 0 ? true : false;
                    signs.Add(sign);
                }
                returnItem.Signatures = signs.ToList();
            }

            return returnItem;
        }


        public ActionResult Edit(string id)
        {
            User user = Lookup.Get<User>();
            Parameter param = Lookup.Get<Parameter>();
            ViewData["ActionName"] = "Update";
            ViewData["Parameter"] = param;

            SignatureHeader reqH = _hRepository.getSignatureHeader(id);
            if (reqH.IsNull())
            {
                ScreenMessages.Submit(ScreenMessage.Error("Account number does not exist!"));
                return RedirectToAction("Index");
            }
            //var branches = getBranches();
            //if (user.BranchCode != "000" && !branches.Exists(x => x.BranchCode == reqH.BranchCode))
            //{
            //    ScreenMessages.Submit(ScreenMessage.Error("Account number does not belong to your branch."));
            //    return RedirectToAction("Index");
            //}
            reqH.IdentityCards = _hRepository.getKTPDet(reqH.AccountNo);
            reqH.Documents = _hRepository.getDocDet(reqH.AccountNo);
            reqH.Signatures = _hRepository.getSignDet(reqH.AccountNo);

            SignatureHView data = ConvertFrom(reqH);

            return CreateView(data);
        }

        [HttpPost]
        public ActionResult Edit(SignatureHView m)
        {
            User user = Lookup.Get<User>();
            Parameter param = Lookup.Get<Parameter>();
            ViewData["ActionName"] = "Update";
            ViewData["Parameter"] = param;

            SignatureHeader reqH = _hRepository.getSignatureHeader(m.AccountNo);
            if (reqH.IsNull())
            {
                ScreenMessages.Submit(ScreenMessage.Error("Account number does not exist."));
                return RedirectToAction("Index");
            }
            //var branches = getBranches();
            //if (user.BranchCode != "000" && !branches.Exists(x => x.BranchCode == reqH.BranchCode))
            //{
            //    ScreenMessages.Submit(ScreenMessage.Error("Account number does not belong to your branch."));
            //    return RedirectToAction("Index");
            //}
            reqH.IdentityCards = _hRepository.getKTPDet(reqH.AccountNo);
            reqH.Documents = _hRepository.getDocDet(reqH.AccountNo);
            reqH.Signatures = _hRepository.getSignDet(reqH.AccountNo);

            SignatureHView data = ConvertFrom(reqH);

            return CreateView(data);
        }

        private IList<SelectListItem> createBranchSelect(string selected)
        {
            IList<SelectListItem> dataList = _branchRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Branch, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                if (dataList.FindElement(item => item.Value == selected) != null) 
                    dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        private IList<SelectListItem> createAccountTypeSelect(string selected)
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = "CS", Text = "CA/SA" });
            dataList.Insert(1, new SelectListItem() { Value = "TD", Text = "TD" });
            if (!selected.IsNullOrEmpty())
            {
                if (dataList.FindElement(item => item.Value == selected) != null)
                    dataList.FindElement(item => item.Value == selected.ToString().Trim()).Selected = true;
            }
            return dataList;
        }
        

        public SelectListItem ConvertFrom(Branch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.BranchCode;
            returnItem.Value = item.BranchCode;
            return returnItem;
        }

        private IList<SelectListItem> createDocTypeSelect(string selected)
        {
            List<RoleDocType> authDocs = Lookup.Get<List<RoleDocType>>();
            List<DocumentType> docList = _docTypeRepository.FindAll().ToList();
            List<DocumentType> finalDocList = new List<DocumentType>();
            foreach (DocumentType item in docList)
            {
                var found = authDocs.FindElement(x => x.DocumentTypeId == item.Id);
                if (!found.IsNull())
                {
                    finalDocList.Add(item);
                }
            }

            IList<SelectListItem> dataList = finalDocList.ConvertAll<SelectListItem>(new Converter<DocumentType, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (!selected.IsNullOrEmpty())
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }

        private IList<SelectListItem> createAllDocTypeSelect()
        {
            List<DocumentType> docList = _docTypeRepository.FindAll().ToList();

            IList<SelectListItem> dataList = docList.ConvertAll<SelectListItem>(new Converter<DocumentType, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
            IList<SelectListItem> sortedList = sortedEnum.ToList();
            return sortedList;
        }

        public SelectListItem ConvertFrom(DocumentType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Description;
            returnItem.Value = item.DocType;
            return returnItem;
        }

        private ViewResult CreateView(SignatureHView data)
        {
            List<RoleDocType> authDocs = Lookup.Get<List<RoleDocType>>();
            ViewData["DocCount"] = authDocs.Count;

            ViewData["BranchMaster"] = createBranchSelect(data.BranchCode);
            ViewData["DocTypeMaster"] = createDocTypeSelect("");
            ViewData["AllDocTypeMaster"] = createAllDocTypeSelect();
            ViewData["AccTypeMaster"] = createAccountTypeSelect(data.AccountType);
            return View("Detail", data);
        }

        public List<Branch> getBranches()
        {
            User user = Lookup.Get<User>();
            IList<string> branchs = new List<string>();
            //if (user.BranchCode == "000")
            //{
            return _branchRepository.getBranchs().ToList();
            //}
            //else
            //{
            //    branchs.Add(user.BranchCode);
            //    return _branchRepository.getBranchs(branchs).ToList();
            //}
        }

        [HttpGet]
        public ActionResult NoAuthCheckViewDocument(int ID)
        {
            JsonResult result = new JsonResult();
            SignatureDDoc doc = _hRepository.getDocDet(ID);
            if (doc.IsNull())
            {
                return Content("Not exist or invalid document");
            }
            byte[] blob = Convert.FromBase64String(doc.FileBlob);

            string filetype = doc.FileType.Split(';')[0].Split(':')[1];
            if (filetype.Trim() == "application/pdf")
            {
                return File(blob, filetype);
            }
            else
            {
                Stream strm = new MemoryStream(Convert.FromBase64String(doc.FileBlob));
                Bitmap bmp = (Bitmap)Bitmap.FromStream(strm);

                ImageFormatConverter imgConverter = new ImageFormatConverter();
                var png = imgConverter.ConvertFromString("PNG");

                MemoryStream ms = new MemoryStream();
                bmp.Save(ms, (ImageFormat)png);

                byte[] bytImg = ms.ToArray();
                string strImg = Convert.ToBase64String(bytImg);
                return Content("<img src='data:image/PNG;base64," + strImg + "' alt='" + doc.DocumentType + "'/>");
            }
        }
    }
}