﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;
using System.Globalization;
using Treemas.Base.Resources;
using UOB.SVS.Interface;
using UOB.SVS.Model;
using LinqSpecs;

namespace UOB.SVS.Controllers
{
    public class MasterCrApprovalCentralizeController : PageController
    {
        private IApplicationRepository _appRepository;
        private IApplicationCrRepository _appCrReporsitory;
        private IAppAuditTrailCentralizeLog _auditRepository;
        private IRegionRepository _regionRepository;
        private IRegionCrRepository _regionCrRepository;
        private IDivisionRepository _divisionRepository;
        private IDivisionCrRepository _divisionCrRepository;
        private IDepartementRepository _departementRepository;
        private IDepartementCrRepository _departementCrRepository;
        private ICentralUnitDocTypeRepository _doctypeRepository;
        private ICentralUnitDocTypeCrRepository _doctypeCrRepository;
        private IUserRepository _userRepository;
        private IVApprovalCentralizeRepository _VApprovalCentralizeRepository;
        //private IVApprovalRepository _VApprovalRepository;
        private IAuthorizationRepository _AuthorizationRepository;

        public MasterCrApprovalCentralizeController(ISessionAuthentication sessionAuthentication, IApplicationRepository appRepository, IApplicationCrRepository appCrReporsitory,
            IAppAuditTrailCentralizeLog auditRepository, IRegionRepository regionRepository, IRegionCrRepository regionCrRepository, IDivisionRepository divisionRepository,
            IDivisionCrRepository divisionCrRepository, IDepartementRepository departementRepository, IDepartementCrRepository departementCrRepository,
            ICentralUnitDocTypeRepository doctypeRepository, ICentralUnitDocTypeCrRepository doctypeCrRepository, IUserRepository userRepository, 
            IVApprovalCentralizeRepository vApprovalCentralizeRepository, IAuthorizationRepository AuthorizationRepository) : base(sessionAuthentication)
        {
            this._appRepository = appRepository;
            this._appCrReporsitory = appCrReporsitory;
            this._auditRepository = auditRepository;
            this._regionRepository = regionRepository;
            this._regionCrRepository = regionCrRepository;
            this._divisionRepository = divisionRepository;
            this._divisionCrRepository = divisionCrRepository;
            this._departementRepository = departementRepository;
            this._departementCrRepository = departementCrRepository;
            this._doctypeRepository = doctypeRepository;
            this._doctypeCrRepository = doctypeCrRepository;
            this._userRepository = userRepository;
            this._VApprovalCentralizeRepository = vApprovalCentralizeRepository;
            this._AuthorizationRepository = AuthorizationRepository;

            Settings.ModuleName = "MasterCrApprovalCentralize";
            Settings.Title = "Approval Master";
        }
        protected override void Startup()
        {
            ViewData["ApprovalFunctionList"] = createApprovalFunctionSelect("");
            ViewData["ApprovalStatusList"] = createApprovalStatusSelect("");
            ViewData["ApprovalTypeList"] = createApprovalTypeSelect("");
            ViewData["loadState"] = (Request.QueryString.GetValues("loadState") == null) ? "0" : Request.QueryString.GetValues("loadState").FirstOrDefault();
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                User user = Lookup.Get<User>();
                ApplicationCRFilter appFilter = new ApplicationCRFilter();

                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];

                appFilter.ApprovalType = Request.QueryString.GetValues("ApprovalType")[0];
                appFilter.ApprovalFuntion = Request.QueryString.GetValues("ApprovalFuntion")[0];
                string mode = Request.QueryString.GetValues("Mode")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").LastOrDefault() + "][name]").LastOrDefault();
                var sortColumnDir = "";
                if (sortColumn == "Id")
                {
                    sortColumn = "MakerDate";
                    sortColumnDir = "desc";
                }
                else
                {
                    sortColumnDir = Request.QueryString.GetValues("order[0][dir]").LastOrDefault();
                }
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;


                var arrFeature = getfeature(user.Username);

                PagedResult<VApprovalCentralize> appData = null;
                
                if(!searchValue.IsNullOrEmpty())
                    appData = _VApprovalCentralizeRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, appFilter, user, arrFeature, searchColumn, searchValue); 
                else
                    appData = _VApprovalCentralizeRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, appFilter, user, arrFeature);

                var items = appData.Items.ToList().ConvertAll<ApplicationCrView>(new Converter<VApprovalCentralize, ApplicationCrView>(ConvertFrom));



                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = appData.TotalItems,
                    recordsFiltered = appData.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public string[] getfeature(string Username)
        {
            IList<Authorization> aaa = _AuthorizationRepository.getUserAuthorization(Username);
            aaa = aaa.Where(e => e.Function.Contains("MasterCrApprovalCentralize"))
                              .ToList();
            int index = 0;
            string[] subbranch = new string[8];
            foreach (var item in aaa)
            {
                if (item.Feature == "ApproveRegion")
                {
                    subbranch[index] = "Region";
                    index++;
                }
                else if (item.Feature == "ApproveDivision")
                {
                    subbranch[index] = "Division";
                    index++;
                }
                else if (item.Feature == "ApproveDepartement")
                {
                    subbranch[index] = "Departement";
                    index++;
                }
                else if (item.Feature == "ApproveCentralizeDocumentType")
                {
                    subbranch[index] = "Centralize Document Type";
                    index++;
                }
            }
            //if (index == 0)
            //    return null;
            return subbranch;
        }

        #region ConvertFromList

        public ApplicationCrView ConvertFrom(VApprovalCentralize item)
        {
            ApplicationCrView returnItem = new ApplicationCrView();
            returnItem.Id = item.Id.ToString().Trim();
            returnItem.ApprovalFuntion = item.ApprovalFuntion;
            returnItem.RequestType = item.ApprovalTypeEnum.ToDescription();
            returnItem.ApprovalStatus = item.ApprovalStatus.Trim() == "N" ? "New Request" : item.ApprovalStatus.Trim() == "A" ? "Approved" : "Rejected";
            returnItem.MakerDate = item.MakerDate.IsNull() ? null : Convert.ToDateTime(item.MakerDate).ToString("dd/MM/yyyy");
            returnItem.MakerUser = item.MakerUser;
            returnItem.Alias = item.Alias;
            returnItem.Name = item.Name;
            return returnItem;
        }


        public ApplicationCrView ConvertFrom(RegionCr item)
        {
            ApplicationCrView returnItem = new ApplicationCrView();
            returnItem.Id = "R" + item.Id.ToString().Trim();
            returnItem.ApprovalFuntion = "Region";
            returnItem.RequestType = item.ApprovalTypeEnum.ToDescription();
            returnItem.ApprovalStatus = item.ApprovalStatus.Trim() == "N" ? "New Request" : item.ApprovalStatus.Trim() == "A" ? "Approved" : "Rejected";
            returnItem.MakerDate = item.CreatedDate.IsNull() ? null : Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
            returnItem.MakerUser = item.CreatedBy;
            return returnItem;
        }
        public ApplicationCrView ConvertFrom(DivisionCr item)
        {
            ApplicationCrView returnItem = new ApplicationCrView();
            returnItem.Id = "D" + item.Id.ToString().Trim();
            returnItem.ApprovalFuntion = "Division";
            returnItem.RequestType = item.ApprovalTypeEnum.ToDescription();
            returnItem.ApprovalStatus = item.ApprovalStatus.Trim() == "N" ? "New Request" : item.ApprovalStatus.Trim() == "A" ? "Approved" : "Rejected";
            returnItem.MakerDate = item.CreatedDate.IsNull() ? null : Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
            returnItem.MakerUser = item.CreatedBy;
            return returnItem;
        }
        public ApplicationCrView ConvertFrom(DepartementCr item)
        {
            ApplicationCrView returnItem = new ApplicationCrView();
            returnItem.Id = "E" + item.Id.ToString().Trim();
            returnItem.ApprovalFuntion = "Departement";
            returnItem.RequestType = item.ApprovalTypeEnum.ToDescription();
            returnItem.ApprovalStatus = item.ApprovalStatus.Trim() == "N" ? "New Request" : item.ApprovalStatus.Trim() == "A" ? "Approved" : "Rejected";
            returnItem.MakerDate = item.CreatedDate.IsNull() ? null : Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
            returnItem.MakerUser = item.CreatedBy;
            return returnItem;
        }
        public ApplicationCrView ConvertFrom(CentralUnitDocTypeCr item)
        {
            ApplicationCrView returnItem = new ApplicationCrView();
            returnItem.Id = "C" + item.Id.ToString().Trim();
            returnItem.ApprovalFuntion = "Centralize Document Type";
            returnItem.RequestType = item.ApprovalTypeEnum.ToDescription();
            returnItem.ApprovalStatus = item.ApprovalStatus.Trim() == "N" ? "New Request" : item.ApprovalStatus.Trim() == "A" ? "Approved" : "Rejected";
            returnItem.MakerDate = item.CreatedDate.IsNull() ? null : Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
            returnItem.MakerUser = item.CreatedBy;
            return returnItem;
        }
        #endregion

        #region CreateSelected
        private IList<SelectListItem> createApprovalFunctionSelect(string selected)
        {
            User user = Lookup.Get<User>();
            IList<Authorization> aaa = _AuthorizationRepository.getUserAuthorization(user.Username);
            aaa = aaa.Where(e => e.Function.Contains("MasterCrApprovalCentralize"))
                              .ToList();
            int index = 1;
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = "", Text = "" });

            foreach (var item in aaa)
            {
                if (item.Feature == "ApproveRegion")
                {
                    dataList.Insert(index, new SelectListItem() { Value = "Region", Text = "Region" });
                    index++;
                }
                else if (item.Feature == "ApproveDivision")
                {
                    dataList.Insert(index, new SelectListItem() { Value = "Division", Text = "Division" });
                    index++;
                }
                else if (item.Feature == "ApproveDepartement")
                {
                    dataList.Insert(index, new SelectListItem() { Value = "Departement", Text = "Departement" });
                    index++;
                }
                else if (item.Feature == "ApproveCentralizeDocumentType")
                {
                    dataList.Insert(index, new SelectListItem() { Value = "Centralize Document Type", Text = "Centralize Document Type" });
                    index++;
                }
            }
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }
        private IList<SelectListItem> createApprovalTypeSelect(string selected)
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = "", Text = "" });
            dataList.Insert(1, new SelectListItem() { Value = "C", Text = "Create" });
            dataList.Insert(2, new SelectListItem() { Value = "U", Text = "Update" });
            dataList.Insert(3, new SelectListItem() { Value = "D", Text = "Delete" });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }
        private IList<SelectListItem> createApprovalActionSelect(string selected)
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = "", Text = "" });
            dataList.Insert(1, new SelectListItem() { Value = "A", Text = "Approve" });
            dataList.Insert(2, new SelectListItem() { Value = "R", Text = "Reject" });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }
        private IList<SelectListItem> createApprovalStatusSelect(string selected)
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = "", Text = "" });
            dataList.Insert(1, new SelectListItem() { Value = "N", Text = "New Request" });
            dataList.Insert(2, new SelectListItem() { Value = "R", Text = "Rejected" });
            dataList.Insert(3, new SelectListItem() { Value = "A", Text = "Approved" });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString()).Selected = true;
            }
            return dataList;
        }
        #endregion

        public ActionResult Approve(string id)
        {
            string appFunction = id.Substring(0, 1);
            long appId = Convert.ToInt32(id.Substring(1, id.Length - 1));
            VApprovalCentralize vapcent = _VApprovalCentralizeRepository.GetApprovalCentralize(id);

            switch (appFunction)
            {
                case "A":
                    ViewData["ActionName"] = "Approve";
                    ViewData["ApprovalActionList"] = createApprovalActionSelect("");
                    ApplicationCr appCr = _appCrReporsitory.getApplication(appId);
                    ApplicationCrView appCrData = ConvertFromApprove(appCr);
                    appCrData.ApprovalType = vapcent.RequestType;
                    ViewData["appCrData"] = appCrData;
                    return View("DetailApp", appCrData);
                case "R":
                    ViewData["ActionName"] = "ApproveRegion";
                    ViewData["ApprovalActionList"] = createApprovalActionSelect("");
                    RegionCr regCr = _regionCrRepository.getRegion(appId);
                    RegionCrView regionCrData = ConvertFromApprove(regCr);
                    regionCrData.ApprovalType = vapcent.RequestType;
                    ViewData["regionCrData"] = regionCrData;
                    ViewData["ApplicationMaster"] = createApplicationSelect(regionCrData.ApplicationID);
                    return View("DetailRegion", regionCrData);
                case "D":
                    ViewData["ActionName"] = "ApproveDivision";
                    ViewData["ApprovalActionList"] = createApprovalActionSelect("");
                    DivisionCr divCr = _divisionCrRepository.getDivision(appId);
                    DivisionCrView divisionCrData = ConvertFromApprove(divCr);
                    divisionCrData.ApprovalType = vapcent.RequestType;
                    ViewData["divisionCrData"] = divisionCrData;
                    ViewData["ApplicationMaster"] = createApplicationSelect(divisionCrData.ApplicationID);
                    return View("DetailDivision", divisionCrData);
                case "E":
                    ViewData["ActionName"] = "ApproveDepartement";
                    ViewData["ApprovalActionList"] = createApprovalActionSelect("");
                    DepartementCr deptCr = _departementCrRepository.getDepartement(appId);
                    DepartementCrView departementCrData = ConvertFromApprove(deptCr);
                    departementCrData.ApprovalType = vapcent.RequestType;
                    ViewData["departementCrData"] = departementCrData;
                    ViewData["ApplicationMaster"] = createApplicationSelect(departementCrData.ApplicationID);
                    return View("DetailDepartement", departementCrData);
                case "C":
                    ViewData["ActionName"] = "ApproveCentralizeDocumentType";
                    ViewData["ApprovalActionList"] = createApprovalActionSelect("");
                    CentralUnitDocTypeCr doctypeCr = _doctypeCrRepository.getCentralizeDoc(appId);
                    CentralUnitDocTypeCrView centdoctypeCrData = ConvertFromApprove(doctypeCr);
                    centdoctypeCrData.ApprovalType = vapcent.RequestType;
                    ViewData["centdoctypeCrData"] = centdoctypeCrData;
                    ViewData["ApplicationMaster"] = createApplicationSelect(centdoctypeCrData.ApplicationID);
                    return View("DetailCentralizeDocumentType", centdoctypeCrData);
                default:
                    return View("Detail");
            }
        }

        #region ConvertFromApproval
        public ApplicationCrView ConvertFromApprove(ApplicationCr item)
        {
            Application app = _appRepository.getApplication(item.ApplicationId);
            User usr = _userRepository.GetUser(item.CreatedBy);

            ApplicationCrView returnItem = new ApplicationCrView();
            returnItem.ApplicationId = item.ApplicationId;
            returnItem.Type = item.Type;
            returnItem.Runtime = item.Runtime;
            returnItem.Description = item.Description;
            returnItem.CSSColor = item.CSSColor;
            returnItem.Icon = item.Icon;
            returnItem.Id = item.Id.ToString().Trim();
            returnItem.Name = item.Name;
            returnItem.ApprovalType = item.ApprovalType;
            returnItem.Approved = item.Approved;

            if (item.ApprovalType == "U")
            {
                returnItem.ApplicationIdBefore = app.ApplicationId;
                returnItem.TypeBefore = app.Type;
                returnItem.RuntimeBefore = app.Runtime;
                returnItem.DescriptionBefore = app.Description;
                returnItem.CSSColorBefore = app.CSSColor;
                returnItem.IconBefore = app.Icon;
                returnItem.NameBefore = app.Name;
            }

            returnItem.MakerUser = usr.FullName;
            returnItem.MakerDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy HH:mm:ss");

            return returnItem;
        }
        public RegionCrView ConvertFromApprove(RegionCr item)
        {
            Application app = _appRepository.getApplication(item._Application);
            Region region = _regionRepository.getRegion(item.RegionId);
            User usr = _userRepository.GetUser(item.CreatedBy);

            RegionCrView returnItem = new RegionCrView();
            returnItem.Application = app.Name;
            returnItem.ApplicationID = item._Application;
            returnItem.Name = item.Name;
            returnItem.Alias = item.Alias;
            returnItem.RegionId = item.RegionId;
            returnItem.Id = item.Id.ToString().Trim();
            returnItem.ApprovalType = item.ApprovalType;
            returnItem.Approved = item.Approved;
            returnItem.MakerUser = usr.FullName;
            returnItem.MakerDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy HH:mm:ss");
            if (item.ApprovalType == "U")
            {
                returnItem.ApplicationBefore = region.Name;
                returnItem.ApplicationIDBefore = region._Application;
                returnItem.AliasBefore = region.Alias;
                returnItem.RegionIdBefore = region.RegionId;
                returnItem.NameBefore = region.Name;
            }
            return returnItem;
        }
        public DivisionCrView ConvertFromApprove(DivisionCr item)
        {
            User usr = _userRepository.GetUser(item.CreatedBy);
            Application app = _appRepository.getApplication(item._Application);
            Division division = _divisionRepository.getDivision(item.DivisonId);

            DivisionCrView returnItem = new DivisionCrView();
            returnItem.Application = app.Name;
            returnItem.ApplicationID = item._Application;
            returnItem.Alias = item.Alias;
            returnItem.Region = item.Region;
            returnItem.DivisionId = item.DivisonId;
            returnItem.Id = item.Id.ToString().Trim();
            returnItem.Name = item.Name;
            returnItem.ApprovalType = item.ApprovalType;
            returnItem.Approved = item.Approved;
            returnItem.MakerUser = usr.FullName;
            returnItem.MakerDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy HH:mm:ss");
            if (item.ApprovalType == "U")
            {
                returnItem.ApplicationBefore = division.Name;
                returnItem.ApplicationIDBefore = division._Application;
                returnItem.AliasBefore = division.Alias;
                returnItem.DivisionIdBefore = division.DivisionId;
                returnItem.NameBefore = division.Name;
                returnItem.RegionBefore = division.Region;
            }
            return returnItem;
        }
        public DepartementCrView ConvertFromApprove(DepartementCr item)
        {
            Application app = _appRepository.getApplication(item._Application);
            User usr = _userRepository.GetUser(item.CreatedBy);
            Departement departement = _departementRepository.getDepartement(item.DepartementId);

            DepartementCrView returnItem = new DepartementCrView();
            returnItem.Application = app.Name;
            returnItem.ApplicationID = item._Application;
            returnItem.Alias = item.Alias;
            returnItem.Region = item.Region;
            returnItem.Division = item.Division;
            returnItem.DepartementId = item.DepartementId;
            returnItem.Id = item.Id.ToString().Trim();
            returnItem.Name = item.Name;
            returnItem.ApprovalType = item.ApprovalType;
            returnItem.Approved = item.Approved;
            returnItem.MakerUser = usr.FullName;
            returnItem.MakerDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy HH:mm:ss");
            if (item.ApprovalType == "U")
            {
                returnItem.ApplicationBefore = departement.Name;
                returnItem.ApplicationIDBefore = departement._Application;
                returnItem.AliasBefore = departement.Alias;
                returnItem.RegionBefore = departement.Region;
                returnItem.DivisionBefore = departement.Division;
                returnItem.DepartementIdBefore = departement.DepartementId;
                returnItem.NameBefore = departement.Name;
            }
            return returnItem;
        }
        public CentralUnitDocTypeCrView ConvertFromApprove(CentralUnitDocTypeCr item)
        {
            Application app = _appRepository.getApplication(item._Application);
            CentralUnitDocType doctype = _doctypeRepository.getCentralUnitDocType(item.DocId);
            User usr = _userRepository.GetUser(item.CreatedBy);

            CentralUnitDocTypeCrView returnItem = new CentralUnitDocTypeCrView();
            //string spaces = createSpaces(item.MenuLevel);
            returnItem.Application = app.Name;
            returnItem.ApplicationID = item._Application;
            returnItem.Alias = item.Alias;
            returnItem.DocId = item.DocId;
            returnItem.Id = item.Id.ToString().Trim();
            returnItem.Name = item.Name;
            returnItem.ApprovalType = item.ApprovalType;
            returnItem.Approved = item.Approved;
            returnItem.MakerUser = usr.FullName;
            returnItem.MakerDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy HH:mm:ss");
            if (item.ApprovalType == "U")
            {
                returnItem.ApplicationBefore = doctype.Name;
                returnItem.ApplicationIDBefore = doctype._Application;
                returnItem.AliasBefore = doctype.Alias;
                returnItem.AliasBefore = doctype.Alias;
                returnItem.NameBefore = doctype.Name;
            }
            return returnItem;
        }
        private string createSpaces(int level)
        {
            string spaces = "";
            for (int i = 0; i < level; i++)
            {
                spaces += "&nbsp;&nbsp;&nbsp;";
            }
            return spaces;
        }
        #endregion

        #region Approval
        [HttpPost]
        public ActionResult Approve(ApplicationCrView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                ApplicationCr appCr = _appCrReporsitory.getApplication(Convert.ToInt32(data.Id.Substring(1, data.Id.Length - 1)));
                appCr.Approved = true;
                appCr.ApprovedBy = user.Username;
                appCr.ApprovedDate = DateTime.Now;
                appCr.ApprovalStatus = data.ApprovalStatus;
                _appCrReporsitory.Save(appCr);

                Application oldData = _appRepository.getApplication(data.ApplicationId);

                if (data.ApprovalStatus == "A")
                {
                    if (data.ApprovalType == "C")
                    {
                        Application newData = new Application(0L);
                        newData.ApplicationId = data.ApplicationId;
                        newData.Name = data.Name;
                        newData.Type = data.Type;
                        newData.Runtime = data.Runtime;
                        newData.Description = data.Description;
                        newData.CSSColor = data.CSSColor;
                        newData.Icon = data.Icon;
                        newData.CreatedBy = appCr.CreatedBy;
                        newData.CreatedDate = appCr.CreatedDate;

                        _appRepository.Add(newData);
                    }
                    else if (data.ApprovalType == "D")
                    {
                        Application appDelete = _appRepository.getApplication(data.ApplicationId);
                        _appRepository.Remove(appDelete);

                    }
                    else
                    {
                        Application appUpdate = _appRepository.getApplication(data.ApplicationId);

                        appUpdate.ApplicationId = data.ApplicationId;
                        appUpdate.Name = data.Name;
                        appUpdate.Type = data.Type;
                        appUpdate.Runtime = data.Runtime;
                        appUpdate.Description = data.Description;
                        appUpdate.CSSColor = data.CSSColor;
                        appUpdate.Icon = data.Icon;
                        appUpdate.ChangedBy = appCr.CreatedBy;
                        appUpdate.ChangedDate = appCr.CreatedDate;

                        _appRepository.Save(appUpdate);
                    }
                }
                else if (data.ApprovalStatus == "R")
                {
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, "Role", oldData, oldData, oldData.CreatedBy, oldData.CreatedDate, user.Username, "Reject");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(ApplicationResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult ApproveRegion(RegionCrView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                RegionCr appCr = _regionCrRepository.getRegion(Convert.ToInt32(data.Id.Substring(1, data.Id.Length - 1)));
                appCr.Approved = true;
                appCr.ApprovedBy = user.Username;
                appCr.ApprovedDate = DateTime.Now;
                appCr.ApprovalStatus = data.ApprovalStatus;
                _regionCrRepository.Save(appCr);
                
                Region oldData = _regionRepository.getRegion(data.RegionId);

                if (data.ApprovalStatus == "A")
                {
                    if (data.ApprovalType == "C")
                    {
                        Region newData = new Region(0L);
                        newData._Application = data.ApplicationID;
                        newData.RegionId = data.RegionId;
                        newData.Name = data.Name;
                        newData.Alias = data.Alias;
                        newData.ChangedBy = user.Username;
                        newData.ChangedDate = DateTime.Now;
                        newData.CreatedBy = appCr.CreatedBy;
                        newData.CreatedDate = DateTime.Now;
                        _regionRepository.Add(newData);

                        _auditRepository.SaveAuditTrail(Settings.ModuleName, "Region", oldData, newData, appCr.CreatedBy, newData.CreatedDate, user.Username, "Create");
                    }
                    else if (data.ApprovalType == "D")
                    {
                        Region appDelete = _regionRepository.getRegion(data.RegionId);
                        _regionRepository.Remove(appDelete);
                        _auditRepository.SaveAuditTrail(Settings.ModuleName, "Region", oldData, appDelete, appCr.CreatedBy, appDelete.CreatedDate, user.Username, "Delete");
                    }
                    else
                    {
                        Region appUpdate = _regionRepository.getRegion(data.RegionId);
                        appUpdate._Application = data.ApplicationID;
                        appUpdate.RegionId = data.RegionId;
                        appUpdate.Name = data.Name;
                        appUpdate.Alias = data.Alias;
                        appUpdate.ChangedBy = user.Username;
                        appUpdate.ChangedDate = DateTime.Now;
                        _regionRepository.Save(appUpdate);

                        _auditRepository.SaveAuditTrail(Settings.ModuleName, "Region", oldData, appUpdate, appCr.CreatedBy, appUpdate.CreatedDate, user.Username, "Update");
                    }
                }
                else if (data.ApprovalStatus == "R")
                {
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, "Region", oldData, oldData, appCr.CreatedBy, appCr.CreatedDate, user.Username, "Reject");
                }
            } 
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(FunctionResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult ApproveDivision(DivisionCrView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                DivisionCr appCr = _divisionCrRepository.getDivision(Convert.ToInt32(data.Id.Substring(1, data.Id.Length - 1)));
                appCr.Approved = true;
                appCr.ApprovedBy = user.Username;
                appCr.ApprovedDate = DateTime.Now;
                appCr.ApprovalStatus = data.ApprovalStatus;
                _divisionCrRepository.Save(appCr);

                Division oldData = _divisionRepository.getDivision(data.DivisionId);

                if (data.ApprovalStatus == "A")
                {
                    if (data.ApprovalType == "C")
                    {
                        Division newData = new Division(0L);
                        newData._Application = data.ApplicationID;
                        newData.DivisionId = data.DivisionId;
                        newData.Name = data.Name;
                        newData.Alias = data.Alias;
                        newData.Region = data.Region;
                        newData.ChangedBy = user.Username;
                        newData.ChangedDate = DateTime.Now;
                        newData.CreatedBy = appCr.CreatedBy;
                        newData.CreatedDate = DateTime.Now;
                        _divisionRepository.Add(newData);

                        _auditRepository.SaveAuditTrail(Settings.ModuleName, "Division", oldData, newData, appCr.CreatedBy, newData.CreatedDate, user.Username, "Create");
                    }
                    else if (data.ApprovalType == "D")
                    {
                        Division delData = _divisionRepository.getDivision(data.DivisionId);
                        _divisionRepository.Remove(delData);

                        _auditRepository.SaveAuditTrail(Settings.ModuleName, "Division", oldData, oldData, appCr.CreatedBy, oldData.CreatedDate, user.Username, "Delete");
                    }
                    else
                    {
                        Division newData = _divisionRepository.getDivision(data.DivisionId);

                        newData._Application = data.ApplicationID;
                        newData.DivisionId = data.DivisionId;
                        newData.Name = data.Name;
                        newData.Alias = data.Alias;
                        newData.Region = data.Region;
                        newData.ChangedBy = user.Username;
                        newData.ChangedDate = DateTime.Now;

                        _divisionRepository.Save(newData);

                        _auditRepository.SaveAuditTrail(Settings.ModuleName, "Division", oldData, newData, appCr.CreatedBy, newData.CreatedDate, user.Username, "Update");
                    }
                }
                else if (data.ApprovalStatus == "R")
                {
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, "Division", oldData, oldData, appCr.CreatedBy, appCr.CreatedDate, user.Username, "Reject");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(FeatureResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult ApproveDepartement(DepartementCrView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                DepartementCr appCr = _departementCrRepository.getDepartement(Convert.ToInt32(data.Id.Substring(1, data.Id.Length - 1)));
                appCr.Approved = true;
                appCr.ApprovedBy = user.Username;
                appCr.ApprovedDate = DateTime.Now;
                appCr.ApprovalStatus = data.ApprovalStatus;
                _departementCrRepository.Save(appCr);

                Departement oldData = _departementRepository.getDepartement(data.DepartementId);

                if (data.ApprovalStatus == "A")
                {
                    if (data.ApprovalType == "C")
                    {
                        Departement newData = new Departement(0L);
                        newData._Application = data.ApplicationID;
                        newData.DepartementId = data.DepartementId;
                        newData.Name = data.Name;
                        newData.Alias = data.Alias;
                        newData.Region = data.Region;
                        newData.Division = data.Division;
                        newData.ChangedBy = appCr.CreatedBy;
                        newData.ChangedDate = appCr.CreatedDate;
                        newData.CreatedBy = appCr.CreatedBy;
                        newData.CreatedDate = appCr.CreatedDate;

                        _departementRepository.Add(newData);

                        _auditRepository.SaveAuditTrail(Settings.ModuleName, "Departement", oldData, newData, appCr.CreatedBy, newData.CreatedDate, user.Username, "Create");
                    }
                    else if (data.ApprovalType == "D")
                    {
                        Departement DeleteData = _departementRepository.getDepartement(data.DepartementId);
                        _departementRepository.Remove(DeleteData);

                        _auditRepository.SaveAuditTrail(Settings.ModuleName, "Departement", oldData, DeleteData, appCr.CreatedBy, DeleteData.CreatedDate, user.Username, "Delete");
                    }
                    else
                    {
                        Departement newData = _departementRepository.getDepartement(data.DepartementId);

                        newData._Application = data.ApplicationID;
                        newData.DepartementId = data.DepartementId;
                        newData.Name = data.Name;
                        newData.Alias = data.Alias;
                        newData.Region = data.Region;
                        newData.Division = data.Division;
                        newData.ChangedBy = appCr.CreatedBy;
                        newData.ChangedDate = appCr.CreatedDate;

                        _departementRepository.Save(newData);

                        _auditRepository.SaveAuditTrail(Settings.ModuleName, "Departement", oldData, newData, appCr.CreatedBy, newData.CreatedDate, user.Username, "Update");
                    }
                }
                else if (data.ApprovalStatus == "R")
                {
                    _auditRepository.SaveAuditTrail(Settings.ModuleName, "Departement", oldData, oldData, appCr.CreatedBy, appCr.CreatedDate, user.Username, "Reject");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                if (data.ApprovalStatus == "A")
                {
                    ScreenMessages.Submit(ScreenMessage.Success(RoleResources.Validation_Approve));
                }
                else
                {
                    ScreenMessages.Submit(ScreenMessage.Success(RoleResources.Validation_Reject));
                }
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult ApproveCentralizeDocumentType(CentralUnitDocTypeCrView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                CentralUnitDocTypeCr appCr = _doctypeCrRepository.getCentralizeDoc(Convert.ToInt32(data.Id.Substring(1, data.Id.Length - 1)));
                appCr.Approved = true;
                appCr.ApprovedBy = user.Username;
                appCr.ApprovedDate = DateTime.Now;
                appCr.ApprovalStatus = data.ApprovalStatus;
                _doctypeCrRepository.Save(appCr);
                if (data.ApprovalStatus == "A")
                {
                    if (data.ApprovalType == "C")
                    {
                        CentralUnitDocType newData = new CentralUnitDocType(0L);
                        newData._Application = data.ApplicationID;
                        newData.DocId = data.DocId;
                        newData.Name = data.Name;
                        newData.Alias = data.Alias;
                        newData.ChangedBy = appCr.CreatedBy;
                        newData.ChangedDate = appCr.CreatedDate;
                        newData.CreatedBy = appCr.CreatedBy;
                        newData.CreatedDate = appCr.CreatedDate;

                        _doctypeRepository.Add(newData);
                        //_userRepository.setUpdateMenu(true);
                    }
                    else if (data.ApprovalType == "D")
                    {
                        CentralUnitDocType newData = _doctypeRepository.getCentralUnitDocType(data.DocId);
                        _doctypeRepository.Remove(newData);
                        //_userRepository.setUpdateMenu(true);
                    }
                    else
                    {
                        CentralUnitDocType newData = _doctypeRepository.getCentralUnitDocType(data.DocId);

                        newData._Application = data.ApplicationID;
                        newData.DocId = data.DocId;
                        newData.Name = data.Name;
                        newData.Alias = data.Alias;
                        newData.ChangedBy = appCr.CreatedBy;
                        newData.ChangedDate = appCr.CreatedDate;

                        _doctypeRepository.Save(newData);
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(MenuResources.Validation_Success));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private ViewResult CreateView(RegionCrView data)
        {
            return View("DetailRegion", data);
        }

        private ViewResult CreateView(DivisionCrView data)
        {
            return View("DetailDivision", data);
        }

        private ViewResult CreateView(ApplicationCrView data)
        {
            return View("DetailApp", data);
        }
        private ViewResult CreateView(DepartementCrView data)
        {
            return View("DetailDepartement", data);
        }
        private ViewResult CreateView(CentralUnitDocTypeCrView data)
        {
            return View("DetailCentralUnitDocType", data);
        }
        #endregion

        private IList<SelectListItem> createApplicationSelect(string selected)
        {
            IList<SelectListItem> dataList = _appRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Application, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        public SelectListItem ConvertFromSelect(Branch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Description;
            returnItem.Value = item.BranchCode;
            return returnItem;
        }
        public SelectListItem ConvertFrom(Application item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Name;
            returnItem.Value = item.ApplicationId;
            return returnItem;
        }

        public SelectListItem ConvertFrom(AuthorizationFunction item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Name;
            returnItem.Value = item.FunctionId;
            return returnItem;
        }
        public SelectListItem ConvertFromMenu(Menu item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.MenuName;
            returnItem.Value = item.Id.ToString();
            return returnItem;
        }
    }
}