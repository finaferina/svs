﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;
using System.Globalization;
using System.Text.RegularExpressions;
using Microsoft.Reporting.WebForms;
using System.IO;
using SpreadsheetLight;
using System.Data;

namespace UOB.SVS.Controllers
{
    public class AppAuditTrailController : PageController
    {
        private IUserRepository _userRepository;
        private IApplicationRepository _appRepository;
        private IAppAuditTrailRepository _auditRepository;

        public AppAuditTrailController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository,
            IApplicationRepository appRepository, IAppAuditTrailRepository auditRepository) : base(sessionAuthentication)
        {
            this._userRepository = userRepository;
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;

            Settings.ModuleName = "AppAuditTrail";
            Settings.Title = "Application Audit Trail";
        }
        protected override void Startup()
        {

        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            try
            {
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string ActionDateStart = Request.QueryString.GetValues("ActionDateStart")[0];
                string ActionDateEnd = Request.QueryString.GetValues("ActionDateEnd")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<AppAuditTrail> data;

                DateTime _RequestTimeStart = DateTime.MinValue;
                DateTime _RequestTimeEnd = DateTime.MinValue;

                if (!ActionDateStart.IsNullOrEmpty())
                {
                    if (!DateTime.TryParseExact(ActionDateStart, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out _RequestTimeStart))
                        throw new Exception("Input date is not valid");
                }
                if (!ActionDateEnd.IsNullOrEmpty())
                {
                    if (!DateTime.TryParseExact(ActionDateEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out _RequestTimeEnd))
                        throw new Exception("Input date is not valid");
                }

                if (_RequestTimeStart > _RequestTimeEnd)
                {
                    throw new Exception("Input date range is not valid");
                }

                if (_RequestTimeEnd == DateTime.MinValue)
                    _RequestTimeEnd = _RequestTimeStart;


                data = _auditRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue, _RequestTimeStart, _RequestTimeEnd);

                var items = data.Items.ToList().ConvertAll<AuditTrailView>(new Converter<AppAuditTrail, AuditTrailView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = this.Json(new
                {
                    draw = Convert.ToInt32(0),
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    data = new List<AppAuditTrail>(),
                    msg = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);

                // Info   
                //Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public AuditTrailView ConvertFrom(AppAuditTrail item)
        {
            //var ar = _auditRepository
            AuditTrailView returnItem = new AuditTrailView();
            returnItem.Id = item.Id;
            returnItem.FunctionName = item.FunctionName;
            returnItem.ObjectName = item.ObjectName;
            returnItem.ObjectValueBefore = item.ObjectValueBefore;
            returnItem.ObjectValueAfter = item.ObjectValueAfter;
            returnItem.Action = item.Action;
            returnItem.TimeStamp = Convert.ToDateTime(item.TimeStamp).ToString("dd/MM/yyyy HH:mm:ss");
            returnItem.UserMaker = item.UserMaker;
            returnItem.UserChecker = item.UserChecker;
            returnItem.RequestDate = item.RequestDate.IsNull() ? null : Convert.ToDateTime(item.RequestDate).ToString("dd/MM/yyyy");
            returnItem.ApproveDate = item.ApproveDate.IsNull() ? null : Convert.ToDateTime(item.ApproveDate).ToString("dd/MM/yyyy");

            return returnItem;
        }

        [HttpPost]
        public ActionResult Export(string searchColumn, string searchValue, string ActionDateTimeStart, string ActionDateTimeEnd)
        {
            DateTime _RequestTimeStart = DateTime.MinValue;
            DateTime _RequestTimeEnd = DateTime.MinValue;

            if (!ActionDateTimeStart.IsNullOrEmpty())
            {
                if (!DateTime.TryParseExact(ActionDateTimeStart, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out _RequestTimeStart))
                {
                    ScreenMessages.Submit(ScreenMessage.Error("Input date is not valid!"));
                    return RedirectToAction("", "AppAuditTrail");
                }
            }
            if (!ActionDateTimeEnd.IsNullOrEmpty())
            {
                if (!DateTime.TryParseExact(ActionDateTimeEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out _RequestTimeEnd))
                {
                    ScreenMessages.Submit(ScreenMessage.Error("Input date is not valid!"));
                    return RedirectToAction("", "AppAuditTrail");
                }
            }

            if (_RequestTimeStart > _RequestTimeEnd)
            {
                ScreenMessages.Submit(ScreenMessage.Error("Input date range is not valid!"));
                return RedirectToAction("", "AppAuditTrail");
            }

            if (_RequestTimeEnd == DateTime.MinValue)
                _RequestTimeEnd = _RequestTimeStart;

            var auditList = _auditRepository.FindForExport(searchColumn, searchValue, _RequestTimeStart, _RequestTimeEnd);
            IList<AuditTrailView> items = auditList.ToList().ConvertAll<AuditTrailView>(new Converter<AppAuditTrail, AuditTrailView>(ConvertFrom));

            DataTable dt = items.ListToDataTable();
            List<AuditTrailView> list = new List<AuditTrailView>();

            dt.Columns.Remove("Id");
            dt.Columns.Remove("TimeStampString");
            dt.Columns.Remove("RequestDateString");
            dt.Columns.Remove("ApproveDateString");
            dt.Columns.Remove("TimeStampStart");
            dt.Columns.Remove("TimeStampEnd");

            dt.Columns["FunctionName"].ColumnName = "Function Name";
            dt.Columns["ObjectName"].ColumnName = "Audit Type";
            dt.Columns["Action"].ColumnName = "Data Type";
            dt.Columns["ObjectValueBefore"].ColumnName = "Before";
            dt.Columns["ObjectValueAfter"].ColumnName = "After";
            dt.Columns["UserMaker"].ColumnName = "Maker";
            dt.Columns["UserChecker"].ColumnName = "Checker";
            dt.Columns["RequestDate"].ColumnName = "Request Date";
            dt.Columns["ApproveDate"].ColumnName = "Approve Date";

            MemoryStream ms = new MemoryStream();
            using (SLDocument sl = new SLDocument())
            {
                sl.ImportDataTable(1, 1, dt, true);
                sl.SelectWorksheet("Worksheet");
                sl.AutoFitColumn(1, 6);
                sl.SaveAs(ms);
            }

            ms.Position = 0;
            string fName = "SVS_Application_Audit_Trail.xls";
            return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fName);
        }

    }
}