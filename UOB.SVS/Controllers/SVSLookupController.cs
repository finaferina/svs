﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using UOB.SVS.Resources;
using UOB.SVS.Model;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System.Linq;
using Treemas.Credential.Model;
using System.Web;

namespace UOB.SVS.Controllers
{
    public class SVSLookupController : PageController
    {
        private ISignatureHRepository _hRepository;
        private IAuditTrailLog _auditRepository;
        private IBranchRepository _branchRepository;

        public SVSLookupController(ISessionAuthentication sessionAuthentication, ISignatureHRepository hRepository,
            IBranchRepository branchRepository, IAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._hRepository = hRepository;
            this._auditRepository = auditRepository;
            this._branchRepository = branchRepository;
            Settings.ModuleName = "SVS Lookup";
            Settings.Title = "SVS Lookup";
        }

        protected override void Startup()
        {

        }

        public ActionResult NoAuthCheckSearch()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                SVSReqFilter filter = new SVSReqFilter();
                filter.Branches = getBranches();
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<SignatureHeader> data;
                if (!searchValue.IsNullOrEmpty())
                {
                    if (searchColumn == "AccountNo")
                        filter.AccountNo = searchValue;
                    else if (searchColumn == "CIFNumber")
                        filter.CIFNumber = searchValue;
                }
                else
                {
                    if (searchColumn == "AccountNo")
                        filter.AccountNo = " ";
                    else if (searchColumn == "CIFNumber")
                        filter.CIFNumber = " ";
                }

                data = _hRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);

                var items = data.Items.ToList().ConvertAll<SignatureHReqView>(new Converter<SignatureHeader, SignatureHReqView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public ActionResult NoAuthCheckSearchInquiry()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                SVSReqFilter filter = new SVSReqFilter();
                //filter.Branches = getBranches();
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<SignatureHeader> data;
                if (!searchValue.IsNullOrEmpty())
                {
                    if (searchColumn == "AccountNo")
                        filter.AccountNo = searchValue;
                    else if (searchColumn == "CIFNumber")
                        filter.CIFNumber = searchValue;
                }
                else
                {
                    if (searchColumn == "AccountNo")
                        filter.AccountNo = " ";
                    else if (searchColumn == "CIFNo")
                        filter.CIFNumber = " ";
                }

                data = _hRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);

                var items = data.Items.ToList().ConvertAll<SignatureHReqView>(new Converter<SignatureHeader, SignatureHReqView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public SignatureHReqView ConvertFrom(SignatureHeader item)
        {
            SignatureHReqView returnItem = new SignatureHReqView();
            returnItem.AccountNo = HttpUtility.HtmlEncode(item.AccountNo);
            returnItem.AccountName = HttpUtility.HtmlEncode(item.AccountName);
            returnItem.AccountType = item.AccountType;
            returnItem.CIFNumber = item.CIFNumber;
            returnItem.BranchCode = item.BranchCode;
            returnItem.Note = HttpUtility.HtmlEncode(item.Note);
            return returnItem;
        }

        public List<Branch> getBranches()
        {
            User user = Lookup.Get<User>();
            IList<string> branchs = new List<string>();
            if (user.BranchCode == "000")
            {
                return _branchRepository.getBranchs().ToList();
            }
            else
            {
                branchs.Add(user.BranchCode);
                return _branchRepository.getBranchs(branchs).ToList();
            }
        }


    }
}