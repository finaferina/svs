﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using UOB.SVS.Resources;
using UOB.SVS.Model;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System.Linq;
using Treemas.Credential.Model;
using System.Drawing.Imaging;
using System.Web;
using System.Drawing;
using UOB.SVS.Model.Helper;

namespace UOB.SVS.Controllers
{
    public class CopySVSCentralizeController : PageController
    {
        private ICentralizeHRepository _hRepository;
        private ICentralizeUnitHReqRepository _signatureHReqRepository;
        private ICentralizeHReqService _signatureHReqService;
        private IParameterCentralUnitRepository _parameterRepository;
        private ICentralizeHReqService _centralizeHReqService;

        private IAccountNoViewRepository _accNoVwRepo;
        private IAuditTrailLog _auditRepository;
        private ICentralUnitDocTypeRepository _doctypeRepository;

        private IRegionRepository _regionRepository;
        private IDivisionRepository _divisionRepository;
        private IDepartementRepository _departementRepository;

        public CopySVSCentralizeController(IParameterCentralUnitRepository parameterRepository,ISessionAuthentication sessionAuthentication, ICentralizeHRepository hRepository,
            ICentralizeUnitHReqRepository signatureHReqRepository, ICentralUnitDocTypeRepository docTypeRepository,
            ICentralizeHReqService signatureHReqService, IAccountNoViewRepository accNoVwRepo, IAuditTrailLog auditRepository,
            IRegionRepository regionRepository, ICentralizeHReqService centralizeHReqService, IDivisionRepository divisionRepository,
            IDepartementRepository departementRepository) : base(sessionAuthentication)
        {
            this._parameterRepository = parameterRepository;
            this._hRepository = hRepository;
            this._signatureHReqRepository = signatureHReqRepository;
            this._signatureHReqService = signatureHReqService;
            this._accNoVwRepo = accNoVwRepo;
            this._auditRepository = auditRepository;
            this._doctypeRepository = docTypeRepository;
            this._regionRepository = regionRepository;
            this._divisionRepository = divisionRepository;
            this._departementRepository = departementRepository;
            this._centralizeHReqService = centralizeHReqService;

            Settings.ModuleName = "CopySVSCentralize";
            Settings.Title = "Copy";
        }

        protected override void Startup()
        {
            Create();
        }

        public ActionResult Create()
        {
            CentralizeUnitHReqView data = new CentralizeUnitHReqView();
            return CreateView(data);
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.
                User user = Lookup.Get<User>();

                CentralizeReqFilter filter = new CentralizeReqFilter();
                string NIK = Request.QueryString.GetValues("NIK")[0];
                string Name = Request.QueryString.GetValues("Name")[0];
                string Titles = Request.QueryString.GetValues("Titles")[0];
                string Region = Request.QueryString.GetValues("Region")[0];

                string Division = string.Empty;
                string Departement = string.Empty;

                if (!(Request.QueryString.GetValues("Division").IsNullOrEmpty()))
                {
                    Division = Request.QueryString.GetValues("Division")[0];
                }

                if (!Request.QueryString.GetValues("Departement").IsNullOrEmpty())
                {
                    Departement = Request.QueryString.GetValues("Departement")[0];
                }
                

                //stepppp
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();

                var sortColumn = "NIK";
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                filter.NIK = NIK;
                filter.Name = Name;
                filter.Titles = Titles;
                filter.Region = Region;
                filter.Division = Division;
                filter.Departement = Departement;

                // Loading.   
                PagedResult<CentralizeHeader> data;

                data = _hRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);
                var items = data.Items.ToList().ConvertAll<CentralizeUnitHReqView>(new Converter<CentralizeHeader, CentralizeUnitHReqView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }


        public CentralizeUnitHReqView ConvertFrom(CentralizeHeader item)
        {
            User user = Lookup.Get<User>();

            CentralizeUnitHReqView returnItem = new CentralizeUnitHReqView();
            returnItem.Note = HttpUtility.HtmlEncode(item.Note);
            returnItem.RequestType = "U";
            returnItem.RequestDate = DateTime.Now;
            returnItem.RequestUser = user.Username;
            returnItem.RequestReason = "";

            returnItem.NIK = item.NIK;
            returnItem.Name = item.Name;
            returnItem.DocType = "";
            returnItem.Titles = item.Titles;
            returnItem.Region = item.Region;
            returnItem.Division = item.Division;
            returnItem.Departement = item.Departement;

            int index = 1;
            if (item.Documents != null)
            {
                IList<CentralizeDocument> docs = new List<CentralizeDocument>();

                foreach (CentralizeDDoc row in item.Documents)
                {
                    CentralizeDocument doc = new CentralizeDocument();
                    doc.ID = row.ID;
                    doc.Index = index++;
                    doc.DocumentType = row.DocumentType;
                    doc.FileBlob = row.FileBlob;
                    doc.FileType = HttpUtility.HtmlEncode(row.FileType);
                    doc.NIK = row.NIK;
                    doc.IsNew = true;
                    doc.FileSeq = row.Seq;
                    doc.IsValidDoc = true;
                    docs.Add(doc);
                }
                returnItem.Documents = docs.ToList();
            }

            if (item.Signatures != null)
            {
                index = 1;
                IList<CentralizeSignature> signs = new List<CentralizeSignature>();
                foreach (CentralizeDSign row in item.Signatures)
                {
                    CentralizeSignature sign = new CentralizeSignature();
                    sign.ID = row.ID;
                    sign.Index = index++;
                    sign.CroppedBlob = HttpUtility.HtmlEncode(row.Signature);
                    sign.ImageType = HttpUtility.HtmlEncode(row.ImageType);
                    sign.NIK = row.NIK;
                    sign.IsNew = true;
                    sign.IsValidImage = row.Signature.Length % 4 == 0 ? true : false;
                    signs.Add(sign);
                }
                returnItem.Signatures = signs.ToList();
            }

            return returnItem;
        }

        public CentralizeUnitHReqView ConvertFromReq(CentralizeUnitHRequest item)
        {
            CentralizeUnitHReqView returnItem = new CentralizeUnitHReqView();
            returnItem.Note = HttpUtility.HtmlEncode(item.Note);
            returnItem.RequestType = item.RequestType;
            returnItem.RequestDate = item.RequestDate;
            returnItem.RequestUser = item.RequestUser;
            returnItem.RequestReason = item.RequestReason;

            int index = 1;
            if (item.Documents != null)
            {
                IList<CentralizeDocument> docs = new List<CentralizeDocument>();
                foreach (CentralizeDDocReq row in item.Documents)
                {
                    CentralizeDocument doc = new CentralizeDocument();
                    doc.ID = row.ID;
                    doc.Index = index++;
                    doc.DocumentType = row.DocumentType;
                    doc.FileBlob = HttpUtility.HtmlEncode(row.FileBlob).IsNull() ? "null" : HttpUtility.HtmlEncode(row.FileBlob);  //row.FileBlob;
                    doc.FileType = row.FileType;
                    doc.NIK = row.NIK;
                    doc.IsNew = false;
                    doc.IsValidDoc = true;
                    docs.Add(doc);
                }
                returnItem.Documents = docs.ToList();
            }

            if (item.Signatures != null)
            {
                index = 1;
                IList<CentralizeSignature> signs = new List<CentralizeSignature>();
                foreach (CentralizeDSignReq row in item.Signatures)
                {
                    CentralizeSignature sign = new CentralizeSignature();
                    sign.ID = row.ID;
                    sign.Index = index++;
                    sign.CroppedBlob = HttpUtility.HtmlEncode(row.Signature);
                    sign.ImageType = HttpUtility.HtmlEncode(row.ImageType);
                    sign.NIK = row.NIK;
                    sign.IsNew = false;
                    sign.IsValidImage = row.Signature.Length % 4 == 0 ? true : false;
                    signs.Add(sign);
                }
                returnItem.Signatures = signs.ToList();
            }
            return returnItem;
        }

        public ActionResult Edit(string Id)
        {
            User user = Lookup.Get<User>();
            Parameter param = Lookup.Get<Parameter>();
            ViewData["ActionName"] = "Copy";
            ViewData["Parameter"] = param;

            CentralizeHeader reqH = _hRepository.getCentralizeHeader(Id);
            if (reqH.IsNull())
            {
                ScreenMessages.Submit(ScreenMessage.Error("Account number does not exist!"));
                return RedirectToAction("Index");
            }

            //reqH.IdentityCards = _hRepository.getKTPDet(reqH.AccountNo);
            reqH.Documents = _hRepository.getDocDet(reqH.NIK);
            reqH.Signatures = _hRepository.getSignDet(reqH.NIK);

            CentralizeUnitHReqView data = ConvertFrom(reqH);
            Session["SignHReqData"] = data;
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult Edit(CentralizeUnitHReqView m)
        {
            User user = Lookup.Get<User>();
            Parameter param = Lookup.Get<Parameter>();
            ViewData["ActionName"] = "Copy";
            ViewData["Parameter"] = param;

            CentralizeHeader reqH = _hRepository.getCentralizeHeader(m.NIK);
            if (reqH.IsNull())
            {
                ScreenMessages.Submit(ScreenMessage.Error("Account number does not exist!"));
                return RedirectToAction("Index");
            }

            //reqH.IdentityCards = _hRepository.getKTPDet(reqH.AccountNo);
            reqH.Documents = _hRepository.getDocDet(reqH.NIK);
            reqH.Signatures = _hRepository.getSignDet(reqH.NIK);

            CentralizeUnitHReqView data = ConvertFrom(reqH);
            return CreateView(data);
        }

        // GET: Function/Create
        public ActionResult Copy()
        {
            Parameter param = Lookup.Get<Parameter>();

            ViewData["ActionName"] = "Copy";
            ViewData["Parameter"] = param;
            CentralizeUnitHReqView data = new CentralizeUnitHReqView();
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult Copy(CentralizeUnitHReqView data)
        {
            ViewData["ActionName"] = "Copy";
            User user = Lookup.Get<User>();

            string message = "";
            
            CentralizeHeader reqH = _hRepository.getCentralizeHeader(data.NIK);
            if (!reqH.IsNull())
            {
                message = "Account number is Already exist!";
            }

            string returnCode = "";
            string warningmessage = "";
            string NIK = "";

            try
            {
                if (message.IsNullOrEmpty())
                {
                    CentralizeUnitHRequest newData = new CentralizeUnitHRequest("");
                    newData.Note = HttpUtility.HtmlDecode(data.Note);
                    newData.RequestType = "C";
                    newData.RequestDate = DateTime.Now;
                    newData.RequestUser = user.Username;
                    newData.RequestReason = "Copy SVS";
                    newData.IsRejected = false;
                    newData.NIK = data.NIK;
                    newData.Name = data.Name;
                    newData.Titles = data.Titles;
                    newData.Region = data.Region;
                    newData.Division = data.Division;
                    newData.Departement = data.Departement;

                    if (data.Signatures != null)
                    {
                        foreach (var item in data.Signatures)
                        {
                            item.NIK = data.NIK;
                            message = SignatureSave(item);
                            if (!message.IsNullOrEmpty())
                                throw new Exception(message);
                        }
                    }
                    /*Save Doc*/

                    if (data.Documents != null)
                    {
                        foreach (var item in data.Documents)
                        {
                            item.NIK = data.NIK;
                            message = DocSave(item);
                            if (!message.IsNullOrEmpty())
                                throw new Exception(message);
                        }
                    }

                    //warningmessage = validateAccount(data, accMaster, user);
                    if (warningmessage == "" || warningmessage == SvsResources.Validation_AccountMasterNotFound)
                    {
                        message = _signatureHReqService.SaveCentralizeReq(newData);
                    }
                    else
                    {
                        message = warningmessage;
                    }

                    // Hapus detail data, biar audit trailnya ngak penuh
                    newData.Documents = null;
                    newData.Signatures = null;
                    //newData.IdentityCards = null;
                }
            }
            catch (Exception e)
            {
                message = e.Message;
                returnCode = "ERR";
                //ScreenMessages.Submit(ScreenMessage.Error(message));
            }

            if (message.IsNullOrEmpty())
            {
                returnCode = "OK";
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
            }
            else
            {
                returnCode = "ERR";
                //ScreenMessages.Submit(ScreenMessage.Error(message));
            }

            JsonResult result = new JsonResult();
            result = this.Json(new
            {
                message = message,
                returnCode = returnCode,
                warningMessage = warningmessage,
                NIK = NIK
            });

            return result;
        }
        public string DocSave(CentralizeDocument data)
        {
            User user = Lookup.Get<User>();
            string message = "";
            string NewFileBlob = "";
            try
            {
                //ParameterCentralUnit param = Lookup.Get<ParameterCentralUnit>();
                ParameterCentralUnit param = _parameterRepository.getParameter();
                if (data.FileBlob.Length % 4 != 0)
                {
                    message = "Invalid uploaded document format!";
                }
                if (data.FileBlob.Length > param.GetSizeDocument)
                {
                    message = "Document " + data.FileName + " exceed maximum file size (" + (data.FileBlob.Length * 1365) + " KB) and cannot be uploaded!";
                }

                if (data.FileBlob == "null")
                {
                    CentralizeDDoc dDoc = _hRepository.getDocDet(data.ID);
                    NewFileBlob = dDoc.FileBlob;
                }

                if (message.IsNullOrEmpty())
                {
                    //CentralizeUnitHRequest reqH = _unithreqRepository.getCentralizeUnitHReq(data.NIK);

                    //if (reqH == null)
                    //{
                    //    message = SvsResources.Validation_AccountNotFound;
                    //}
                    //else if (NewFileBlob == "null")
                    if (NewFileBlob == "null")
                    {
                        message = "Invalid Document " + data.DocumentType;
                    }
                    else
                    {
                        CentralizeDDocReq doc = new CentralizeDDocReq(0);
                        doc.NIK = data.NIK;
                        doc.DocumentType = data.DocumentType;
                        doc.FileBlob = data.FileBlob == "null" ? NewFileBlob : data.FileBlob;
                        doc.FileType = data.FileType;
                        doc.FileSeq = data.Index;
                        doc.RequestType = "C";
                        doc.RequestDate = DateTime.Now;
                        doc.RequestUser = user.Username;
                        doc.RequestReason = "Create New";
                        message = _centralizeHReqService.SaveDocument(doc);
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
                //returnCode = "ERR";
            }

            if (message.IsNullOrEmpty())
            {
                message = "";
                //returnCode = "OK";
            }
            else
            {
                ScreenMessages.Submit(ScreenMessage.Error(message));
                //returnCode = "ERR";
            }

            return message;
        }

        public string SignatureSave(CentralizeSignature data)
        {
            User user = Lookup.Get<User>();
            //_unithreqRepository.IsDuplicate(data.NIK);
            //_doctypeRepository
            string message = "";
            try
            {
                //ParameterCentralUnit param = Lookup.Get<ParameterCentralUnit>();
                ParameterCentralUnit param = _parameterRepository.getParameter();

                if (data.CroppedBlob.Length % 4 != 0)
                {
                    message = "Invalid uploaded image format!";
                    throw new Exception(message);
                }
                if (data.CroppedBlob.Length > param.GetSizeSignature)
                {
                    message = "Signature image " + data.FileName + " exceed maximum file size (" + (data.CroppedBlob.Length * 1365) + " KB) and cannot be uploaded!, (JPG/JPEG Recommended format)";
                    //message = SvsResources.Validation_MaxSizeExceeded;
                    throw new Exception(message);
                }

                if (message.IsNullOrEmpty())
                {
                    //CentralizeUnitHRequest reqH = _unithreqRepository.getCentralizeUnitHReq(data.NIK);
                    //if (reqH == null)
                    //{
                    //    message = SvsResources.Validation_AccountNotFound;
                    //    throw new Exception(message);
                    //}
                    //else
                    //{
                    CentralizeDSignReq sign = new CentralizeDSignReq(0);
                    sign.NIK = data.NIK;
                    sign.Signature = data.CroppedBlob;
                    sign.ImageType = data.ImageType;
                    sign.RequestType = "C";
                    sign.FileSeq = data.Index;
                    sign.RequestDate = DateTime.Now;
                    sign.RequestUser = user.Username;
                    sign.RequestReason = "Create New";
                    message = _centralizeHReqService.SaveSign(sign);
                    //}

                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }

        private ViewResult CreateView(CentralizeUnitHReqView data)
        {
            //filter falah
            List<RoleDocType> authDocs = Lookup.Get<List<RoleDocType>>();
            ViewData["DocCount"] = authDocs.Count;

            ViewData["AllDocTypeMaster"] = createAllDocTypeSelect();
            ViewData["DocTypeMaster"] = createDocTypeSelect("");
            ViewData["RegionMaster"] = createRegionSelect("");
            ViewData["DivisionMaster"] = createDivisionSelect(data.Region.IsNullOrEmpty() ? "-" : data.Region, data.Division.IsNullOrEmpty() ? "-" : data.Division);
            ViewData["DepartementMaster"] = createDepartementSelect(data.Region.IsNullOrEmpty() ? "-" : data.Region, data.Division.IsNullOrEmpty() ? "-" : data.Division, data.Departement.IsNullOrEmpty() ? "-" : data.Departement);
            return View("Detail", data);
        }

        private IList<SelectListItem> createRegionSelect(string selected)
        {
            IList<SelectListItem> dataList = _regionRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Treemas.Credential.Model.Region, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Value);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (!selected.IsNullOrEmpty())
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }

        private IList<SelectListItem> createDivisionSelect(string Region, string selected)
        {
            IList<SelectListItem> dataList = _divisionRepository.getDivisionbyRegion(Region).ToList().ConvertAll<SelectListItem>(new Converter<Division, SelectListItem>(ConvertFrom));
            IEnumerable<SelectListItem> sortedEnum = null;
            IList<SelectListItem> sortedList = null;

            if ((selected != "-"))
            {
                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                if (dataList.Count > 0)
                {
                    sortedList.FindElement(item => item.Value == selected).Selected = true;
                }
            }
            else
            {
                SelectListItem item = new SelectListItem();
                item.Text = "Select";
                item.Value = "";
                dataList.Add(item);

                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                sortedList.FindElement(x => x.Value == "").Selected = true;
            }

            return sortedList;
        }
        private IList<SelectListItem> createDepartementSelect(string Region, string Division, string selected)
        {
            IList<SelectListItem> dataList = _departementRepository.getDepartementbyRegion(Region, Division).ToList().ConvertAll<SelectListItem>(new Converter<Departement, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = null;
            IList<SelectListItem> sortedList = null;

            if ((selected != "-"))
            {
                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                if (dataList.Count > 0)
                {
                    sortedList.FindElement(item => item.Value == selected).Selected = true;
                }
            }
            else
            {
                SelectListItem item = new SelectListItem();
                item.Text = "Select";
                item.Value = "";
                dataList.Add(item);

                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                sortedList.FindElement(x => x.Value == "").Selected = true;
            }

            return sortedList;
        }

        public SelectListItem ConvertFrom(Treemas.Credential.Model.Region item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }

        public SelectListItem ConvertFrom(Division item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }

        public SelectListItem ConvertFrom(Departement item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }

        private string validateData(CentralizeUnitHReqView data, string mode, AccountMaster accMaster, User user)
        {
            user = Lookup.Get<User>();
            return "";
        }

        private string validateAccount(CentralizeUnitHReqView data, AccountMaster accMaster, User user)
        {
            if (accMaster == null)
            {
                return SvsResources.Validation_AccountMasterNotFound;
            }

            string message = "";
            return message;
        }

        #region Dropdownlist

        private IList<SelectListItem> createDocTypeSelect(string selected)
        {
            //List<RoleDocType> authDocs = Lookup.Get<List<RoleDocType>>();
            List<CentralUnitDocType> docList = _doctypeRepository.FindAll().ToList();
            List<CentralUnitDocType> finalDocList = new List<CentralUnitDocType>();
            foreach (CentralUnitDocType item in docList)
            {
                //var found = authDocs.FindElement(x => x.DocumentTypeId == item.Id);
                //if (!found.IsNull())
                //{
                    finalDocList.Add(item);
                //}
            }

            IList<SelectListItem> dataList = finalDocList.ConvertAll<SelectListItem>(new Converter<CentralUnitDocType, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (!selected.IsNullOrEmpty())
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }
        private IList<SelectListItem> createAllDocTypeSelect()
        {
            List<CentralUnitDocType> docList = _doctypeRepository.FindAll().ToList();

            IList<SelectListItem> dataList = docList.ConvertAll<SelectListItem>(new Converter<CentralUnitDocType, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
            IList<SelectListItem> sortedList = sortedEnum.ToList();
            return sortedList;
        }

        public SelectListItem ConvertFrom(CentralUnitDocType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }

        private IList<SelectListItem> createAccountTypeSelect(string selected)
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = "CS", Text = "CA/SA" });
            dataList.Insert(1, new SelectListItem() { Value = "TD", Text = "TD" });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString().Trim()).Selected = true;
            }
            return dataList;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult ChangeDropdownRegion(string Region)
        {
            CentralizeUnitHReqView item = new CentralizeUnitHReqView();
            item.Divisions = _divisionRepository.getDivisionbyRegion(Region);

            return Json(item, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult ChangeDropdownDepartements(string region, string division = "")
        {
            CentralizeUnitHReqView item = new CentralizeUnitHReqView();
            item.Departements = _departementRepository.getDepartementbyRegion(region, division);

            return Json(item, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult NoAuthCheckViewDocument(int ID)
        {
            JsonResult result = new JsonResult();
            CentralizeDDoc doc = _hRepository.getDocDet(ID);
            if (doc.IsNull())
            {
                return Content("Not exist or invalid document");
            }
            byte[] blob = Convert.FromBase64String(doc.FileBlob);

            string filetype = doc.FileType.Split(';')[0].Split(':')[1];
            if (filetype.Trim() == "application/pdf")
            {
                return File(blob, filetype);
            }
            else
            {
                Stream strm = new MemoryStream(Convert.FromBase64String(doc.FileBlob));
                Bitmap bmp = (Bitmap)Bitmap.FromStream(strm);

                ImageFormatConverter imgConverter = new ImageFormatConverter();
                var png = imgConverter.ConvertFromString("PNG");

                MemoryStream ms = new MemoryStream();
                bmp.Save(ms, (ImageFormat)png);

                byte[] bytImg = ms.ToArray();
                string strImg = Convert.ToBase64String(bytImg);
                return Content("<img src='data:image/PNG;base64," + strImg + "' alt='" + doc.DocumentType + "'/>");
            }
        }

        #endregion
    }
}