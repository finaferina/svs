﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;

namespace UOB.SVS.Controllers
{
    public class DivisionController : PageController
    {
        private string _application = "SVS";
        private IDivisionRepository _divisionRepository;
        private IDivisionCrRepository _divisionCrRepository;
        private IApplicationRepository _appRepository;
        private IAppAuditTrailLog _auditRepository;
        private IRegionRepository _regionRepository;
        private IDepartementRepository _departementRepository;
        public DivisionController(ISessionAuthentication sessionAuthentication, IDivisionRepository divisionRepository, IDivisionCrRepository divisionCrRepository,
            IApplicationRepository appRepository, IAppAuditTrailLog auditRepository, IRegionRepository regionRepository, 
            IDepartementRepository departementRepository) : base(sessionAuthentication)
        {
            this._divisionRepository = divisionRepository;
            this._divisionCrRepository = divisionCrRepository;
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;
            this._regionRepository = regionRepository;
            this._departementRepository = departementRepository;
            Settings.ModuleName = "Division";
            Settings.Title = "Division / Branch";
        }

        protected override void Startup()
        {
            ViewData["loadState"] = (Request.QueryString.GetValues("loadState") == null) ? "0" : Request.QueryString.GetValues("loadState").FirstOrDefault();
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<Division> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _divisionRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _divisionRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<DivisionView>(new Converter<Division, DivisionView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public DivisionView ConvertFrom(Division item)
        {
            DivisionView returnItem = new DivisionView();
            returnItem.Application = item._Application;
            returnItem.Alias = item.Alias;
            returnItem.DivisionId = item.DivisionId;
            returnItem.Id = item.Id;
            returnItem.Name = item.Name;
            returnItem.Region = item.Region;
            return returnItem;
        }

        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            DivisionView data = new DivisionView();
            return CreateView(data);
        }

        private ViewResult CreateView(DivisionView data)
        {
            ViewData["RegionMaster"] = createRegionSelect(data.Region);
            return View("Detail", data);
        }

        private IList<SelectListItem> createRegionSelect(string selected)
        {
            IList<SelectListItem> dataList = _regionRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Region, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Value);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (!selected.IsNullOrEmpty())
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }

        public SelectListItem ConvertFrom(Region item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }

        // POST: Function/Create
        [HttpPost]
        public ActionResult Create(DivisionView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    DivisionCr newData = new DivisionCr(0L);
                    newData._Application = _application;
                    newData.DivisonId = data.DivisionId;
                    newData.Name = data.Name;
                    newData.Alias = data.Alias;
                    newData.Region = data.Region;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    newData.ApprovalType = "C";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;
                    //_regionRepository.Add(newData);
                    _divisionCrRepository.Add(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success("Data Berhasil Di Tambahkan"));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(DivisionView data, string mode)
        {
            if (data.Name == "")
                return "Nama Tidak Boleh Kosong";

            if (data.DivisionId == "")
                return "Division ID Tidak Boleh Kosong";

            if (mode == "Create")
            {
                if (_divisionRepository.IsDuplicate(_application, data.DivisionId))
                    return "Division ID dan Nama Sudah Pernah Ada";
                else if (_divisionRepository.IsDuplicateCR(_application, data.DivisionId, "N"))
                    return "Division ID dan Nama Sudah Ada di Approval";

                if (_divisionRepository.IsDuplicateByAlias(_application, data.Alias))
                    return "Alias Sudah Pernah Ada";
                else if (_divisionRepository.IsDuplicateCRByAlias(_application, data.Alias, "N"))
                    return "Alias Sudah Ada di Approval";
            }
            return "";
        }

        // GET: Function/Edit/5
        public ActionResult Edit(long id)
        {
            ViewData["ActionName"] = "Edit";
            Division autoDivision = _divisionRepository.getDivision(id);
            DivisionView data = ConvertFrom(autoDivision);

            return CreateView(data);
        }

        // POST: Function/Edit/5
        [HttpPost]
        public ActionResult Edit(DivisionView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    //new additional by fahmi
                    Division app = _divisionRepository.getDivision(data.Id);
                    //if (!app.IsNull())
                    //{
                    //    _divisionRepository.UpdateDivision(data.Id, data.DivisionId, data.Name, data.Alias, data.Region, user.Username, DateTime.Now);
                    //}
                    DivisionCr newData = new DivisionCr(0L);
                    newData._Application = _application;
                    newData.DivisonId = data.DivisionId;
                    newData.Name = data.Name;
                    newData.Alias = data.Alias;
                    newData.Region = data.Region;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    newData.ApprovalType = "U";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;
                    _divisionCrRepository.Add(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success("Data Berhasil di Ubah"));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        public ActionResult Delete(long id)
        {
            ViewData["ActionName"] = "Delete";
            Division autoDivision = _divisionRepository.getDivision(id);
            DivisionView data = ConvertFrom(autoDivision);

            return CreateView(data);
        }

        // POST: Function/Delete/5
        [HttpPost]
        public ActionResult Delete(DivisionView data)
        {
            User user = Lookup.Get<User>();
            Division divisiondata = _divisionRepository.getDivision(data.Id);
            string message = "";
            try
            {
                message = validateDelete(divisiondata.Alias);
                if (message.IsNullOrEmpty())
                {
                    //_divisionRepository.Remove(divisiondata);
                    DivisionCr newData = new DivisionCr(0L);
                    newData._Application = divisiondata._Application;
                    newData.DivisonId = divisiondata.DivisionId;
                    newData.Name = divisiondata.Name;
                    newData.Alias = divisiondata.Alias;
                    newData.Region = divisiondata.Region;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    newData.ApprovalType = "D";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;

                    _divisionCrRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success("Data Berhasil di Hapus"));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(data.Id);
        }
        private string validateDelete(string divisonid)
        {
            //if (_departementRepository.IsFoundRegion(divisonid))
            //    return "Region Masih Terdaftar di Departement";

            if (_departementRepository.IsFoundDivision(divisonid))
                return "Departement is existed row data";

            return "";
        }
    }
}