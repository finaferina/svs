﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;
using System.Globalization;
using Treemas.Base.Resources;
using UOB.SVS.Interface;
using UOB.SVS.Model;

namespace UOB.SVS.Controllers
{
    public class AccountPrefixController : PageController
    {
        private IUserRepository _userRepository;
        private IAccountPrefixRepository _accountPrefixRepository;
        private IAccountPrefixCrRepository _accountPrefixCrRepository;
        private IBranchAccountPrefixRepository _branchAccountPrefixRepository;
        private IAppAuditTrailLog _auditRepository;
        public AccountPrefixController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository, IAccountPrefixRepository accountPrefixRepository, IAccountPrefixCrRepository accountPrefixCrRepository,
            IBranchAccountPrefixRepository branchAccountPrefixRepository, IAppAuditTrailLog auditRepository) : base(sessionAuthentication)
        {
            this._accountPrefixRepository = accountPrefixRepository;
            this._userRepository = userRepository;
            this._accountPrefixCrRepository = accountPrefixCrRepository;
            this._auditRepository = auditRepository;
            this._branchAccountPrefixRepository =  branchAccountPrefixRepository;

            Settings.ModuleName = "Master Account Prefix";
            Settings.Title = "Master Account Prefix";
        }

        protected override void Startup()
        {
            ViewData["loadState"] = (Request.QueryString.GetValues("loadState") == null) ? "0" : Request.QueryString.GetValues("loadState").FirstOrDefault();
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.  

                PagedResult<AccountPrefix> appData;
                if (searchValue.IsNullOrEmpty())
                {
                    appData = _accountPrefixRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    appData = _accountPrefixRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = appData.Items.ToList().ConvertAll<AccountPrefixView>(new Converter<AccountPrefix, AccountPrefixView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = appData.TotalItems,
                    recordsFiltered = appData.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public AccountPrefixView ConvertFrom(AccountPrefix item)
        {
            AccountPrefixView returnItem = new AccountPrefixView();

            returnItem.Id = item.Id;
            returnItem.PrefixCode = item.PrefixCode;
            returnItem.Description = item.Description;
            returnItem.CreatedDate = item.CreatedDate.IsNull() ? null : Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
            returnItem.CreatedBy = item.CreatedBy;
            returnItem.ChangedDate = item.ChangedDate.IsNull() ? null : Convert.ToDateTime(item.ChangedDate).ToString("dd/MM/yyyy");
            returnItem.ChangedBy = item.ChangedBy;
            return returnItem;
        }

        public SelectListItem ConvertFrom(AccountPrefixView item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.PrefixCode;
            returnItem.Value = item.Description;
            return returnItem;
        }

        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            AccountPrefixView data = new AccountPrefixView();
            return CreateView(data);
        }

        private ViewResult CreateView(AccountPrefixView data)
        {
            return View("Detail", data);
        }

        [HttpPost]
        public ActionResult Create(AccountPrefixView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    AccountPrefixCr currentReq = _accountPrefixCrRepository.getExistingRequest("C", data.PrefixCode);
                    if (!currentReq.IsNull())
                    {
                        ScreenMessages.Submit(ScreenMessage.Error("Create failed, accountPrefix code " + data.PrefixCode.Trim() + " is awaiting for CREATE approval!"));
                        CollectScreenMessages();
                        return CreateView(data);
                    }

                    AccountPrefixCr newData = new AccountPrefixCr(0L);
                    newData.PrefixCode = data.PrefixCode;
                    newData.Description = data.Description;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;

                    newData.ApprovalType = "C";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;

                    _accountPrefixCrRepository.Add(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("", "AccountPrefix");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(AccountPrefixView data, string mode)
        {
            if (data.PrefixCode == "")
                return "Please fill Prefix Code";
            
            if (mode == "Create")
            {
                if (_accountPrefixRepository.IsDuplicate(data.PrefixCode))
                    return "Duplicate Prefix Code";
            }
            return "";
        }

        public ActionResult Edit(long id)
        {
            ViewData["ActionName"] = "Edit";
            AccountPrefix accountPrefix = _accountPrefixRepository.getAccountPrefix(id);
            AccountPrefixView data = ConvertFrom(accountPrefix);
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult Edit(AccountPrefixView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");

                AccountPrefix accData = _accountPrefixRepository.getAccountPrefix(data.Id);
                if (accData.Description == data.Description)
                {
                    ScreenMessages.Submit(ScreenMessage.Error("No changes to submitted!"));
                    CollectScreenMessages();
                    return CreateView(data);
                }

                AccountPrefixCr currentReq = _accountPrefixCrRepository.getExistingRequest("U", data.PrefixCode);
                if (!currentReq.IsNull())
                {
                    ScreenMessages.Submit(ScreenMessage.Error("Update failed, accountPrefix code " + data.PrefixCode.Trim() + " is awaiting for UPDATE approval!"));
                    CollectScreenMessages();
                    return CreateView(data);
                }
                AccountPrefixCr currentDReq = _accountPrefixCrRepository.getExistingRequest("D", data.PrefixCode);
                if (!currentDReq.IsNull())
                {
                    ScreenMessages.Submit(ScreenMessage.Error("Update failed, accountPrefix code " + data.PrefixCode.Trim() + " is awaiting for DELETE approval!"));
                    CollectScreenMessages();
                    return CreateView(data);
                }


                if (message.IsNullOrEmpty())
                {
                    AccountPrefixCr newData = new AccountPrefixCr(0L);

                    newData.PrefixCode = data.PrefixCode;
                    newData.Description = data.Description;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;

                    newData.ApprovalType = "U";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;

                    _accountPrefixCrRepository.Add(newData);

                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("", "AccountPrefix");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult Delete(long Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                AccountPrefix appData = _accountPrefixRepository.getAccountPrefix(Id);
                bool branchAccPref = _branchAccountPrefixRepository.IsDuplicate(appData.PrefixCode);
                
                if (branchAccPref)
                {
                    ScreenMessages.Submit(ScreenMessage.Error("Delete failed, accountPrefix code " + appData.PrefixCode.Trim() + " is still used by any Branch!"));
                    CollectScreenMessages();
                    return Edit(Id);
                }

                AccountPrefixCr currentUReq = _accountPrefixCrRepository.getExistingRequest("U", appData.PrefixCode);
                if (!currentUReq.IsNull())
                {
                    ScreenMessages.Submit(ScreenMessage.Error("Delete failed, accountPrefix code " + appData.PrefixCode.Trim() + " is awaiting for UPDATE approval!"));
                    CollectScreenMessages();
                    return Edit(Id);
                }

                AccountPrefixCr currentDReq = _accountPrefixCrRepository.getExistingRequest("D", appData.PrefixCode);
                if (!currentDReq.IsNull())
                {
                    ScreenMessages.Submit(ScreenMessage.Error("Delete failed, accountPrefix code " + appData.PrefixCode.Trim() + " is awaiting for DELETE approval!"));
                    CollectScreenMessages();
                    return Edit(Id);
                }

                AccountPrefixCr newData = new AccountPrefixCr(0L);

                newData.PrefixCode = appData.PrefixCode;
                newData.Description = appData.Description;

                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;

                newData.ApprovalType = "D";
                newData.ApprovalStatus = "N";
                newData.Approved = false;

                _accountPrefixCrRepository.Add(newData);

            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("", "AccountPrefix");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
        
    }
}