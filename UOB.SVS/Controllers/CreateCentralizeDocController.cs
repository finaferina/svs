﻿using System;
using System.IO;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using UOB.SVS.Resources;
using UOB.SVS.Model;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System.Linq;
using Treemas.Credential.Model;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Drawing;
using System.Web;

namespace UOB.SVS.Controllers
{
    public class CreateCentralizeDocController : PageController
    {
        //private IParameterRepository _parameterRepository;
        private IParameterCentralUnitRepository _parameterRepository;
        private IRegionRepository _regionRepository;
        private IDivisionRepository _divisionRepository;
        private IDepartementRepository _departementRepository;
        private ICentralUnitDocTypeRepository _doctypeRepository;
        private ICentralizeUnitHReqRepository _unithreqRepository;
        private ICentralizeHReqService _centralizeHReqService;
        private ICentralizeHRepository _hRepository;

        //private IList<RoleDocType> roleDocType;

        public CreateCentralizeDocController(ISessionAuthentication sessionAuthentication, IParameterCentralUnitRepository parameterRepository, IRegionRepository regionRepository,
            IDivisionRepository divisionRepository, IDepartementRepository departementRepository, ICentralUnitDocTypeRepository unitDocTypeRepository, 
            ICentralizeUnitHReqRepository centralizeUnitHReqRepository, ICentralizeHReqService centralizeHReqService, ICentralizeHRepository centralizeHRepository
            ) : base(sessionAuthentication)
        {
            this._parameterRepository = parameterRepository;
            this._regionRepository = regionRepository;
            this._divisionRepository = divisionRepository;
            this._departementRepository = departementRepository;
            this._doctypeRepository = unitDocTypeRepository;
            this._unithreqRepository = centralizeUnitHReqRepository;
            this._centralizeHReqService = centralizeHReqService;
            this._hRepository = centralizeHRepository;
            Settings.ModuleName = "CreateCentralizeDoc";
            Settings.Title = "Add New";
        }
        protected override void Startup()
        {

            Create();
            double BlobSize = 0;
            Session.Add("BlobSize", BlobSize);
        }
        public ActionResult Create()
        {
            //List<RoleDocType> authDocs = Lookup.Get<List<RoleDocType>>();
            //ViewData["DocCount"] = authDocs.Count;
            Response.Clear();

            Parameter param = Lookup.Get<Parameter>();

            ViewData["ActionName"] = "Create";
            ViewData["Parameter"] = param;

            CentralizeUnitHReqView data = new CentralizeUnitHReqView();
            return CreateView(data);
        }
        private ViewResult CreateView(CentralizeUnitHReqView data)
        {
            //List<RoleDocType> authDocs = Lookup.Get<List<RoleDocType>>();
            //ViewData["DocCount"] = authDocs.Count;

            ViewData["DocTypeMaster"] = createDocTypeSelect("");
            ViewData["RegionMaster"] = createRegionSelect("");
            ViewData["DivisionMaster"] = createDivisionSelect(data.Region.IsNullOrEmpty() ? "-" : data.Region, data.Division.IsNullOrEmpty() ? "-" : data.Division);
            ViewData["DepartementMaster"] = createDepartementSelect(data.Region.IsNullOrEmpty() ? "-" : data.Region, data.Division.IsNullOrEmpty() ? "-" : data.Division, data.Departement.IsNullOrEmpty() ? "-" : data.Departement);
            ViewData["AllDocTypeMaster"] = createAllDocTypeSelect();
            return View("Detail", data);
        }
        private IList<SelectListItem> createDocTypeSelect(string selected)
        {
            List<CentralUnitDocType> docList = _doctypeRepository.FindAll().ToList();
            List<CentralUnitDocType> finalDocList = new List<CentralUnitDocType>();
            foreach (CentralUnitDocType item in docList)
            {
                //var found = authDocs.FindElement(x => x.DocumentTypeId == item.Id);
                //if (!found.IsNull())
                //{
                    finalDocList.Add(item);
                //}
            }

            IList<SelectListItem> dataList = finalDocList.ConvertAll<SelectListItem>(new Converter<CentralUnitDocType, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (!selected.IsNullOrEmpty())
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }
        private IList<SelectListItem> createRegionSelect(string selected)
        {
            IList<SelectListItem> dataList = _regionRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Treemas.Credential.Model.Region, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Value);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (!selected.IsNullOrEmpty())
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }

        private IList<SelectListItem> createDivisionSelect(string Region, string selected)
        {
            IList<SelectListItem> dataList = _divisionRepository.getDivisionbyRegion(Region).ToList().ConvertAll<SelectListItem>(new Converter<Division, SelectListItem>(ConvertFrom));
            IEnumerable<SelectListItem> sortedEnum = null;
            IList<SelectListItem> sortedList = null;

            if ((selected != "-"))
            {
                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                if (dataList.Count > 0)
                {
                    sortedList.FindElement(item => item.Value == selected).Selected = true;
                }
            }
            else
            {
                SelectListItem item = new SelectListItem();
                item.Text = "Select";
                item.Value = "";
                dataList.Add(item);

                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                sortedList.FindElement(x => x.Value == "").Selected = true;
            }

            return sortedList;
        }

        private IList<SelectListItem> createDepartementSelect(string Region, string Division, string selected)
        {
            IList<SelectListItem> dataList = _departementRepository.getDepartementbyRegion(Region, Division).ToList().ConvertAll<SelectListItem>(new Converter<Departement, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = null;
            IList<SelectListItem> sortedList = null;

            if ((selected != "-"))
            {
                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                if (dataList.Count > 0)
                {
                    sortedList.FindElement(item => item.Value == selected).Selected = true;
                }
            }
            else
            {
                SelectListItem item = new SelectListItem();
                item.Text = "Select";
                item.Value = "";
                dataList.Add(item);

                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                sortedList.FindElement(x => x.Value == "").Selected = true;
            }

            return sortedList;
        }

        private IList<SelectListItem> createAllDocTypeSelect()
        {
            List<CentralUnitDocType> docList = _doctypeRepository.FindAll().ToList();

            IList<SelectListItem> dataList = docList.ConvertAll<SelectListItem>(new Converter<CentralUnitDocType, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
            IList<SelectListItem> sortedList = sortedEnum.ToList();
            return sortedList;
        }
        public SelectListItem ConvertFrom(CentralUnitDocType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }
        public SelectListItem ConvertFrom(Treemas.Credential.Model.Region item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }
        public SelectListItem ConvertFrom(Division item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }
        public SelectListItem ConvertFrom(Departement item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }
        // POST: Create new Data
        [HttpPost]
        public ActionResult Create(CentralizeUnitHReqView data)
        {
            Response.ClearContent();

            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            string returnCode = "";
            string warningmessage = "";
            string NIK = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    CentralizeUnitHRequest newData = new CentralizeUnitHRequest("");
                    NIK = data.NIK;
                    newData.NIK = data.NIK;
                    newData.Name = data.Name;
                    newData.DocType = null;
                    newData.Region = data.Region;
                    newData.Division = data.Division;
                    newData.Departement = data.Departement;
                    newData.Titles = data.Titles;
                    newData.Note = data.Note;
                    newData.RequestType = "C";
                    newData.RequestDate = DateTime.Now;
                    newData.RequestUser = user.Username;
                    newData.RequestReason = "Create New";
                    newData.IsRejected = false;

                    /*Save Signature*/
                    if (data.Signatures != null)
                    {
                        foreach (var item in data.Signatures)
                        {
                            item.NIK = data.NIK;
                            message = SignatureSave(item);
                            if (!message.IsNullOrEmpty())
                                throw new Exception(message);
                        }
                    }
                    /*Save Doc*/

                    if (data.Documents != null) { 
                        foreach (var item in data.Documents)
                        {
                            item.NIK = data.NIK;
                            message = DocSave(item);
                            if (!message.IsNullOrEmpty())
                                throw new Exception(message);
                        }
                    }

                    message = _centralizeHReqService.SaveCentralizeReq(newData);
                    // Hapus detail data, biar audit trailnya ngak penuh
                    newData.Documents = null;
                    newData.Signatures = null;
                }
            }
            catch (Exception e)
            {
                message = e.Message;
                returnCode = "ERR";
                //ScreenMessages.Submit(ScreenMessage.Error(message));
            }

            if (message.IsNullOrEmpty())
            {
                returnCode = "OK";
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
            }
            else
            {
                returnCode = "ERR";
                //ScreenMessages.Submit(ScreenMessage.Error("Error Entry"));
            }

            JsonResult result = new JsonResult();
            result = this.Json(new
            {
                message = message,
                returnCode = returnCode,
                NIK = NIK
            });

            return result;
        }

        public string DocSave(CentralizeDocument data) {
            User user = Lookup.Get<User>();
            string message = "";
            string NewFileBlob = "";
            try
            {
                //ParameterCentralUnit param = Lookup.Get<ParameterCentralUnit>();
                ParameterCentralUnit param = _parameterRepository.getParameter();
                if (data.FileBlob.Length % 4 != 0)
                {
                    message = "Invalid uploaded document format!";
                }
                if (data.FileBlob.Length > param.GetSizeDocument)
                {
                    message = "Document " + data.FileName + " exceed maximum file size (" + (data.FileBlob.Length * 1365) + " KB) and cannot be uploaded!";
                }

                if (data.FileBlob == "null")
                {
                    CentralizeDDoc dDoc = _hRepository.getDocDet(data.ID);
                    NewFileBlob = dDoc.FileBlob;
                }

                if (message.IsNullOrEmpty())
                {
                    //CentralizeUnitHRequest reqH = _unithreqRepository.getCentralizeUnitHReq(data.NIK);

                    //if (reqH == null)
                    //{
                    //    message = SvsResources.Validation_AccountNotFound;
                    //}
                    //else if (NewFileBlob == "null")
                    if (NewFileBlob == "null")
                    {
                        message = "Invalid Document " + data.DocumentType;
                    }
                    else
                    {
                        CentralizeDDocReq doc = new CentralizeDDocReq(0);
                        doc.NIK = data.NIK;
                        doc.DocumentType = data.DocumentType;
                        doc.FileBlob = data.FileBlob == "null" ? NewFileBlob : data.FileBlob;
                        doc.FileType = data.FileType;
                        doc.FileSeq = data.Index;
                        doc.RequestType = "C";
                        doc.RequestDate = DateTime.Now;
                        doc.RequestUser = user.Username;
                        doc.RequestReason = "Create New";
                        message = _centralizeHReqService.SaveDocument(doc);
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
                //returnCode = "ERR";
            }

            if (message.IsNullOrEmpty())
            {
                message = "";
                //returnCode = "OK";
            }
            else
            {
                ScreenMessages.Submit(ScreenMessage.Error(message));
                //returnCode = "ERR";
            }

            return message;
        }

        public string SignatureSave(CentralizeSignature data)
        {
            User user = Lookup.Get<User>();
            //_unithreqRepository.IsDuplicate(data.NIK);
            //_doctypeRepository
            string message = "";
            try
            {
                //ParameterCentralUnit param = Lookup.Get<ParameterCentralUnit>();
                ParameterCentralUnit param = _parameterRepository.getParameter();

                if (data.CroppedBlob.Length % 4 != 0)
                {
                    message = "Invalid uploaded image format!";
                    throw new Exception(message);
                }
                if (data.CroppedBlob.Length > param.GetSizeSignature)
                {
                    message = "Signature image " + data.FileName + " exceed maximum file size (" + (data.CroppedBlob.Length * 1365) + " KB) and cannot be uploaded!, (JPG/JPEG Recommended format) ";
                    //message = SvsResources.Validation_MaxSizeExceeded;
                    throw new Exception(message);
                }

                if (message.IsNullOrEmpty())
                {
                    //CentralizeUnitHRequest reqH = _unithreqRepository.getCentralizeUnitHReq(data.NIK);
                    //if (reqH == null)
                    //{
                    //    message = SvsResources.Validation_AccountNotFound;
                    //    throw new Exception(message);
                    //}
                    //else
                    //{
                        CentralizeDSignReq sign = new CentralizeDSignReq(0);
                        sign.NIK = data.NIK;
                        sign.Signature = data.CroppedBlob;
                        sign.ImageType = data.ImageType;
                        sign.RequestType = "C";
                        sign.FileSeq = data.Index;
                        sign.RequestDate = DateTime.Now;
                        sign.RequestUser = user.Username;
                        sign.RequestReason = "Create New";
                        message = _centralizeHReqService.SaveSign(sign);
                    //}

                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }

        private string validateData(CentralizeUnitHReqView data, string mode)
        {
            User user = Lookup.Get<User>();

            if (data.NIK == "")
                return SvsResources.Validation_FillAccountNo;

            if (data.Name == "")
                return SvsResources.Validation_FillCustName;

            if (mode == "Create")
            {
                if (_unithreqRepository.IsDuplicate(data.NIK))
                    return SvsResources.Validation_DuplicateData;
            }

            return "";
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult ChangeDropdownRegion(string Region)
        {
            CentralizeUnitHReqView item = new CentralizeUnitHReqView();
            item.Divisions = _divisionRepository.getDivisionbyRegion(Region);

            return Json(item, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult ChangeDropdownDepartements(string region, string division = "")
        {
            CentralizeUnitHReqView item = new CentralizeUnitHReqView();
            item.Departements = _departementRepository.getDepartementbyRegion(region, division);

            return Json(item, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult NoAuthCheckViewDocument(int ID)
        {
            JsonResult result = new JsonResult();
            CentralizeDDocReq doc = _unithreqRepository.getDocReqD(ID);
            if (doc.IsNull())
            {
                return Content("Not exist or invalid document");
            }
            byte[] blob = Convert.FromBase64String(doc.FileBlob);

            string filetype = doc.FileType.Split(';')[0].Split(':')[1];
            if (filetype.Trim() == "application/pdf")
            {
                return File(blob, filetype);
            }
            else
            {
                Stream strm = new MemoryStream(Convert.FromBase64String(doc.FileBlob));
                Bitmap bmp = (Bitmap)Bitmap.FromStream(strm);

                ImageFormatConverter imgConverter = new ImageFormatConverter();
                var png = imgConverter.ConvertFromString("PNG");

                MemoryStream ms = new MemoryStream();
                bmp.Save(ms, (ImageFormat)png);

                byte[] bytImg = ms.ToArray();
                string strImg = Convert.ToBase64String(bytImg);
                return Content("<img src='data:image/PNG;base64," + strImg + "' alt='" + doc.DocumentType + "'/>");
            }

        }
    }
}