﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;
using UOB.SVS.Interface;
using UOB.SVS.Model;

namespace UOB.SVS.Controllers
{
    public class BranchAccountPrefixController : PageController
    {
        private IUserRepository _userRepository;
        private IBranchRepository _branchRepository;
        private IBranchAccountPrefixRepository _branchAccountPrefixRepository;
        private IBranchAccountPrefixCrRepository _branchAccountPrefixCrRepository;
        private IAccountPrefixRepository _accountPrefixRepository;
        private IAppAuditTrailLog _auditRepository;
        private IList<BranchAccountPrefix> branchAPList;
        private IList<BranchAccountPrefixCr> branchAPCRList;
        private List<BranchAccountPrefixSelectHelper> branchAPSelectList;
        public BranchAccountPrefixController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository, IBranchRepository branchRepository, IBranchCrRepository branchCrRepository,
            IAppAuditTrailLog auditRepository, IBranchAccountPrefixRepository branchAccountPrefixRepository, IAccountPrefixRepository accountPrefixRepository,
            IBranchAccountPrefixCrRepository branchAccountPrefixCrRepository) : base(sessionAuthentication)
        {
            this._branchRepository = branchRepository;
            this._branchAccountPrefixRepository = branchAccountPrefixRepository;
            this._branchAccountPrefixCrRepository = branchAccountPrefixCrRepository;
            this._userRepository = userRepository;
            this._auditRepository = auditRepository;
            this._accountPrefixRepository = accountPrefixRepository;

            Settings.ModuleName = "Branch Account Prefix Mapping";
            Settings.Title = "Branch Account Prefix Mapping";
        }

        protected override void Startup()
        {
            string branchId = Request.RequestContext.RouteData.Values["Id"].ToString();
            if (branchId.Trim()=="")
            {
                ScreenMessages.Submit(ScreenMessage.Error("Invalid branch code!"));
                Response.RedirectToRoute("", "Branch");
            }

            Branch branch = _branchRepository.getBranch(branchId);
            if (branch.IsNull())
            {
                ScreenMessages.Submit(ScreenMessage.Error("Invalid branch code!"));
                Response.RedirectToRoute("", "Branch");
            }

            Lookup.Remove<Branch>();
            Lookup.Add(branch);
            branchAPSelectList = new List<BranchAccountPrefixSelectHelper>();
            Session.Add("bapsellist", branchAPSelectList);
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                Branch brc = Lookup.Get<Branch>();
                List<string> brcList = new List<string>();
                brcList.Add(brc.BranchCode);

                branchAPList = _branchAccountPrefixRepository.getAccountPrefixes(brcList);
                branchAPCRList = _branchAccountPrefixCrRepository.getAccountPrefixesRequest(brcList);

                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                PagedResult<AccountPrefix> data = _accountPrefixRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue, brc.BranchCode);
                
                var items = data.Items.ToList().ConvertAll<BranchAccountPrefixView>(new Converter<AccountPrefix, BranchAccountPrefixView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = this.Json(new
                {
                    draw = Convert.ToInt32(0),
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    data = new List<BranchAccountPrefix>(),
                    msg = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
            }
            // Return info.   
            return result;
        }

        private BranchAccountPrefixView ConvertFrom(AccountPrefix input)
        {
            BranchAccountPrefixView returnitem = new BranchAccountPrefixView();
            Branch brc = Lookup.Get<Branch>();
            List<BranchAccountPrefixSelectHelper> bapsellist = (List<BranchAccountPrefixSelectHelper>)Session["bapsellist"];
            BranchAccountPrefixSelectHelper bap = bapsellist.Find(a => a.PrefixCode == input.PrefixCode);
            var selected = branchAPList.FindElement(p => p.PrefixCode == input.PrefixCode);
            var selectedCr = branchAPCRList.FindElement(c => c.PrefixCode == input.PrefixCode);

            returnitem.BranchCode = brc.BranchCode;
            returnitem.PrefixCode = input.PrefixCode;

            if (selected.IsNull())
            {
                returnitem.selected = false;
                if (!bap.IsNull())
                {
                    returnitem.selected = bap.selected;
                }
                else if (!selectedCr.IsNull())
                {
                    returnitem.selected = selectedCr.ApprovalType.Trim() == "C" ? true : false;
                }
            }
            else
            {
                returnitem.selected = true;
                if (!bap.IsNull())
                {
                    returnitem.selected = bap.selected;
                }
                else if (!selectedCr.IsNull())
                {
                    returnitem.selected = selectedCr.ApprovalType.Trim() == "C" ? true : false;
                }
            }

            return returnitem;
        }

        public ActionResult CreateTemp(BranchAccountPrefixSelectHelper select)
        {
            JsonResult result = new JsonResult();
            Branch brc = Lookup.Get<Branch>();

            BranchAccountPrefixCr currBapsCr = _branchAccountPrefixCrRepository.getAccountPrefixExisting(select.PrefixCode);
            if (!currBapsCr.IsNull())
                if (currBapsCr.BranchCode != brc.BranchCode)
                {
                    result = this.Json(new
                    {
                        message = "Prefix " + currBapsCr.PrefixCode + " already signed request to another branch (" + currBapsCr.BranchCode + ")!"
                    }, JsonRequestBehavior.AllowGet);
                    return result;
                }

            List<BranchAccountPrefixSelectHelper> bapsellist = (List<BranchAccountPrefixSelectHelper>)Session["bapsellist"];

            BranchAccountPrefixSelectHelper bap = bapsellist.Find(a => a.PrefixCode == select.PrefixCode);

            if (!bap.IsNull())
            {
                if (bap.selected != select.selected)
                {
                    bapsellist.Remove(bap);
                }
            }
            else
            {
                bapsellist.Add(select);
            }


            Session["rdtlist"] = bapsellist;

            result = this.Json(new
            {
                message = "OK",
            }, JsonRequestBehavior.AllowGet);

            // Return info.   
            return result;
        }

        public ActionResult Create()
        {
            JsonResult result = new JsonResult();
            string message = "";
            Branch brc = Lookup.Get<Branch>();

            List<BranchAccountPrefixSelectHelper> bapsellist = (List<BranchAccountPrefixSelectHelper>)Session["bapsellist"];
            if (bapsellist.Count == 0)
            {
                result = this.Json(new
                {
                    message = "No changes to submitted!",
                    Id = 0
                }, JsonRequestBehavior.AllowGet);
                return result;
            }
            if (bapsellist.Count > 0)
            {
                List<string> brcList = new List<string>();
                brcList.Add(brc.BranchCode);

                IList<BranchAccountPrefix> currBaps = _branchAccountPrefixRepository.getAccountPrefixes(brcList);
                List<BranchAccountPrefixSelectHelper> currBapselh = new List<BranchAccountPrefixSelectHelper>();

                foreach (BranchAccountPrefix currBap in currBaps)
                {
                    BranchAccountPrefixSelectHelper bapsh = new BranchAccountPrefixSelectHelper();
                    bapsh.PrefixCode = currBap.PrefixCode;
                    bapsh.selected = true;
                    currBapselh.Add(bapsh);
                }

                if (bapsellist.OrderBy(o => o.PrefixCode).ToArray().SequenceEqual(currBapselh))
                {
                    result = this.Json(new
                    {
                        message = "No changes to submitted!",
                        Id = 0
                    }, JsonRequestBehavior.AllowGet);
                    return result;
                }
                foreach (var item in bapsellist)
                {
                    IList<BranchAccountPrefixCr> currBapsCr = _branchAccountPrefixCrRepository.getAccountPrefixesRequest(item.PrefixCode);
                    foreach (var bapsCr in currBapsCr)
                    {
                        if(bapsCr.BranchCode != brc.BranchCode)
                        {
                            result = this.Json(new
                            {
                                message = "Prefix " + bapsCr.PrefixCode + " already signed request to another branch (" + bapsCr.BranchCode + ")!",
                                Id = 0
                            }, JsonRequestBehavior.AllowGet);
                            return result;
                        }
                    }
                }

            }

            User user = Lookup.Get<User>();
            long Id = 0;

            try
            {
                Role role = Lookup.Get<Role>();
                foreach (BranchAccountPrefixSelectHelper baps in bapsellist)
                {
                    BranchAccountPrefix bap = _branchAccountPrefixRepository.getAccountPrefix(baps.PrefixCode, brc.BranchCode);
                    BranchAccountPrefixCr bapcr = _branchAccountPrefixCrRepository.getAccountPrefix(baps.PrefixCode, brc.BranchCode);

                    if (baps.selected)
                    {
                        if (bapcr.IsNull())
                        {
                            Id = Insert(brc.BranchCode, baps.PrefixCode);
                        }
                        else
                        {
                            if (bapcr.ApprovalType == "D")
                                message = DeleteCr(brc.BranchCode, baps.PrefixCode);
                        }
                    }
                    else
                    {
                        if (!bapcr.IsNull())
                        {
                            if (bapcr.ApprovalType == "C")
                            {
                                message = DeleteCr(brc.BranchCode, baps.PrefixCode);
                            }
                        }
                        else
                        {
                            if (!bap.IsNull())
                            {
                                message = Delete(brc.BranchCode, baps.PrefixCode);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            result = this.Json(new
            {
                message = BranchResources.Validation_Success,
                Id = Id
            }, JsonRequestBehavior.AllowGet);

            // Return info.   
            return result;
        }

        private long Insert(string branchCode, string prefixCode)
        {
            User user = Lookup.Get<User>();
            long returnId = -1;
            try
            {
                BranchAccountPrefixCr newData = new BranchAccountPrefixCr(0L);
                newData.PrefixCode = prefixCode;
                newData.BranchCode = branchCode;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                newData.ApprovalType = "C";
                newData.ApprovalStatus = "N";
                newData.Approved = false;

                _branchAccountPrefixCrRepository.Add(newData);
                returnId = newData.Id;
            }
            catch 
            {
                returnId = -1;
            }
            return returnId;
        }

        public string Delete(string branchCode, string prefixCode)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                BranchAccountPrefixCr newData = new BranchAccountPrefixCr(0L);
                newData.PrefixCode = prefixCode;
                newData.BranchCode = branchCode;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                newData.ApprovalType = "D";
                newData.ApprovalStatus = "N";
                newData.Approved = false;

                _branchAccountPrefixCrRepository.Add(newData);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }

        public string DeleteCr(string branchCode, string prefixCode)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                _branchAccountPrefixCrRepository.deleteRequest(branchCode, prefixCode);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }
    }
}