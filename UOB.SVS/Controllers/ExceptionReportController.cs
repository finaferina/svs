﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Utilities.Queries;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using UOB.SVS.Interface;
using UOB.SVS.Models;
using UOB.SVS.Model;
using Treemas.Base.Utilities;
using System.Data;
using System.IO;
using SpreadsheetLight;
using System.Text.RegularExpressions;
using Treemas.Credential.Model;

namespace UOB.SVS.Controllers
{
    public class ExceptionReportController : PageController
    {
        private IUserRepository _userRepository;
        private IExceptionReportRepository _exceptionReportRepository;
        private IAccountMasterRepository _accountMasterRepository;
        private IBranchRepository _branchRepository;

        public ExceptionReportController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository, IBranchRepository branchRepository,
             IExceptionReportRepository exceptionReportRepository, IAccountMasterRepository accountMasterRepository) : base(sessionAuthentication)
        {
            this._userRepository = userRepository;
            this._exceptionReportRepository = exceptionReportRepository;
            this._accountMasterRepository = accountMasterRepository;
            this._branchRepository = branchRepository;

            Settings.ModuleName = "ExceptionReport";
            Settings.Title = "SVS Exception Report";
        }

        protected override void Startup()
        {

        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            User user = Lookup.Get<User>();

            try
            {
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string ActionDateStart = Request.QueryString.GetValues("ActionDateStart")[0];
                string ActionDateEnd = Request.QueryString.GetValues("ActionDateEnd")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;
                string ReportType = Request.QueryString.GetValues("ReportType")[0];

                // Loading.   
                PagedResult<ExceptionReport> data;

                DateTime _RequestTimeStart = DateTime.MinValue;
                DateTime _RequestTimeEnd = DateTime.MinValue;

                if (!ActionDateStart.IsNullOrEmpty())
                {
                    if (!DateTime.TryParseExact(ActionDateStart, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out _RequestTimeStart))
                        throw new Exception("Input date is not valid");
                }
                if (!ActionDateEnd.IsNullOrEmpty())
                {
                    if (!DateTime.TryParseExact(ActionDateEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out _RequestTimeEnd))
                        throw new Exception("Input date is not valid");
                }

                if (_RequestTimeStart > _RequestTimeEnd)
                {
                    throw new Exception("Input date range is not valid");
                }

                if (_RequestTimeEnd == DateTime.MinValue)
                    _RequestTimeEnd = _RequestTimeStart;

                var branches = getSubBranch(user.BranchCode);

                data = _exceptionReportRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue, ReportType, _RequestTimeStart, _RequestTimeEnd, user, branches);

                var items = data.Items.ToList().ConvertAll<ExceptionReportView>(new Converter<ExceptionReport, ExceptionReportView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = this.Json(new
                {
                    draw = Convert.ToInt32(0),
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    data = new List<SignatureHHistoryView>(),
                    msg = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);

                // Info   
                //Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public string[] getSubBranch(string branchCode)
        {
            IList<Branch> b = _branchRepository.getBranchs(branchCode);
            if (b.IsNullOrEmpty())
            {
                return null;
            }
            int index = 0;
            foreach (Branch item in b)
            {
                index++;
            }
            string[] subbranch = new string[index];
            index = 0;
            foreach (Branch item in b)
            {
                subbranch[index] = item.BranchCode;
                index++;
            }

            return subbranch;
        }
        private ExceptionReportView ConvertFrom(ExceptionReport input)
        {
            ExceptionReportView exceptionReport = new ExceptionReportView();
            exceptionReport.AccountNo = input.AccountNo;
            exceptionReport.AccountName = input.AccountName;
            exceptionReport.CIFNumber = input.CIFNumber;
            exceptionReport.BranchCode = input.BranchCode;
            exceptionReport.RequestType = input.RequestType;
            exceptionReport.Reason = input.Reason;
            exceptionReport.OpenDate = input.OpenDate.Value.ToString("dd/MM/yyyy");
            exceptionReport.CloseDate = input.CloseDate.IsNull() ? "" : input.CloseDate.Value.ToString("dd/MM/yyyy");
            return exceptionReport;
        }

        [HttpPost]
        public ActionResult Export(string searchColumn, string searchValue, string ReportType, string ActionDateTimeStart, string ActionDateTimeEnd)
        {
            DateTime _RequestTimeStart = DateTime.MinValue;
            DateTime _RequestTimeEnd = DateTime.MinValue;
            User user = Lookup.Get<User>();

            if (!ActionDateTimeStart.IsNullOrEmpty())
            {
                if (!DateTime.TryParseExact(ActionDateTimeStart, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out _RequestTimeStart))
                {
                    ScreenMessages.Submit(ScreenMessage.Error("Input date is not valid!"));
                    return RedirectToAction("", "ExceptionReport");
                }
            }
            if (!ActionDateTimeEnd.IsNullOrEmpty())
            {
                if (!DateTime.TryParseExact(ActionDateTimeEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out _RequestTimeEnd))
                {
                    ScreenMessages.Submit(ScreenMessage.Error("Input date is not valid!"));
                    return RedirectToAction("", "ExceptionReport");
                }
            }

            if (_RequestTimeStart > _RequestTimeEnd)
            {
                ScreenMessages.Submit(ScreenMessage.Error("Input date range is not valid!"));
                return RedirectToAction("", "ExceptionReport");
            }

            if (_RequestTimeEnd == DateTime.MinValue)
                _RequestTimeEnd = _RequestTimeStart;

            var branches = getSubBranch(user.BranchCode);

            var expList = _exceptionReportRepository.FindAllExport(searchColumn, searchValue, ReportType, _RequestTimeStart, _RequestTimeEnd, user, branches);
            var items = expList.ToList().ConvertAll<ExceptionReportView>(new Converter<ExceptionReport, ExceptionReportView>(ConvertFrom));

            DataTable dt = new DataTable();
            dt.Columns.Add("Account No");
            dt.Columns.Add("Full Name");
            dt.Columns.Add("CIF");
            dt.Columns.Add("Open Date");
            dt.Columns.Add("Close Date");
            dt.Columns.Add("Remark");

            foreach (ExceptionReportView item in items)
            {
                DataRow dr = dt.NewRow();

                dr["Account No"] = item.AccountNo;
                dr["Full Name"] = item.AccountName;
                dr["CIF"] = item.CIFNumber;
                dr["Open Date"] = item.OpenDate;
                dr["Close Date"] = item.CloseDate;
                dr["Remark"] = item.Reason;
                dt.Rows.Add(dr);
            }

            MemoryStream ms = new MemoryStream();
            using (SLDocument sl = new SLDocument())
            {
                sl.ImportDataTable(1, 1, dt, true);
                sl.SelectWorksheet("Worksheet");
                sl.AutoFitColumn(1, 6);
                sl.SaveAs(ms);
            }

            ms.Position = 0;
            string fName = "SVS_Exception_Report.xls";
            return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fName);


        }
    }
}