﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;
using System.Globalization;
using System.Text.RegularExpressions;
using Microsoft.Reporting.WebForms;
using System.IO;
using SpreadsheetLight;
using System.Data;
using Newtonsoft.Json;

namespace UOB.SVS.Controllers
{
    public class ParameterAuditTrailController : PageController
    {
        private IUserRepository _userRepository;
        private IApplicationRepository _appRepository;
        private IParameterAuditTrailRepository _auditRepository;
        public ParameterAuditTrailController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository,
            IApplicationRepository appRepository, IParameterAuditTrailRepository auditRepository) : base(sessionAuthentication)
        {
            this._userRepository = userRepository;
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;

            Settings.ModuleName = "ParameterAuditTrail";
            Settings.Title = "Parameter Audit Trail";
        }
        protected override void Startup()
        {
            ViewData["loadState"] = (Request.QueryString.GetValues("loadState") == null) ? "0" : Request.QueryString.GetValues("loadState").FirstOrDefault();
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            //CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            try
            {
                //AuditTrailFilter auditFilter = new AuditTrailFilter();
                //auditFilter.FunctionName = Request.QueryString.GetValues("FunctionName")[0].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("FunctionName")[0];
                //auditFilter.ObjectName = Request.QueryString.GetValues("ObjectName")[0].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("ObjectName")[0];
                ////auditFilter.ActionName = Request.QueryString.GetValues("Action")[0].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("Action")[0];
                //auditFilter.ActionDateEnd = !Request.QueryString["ActionDateEnd"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("ActionDateEnd")[0], "d/M/yyyy", culture) : (DateTime?)null;
                //auditFilter.ActionDateStart = !Request.QueryString["ActionDateStart"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("ActionDateStart")[0], "d/M/yyyy", culture) : (DateTime?)null;


                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string ActionDateStart = Request.QueryString.GetValues("ActionDateStart")[0];
                string ActionDateEnd = Request.QueryString.GetValues("ActionDateEnd")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<ParameterAuditTrail> data;

                DateTime _RequestTimeStart = DateTime.MinValue;
                DateTime _RequestTimeEnd = DateTime.MinValue;

                if (!ActionDateStart.IsNullOrEmpty())
                {
                    if (!DateTime.TryParseExact(ActionDateStart, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out _RequestTimeStart))
                        throw new Exception("Input date is not valid");
                }
                if (!ActionDateEnd.IsNullOrEmpty())
                {
                    if (!DateTime.TryParseExact(ActionDateEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out _RequestTimeEnd))
                        throw new Exception("Input date is not valid");
                }
                if (_RequestTimeEnd < _RequestTimeStart)
                {
                    throw new Exception("Input date range is not valid");
                }
                if (_RequestTimeEnd == DateTime.MinValue)
                    _RequestTimeEnd = _RequestTimeStart;

                data = _auditRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue, _RequestTimeStart, _RequestTimeEnd);
                
                var items = data.Items.ToList().ConvertAll<AuditTrailView>(new Converter<ParameterAuditTrail, AuditTrailView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = this.Json(new
                {
                    draw = Convert.ToInt32(0),
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    data = new List<AppAuditTrail>(),
                    msg = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
                //// Info   
                //Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public AuditTrailView ConvertFrom(ParameterAuditTrail item)
        {
            //var ar = _auditRepository
            AuditTrailView returnItem = new AuditTrailView();
            returnItem.Id = item.Id;
            returnItem.FunctionName = item.FunctionName;
            returnItem.ObjectName = item.ObjectName;
            returnItem.ObjectValueBefore = item.ObjectValueBefore;
            returnItem.ObjectValueAfter = item.ObjectValueAfter;
            returnItem.Action = item.Action;
            returnItem.TimeStamp = Convert.ToDateTime(item.TimeStamp).ToString("dd/MM/yyyy HH:mm:ss");
            returnItem.UserMaker = item.UserMaker;
            returnItem.UserChecker = item.UserChecker;
            returnItem.RequestDate = item.RequestDate.IsNull() ? null : Convert.ToDateTime(item.RequestDate).ToString("dd/MM/yyyy");
            returnItem.ApproveDate = item.ApproveDate.IsNull() ? null : Convert.ToDateTime(item.ApproveDate).ToString("dd/MM/yyyy");

            return returnItem;
        }

        [HttpPost]
        //public ActionResult Export(AuditTrailView data)
        //{
        //    CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
        //    AuditTrailFilter auditFilter = new AuditTrailFilter();
        //    auditFilter.FunctionName = data.FunctionName.IsNullOrEmpty() ? "" : data.FunctionName.Trim();// Request.QueryString.GetValues("FunctionName")[0].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("FunctionName")[0];
        //    auditFilter.ObjectName = data.ObjectName.IsNullOrEmpty() ? "" : data.ObjectName.Trim(); //Request.QueryString.GetValues("ObjectName")[0].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("ObjectName")[0];
        //    auditFilter.ActionName = data.Action.IsNullOrEmpty() ? "" : data.Action.Trim(); //Request.QueryString.GetValues("ActionName")[0].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("ActionName")[0];
        //    auditFilter.ActionDateEnd = !data.TimeStampEnd.IsNullOrEmpty() ? DateTime.ParseExact(data.TimeStampEnd, "d/M/yyyy", culture) : (DateTime?)null;
        //    auditFilter.ActionDateStart = !data.TimeStampStart.IsNullOrEmpty() ? DateTime.ParseExact(data.TimeStampStart, "d/M/yyyy", culture) : (DateTime?)null;

        //    var auditList = _auditRepository.FindForExport(auditFilter);

        //    IList<AuditTrailView> items = auditList.ToList().ConvertAll<AuditTrailView>(new Converter<ParameterAuditTrail, AuditTrailView>(ConvertFrom));

        //    DataTable dt = items.ListToDataTable();
        //    List<AuditTrailView> list = new List<AuditTrailView>();

        //    dt.Columns.Remove("Id");
        //    dt.Columns.Remove("TimeStampString");
        //    dt.Columns.Remove("RequestDateString");
        //    dt.Columns.Remove("ApproveDateString");
        //    dt.Columns.Remove("TimeStampStart");
        //    dt.Columns.Remove("TimeStampEnd");

        //    MemoryStream ms = new MemoryStream();
        //    using (SLDocument sl = new SLDocument())
        //    {
        //        sl.ImportDataTable(1, 1, dt, true);
        //        sl.SelectWorksheet("Worksheet");
        //        sl.SaveAs(ms);
        //    }

        //    ms.Position = 0;
        //    string fName = "SVS_Parameter_Audit_Trail.xls";
        //    return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fName);
        //}
        public ActionResult Export(string searchColumn, string searchValue, string ActionDateTimeStart, string ActionDateTimeEnd)
        {

            DateTime _RequestTimeStart = DateTime.MinValue;
            DateTime _RequestTimeEnd = DateTime.MinValue;

            if (!ActionDateTimeStart.IsNullOrEmpty())
            {
                if (!DateTime.TryParseExact(ActionDateTimeStart, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out _RequestTimeStart))
                {
                    ScreenMessages.Submit(ScreenMessage.Error("Input date is not valid!"));
                    return RedirectToAction("", "ParameterAuditTrail");
                }
            }
            if (!ActionDateTimeEnd.IsNullOrEmpty())
            {
                if (!DateTime.TryParseExact(ActionDateTimeEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out _RequestTimeEnd))
                {
                    ScreenMessages.Submit(ScreenMessage.Error("Input date is not valid!"));
                    return RedirectToAction("", "ParameterAuditTrail");
                }
            }

            if (_RequestTimeStart > _RequestTimeEnd)
            {
                ScreenMessages.Submit(ScreenMessage.Error("Input date range is not valid!"));
                return RedirectToAction("", "ParameterAuditTrail");
            }

            if (_RequestTimeEnd == DateTime.MinValue)
                _RequestTimeEnd = _RequestTimeStart;


            var auditList = _auditRepository.FindAllPagedForExport(searchColumn, searchValue, _RequestTimeStart, _RequestTimeEnd);
            IList<AuditTrailView> items = auditList.ToList().ConvertAll<AuditTrailView>(new Converter<ParameterAuditTrail, AuditTrailView>(ConvertFrom));

            //DataTable dt = items.ListToDataTable();
            DataTable newdt = new DataTable();

            newdt.Columns.Add("TimeStamp");
            newdt.Columns.Add("AuditType");
            newdt.Columns.Add("DataType");
            newdt.Columns.Add("MakerUser");
            newdt.Columns.Add("RequestDate");
            newdt.Columns.Add("ApproveUser");
            newdt.Columns.Add("ApproveDate");
            newdt.Columns.Add("DataBefore");
            newdt.Columns.Add("DataAfter");

            foreach (AuditTrailView item in items)
            {
                //Dibuat kamus map dictionary
                //Dictionary<string, string> mapObjectValueBefore = JsonConvert.DeserializeObject<Dictionary<string, string>>(item.ObjectValueBefore);
                //Dictionary<string, string> mapObjectValueAfter = JsonConvert.DeserializeObject<Dictionary<string, string>>(item.ObjectValueAfter);

                ////Temp 
                //Dictionary<string, string>.KeyCollection tempMapObjectValueBefore = new Dictionary<string, string>.KeyCollection(mapObjectValueBefore);
                //Dictionary<string, string>.KeyCollection tempMapObjectValueAfter = new Dictionary<string, string>.KeyCollection(mapObjectValueAfter);

                //string[] KeymapObjectValueBefore = new string[mapObjectValueBefore.Count];
                //string[] KeymapObjectValueAfter = new string[mapObjectValueAfter.Count];

                //tempMapObjectValueBefore.CopyTo(KeymapObjectValueBefore, 0);
                //tempMapObjectValueAfter.CopyTo(KeymapObjectValueAfter, 0);

                ////Hapuss mapObjectValueBefore
                //foreach (var itemmapObjectValueBefore in KeymapObjectValueBefore)
                //{
                //    if (itemmapObjectValueBefore == "UserId" || itemmapObjectValueBefore == "ParamId" || itemmapObjectValueBefore == "Password"
                //        || itemmapObjectValueBefore == "NewUserId" || itemmapObjectValueBefore == "NewPassword" || itemmapObjectValueBefore == "NewStaggingUrl"
                //        || itemmapObjectValueBefore == "NewSessionTimeout" || itemmapObjectValueBefore == "NewLoginAttempt" || itemmapObjectValueBefore == "NewPasswordExp"
                //        || itemmapObjectValueBefore == "NewDomainServer" || itemmapObjectValueBefore == "DomainServer" || itemmapObjectValueBefore == "ApprovalStatus"
                //        || itemmapObjectValueBefore == "ApprovalStatusStateEnum" || itemmapObjectValueBefore == "ChangedBy" || itemmapObjectValueBefore == "ApprovedBy"
                //        || itemmapObjectValueBefore == "NewSizeKTP" || itemmapObjectValueBefore == "NewSizeSignature" || itemmapObjectValueBefore == "NewSizeDocument"
                //        || itemmapObjectValueBefore == "NewKeepHistory" || itemmapObjectValueBefore == "NewKeepAuditTrail" || itemmapObjectValueBefore == "NewKeepPendingAppr"
                //        || itemmapObjectValueBefore == "NewMaxDiffAccount" || itemmapObjectValueBefore == "StaggingUrl" || itemmapObjectValueBefore == "RecordVersion"
                //        || itemmapObjectValueBefore == "GetSizeKTP" || itemmapObjectValueBefore == "GetSizeSignature" || itemmapObjectValueBefore == "GetSizeDocument"
                //        || itemmapObjectValueBefore == "GetSizeDocumentJS" || itemmapObjectValueBefore == "Id"
                //        )
                //    {
                //        mapObjectValueBefore.Remove(itemmapObjectValueBefore);
                //    }

                //}


                ////Hapuss mapitemObjectValueAfter
                //foreach (var itemmapObjectValueAfter in KeymapObjectValueAfter)
                //{
                //    if (itemmapObjectValueAfter == "UserId" || itemmapObjectValueAfter == "ParamId" || itemmapObjectValueAfter == "Password"
                //        || itemmapObjectValueAfter == "DomainServer" || itemmapObjectValueAfter == "ChangedBy" || itemmapObjectValueAfter == "ChangedDate"
                //        || itemmapObjectValueAfter == "GetSizeKTP" || itemmapObjectValueAfter == "GetSizeSignature" || itemmapObjectValueAfter == "GetSizeDocument"
                //        || itemmapObjectValueAfter == "GetSizeDocumentJS" || itemmapObjectValueAfter == "StaggingUrl" || itemmapObjectValueAfter == "NewStaggingUrl"
                //        || itemmapObjectValueAfter == "NewDomainServer" || itemmapObjectValueAfter == "NewUserId" || itemmapObjectValueAfter == "NewPassword"
                //        || itemmapObjectValueAfter == "RecordVersion" || itemmapObjectValueAfter == "ApprovalStatusStateEnum" || itemmapObjectValueAfter == "Id"
                //        || itemmapObjectValueAfter == "ApprovalStatus" || itemmapObjectValueAfter == "ID"
                //        )
                //    {
                //        mapObjectValueAfter.Remove(itemmapObjectValueAfter);
                //    }

                //}

                ////susun kembali
                //var newObjectValueBefore = JsonConvert.SerializeObject(mapObjectValueBefore, Formatting.None);
                //var newObjectValueAfter = JsonConvert.SerializeObject(mapObjectValueAfter, Formatting.None);






                DataRow _audittrailuser = newdt.NewRow();
                _audittrailuser["TimeStamp"] = item.TimeStamp;
                _audittrailuser["AuditType"] = item.ObjectName;
                _audittrailuser["DataType"] = item.Action;
                _audittrailuser["MakerUser"] = item.UserMaker;
                _audittrailuser["RequestDate"] = item.RequestDate;
                _audittrailuser["ApproveUser"] = item.UserChecker;
                _audittrailuser["ApproveDate"] = item.ApproveDate;
                _audittrailuser["DataBefore"] = item.ObjectValueBefore;
                _audittrailuser["DataAfter"] = item.ObjectValueAfter;
                newdt.Rows.Add(_audittrailuser);
            }

            //List<AuditTrailView> list = new List<AuditTrailView>();

            //dt.Columns.Remove("Id");
            //dt.Columns.Remove("TimeStampString");
            //dt.Columns.Remove("RequestDateString");
            //dt.Columns.Remove("ApproveDateString");
            //dt.Columns.Remove("TimeStampStart");
            //dt.Columns.Remove("TimeStampEnd");

            MemoryStream ms = new MemoryStream();
            using (SLDocument sl = new SLDocument())
            {
                sl.ImportDataTable(1, 1, newdt, true);
                sl.SelectWorksheet("Worksheet");
                sl.AutoFitColumn(1, 6);
                sl.SaveAs(ms);
            }

            ms.Position = 0;
            string fName = "SVS_Parameter_Audit_Trail.xls";
            return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fName);
        }

    }
}