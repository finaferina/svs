﻿using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Web.Platform;
using UOB.SVS.Resources;
using System.Collections.Generic;
using System.Linq;

namespace UOB.SVS.Controllers
{
    public class HomeController : PageController
    {
        private IUserRepository _userRepository;
        private IRoleDocTypeRepository _roleDocRepo;

        public HomeController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository, 
            IRoleDocTypeRepository roleDocRepo) : base(sessionAuthentication)
        {
            this._userRepository = userRepository;
            this._roleDocRepo = roleDocRepo;
            Settings.Title = ApplicationLabel.PageTitle;
        }
        // GET: Home
        protected override void Startup()
        {
            User user = null;
            Settings.Title = ApplicationLabel.PageTitle;

            if (!SystemSettings.Instance.Security.EnableAuthentication)
                user = _userRepository.GetUser(SystemSettings.Instance.Security.SimulatedAuthenticatedUser.Username);
            else
                user = Lookup.Get<User>();

            var a = user.Roles;
            long idRole = a[0].Id;
            List<RoleDocType> roleDocList = new List<RoleDocType>();
            roleDocList = _roleDocRepo.getDocTypes(idRole).ToList();
            Lookup.Add(roleDocList);

            ViewData["User"] = user;
        }
    }
}