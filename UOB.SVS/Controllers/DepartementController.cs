﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;

namespace UOB.SVS.Controllers
{
    public class DepartementController : PageController
    {
        private string _application = "SVS";
        private IDepartementRepository _departementRepository;
        private IDepartementCrRepository _departementCrRepository;
        private IApplicationRepository _appRepository;
        private IAppAuditTrailLog _auditRepository;
        private IRegionRepository _regionRepository;
        private IDivisionRepository _divisionRepository;
        public DepartementController(ISessionAuthentication sessionAuthentication, IDepartementRepository departementRepository, IDepartementCrRepository departementCrRepository,
            IApplicationRepository appRepository, IAppAuditTrailLog auditRepository, IRegionRepository regionRepository, IDivisionRepository divisionRepository) : base(sessionAuthentication)
        {
            this._departementRepository = departementRepository;
            this._departementCrRepository = departementCrRepository;
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;
            this._regionRepository = regionRepository;
            this._divisionRepository = divisionRepository;
            Settings.ModuleName = "Departement";
            Settings.Title = "Departement / SubBranch";
        }

        protected override void Startup()
        {
            ViewData["loadState"] = (Request.QueryString.GetValues("loadState") == null) ? "0" : Request.QueryString.GetValues("loadState").FirstOrDefault();
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<Departement> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _departementRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _departementRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<DepartementView>(new Converter<Departement, DepartementView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult ChangeDropdownRegion(string Region)
        {
            CentralizeUnitHReqView item = new CentralizeUnitHReqView();
            item.Divisions = _divisionRepository.getDivisionbyRegion(Region);

            return Json(item, JsonRequestBehavior.AllowGet);
        }

        public DepartementView ConvertFrom(Departement item)
        {
            DepartementView returnItem = new DepartementView();
            returnItem.Application = item._Application;
            returnItem.Alias = item.Alias;
            returnItem.DepartementId = item.DepartementId;
            returnItem.Id = item.Id;
            returnItem.Name = item.Name;
            returnItem.Region = item.Region;
            returnItem.Division = item.Division;
            return returnItem;
        }

        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            DepartementView data = new DepartementView();
            return CreateView(data);
        }

        private ViewResult CreateView(DepartementView data)
        {
            ViewData["RegionMaster"] = createRegionSelect(data.Region);
            ViewData["DivisionMaster"] = createDivisionSelect(data.Region.IsNullOrEmpty() ? "-": data.Region, data.Division.IsNullOrEmpty() ? "-" : data.Division);
            return View("Detail", data);
        }

        private IList<SelectListItem> createRegionSelect(string selected)
        {
            IList<SelectListItem> dataList = _regionRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Region, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Value);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (!selected.IsNullOrEmpty())
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }

        private IList<SelectListItem> createDivisionSelect(string Region, string selected)
        {
            IList<SelectListItem> dataList = _divisionRepository.getDivisionbyRegion(Region).ToList().ConvertAll<SelectListItem>(new Converter<Division, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Value);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (dataList.Count > 0)
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }

        public SelectListItem ConvertFrom(Region item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }
        public SelectListItem ConvertFrom(Division item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }

        // POST: Function/Create
        [HttpPost]
        public ActionResult Create(DepartementView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    DepartementCr newData = new DepartementCr(0L);
                    newData._Application = _application;
                    newData.DepartementId = data.DepartementId;
                    newData.Name = data.Name;
                    newData.Alias = data.Alias;
                    newData.Region = data.Region;
                    newData.Division = data.Division;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    newData.ApprovalType = "C";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;
                    _departementCrRepository.Add(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success("Data Berhasil di Tambahkan"));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(DepartementView data, string mode)
        {
            if (data.Name == "")
                return "Nama Tidak Boleh Kosong";

            if (data.DepartementId == "")
                return "Departement ID Tidak Boleh Kosong";

            if (mode == "Create")
            {
                if (_departementRepository.IsDuplicate(_application, data.DepartementId))
                    return "Departement ID dan Nama Sudah Pernah Ada";
                else if (_departementRepository.IsDuplicateCR(_application, data.DepartementId, "N"))
                    return "Departement ID dan Nama Sudah Ada di Approval";

                if (_departementRepository.IsDuplicateByAlias(_application, data.Alias))
                    return "Alias Sudah Pernah Ada";
                else if (_departementRepository.IsDuplicateCRByAlias(_application, data.Alias, "N"))
                    return "Alias Sudah Ada di Approval";
            }
            return "";
        }

        // GET: Function/Edit/5
        public ActionResult Edit(long id)
        {
            ViewData["ActionName"] = "Edit";
            Departement autoDepartement = _departementRepository.getDepartement(id);
            DepartementView data = ConvertFrom(autoDepartement);

            return CreateView(data);
        }

        public ActionResult Delete(long id)
        {
            ViewData["ActionName"] = "Delete";
            Departement autoDepartement = _departementRepository.getDepartement(id);
            DepartementView data = ConvertFrom(autoDepartement);

            return CreateView(data);
        }

        // POST: Function/Edit/5
        [HttpPost]
        public ActionResult Edit(DepartementView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    //new additional by fahmi
                    Departement app = _departementRepository.getDepartement(data.Id);
                    //Directorate app = _directorateRepository.getExistingRequest(data.Id, "U");
                    //if (!app.IsNull())
                    //{
                    //    _departementRepository.UpdateDepartement(data.Id, data.DepartementId, data.Name, data.Alias, data.Region, data.Division, user.Username, DateTime.Now);
                    //}
                    DepartementCr newData = new DepartementCr(0L);
                    newData._Application = _application;
                    newData.DepartementId = data.DepartementId;
                    newData.Name = data.Name;
                    newData.Alias = data.Alias;
                    newData.Region = data.Region;
                    newData.Division = data.Division;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    newData.ApprovalType = "U";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;
                    _departementCrRepository.Add(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success("Data Berhasil di Ubah"));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        // POST: Function/Delete/5
        [HttpPost]
        public ActionResult Delete(DepartementView data)
        {
            User user = Lookup.Get<User>();
            Departement departementdata = _departementRepository.getDepartement(data.Id);
            string message = "";
            try
            {
                //_departementRepository.Remove(departementdata);
                DepartementCr newData = new DepartementCr(0L);
                newData._Application = departementdata._Application;
                newData.DepartementId = departementdata.DepartementId;
                newData.Name = departementdata.Name;
                newData.Alias = departementdata.Alias;
                newData.Region = departementdata.Region;
                newData.Division = departementdata.Division;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                newData.ApprovalType = "D";
                newData.ApprovalStatus = "N";
                newData.Approved = false;

                _departementCrRepository.Save(newData);


                //DepartementCr newData = new DepartementCr(0L);
                //newData._Application = _application;
                //newData.DepartementId = data.DepartementId;
                //newData.Name = data.Name;
                //newData.Alias = data.Alias;
                //newData.Region = data.Region;
                //newData.Division = data.Division;
                //newData.ChangedBy = user.Username;
                //newData.ChangedDate = DateTime.Now;
                //newData.CreatedBy = user.Username;
                //newData.CreatedDate = DateTime.Now;
                //newData.ApprovalType = "C";
                //newData.ApprovalStatus = "N";
                //newData.Approved = false;
                //_departementCrRepository.Add(newData);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success("Data Berhasil di Hapus"));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(data.Id);
        }
    }
}