﻿using System;
using System.IO;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using UOB.SVS.Resources;
using UOB.SVS.Model;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System.Linq;
using Treemas.Credential.Model;
using System.Collections.Generic;
using System.Drawing.Imaging;
//using System.Drawing;
using System.Web;
using UOB.SVS.Model.Helper;

namespace UOB.SVS.Controllers
{
    public class UpdateCentralizeController : PageController
    {
        private ICentralizeHRepository _hRepository;
        private ICentralizeUnitHReqRepository _centralizeHReqRepository;
        private ICentralizeHReqService _centralizeHReqService;
        private IAuditTrailLog _auditRepository;
        private ICentralUnitDocTypeRepository _doctypeRepository;
        private IRegionRepository _regionRepository;
        private IDivisionRepository _divisionRepository;
        private IDepartementRepository _departementRepository;
        private IParameterCentralUnitRepository _parameterRepository;

        public UpdateCentralizeController(IParameterCentralUnitRepository parameterRepository,ISessionAuthentication sessionAuthentication, ICentralizeHRepository hRepository, ICentralizeUnitHReqRepository centralizeHReqRepository,
            ICentralizeHReqService centralizeHReqService, IAuditTrailLog auditRepository, ICentralUnitDocTypeRepository centralUnitDocTypeRepository, 
            IRegionRepository regionRepository, IDivisionRepository divisionRepository, IDepartementRepository departementRepository) : base(sessionAuthentication)
        {
            this._parameterRepository = parameterRepository;
            this._hRepository = hRepository;
            this._centralizeHReqRepository = centralizeHReqRepository;
            this._centralizeHReqService = centralizeHReqService;
            this._auditRepository = auditRepository;
            this._doctypeRepository = centralUnitDocTypeRepository;
            this._regionRepository = regionRepository;
            this._divisionRepository = divisionRepository;
            this._departementRepository = departementRepository;
            Settings.ModuleName = "UpdateCentralize";
            Settings.Title = "Edit/Update";
        }
        protected override void Startup()
        {
            Create();
        }
        public ActionResult Create()
        {
            Parameter param = Lookup.Get<Parameter>();

            ViewData["ActionName"] = "Create";
            //ViewData["Parameter"] = param;

            CentralizeUnitHReqView data = new CentralizeUnitHReqView();
            return CreateView(data);
        }
        private ViewResult CreateView(CentralizeUnitHReqView data)
        {
            List<RoleDocType> authDocs = Lookup.Get<List<RoleDocType>>();
            ViewData["DocCount"] = authDocs.Count;

            ViewData["AllDocTypeMaster"] = createAllDocTypeSelect();
            ViewData["DocTypeMaster"] = createDocTypeSelect("");
            ViewData["RegionMaster"] = createRegionSelect("");
            ViewData["DivisionMaster"] = createDivisionSelect(data.Region.IsNullOrEmpty() ? "-" : data.Region, data.Division.IsNullOrEmpty() ? "-" : data.Division);
            ViewData["DepartementMaster"] = createDepartementSelect(data.Region.IsNullOrEmpty() ? "-" : data.Region, data.Division.IsNullOrEmpty() ? "-" : data.Division, data.Departement.IsNullOrEmpty() ? "-" : data.Departement);
            return View("Detail", data);
        }
        private IList<SelectListItem> createDocTypeSelect(string selected)
        {
            List<CentralUnitDocType> docList = _doctypeRepository.FindAll().ToList();
            List<CentralUnitDocType> finalDocList = new List<CentralUnitDocType>();
            foreach (CentralUnitDocType item in docList)
            {
                finalDocList.Add(item);
            }

            IList<SelectListItem> dataList = finalDocList.ConvertAll<SelectListItem>(new Converter<CentralUnitDocType, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (!selected.IsNullOrEmpty())
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }
        private IList<SelectListItem> createRegionSelect(string selected)
        {
            IList<SelectListItem> dataList = _regionRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Region, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Value);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (!selected.IsNullOrEmpty())
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }
        private IList<SelectListItem> createDivisionSelect(string Region, string selected)
        {
            IList<SelectListItem> dataList = _divisionRepository.getDivisionbyRegion(Region).ToList().ConvertAll<SelectListItem>(new Converter<Division, SelectListItem>(ConvertFrom));
            IEnumerable<SelectListItem> sortedEnum = null;
            IList<SelectListItem> sortedList = null;

            if ((selected != "-"))
            {
                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                if (dataList.Count > 0)
                {
                    sortedList.FindElement(item => item.Value == selected).Selected = true;
                }
            }
            else
            {
                SelectListItem item = new SelectListItem();
                item.Text = "Select";
                item.Value = "";
                dataList.Add(item);

                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                sortedList.FindElement(x => x.Value == "").Selected = true;
            }

            return sortedList;
        }
        private IList<SelectListItem> createDepartementSelect(string Region,string Division, string selected)
        {          
            IList<SelectListItem> dataList = _departementRepository.getDepartementbyRegion(Region,Division).ToList().ConvertAll<SelectListItem>(new Converter<Departement, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = null;
            IList<SelectListItem> sortedList = null;

            if ((selected != "-"))
            {
                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                if (dataList.Count > 0)
                {
                    sortedList.FindElement(item => item.Value == selected).Selected = true;
                }
            }
            else
            {
                SelectListItem item = new SelectListItem();
                item.Text = "Select";
                item.Value = "";
                dataList.Add(item);

                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                sortedList.FindElement(x => x.Value == "").Selected = true;
            }

            return sortedList;
        }
        private IList<SelectListItem> createAllDocTypeSelect()
        {
            List<CentralUnitDocType> docList = _doctypeRepository.FindAll().ToList();

            IList<SelectListItem> dataList = docList.ConvertAll<SelectListItem>(new Converter<CentralUnitDocType, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
            IList<SelectListItem> sortedList = sortedEnum.ToList();
            return sortedList;
        }
        public SelectListItem ConvertFrom(Region item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }
        public SelectListItem ConvertFrom(Division item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }
        public SelectListItem ConvertFrom(Departement item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }
        public SelectListItem ConvertFrom(CentralUnitDocType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult ChangeDropdownRegion(string Region)
        {
            CentralizeUnitHReqView item = new CentralizeUnitHReqView();
            item.Divisions = _divisionRepository.getDivisionbyRegion(Region);

            return Json(item, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult ChangeDropdownDepartements(string region, string division)
        {
            CentralizeUnitHReqView item = new CentralizeUnitHReqView();
            item.Departements = _departementRepository.getDepartementbyRegion(region, division);

            return Json(item, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Search()
        {
            JsonResult result = new JsonResult();

            try
            {
                // Initialization.
                User user = Lookup.Get<User>();

                CentralizeReqFilter filter = new CentralizeReqFilter();

                string NIK = Request.QueryString.GetValues("NIK")[0];
                string Name = Request.QueryString.GetValues("Name")[0];
                string Titles = Request.QueryString.GetValues("Titles")[0];
                string Region = Request.QueryString.GetValues("Region")[0];

                string Division = string.Empty;
                string Departement = string.Empty;
                if (!(Request.QueryString.GetValues("Division").IsNullOrEmpty()))
                {
                    Division = Request.QueryString.GetValues("Division")[0];
                }

                if (!Request.QueryString.GetValues("Departement").IsNullOrEmpty())
                {
                    Departement = Request.QueryString.GetValues("Departement")[0];
                }

                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();

                var sortColumn = "NIK";
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                filter.NIK = NIK;
                filter.Name = Name;
                filter.Titles = Titles;
                filter.Region = Region;
                filter.Division = Division;
                filter.Departement = Departement;
                // Loading.   
                PagedResult<CentralizeHeader> data;

                data = _hRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);
                var items = data.Items.ToList().ConvertAll<CentralizeUnitHReqView>(new Converter<CentralizeHeader, CentralizeUnitHReqView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }
        
        public CentralizeUnitHReqView ConvertFrom(CentralizeHeader item)
        {
            User user = Lookup.Get<User>();

            CentralizeUnitHReqView returnItem = new CentralizeUnitHReqView();
            returnItem.NIK = item.NIK;
            returnItem.Name = HttpUtility.HtmlEncode(item.Name);
            returnItem.Region = item.Region;
            returnItem.Division = item.Division;
            returnItem.Departement = item.Departement;
            returnItem.Titles = item.Titles;
            returnItem.Note = HttpUtility.HtmlEncode(item.Note);
            returnItem.RequestType = "U";
            returnItem.RequestDate = DateTime.Now;
            returnItem.RequestUser = user.Username;
            returnItem.RequestReason = "";

            int index = 1;
            if (item.Documents != null)
            {
                IList<CentralizeDocument> docs = new List<CentralizeDocument>();
                foreach (CentralizeDDoc row in item.Documents)
                {
                    CentralizeDocument doc = new CentralizeDocument();
                    doc.ID = row.ID;
                    doc.Index = index++;
                    doc.DocumentType = row.DocumentType;
                    doc.FileBlob = HttpUtility.HtmlEncode(row.FileBlob).IsNull() ? "null" : HttpUtility.HtmlEncode(row.FileBlob);
                    doc.FileType = HttpUtility.HtmlEncode(row.FileType);
                    doc.NIK = row.NIK;
                    doc.IsNew = true;
                    doc.FileSeq = 1;//row.Seq;
                    doc.IsValidDoc = true;
                    docs.Add(doc);
                }
                returnItem.Documents = docs.ToList();
            }

            if (item.Signatures != null)
            {
                index = 1;
                IList<CentralizeSignature> signs = new List<CentralizeSignature>();
                foreach (CentralizeDSign row in item.Signatures)
                {
                    CentralizeSignature sign = new CentralizeSignature();
                    sign.ID = row.Id;
                    sign.Index = index++;
                    sign.CroppedBlob = HttpUtility.HtmlEncode(row.Signature);
                    sign.ImageType = HttpUtility.HtmlEncode(row.ImageType);
                    sign.NIK = row.NIK;
                    sign.IsNew = true;
                    sign.IsValidImage = row.Signature.Length % 4 == 0 ? true : false;
                    signs.Add(sign);
                }
                returnItem.Signatures = signs.ToList();
            }

            return returnItem;
        }
        public ActionResult Edit(string id)
        {
            User user = Lookup.Get<User>();
            Parameter param = Lookup.Get<Parameter>();
            ViewData["ActionName"] = "Update";
            ViewData["Parameter"] = param;

            //CentralizeHeader reqH = _hRepository.getCentralizeHeader(nik, name, titles, region, division, departement);
            CentralizeHeader reqH = _hRepository.getCentralizeHeader(id);
            if (reqH.IsNull())
            {
                ScreenMessages.Submit(ScreenMessage.Error("NIK does not exist!"));
                return RedirectToAction("Index");
            }

            //var branches = getBranches();
            //if (user.BranchCode != "000" && !branches.Exists(x => x.BranchCode == reqH.BranchCode))
            //{
            //    ScreenMessages.Submit(ScreenMessage.Error("Account number does not belong to your branch!"));
            //    return RedirectToAction("Index");
            //}
            reqH.Documents = _hRepository.getDocDet(reqH.NIK);
            reqH.Signatures = _hRepository.getSignDet(reqH.NIK);

            CentralizeUnitHReqView data = ConvertFrom(reqH);
            Session["SignHReqData"] = data;

            return CreateView(data);
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult Update(CentralizeUnitHReqView data)
        {
            ViewData["ActionName"] = "Update";

            User user = Lookup.Get<User>();
            string returnCode = "";
            string warningmessage = "";
            string NIK = "";
            string message = "";
            try
            {
                //AccountMaster accMaster = _accountMasterRepository.getAccount(data.NIK);

                message = validateData(data, "Update");

                if (message.IsNullOrEmpty())
                {
                    CentralizeUnitHRequest newData = new CentralizeUnitHRequest("");
                    NIK = data.NIK;
                    newData.NIK = data.NIK;
                    newData.Name = data.Name;
                    newData.DocType = "";
                    newData.Titles = data.Titles;
                    newData.Region = data.Region;
                    newData.Division = data.Division;
                    newData.Departement = data.Departement;
                    newData.Note = data.Note;
                    newData.RequestType = "U";
                    newData.RequestDate = DateTime.Now;
                    newData.RequestUser = user.Username;
                    newData.RequestReason = "Update SVS";

                    IList<CentralizeDDocReq> docs = new List<CentralizeDDocReq>();
                    if (data.Documents != null)
                    {
                        foreach (CentralizeDocument item in data.Documents)
                        {
                            CentralizeDDocReq doc = new CentralizeDDocReq(item.ID);
                            docs.Add(doc);
                        }
                    }
                    newData.Documents = docs;

                    IList<CentralizeDSignReq> signs = new List<CentralizeDSignReq>();
                    if (data.Signatures != null)
                    {
                        foreach (CentralizeSignature item in data.Signatures)
                        {
                            CentralizeDSignReq sign = new CentralizeDSignReq(item.ID);
                            signs.Add(sign);
                        }
                    }
                    newData.Signatures = signs;

                    message = _centralizeHReqService.UpdateCentralizeReq(newData);

                    if (data.Signatures != null)
                    {
                        foreach (var item in data.Signatures)
                        {
                            item.NIK = data.NIK;
                            message = SignatureSave(item);
                            if (!message.IsNullOrEmpty())
                                throw new Exception(message);
                        }
                    }
                    /*Save Doc*/

                    if (data.Documents != null)
                    {
                        foreach (var item in data.Documents)
                        {
                            item.NIK = data.NIK;
                            message = DocSave(item);
                            if (!message.IsNullOrEmpty())
                                throw new Exception(message);
                        }
                    }

                    // Hapus detail data, biar audit trailnya ngak penuh
                    newData.Documents = null;
                    newData.Signatures = null;
                }
            }
            catch (Exception e)
            {
                message = e.Message;
                returnCode = "ERR";
            }

            if (message.IsNullOrEmpty())
            {
                returnCode = "OK";
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
            }
            else
            {
                returnCode = "ERR";
                ScreenMessages.Submit(ScreenMessage.Error(message));
            }

            JsonResult result = new JsonResult();
            result = this.Json(new
            {
                message = message,
                returnCode = returnCode,
                warningMessage = warningmessage,
                NIK = NIK
            });
            return result;
        }

        public string DocSave(CentralizeDocument data)
        {
            User user = Lookup.Get<User>();
            string message = "";
            string NewFileBlob = "";
            try
            {
                //ParameterCentralUnit param = Lookup.Get<ParameterCentralUnit>();
                ParameterCentralUnit param = _parameterRepository.getParameter();
                if (data.FileBlob.Length % 4 != 0)
                {
                    message = "Invalid uploaded document format!";
                }
                if (data.FileBlob.Length > param.GetSizeDocument)
                {
                    message = "Document " + data.FileName + " exceed maximum file size (" + (data.FileBlob.Length * 1365) + " KB) and cannot be uploaded!";
                }

                if (data.FileBlob == "null")
                {
                    CentralizeDDoc dDoc = _hRepository.getDocDet(data.ID);
                    NewFileBlob = dDoc.FileBlob;
                }

                if (message.IsNullOrEmpty())
                {
                    //CentralizeUnitHRequest reqH = _unithreqRepository.getCentralizeUnitHReq(data.NIK);

                    //if (reqH == null)
                    //{
                    //    message = SvsResources.Validation_AccountNotFound;
                    //}
                    //else if (NewFileBlob == "null")
                    if (NewFileBlob == "null")
                    {
                        message = "Invalid Document " + data.DocumentType;
                    }
                    else
                    {
                        CentralizeDDocReq doc = new CentralizeDDocReq(0);
                        doc.NIK = data.NIK;
                        doc.DocumentType = data.DocumentType;
                        doc.FileBlob = data.FileBlob == "null" ? NewFileBlob : data.FileBlob;
                        doc.FileType = data.FileType;
                        doc.FileSeq = data.Index;
                        doc.RequestType = "C";
                        doc.RequestDate = DateTime.Now;
                        doc.RequestUser = user.Username;
                        doc.RequestReason = "Create New";
                        message = _centralizeHReqService.SaveDocument(doc);
                    }
                }
            }
            catch (Exception e)
            {
                message = e.Message;
                //returnCode = "ERR";
            }

            if (message.IsNullOrEmpty())
            {
                message = "";
                //returnCode = "OK";
            }
            else
            {
                ScreenMessages.Submit(ScreenMessage.Error(message));
                //returnCode = "ERR";
            }

            return message;
        }

        public string SignatureSave(CentralizeSignature data)
        {
            User user = Lookup.Get<User>();
            //_unithreqRepository.IsDuplicate(data.NIK);
            //_doctypeRepository
            string message = "";
            try
            {
                //ParameterCentralUnit param = Lookup.Get<ParameterCentralUnit>();
                ParameterCentralUnit param = _parameterRepository.getParameter();

                if (data.CroppedBlob.Length % 4 != 0)
                {
                    message = "Invalid uploaded image format!";
                    throw new Exception(message);
                }
                if (data.CroppedBlob.Length > param.GetSizeSignature)
                {
                    message = "Signature image " + data.FileName + " exceed maximum file size (" + (data.CroppedBlob.Length * 1365) + " KB) and cannot be uploaded!";
                    //message = SvsResources.Validation_MaxSizeExceeded;
                    throw new Exception(message);
                }

                if (message.IsNullOrEmpty())
                {
                    //CentralizeUnitHRequest reqH = _unithreqRepository.getCentralizeUnitHReq(data.NIK);
                    //if (reqH == null)
                    //{
                    //    message = SvsResources.Validation_AccountNotFound;
                    //    throw new Exception(message);
                    //}
                    //else
                    //{
                    CentralizeDSignReq sign = new CentralizeDSignReq(0);
                    sign.NIK = data.NIK;
                    sign.Signature = data.CroppedBlob;
                    sign.ImageType = data.ImageType;
                    sign.RequestType = "C";
                    sign.FileSeq = data.Index;
                    sign.RequestDate = DateTime.Now;
                    sign.RequestUser = user.Username;
                    sign.RequestReason = "Create New";
                    message = _centralizeHReqService.SaveSign(sign);
                    //}
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }

        private string validateData(CentralizeUnitHReqView data, string mode)
        {
            User user = Lookup.Get<User>();

            if (data.NIK == "")
                return "Null NIK";

            if (data.Name == "")
                return "Null Name";

            CentralizeUnitHReqView currData = (CentralizeUnitHReqView)Session["SignHReqData"];
            CentralizeUnitHRequest existingSHQ = _centralizeHReqRepository.getCentralizeUnitHReq(data.NIK);

            if (data.NIK.Trim() != currData.NIK.Trim())
            {
                return "Update failed, invalid or not exist account number!";
            }

            if (!existingSHQ.IsNull())
            {
                if (existingSHQ.RequestType == "D" && existingSHQ.IsRejected == false)
                {
                    return "Update failed, NIK " + data.NIK.Trim() + " is awaiting for DELETE approval";
                }
                else if (existingSHQ.RequestType == "U" && existingSHQ.IsRejected == false)
                {
                    return "Update failed, NIK " + data.NIK.Trim() + " is awaiting for UPDATE approval";
                }

            }

            //if (_signatureHReqRepository.IsDuplicate(data.NIK))
            //    return SvsResources.Validation_DuplicateData;

            return "";
        }

        [HttpGet]
        public ActionResult NoAuthCheckViewDocument(int ID)
        {
            JsonResult result = new JsonResult();
            CentralizeDDoc doc = _hRepository.getDocDet(ID);
            if (doc.IsNull())
            {
                return Content("Not exist or invalid document");
            }
            byte[] blob = Convert.FromBase64String(doc.FileBlob);

            string filetype = doc.FileType.Split(';')[0].Split(':')[1];
            if (filetype.Trim() == "application/pdf")
            {
                return File(blob, filetype);
            }
            else
            {
                Stream strm = new MemoryStream(Convert.FromBase64String(doc.FileBlob));
                System.Drawing.Bitmap bmp = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(strm);

                System.Drawing.ImageFormatConverter imgConverter = new System.Drawing.ImageFormatConverter();
                var png = imgConverter.ConvertFromString("PNG");

                MemoryStream ms = new MemoryStream();
                bmp.Save(ms, (ImageFormat)png);

                byte[] bytImg = ms.ToArray();
                string strImg = Convert.ToBase64String(bytImg);
                return Content("<img src='data:image/PNG;base64," + strImg + "' alt='" + doc.DocumentType + "'/>");
            }
        }

    }
}