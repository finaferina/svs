﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;

namespace UOB.SVS.Controllers
{
    public class ErrorController : PageController
    {
        public ErrorController(ISessionAuthentication sessionAuthentication) : base(sessionAuthentication)
        {
        }

        protected override void Startup()
        {
        }

        public ActionResult NotFound()
        {
            return View();
        }
    }
}