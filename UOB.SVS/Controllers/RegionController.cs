﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;

namespace UOB.SVS.Controllers
{
    public class RegionController : PageController
    {
        private string _application = "SVS";
        private IRegionRepository _regionRepository;
        private IRegionCrRepository _regionCrRepository;
        private IApplicationRepository _appRepository;
        private IAppAuditTrailLog _auditRepository;
        private IDivisionRepository _divisionRepository;
        private IDepartementRepository _departementRepository;
        public RegionController(ISessionAuthentication sessionAuthentication, IRegionRepository regionRepository, IRegionCrRepository regionCrRepository,
            IApplicationRepository appRepository, IAppAuditTrailLog auditRepository, IDivisionRepository divisionRepository, 
            IDepartementRepository departementRepository) : base(sessionAuthentication)
        {
            this._regionRepository = regionRepository;
            this._regionCrRepository = regionCrRepository;
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;
            this._divisionRepository = divisionRepository;
            this._departementRepository = departementRepository;
            Settings.ModuleName = "Region";
            Settings.Title = "Directorate / Region";
        }

        protected override void Startup()
        {
            ViewData["loadState"] = (Request.QueryString.GetValues("loadState") == null) ? "0" : Request.QueryString.GetValues("loadState").FirstOrDefault();
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<Region> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _regionRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _regionRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<RegionView>(new Converter<Region, RegionView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public RegionView ConvertFrom(Region item)
        {
            RegionView returnItem = new RegionView();
            returnItem.Application = item._Application;
            returnItem.Alias = item.Alias;
            returnItem.RegionId = item.RegionId;
            returnItem.Id = item.Id;
            returnItem.Name = item.Name;
            return returnItem;
        }

        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            RegionView data = new RegionView();
            return CreateView(data);
        }

        private ViewResult CreateView(RegionView data)
        {
            return View("Detail", data);
        }

        // POST: Function/Create
        [HttpPost]
        public ActionResult Create(RegionView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    //Region newData = new Region(0L);
                    RegionCr newData = new RegionCr(0L);
                    newData._Application = _application;
                    newData.RegionId = data.RegionId;
                    newData.Name = data.Name;
                    newData.Alias = data.Alias;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    newData.ApprovalType = "C";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;
                    //_regionRepository.Add(newData);
                    _regionCrRepository.Add(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success("Data Berhasil di Simpan!"));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(RegionView data, string mode)
        {
            if (data.Name == "")
                return "Nama Tidak Boleh Kosong";

            if (data.RegionId == "")
                return "Region ID Tidak Boleh Kosong";

            if (mode == "Create")
            {
                if (_regionRepository.IsDuplicate(_application, data.RegionId))
                    return "Region ID dan Nama Sudah Pernah Ada";
                else if (_regionRepository.IsDuplicateCR(_application, data.RegionId, "N"))
                    return "Region ID dan Nama Sudah Ada di Approval";

                if (_regionRepository.IsDuplicateByAlias(_application, data.Alias))
                    return "Alias Sudah Pernah Ada";
                else if (_regionRepository.IsDuplicateCRByAlias(_application, data.Alias, "N"))
                    return "Alias Sudah Ada di Approval";
            }
            return "";
        }

        // GET: Function/Edit/5
        public ActionResult Edit(long id)
        {
            ViewData["ActionName"] = "Edit";
            Region autoRegion = _regionRepository.getRegion(id);
            RegionView data = ConvertFrom(autoRegion);

            return CreateView(data);
        }

        // GET: Function/Delete/5
        public ActionResult Delete(long id)
        {
            ViewData["ActionName"] = "Delete";
            Region autoRegion = _regionRepository.getRegion(id);
            RegionView data = ConvertFrom(autoRegion);

            return CreateView(data);
        }

        // POST: Function/Edit/5
        [HttpPost]
        public ActionResult Edit(RegionView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    //Region app = _regionRepository.getRegion(data.Id);
                    //if (!app.IsNull())
                    //{
                    //    _regionRepository.UpdateRegion(data.Id, data.RegionId, data.Name, data.Alias, user.Username, DateTime.Now);
                    //}
                    RegionCr newData = new RegionCr(0L);
                    newData._Application = _application;
                    newData.RegionId = data.RegionId;
                    newData.Name = data.Name;
                    newData.Alias = data.Alias;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    newData.ApprovalType = "U";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;
                    _regionCrRepository.Add(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success("Data Berhasil di Ubah!"));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        // POST: Function/Delete/5
        [HttpPost]
        public ActionResult Delete(RegionView data)
        {
            User user = Lookup.Get<User>();
            Region regiondata = _regionRepository.getRegion(data.Id);
            string message = "";
            try
            {
                message = validateDelete(regiondata.Alias);
                if(message.IsNullOrEmpty())
                {
                    //_regionRepository.Remove(regiondata);
                    RegionCr newData = new RegionCr(0L);
                    newData._Application = regiondata._Application;
                    newData.RegionId = regiondata.RegionId;
                    newData.Name = regiondata.Name;
                    newData.Alias = regiondata.Alias;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    newData.ApprovalType = "D";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;

                    _regionCrRepository.Save(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success("Data Berhasil di Hapus!"));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(data.Id);
        }

        private string validateDelete(string regionid)
        {
            if (_divisionRepository.IsFound(regionid))
                return "Region Masih Terdaftar di Division";

            if (_departementRepository.IsFoundRegion(regionid))
                return "Region Masih Terdaftar di Departement";

            return "";
        }
    }
}