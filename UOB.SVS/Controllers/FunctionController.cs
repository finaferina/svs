﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;

namespace UOB.SVS.Controllers
{
    public class FunctionController : PageController
    {
        private IFunctionRepository _funcRepository;
        private IApplicationRepository _appRepository;
        private IAppAuditTrailLog _auditRepository;
        private IFunctionCrRepository _funCrRepository;
        public FunctionController(ISessionAuthentication sessionAuthentication, IFunctionRepository funcRepository,
            IApplicationRepository appRepository, IAppAuditTrailLog auditRepository, IFunctionCrRepository funCrRepository) : base(sessionAuthentication)
        {
            this._funcRepository = funcRepository;
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;
            this._funCrRepository = funCrRepository;
            Settings.ModuleName = "Function";
            Settings.Title = FunctionResources.PageTitle;
        }

        protected override void Startup()
        {
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<AuthorizationFunction> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _funcRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _funcRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<FunctionView>(new Converter<AuthorizationFunction, FunctionView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public FunctionView ConvertFrom(AuthorizationFunction item)
        {
            FunctionView returnItem = new FunctionView();
            returnItem.Application = item.Application;
            returnItem.Description = item.Description;
            returnItem.FunctionId = item.FunctionId;
            returnItem.Id = item.Id;
            returnItem.Name = item.Name;
            return returnItem;
        }

        public SelectListItem ConvertFrom(Application item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Name;
            returnItem.Value = item.ApplicationId;
            return returnItem;
        }

        // GET: Function/Create
        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            FunctionView data = new FunctionView();
            return CreateView(data);
        }

        private ViewResult CreateView(FunctionView data)
        {
            ViewData["ApplicationMaster"] = createApplicationSelect(data.Application);
            return View("Detail", data);
        }

        private IList<SelectListItem> createApplicationSelect(string selected)
        {
            IList<SelectListItem> dataList = _appRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Application, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        // POST: Function/Create
        [HttpPost]
        public ActionResult Create(FunctionView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    //AuthorizationFunction newData = new AuthorizationFunction(0L);
                    AuthorizationFunctionCr newData = new AuthorizationFunctionCr(0L);
                    newData.Application = data.Application;
                    newData.FunctionId = data.FunctionId;
                    newData.Name = data.Name;
                    newData.Description = data.Description;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    //new modified by fahmi, approval additional field
                    newData.ApprovalType = "C";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;
                    //_funcRepository.Add(newData);
                    _funCrRepository.Add(newData);
                    //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(FunctionView data, string mode)
        {
            if (data.Application == "")
                return FunctionResources.Validation_SelectApplication;

            if (data.Name == "")
                return FunctionResources.Validation_FillFunctionName;

            if (data.FunctionId == "")
                return FunctionResources.Validation_FillFunctionId;

            if (mode == "Create")
            {
                if (_funcRepository.IsDuplicate(data.Application, data.FunctionId))
                    return FunctionResources.Validation_DuplicateData;
            }


            return "";
        }


        // GET: Function/Edit/5
        public ActionResult Edit(long id)
        {
            ViewData["ActionName"] = "Edit";
            AuthorizationFunction autoFunc = _funcRepository.getFunction(id);
            FunctionView data = ConvertFrom(autoFunc);

            return CreateView(data);
        }

        // POST: Function/Edit/5
        [HttpPost]
        public ActionResult Edit(FunctionView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    //new additional by fahmi
                    AuthorizationFunctionCr appCr = _funCrRepository.getExistingRequest(data.FunctionId);
                    if (!appCr.IsNull())
                    {
                        _funCrRepository.deleteExistingRequest(data.FunctionId);
                    }

                    //AuthorizationFunction newData = new AuthorizationFunction(data.Id);
                    // _auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "BeforeEdit");

                    AuthorizationFunctionCr newData = new AuthorizationFunctionCr(0L);

                    newData.Application = data.Application;
                    newData.FunctionId = data.FunctionId;
                    newData.Name = data.Name;
                    newData.Description = data.Description;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;
                    //new modified by fahmi, approval additional field
                    newData.ApprovalType = "U";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;
                    //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                    //_funcRepository.Save(newData);
                    _funCrRepository.Add(newData);
                    //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Update Request");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        // POST: Function/Delete/5
        [HttpPost]
        public ActionResult Delete(int Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {

                AuthorizationFunction FunctionData = _funcRepository.getFunction(Id);
                AuthorizationFunctionCr newData = new AuthorizationFunctionCr(0L);

                newData.Application = FunctionData.Application;
                newData.FunctionId = FunctionData.FunctionId;
                newData.Name = FunctionData.Name;
                newData.Description = FunctionData.Description;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                //new modified by fahmi, approval additional field
                newData.ApprovalType = "D";
                newData.ApprovalStatus = "N";
                newData.Approved = false;
                //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "AfterEdit");
                //_funcRepository.Save(newData);
                _funCrRepository.Save(newData);
                //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete Request");

                //AuthorizationFunction newData = new AuthorizationFunction(Id);
                //_funcRepository.Remove(newData);
                //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }
    }
}