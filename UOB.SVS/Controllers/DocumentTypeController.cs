﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;
using System.Globalization;
using Treemas.Base.Resources;
using UOB.SVS.Interface;
using UOB.SVS.Model;

namespace UOB.SVS.Controllers
{
    public class DocumentTypeController : PageController
    {
        private IUserRepository _userRepository;
        private IDocumentTypeRepository _DocumentTypeRepository;
        private IDocumentTypeCrRepository _DocumentTypeCrRepository;
        private IAuditTrailLog _auditRepository;
        private IRoleDocTypeRepository _roleDocTypeRepository;

        public DocumentTypeController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository, IDocumentTypeRepository DocumentTypeRepository,
            IDocumentTypeCrRepository DocumentTypeCrRepository, IAuditTrailLog auditRepository, IRoleDocTypeRepository roleDocTypeRepository) : base(sessionAuthentication)
        {
            this._DocumentTypeRepository = DocumentTypeRepository;
            this._DocumentTypeCrRepository = DocumentTypeCrRepository;
            this._userRepository = userRepository;
            this._auditRepository = auditRepository;
            this._roleDocTypeRepository = roleDocTypeRepository;

            Settings.ModuleName = "Master DocumentType";
            Settings.Title = "Master DocumentType";
        }

        protected override void Startup()
        {

        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.  

                PagedResult<DocumentType> appData = _DocumentTypeRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);

                var items = appData.Items.ToList().ConvertAll<DocumentTypeView>(new Converter<DocumentType, DocumentTypeView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = appData.TotalItems,
                    recordsFiltered = appData.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public DocumentTypeView ConvertFrom(DocumentType item)
        {
            DocumentTypeView returnItem = new DocumentTypeView();
            //DocumentType mainb = _DocumentTypeRepository.getDocumentType(Convert.ToInt64(item.DocType));

            returnItem.Id = item.Id;
            returnItem.DocType = item.DocType;
            returnItem.Description = item.Description;
            returnItem.CreatedDate = item.CreatedDate.IsNull() ? null : Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
            returnItem.CreatedBy = item.CreatedBy;
            returnItem.ChangedDate = item.ChangedDate.IsNull() ? null : Convert.ToDateTime(item.ChangedDate).ToString("dd/MM/yyyy");
            returnItem.ChangedBy = item.ChangedBy;
            return returnItem;
        }

        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            //ViewData["MainDocumentTypeList"] = createMainDocumentTypeSelect("");
            DocumentTypeView data = new DocumentTypeView();
            return CreateView(data);
        }

        private ViewResult CreateView(DocumentTypeView data)
        {
            //ViewData["MainDocumentTypeList"] = createMainDocumentTypeSelect(data.MainDocumentTypeID);
            return View("Detail", data);
        }

        [HttpPost]
        public ActionResult Create(DocumentTypeView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");

                if (message.IsNullOrEmpty())
                {
                    bool docCrExist = _DocumentTypeCrRepository.IsExist(data.DocType, ApprovalType.C);
                    if (docCrExist)
                    {
                        ScreenMessages.Submit(ScreenMessage.Error("Create failed, Doc Type " + data.DocType.Trim() + " is awaiting for CREATE approval!"));
                        CollectScreenMessages();
                        return CreateView(data);
                    }

                    DocumentTypeCr newData = new DocumentTypeCr(0L);
                    newData.DocType = data.DocType;
                    newData.Description = data.Description;

                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;

                    newData.ApprovalType = "C";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;

                    _DocumentTypeCrRepository.Add(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("", "DocumentType");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(DocumentTypeView data, string mode)
        {
            if (mode == "Create")
            {
                if (_DocumentTypeRepository.IsDuplicate(data.DocType))
                return ApplicationResources.Validation_DuplicateData + " Document Type Id";
            }
            //ApprovalType appType = new ApprovalType();
            //if (mode == "Create")
            //    appType = ApprovalType.C;
            //if (mode == "Edit")
            //    appType = ApprovalType.U;
            //if (mode == "Delete")
            //    appType = ApprovalType.D;

            //bool docCr = _DocumentTypeCrRepository.IsExist(data.DocType, appType);

            //if (docCr)
            //{
            //    return "There is already same request that need approval.";
            //}
            return "";
        }

        private string ValidateApproval(DocumentTypeView data,  string mode)
        {
            ApprovalType appType = new ApprovalType();
            if (mode == "Create")
                appType = ApprovalType.C;
            if (mode == "Edit")
                appType = ApprovalType.U;
            if (mode == "Delete")
                appType = ApprovalType.D;

            DocumentTypeCr docCr = _DocumentTypeCrRepository.getDocTypeCr(data.Id, appType);
            if (!docCr.IsNull())
            {
                return "There is already same request that need approval.";
            }
            return "";
        }

        public ActionResult Edit(long id)
        {
            ViewData["ActionName"] = "Edit";
            DocumentType DocumentType = _DocumentTypeRepository.getDocumentType(id);
            DocumentTypeView data = ConvertFrom(DocumentType);
            //ViewData["MainDocumentTypeList"] = createMainDocumentTypeSelect(data.MainDocumentTypeID);
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult Edit(DocumentTypeView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                DocumentType DocumentType = _DocumentTypeRepository.getDocumentType(data.Id);
                if (DocumentType.Description == data.Description)
                {
                    ScreenMessages.Submit(ScreenMessage.Error("No changes to submitted!"));
                    CollectScreenMessages();
                    return CreateView(data);
                }

                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {
                    bool docUExist = _DocumentTypeCrRepository.IsExist(data.DocType, ApprovalType.U);
                    if (docUExist)
                    {
                        ScreenMessages.Submit(ScreenMessage.Error("Update failed, Doc Type " + data.DocType.Trim() + " is awaiting for UPDATE approval!"));
                        CollectScreenMessages();
                        return CreateView(data);
                    }
                    bool docDExist = _DocumentTypeCrRepository.IsExist(data.DocType, ApprovalType.D);
                    if (docDExist)
                    {
                        ScreenMessages.Submit(ScreenMessage.Error("Update failed, Doc Type " + data.DocType.Trim() + " is awaiting for DELETE approval!"));
                        CollectScreenMessages();
                        return CreateView(data);
                    }

                    DocumentTypeCr newData = new DocumentTypeCr(0L);

                    newData.DocType = data.DocType;
                    newData.Description = data.Description;
                    newData.DocTypeId = data.Id;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;

                    newData.ApprovalType = "U";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;

                    _DocumentTypeCrRepository.Add(newData);

                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("", "DocumentType");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult Delete(long Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                DocumentType appData = _DocumentTypeRepository.getDocumentType(Id);
                DocumentTypeView data = ConvertFrom(appData);

                RoleDocType rddata = _roleDocTypeRepository.getRoleDocByDocType(Id);
                if (!rddata.IsNull())
                {
                    ScreenMessages.Submit(ScreenMessage.Error("Delete failed, Doc Type " + appData.DocType.Trim() + " is still used at Role Document Type!"));
                    CollectScreenMessages();
                    return CreateView(data);
                }


                message = validateData(data, "Delete");

                if (message.IsNullOrEmpty())
                {
                    bool docUExist = _DocumentTypeCrRepository.IsExist(data.DocType, ApprovalType.U);
                    if (docUExist)
                    {
                        ScreenMessages.Submit(ScreenMessage.Error("Delete failed, Doc Type " + data.DocType.Trim() + " is awaiting for UPDATE approval!"));
                        CollectScreenMessages();
                        return CreateView(data);
                    }
                    bool docDExist = _DocumentTypeCrRepository.IsExist(data.DocType, ApprovalType.D);
                    if (docDExist)
                    {
                        ScreenMessages.Submit(ScreenMessage.Error("Delete failed, Doc Type " + data.DocType.Trim() + " is awaiting for DELETE approval!"));
                        CollectScreenMessages();
                        return CreateView(data);
                    }

                    DocumentTypeCr newData = new DocumentTypeCr(0L);
                    newData.DocType = appData.DocType;
                    newData.Description = appData.Description;
                    newData.DocTypeId = appData.Id;
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;

                    newData.ApprovalType = "D";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;

                    _DocumentTypeCrRepository.Add(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("", "DocumentType");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }

        //private IList<SelectListItem> createMainDocumentTypeSelect(string selected)
        //{
        //    IList<DocumentType> DocumentTypes = _DocumentTypeRepository.getDocumentTypes();
        //    var gb = DocumentTypes.AsEnumerable().GroupBy(g => g.MainDocumentType);

        //    List<DocumentType> mainDocumentTypes = new List<DocumentType>();

        //    foreach(IGrouping<string, DocumentType> b in gb)
        //    {
        //        DocumentType nb = DocumentTypes.AsEnumerable().Where(t => t.DocumentTypeCode == b.Key).SingleOrDefault();
        //        if (!nb.IsNull())
        //        {  
        //            if(!mainDocumentTypes.Contains(nb))
        //            mainDocumentTypes.Add(nb);
        //        }
        //    }

        //    IList<SelectListItem> dataList = mainDocumentTypes.ToList().ConvertAll<SelectListItem>(new Converter<DocumentType, SelectListItem>(ConvertFromSelect));


        //    if (!selected.IsNullOrEmpty())
        //    {
        //        dataList.FindElement(item => item.Value == selected).Selected = true;
        //    }
        //    return dataList;
        //}

        public SelectListItem ConvertFromSelect(DocumentType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Description;
            returnItem.Value = item.DocType;
            return returnItem;
        }

    }
}