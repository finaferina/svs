﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;
using System.Globalization;
using Treemas.Base.Resources;
using UOB.SVS.Interface;
using UOB.SVS.Model;

namespace UOB.SVS.Controllers
{
    public class BranchController : PageController
    {
        private IUserRepository _userRepository;
        private IBranchRepository _branchRepository;
        private IBranchCrRepository _branchCrRepository;
        private IAppAuditTrailLog _auditRepository;
        private IBranchAccountPrefixRepository _bapRepository;

        public BranchController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository, IBranchRepository branchRepository, IBranchCrRepository branchCrRepository,
            IAppAuditTrailLog auditRepository, IBranchAccountPrefixRepository bapRepository) : base(sessionAuthentication)
        {
            this._branchRepository = branchRepository;
            this._userRepository = userRepository;
            this._branchCrRepository = branchCrRepository;
            this._auditRepository = auditRepository;
            this._bapRepository = bapRepository;

            Settings.ModuleName = "Master Branch";
            Settings.Title = "Master Branch";
        }

        protected override void Startup()
        {
            ViewData["loadState"] = (Request.QueryString.GetValues("loadState") == null) ? "0" : Request.QueryString.GetValues("loadState").FirstOrDefault();
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.  

                PagedResult<Branch> appData;
                if (searchValue.IsNullOrEmpty())
                {
                    appData = _branchRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    appData = _branchRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = appData.Items.ToList().ConvertAll<BranchView>(new Converter<Branch, BranchView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = appData.TotalItems,
                    recordsFiltered = appData.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public BranchView ConvertFrom(Branch item)
        {
            BranchView returnItem = new BranchView();
            Branch mainb = _branchRepository.getBranch(item.MainBranch);

            returnItem.Id = item.Id;
            returnItem.BranchCode = item.BranchCode;
            returnItem.Description = item.Description;
            returnItem.MainBranch = mainb.IsNull() ? item.MainBranch : mainb.Description;
            returnItem.MainBranchID = item.MainBranch;
            if (item.MainBranch == item.BranchCode)
            {
                returnItem.isMainBranch = true;
            }
            returnItem.CreatedDate = item.CreatedDate.IsNull() ? null : Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
            returnItem.CreatedBy = item.CreatedBy;
            returnItem.ChangedDate = item.ChangedDate.IsNull() ? null : Convert.ToDateTime(item.ChangedDate).ToString("dd/MM/yyyy");
            returnItem.ChangedBy = item.ChangedBy;
            return returnItem;
        }

        public SelectListItem ConvertFrom(BranchView item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.BranchCode;
            returnItem.Value = item.Description;
            return returnItem;
        }

        public ActionResult Create()
        {
            ViewData["ActionName"] = "Create";
            ViewData["MainBranchList"] = createMainBranchSelect("");
            BranchView data = new BranchView();
            return CreateView(data);
        }

        private ViewResult CreateView(BranchView data)
        {
            ViewData["MainBranchList"] = createMainBranchSelect(data.MainBranchID);
            return View("Detail", data);
        }

        [HttpPost]
        public ActionResult Create(BranchView data)
        {
            ViewData["ActionName"] = "Create";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Create");
                if (message.IsNullOrEmpty())
                {
                    BranchCr currentReq = _branchCrRepository.getExistingRequest("C", data.BranchCode);
                    if (!currentReq.IsNull())
                    {
                        ScreenMessages.Submit(ScreenMessage.Error("Create failed, branch code " + data.BranchCode.Trim() + " is awaiting for CREATE approval!"));
                        CollectScreenMessages();
                        return CreateView(data);
                    }

                    BranchCr newData = new BranchCr(0L);
                    newData.BranchCode = data.BranchCode;
                    newData.Description = data.Description;
                    if (data.isMainBranch == true)
                    {
                        newData.MainBranch = data.BranchCode;
                    }
                    else
                    {
                        newData.MainBranch = data.MainBranchID;
                    }
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;

                    newData.ApprovalType = "C";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;

                    _branchCrRepository.Add(newData);
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("", "Branch");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        private string validateData(BranchView data, string mode)
        {
            if (data.BranchCode == "")
                return BranchResources.Validate_BranchCode;

            if (data.Description == "")
                return BranchResources.Validate_Description;

            if (data.MainBranch == "")
                return BranchResources.Validate_MainBranch;

            if (!data.isMainBranch)
            {
                if (data.BranchCode.Trim() == data.MainBranchID)
                {
                    return "Please select other Main Branch!";
                }
            }
            if (mode == "Create")
            {
                if (_branchRepository.IsDuplicate(data.BranchCode))
                    return BranchResources.Validation_DuplicateData;
            }
            return "";
        }

        public ActionResult Edit(long id)
        {
            ViewData["ActionName"] = "Edit";
            Branch branch = _branchRepository.getBranch(id);
            BranchView data = ConvertFrom(branch);
            ViewData["MainBranchList"] = createMainBranchSelect(data.MainBranchID);
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult Edit(BranchView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");

                Branch brcData = _branchRepository.getBranch(data.Id);
                if (brcData.Description == data.Description && brcData.MainBranch == data.MainBranchID && brcData.isMainBranch == data.isMainBranch)
                {
                    ScreenMessages.Submit(ScreenMessage.Error("No changes to submitted!"));
                    CollectScreenMessages();
                    return CreateView(data);
                }

                if (brcData.isMainBranch == true && data.isMainBranch == false)
                {
                    IList<Branch> subBranch = _branchRepository.getBranchs(data.BranchCode);
                    if (subBranch.Count > 1)
                    {
                        ScreenMessages.Submit(ScreenMessage.Error("Update failed, branch code " + data.BranchCode.Trim() + " is still have a sub-branch(s) data!"));
                        CollectScreenMessages();
                        return CreateView(data);
                    }
                }

                BranchCr currentReq = _branchCrRepository.getExistingRequest("U", data.BranchCode);
                if (!currentReq.IsNull())
                {
                    ScreenMessages.Submit(ScreenMessage.Error("Update failed, branch code " + data.BranchCode.Trim() + " is awaiting for UPDATE approval!"));
                    CollectScreenMessages();
                    return CreateView(data);
                }
                BranchCr currentDReq = _branchCrRepository.getExistingRequest("D", data.BranchCode);
                if (!currentDReq.IsNull())
                {
                    ScreenMessages.Submit(ScreenMessage.Error("Update failed, branch code " + data.BranchCode.Trim() + " is awaiting for DELETE approval!"));
                    CollectScreenMessages();
                    return CreateView(data);
                }


                if (message.IsNullOrEmpty())
                {
                    BranchCr newData = new BranchCr(0L);

                    newData.BranchCode = data.BranchCode;
                    newData.Description = data.Description;
                    if (data.isMainBranch == true)
                    {
                        newData.MainBranch = data.BranchCode;
                    }
                    else
                    {
                        newData.MainBranch = data.MainBranchID;
                    }
                    newData.ChangedBy = user.Username;
                    newData.ChangedDate = DateTime.Now;
                    newData.CreatedBy = user.Username;
                    newData.CreatedDate = DateTime.Now;

                    newData.ApprovalType = "U";
                    newData.ApprovalStatus = "N";
                    newData.Approved = false;

                    _branchCrRepository.Add(newData);

                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("", "Branch");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);
        }

        [HttpPost]
        public ActionResult Delete(long Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                Branch appData = _branchRepository.getBranch(Id);
                User usr = _userRepository.GetUserByBranch(appData.BranchCode);

                if (appData.isMainBranch)
                {
                    IList<Branch> subBranch = _branchRepository.getBranchs(appData.MainBranch);
                    if (subBranch.Count > 1)
                    {
                        ScreenMessages.Submit(ScreenMessage.Error("Delete failed, branch code " + appData.BranchCode.Trim() + " is still have a sub-branch(s) data!"));
                        CollectScreenMessages();
                        return Edit(Id);
                    }
                }

                if (!usr.IsNull())
                {
                    ScreenMessages.Submit(ScreenMessage.Error("Delete failed, branch code " + appData.BranchCode.Trim() + " is still used by any User!"));
                    CollectScreenMessages();
                    return Edit(Id);
                }

                IList<BranchAccountPrefix> bap = _bapRepository.getAccountPrefixes(appData.BranchCode);
                if (!bap.IsNull())
                {
                    if (bap.Count > 0)
                    {
                        ScreenMessages.Submit(ScreenMessage.Error("Delete failed, branch code " + appData.BranchCode.Trim() + " is still tagged an account prefix!"));
                        CollectScreenMessages();
                        return Edit(Id);
                    }
                }

                BranchCr currentUReq = _branchCrRepository.getExistingRequest("U", appData.BranchCode);
                if (!currentUReq.IsNull())
                {
                    ScreenMessages.Submit(ScreenMessage.Error("Delete failed, branch code " + appData.BranchCode.Trim() + " is awaiting for UPDATE approval!"));
                    CollectScreenMessages();
                    return Edit(Id);
                }

                BranchCr currentDReq = _branchCrRepository.getExistingRequest("D", appData.BranchCode);
                if (!currentDReq.IsNull())
                {
                    ScreenMessages.Submit(ScreenMessage.Error("Delete failed, branch code " + appData.BranchCode.Trim() + " is awaiting for DELETE approval!"));
                    CollectScreenMessages();
                    return Edit(Id);
                }

                BranchCr newData = new BranchCr(0L);

                newData.BranchCode = appData.BranchCode;
                newData.Description = appData.Description;
                newData.MainBranch = appData.MainBranch;

                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;

                newData.ApprovalType = "D";
                newData.ApprovalStatus = "N";
                newData.Approved = false;

                _branchCrRepository.Add(newData);

            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("", "Branch");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return Edit(Id);
        }

        private IList<SelectListItem> createMainBranchSelect(string selected)
        {
            IList<Branch> branchs = _branchRepository.getBranchs();
            var gb = branchs.AsEnumerable().GroupBy(g => g.MainBranch);

            List<Branch> mainbranchs = new List<Branch>();

            foreach (IGrouping<string, Branch> b in gb)
            {
                Branch nb = branchs.AsEnumerable().Where(t => t.BranchCode == b.Key).SingleOrDefault();
                if (!nb.IsNull())
                {
                    if (!mainbranchs.Contains(nb))
                        mainbranchs.Add(nb);
                }
            }

            IList<SelectListItem> dataList = mainbranchs.ToList().ConvertAll<SelectListItem>(new Converter<Branch, SelectListItem>(ConvertFromSelect));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Value);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (!selected.IsNullOrEmpty())
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }

        public SelectListItem ConvertFromSelect(Branch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.BranchCode + " - " + item.Description;
            returnItem.Value = item.BranchCode;
            return returnItem;
        }

    }
}