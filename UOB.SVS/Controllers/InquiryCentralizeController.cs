﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using UOB.SVS.Resources;
using UOB.SVS.Model;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System.Linq;
using Treemas.Credential.Model;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;
using UOB.SVS.Model.Helper;

namespace UOB.SVS.Controllers
{
    public class InquiryCentralizeController : PageController
    {
        private ICentralizeUnitHReqRepository _signatureHReqRepository;
        private ICentralizeHReqService _signatureHReqService;
        private ICentralizeHRepository _hRepository;
        private IAuditTrailLog _auditRepository;
        private IBranchRepository _branchRepository;
        //private IDocumentTypeRepository _docTypeRepository;
        private ICentralUnitDocTypeRepository _docTypeRepository;

        private IRegionRepository _regionRepository;
        private IDivisionRepository _divisionRepository;
        private IDepartementRepository _departementRepository;

        public InquiryCentralizeController(ISessionAuthentication sessionAuthentication, ICentralizeHRepository hRepository,
            IBranchRepository branchRepository, ICentralUnitDocTypeRepository docTypeRepository,
            ICentralizeUnitHReqRepository signatureHReqRepository,
            ICentralizeHReqService signatureHReqService,
            IAuditTrailLog auditRepository,
            IRegionRepository regionRepository, IDivisionRepository divisionRepository,
            IDepartementRepository departementRepository) : base(sessionAuthentication)
        {
            this._signatureHReqRepository = signatureHReqRepository;
            this._signatureHReqService = signatureHReqService;
            this._hRepository = hRepository;
            this._auditRepository = auditRepository;
            this._branchRepository = branchRepository;
            this._docTypeRepository = docTypeRepository;
            this._regionRepository = regionRepository;
            this._divisionRepository = divisionRepository;
            this._departementRepository = departementRepository;
            Settings.ModuleName = "InquiryCentralize";
            Settings.Title = "Inquiry/Display";
        }

        protected override void Startup()
        {
            Create();
        }
        public ActionResult Create()
        {
            CentralizeUnitHReqView data = new CentralizeUnitHReqView();
            return CreateView(data);
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.
                User user = Lookup.Get<User>();

                CentralizeReqFilter filter = new CentralizeReqFilter();

                string NIK = Request.QueryString.GetValues("NIK")[0];
                string Name = Request.QueryString.GetValues("Name")[0];
                string Titles = Request.QueryString.GetValues("Titles")[0];
                string Region = Request.QueryString.GetValues("Region")[0];
                string Division = string.Empty;
                string Departement = string.Empty;
                if (!(Request.QueryString.GetValues("Division").IsNullOrEmpty()))
                {
                    Division = Request.QueryString.GetValues("Division")[0];
                }

                if (!Request.QueryString.GetValues("Departement").IsNullOrEmpty())
                {
                    Departement = Request.QueryString.GetValues("Departement")[0];
                }

                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();

                var sortColumn = "NIK";
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                filter.NIK = NIK;
                filter.Name = Name;
                filter.Titles = Titles;
                filter.Region = Region;
                filter.Division = Division;
                filter.Departement = Departement;
                // Loading.   
                PagedResult<CentralizeHeader> data;

                data = _hRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);
                var items = data.Items.ToList().ConvertAll<CentralizeUnitHReqView>(new Converter<CentralizeHeader, CentralizeUnitHReqView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }


        public CentralizeUnitHReqView ConvertFrom(CentralizeHeader item)
        {
            User user = Lookup.Get<User>();

            CentralizeUnitHReqView returnItem = new CentralizeUnitHReqView();
            returnItem.Note = HttpUtility.HtmlEncode(item.Note);
            returnItem.RequestType = "U";
            returnItem.RequestDate = DateTime.Now;
            returnItem.RequestUser = user.Username;
            returnItem.RequestReason = "";

            returnItem.NIK = item.NIK;
            returnItem.Name = item.Name;
            returnItem.DocType = "";
            returnItem.Titles = item.Titles;
            returnItem.Region = item.Region;
            returnItem.Division = item.Division;
            returnItem.Departement = item.Departement;

            int index = 1;
            if (item.Documents != null)
            {
                IList<CentralizeDocument> docs = new List<CentralizeDocument>();
                foreach (CentralizeDDoc row in item.Documents)
                {
                    CentralizeDocument doc = new CentralizeDocument();
                    doc.ID = row.ID;
                    doc.Index = index++;
                    doc.DocumentType = row.DocumentType;
                    doc.FileBlob = row.FileBlob;
                    doc.FileType = HttpUtility.HtmlEncode(row.FileType);
                    doc.NIK = row.NIK;
                    doc.IsNew = false;
                    doc.IsValidDoc = true;
                    docs.Add(doc);
                }
                returnItem.Documents = docs.ToList();
            }

            if (item.Signatures != null)
            {
                index = 1;
                IList<CentralizeSignature> signs = new List<CentralizeSignature>();
                foreach (CentralizeDSign row in item.Signatures)
                {
                    CentralizeSignature sign = new CentralizeSignature();
                    sign.ID = row.Id;
                    sign.Index = index++;
                    sign.CroppedBlob = HttpUtility.HtmlEncode(row.Signature);
                     sign.ImageType = HttpUtility.HtmlEncode(row.ImageType);
                    sign.NIK = row.NIK;
                    sign.IsNew = false;
                    sign.IsValidImage = row.Signature.Length % 4 == 0 ? true : false;
                    signs.Add(sign);
                }
                returnItem.Signatures = signs.ToList();
            }

            return returnItem;
        }

        public ActionResult Edit(string id)
        {
            User user = Lookup.Get<User>();
            Parameter param = Lookup.Get<Parameter>();
            ViewData["ActionName"] = "Update";
            ViewData["Parameter"] = param;

            CentralizeHeader reqH = _hRepository.getCentralizeHeader(id);
            if (reqH.IsNull())
            {
                ScreenMessages.Submit(ScreenMessage.Error("NIK number does not exist!"));
                return RedirectToAction("Index");
            }
         
            reqH.Documents = _hRepository.getDocDet(reqH.NIK);
            reqH.Signatures = _hRepository.getSignDet(reqH.NIK);

            CentralizeUnitHReqView data = ConvertFrom(reqH);

            return CreateView(data);
        }

        [HttpPost]
        public ActionResult Edit(CentralizeUnitHReqView m)
        {
            User user = Lookup.Get<User>();
            Parameter param = Lookup.Get<Parameter>();
            ViewData["ActionName"] = "Update";
            ViewData["Parameter"] = param;

            CentralizeHeader reqH = _hRepository.getCentralizeHeader(m.NIK);
            if (reqH.IsNull())
            {
                ScreenMessages.Submit(ScreenMessage.Error("Account number does not exist."));
                return RedirectToAction("Index");
            }
            //var branches = getBranches();
            //if (user.BranchCode != "000" && !branches.Exists(x => x.BranchCode == reqH.BranchCode))
            //{
            //    ScreenMessages.Submit(ScreenMessage.Error("Account number does not belong to your branch."));
            //    return RedirectToAction("Index");
            //}
            //reqH.IdentityCards = _hRepository.getKTPDet(reqH.NIK);
            reqH.Documents = _hRepository.getDocDet(reqH.NIK);
            reqH.Signatures = _hRepository.getSignDet(reqH.NIK);

            CentralizeUnitHReqView data = ConvertFrom(reqH);

            return CreateView(data);
        }

        private IList<SelectListItem> createBranchSelect(string selected)
        {
            IList<SelectListItem> dataList = _branchRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Branch, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                if (dataList.FindElement(item => item.Value == selected) != null) 
                    dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        private IList<SelectListItem> createAccountTypeSelect(string selected)
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = "CS", Text = "CA/SA" });
            dataList.Insert(1, new SelectListItem() { Value = "TD", Text = "TD" });
            if (!selected.IsNullOrEmpty())
            {
                if (dataList.FindElement(item => item.Value == selected) != null)
                    dataList.FindElement(item => item.Value == selected.ToString().Trim()).Selected = true;
            }
            return dataList;
        }
        

        public SelectListItem ConvertFrom(Branch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.BranchCode;
            returnItem.Value = item.BranchCode;
            return returnItem;
        }

        private IList<SelectListItem> createDocTypeSelect(string selected)
        {
            List<RoleDocType> authDocs = Lookup.Get<List<RoleDocType>>();
            List<CentralUnitDocType> docList = _docTypeRepository.FindAll().ToList();
            List<CentralUnitDocType> finalDocList = new List<CentralUnitDocType>();
            foreach (CentralUnitDocType item in docList)
            {
                var found = authDocs.FindElement(x => x.DocumentTypeId == item.Id);
                if (!found.IsNull())
                {
                    finalDocList.Add(item);
                }
            }

            IList<SelectListItem> dataList = finalDocList.ConvertAll<SelectListItem>(new Converter<CentralUnitDocType, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (!selected.IsNullOrEmpty())
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }

        private IList<SelectListItem> createAllDocTypeSelect()
        {
            List<CentralUnitDocType> docList = _docTypeRepository.FindAll().ToList();

            IList<SelectListItem> dataList = docList.ConvertAll<SelectListItem>(new Converter<CentralUnitDocType, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
            IList<SelectListItem> sortedList = sortedEnum.ToList();
            return sortedList;
        }

        public SelectListItem ConvertFrom(CentralUnitDocType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }

        private ViewResult CreateView(CentralizeUnitHReqView data)
        {
            List<RoleDocType> authDocs = Lookup.Get<List<RoleDocType>>();
            ViewData["DocCount"] = authDocs.Count;

            ViewData["DocTypeMaster"] = createDocTypeSelect("");
            ViewData["AllDocTypeMaster"] = createAllDocTypeSelect();
            ViewData["RegionMaster"] = createRegionSelect("");
            ViewData["DivisionMaster"] = createDivisionSelect(data.Region.IsNullOrEmpty() ? "-" : data.Region, data.Division.IsNullOrEmpty() ? "-" : data.Division);
            ViewData["DepartementMaster"] = createDepartementSelect(data.Region.IsNullOrEmpty() ? "-" : data.Region, data.Division.IsNullOrEmpty() ? "-" : data.Division, data.Departement.IsNullOrEmpty() ? "-" : data.Departement);

            //ViewData["DivisionMaster"] = _divisionRepository.getDivisionbyRegion("");
            ////item.Departements = _departementRepository.getDepartementbyRegion(Region);
            return View("Detail", data);
        }

        private IList<SelectListItem> createRegionSelect(string selected)
        {
            IList<SelectListItem> dataList = _regionRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Treemas.Credential.Model.Region, SelectListItem>(ConvertFrom));
            
            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Value);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (!selected.IsNullOrEmpty())
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }

        private IList<SelectListItem> createDivisionSelect(string Region, string selected)
        {
            IList<SelectListItem> dataList = _divisionRepository.getDivisionbyRegion(Region).ToList().ConvertAll<SelectListItem>(new Converter<Division, SelectListItem>(ConvertFrom));
            IEnumerable<SelectListItem> sortedEnum = null;
            IList<SelectListItem> sortedList = null;

            if ((selected != "-"))
            {
                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                if (dataList.Count > 0)
                {
                    sortedList.FindElement(item => item.Value == selected).Selected = true;
                }
            }
            else
            {
                SelectListItem item = new SelectListItem();
                item.Text = "Select";
                item.Value = "";
                dataList.Add(item);

                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                sortedList.FindElement(x => x.Value == "").Selected = true;
            }

            return sortedList;
        }
        private IList<SelectListItem> createDepartementSelect(string Region, string Division, string selected)
        {
            IList<SelectListItem> dataList = _departementRepository.getDepartementbyRegion(Region, Division).ToList().ConvertAll<SelectListItem>(new Converter<Departement, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = null;
            IList<SelectListItem> sortedList = null;

            if ((selected != "-"))
            {
                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                if (dataList.Count > 0)
                {
                    sortedList.FindElement(item => item.Value == selected).Selected = true;
                }
            }
            else
            {
                SelectListItem item = new SelectListItem();
                item.Text = "Select";
                item.Value = "";
                dataList.Add(item);

                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                sortedList.FindElement(x => x.Value == "").Selected = true;
            }

            return sortedList;
        }

        public SelectListItem ConvertFrom(Treemas.Credential.Model.Region item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult ChangeDropdownRegion(string Region)
        {
            CentralizeUnitHReqView item = new CentralizeUnitHReqView();
            item.Divisions = _divisionRepository.getDivisionbyRegion(Region);

            return Json(item, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult ChangeDropdownDepartements(string region, string division="")
        {
            CentralizeUnitHReqView item = new CentralizeUnitHReqView();
            item.Departements = _departementRepository.getDepartementbyRegion(region, division);

            return Json(item, JsonRequestBehavior.AllowGet);
        }

        public SelectListItem ConvertFrom(Division item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }

        public SelectListItem ConvertFrom(Departement item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }

        [HttpGet]
        public ActionResult NoAuthCheckViewDocument(int ID)
        {
            JsonResult result = new JsonResult();
            CentralizeDDoc doc = _hRepository.getDocDet(ID);
            if (doc.IsNull())
            {
                return Content("Not exist or invalid document");
            }
            byte[] blob = Convert.FromBase64String(doc.FileBlob);

            string filetype = doc.FileType.Split(';')[0].Split(':')[1];
            if (filetype.Trim() == "application/pdf")
            {
                return File(blob, filetype);
            }
            else
            {
                Stream strm = new MemoryStream(Convert.FromBase64String(doc.FileBlob));
                Bitmap bmp = (Bitmap)Bitmap.FromStream(strm);

                ImageFormatConverter imgConverter = new ImageFormatConverter();
                var png = imgConverter.ConvertFromString("PNG");

                MemoryStream ms = new MemoryStream();
                bmp.Save(ms, (ImageFormat)png);

                byte[] bytImg = ms.ToArray();
                string strImg = Convert.ToBase64String(bytImg);
                return Content("<img src='data:image/PNG;base64," + strImg + "' alt='" + doc.DocumentType + "'/>");
            }
        }
    }
}