﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using UOB.SVS.Resources;
using UOB.SVS.Model;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System.Linq;
using Treemas.Credential.Model;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;
using UOB.SVS.Model.Helper;

namespace UOB.SVS.Controllers
{
    public class DeleteCentralizeController : PageController
    {
        private IAccountNoViewRepository _accNoVwRepo;
        private IAuditTrailLog _auditRepository;
        private IBranchRepository _branchRepository;
        private IDocumentTypeRepository _docTypeRepository;

        private ICentralizeHRepository _hRepository;
        private ICentralizeUnitHReqRepository _signatureHReqRepository;
        private ICentralizeHReqService _signatureHReqService;

        private IRegionRepository _regionRepository;
        private IDivisionRepository _divisionRepository;
        private IDepartementRepository _departementRepository;

        public DeleteCentralizeController(ISessionAuthentication sessionAuthentication, ICentralizeHRepository hRepository,
            ICentralizeUnitHReqRepository signatureHReqRepository,
            ICentralizeHReqService signatureHReqService, IAccountNoViewRepository accNoVwRepo, IAuditTrailLog auditRepository,
            IBranchRepository branchRepository, IDocumentTypeRepository docTypeRepository,
            IRegionRepository regionRepository, IDivisionRepository divisionRepository,
            IDepartementRepository departementRepository) : base(sessionAuthentication)
        {
            this._hRepository = hRepository;
            this._signatureHReqRepository = signatureHReqRepository;
            this._signatureHReqService = signatureHReqService;
            this._accNoVwRepo = accNoVwRepo;
            this._auditRepository = auditRepository;
            this._branchRepository = branchRepository;
            this._docTypeRepository = docTypeRepository;
            this._regionRepository = regionRepository;
            this._divisionRepository = divisionRepository;
            this._departementRepository = departementRepository;
            Settings.ModuleName = "DeleteCentralize";
            Settings.Title = "Delete";
        }

        protected override void Startup()
        {
            Create();
        }

        public ActionResult Create()
        {
            CentralizeUnitHReqView data = new CentralizeUnitHReqView();
            return CreateView(data);
        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.
                User user = Lookup.Get<User>();

                CentralizeReqFilter filter = new CentralizeReqFilter();
                string NIK = Request.QueryString.GetValues("NIK")[0];
                string Name = Request.QueryString.GetValues("Name")[0];
                string Titles = Request.QueryString.GetValues("Titles")[0];
                string Region = Request.QueryString.GetValues("Region")[0];
                string Division = string.Empty;
                string Departement = string.Empty;
                if (!(Request.QueryString.GetValues("Division").IsNullOrEmpty()))
                {
                    Division = Request.QueryString.GetValues("Division")[0];
                }

                if (!Request.QueryString.GetValues("Departement").IsNullOrEmpty())
                {
                    Departement = Request.QueryString.GetValues("Departement")[0];
                }

                //stepppp
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();

                var sortColumn = "NIK";
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                filter.NIK = NIK;
                filter.Name = Name;
                filter.Titles = Titles;
                filter.Region = Region;
                filter.Division = Division;
                filter.Departement = Departement;
                // Loading.   
                PagedResult<CentralizeHeader> data;

                data = _hRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, filter);
                var items = data.Items.ToList().ConvertAll<CentralizeUnitHReqView>(new Converter<CentralizeHeader, CentralizeUnitHReqView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult ChangeDropdownRegion(string Region)
        {
            CentralizeUnitHReqView item = new CentralizeUnitHReqView();
            item.Divisions = _divisionRepository.getDivisionbyRegion(Region);

            return Json(item, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult ChangeDropdownDepartements(string region, string division = "")
        {
            CentralizeUnitHReqView item = new CentralizeUnitHReqView();
            item.Departements = _departementRepository.getDepartementbyRegion(region, division);

            return Json(item, JsonRequestBehavior.AllowGet);
        }
        public CentralizeUnitHReqView ConvertFrom(CentralizeHeader item)
        {
            User user = Lookup.Get<User>();

            CentralizeUnitHReqView returnItem = new CentralizeUnitHReqView();
            returnItem.Note = HttpUtility.HtmlEncode(item.Note);
            returnItem.RequestType = "U";
            returnItem.RequestDate = DateTime.Now;
            returnItem.RequestUser = user.Username;
            returnItem.RequestReason = "";

            returnItem.NIK = item.NIK;
            returnItem.Name = item.Name;
            returnItem.DocType = "";
            returnItem.Titles = item.Titles;
            returnItem.Region = item.Region;
            returnItem.Division = item.Division;
            returnItem.Departement = item.Departement;

            int index = 1;
            if (item.Documents != null)
            {
                IList<CentralizeDocument> docs = new List<CentralizeDocument>();
                foreach (CentralizeDDoc row in item.Documents)
                {
                    CentralizeDocument doc = new CentralizeDocument();
                    doc.ID = row.ID;
                    doc.Index = index++;
                    doc.DocumentType = row.DocumentType;
                    doc.FileBlob = HttpUtility.HtmlEncode(row.FileBlob).IsNull() ? "null" : HttpUtility.HtmlEncode(row.FileBlob);
                    doc.FileType = HttpUtility.HtmlEncode(row.FileType);
                    doc.NIK = row.NIK;
                    doc.IsNew = true;
                    doc.FileSeq = row.Seq;
                    doc.IsValidDoc = true;
                    docs.Add(doc);
                }
                returnItem.Documents = docs.ToList();
            }

            if (item.Signatures != null)
            {
                index = 1;
                IList<CentralizeSignature> signs = new List<CentralizeSignature>();
                foreach (CentralizeDSign row in item.Signatures)
                {
                    CentralizeSignature sign = new CentralizeSignature();
                    sign.ID = row.Id;
                    sign.Index = index++;
                    sign.CroppedBlob = HttpUtility.HtmlEncode(row.Signature);
                    sign.ImageType = HttpUtility.HtmlEncode(row.ImageType);
                    sign.NIK = row.NIK;
                    sign.IsNew = true;
                    sign.IsValidImage = row.Signature.Length % 4 == 0 ? true : false;
                    signs.Add(sign);
                }
                returnItem.Signatures = signs.ToList();
            }

            return returnItem;
        }

        public ActionResult Edit(string id)
        {
            User user = Lookup.Get<User>();
            Parameter param = Lookup.Get<Parameter>();
            ViewData["ActionName"] = "Delete";
            ViewData["Parameter"] = param;

            CentralizeHeader reqH = _hRepository.getCentralizeHeader(id);
            if (reqH.IsNull())
            {
                ScreenMessages.Submit(ScreenMessage.Error("Account number does not exist!"));
                return RedirectToAction("Index");
            }
            //var branches = getBranches();
            //if (user.BranchCode != "000" && !branches.Exists(x => x.BranchCode == reqH.BranchCode))
            //{
            //    ScreenMessages.Submit(ScreenMessage.Error("Account number does not belong to your branch!"));
            //    return RedirectToAction("Index");
            //}
            //reqH.IdentityCards = _hRepository.getKTPDet(reqH.AccountNo);
            reqH.Documents = _hRepository.getDocDet(reqH.NIK);
            reqH.Signatures = _hRepository.getSignDet(reqH.NIK);

            CentralizeUnitHReqView data = ConvertFrom(reqH);
            Session["SignHReqData"] = data;

            return CreateView(data);
        }

        [HttpPost]
        public ActionResult Edit(SignatureHReqView m)
        {
            User user = Lookup.Get<User>();
            Parameter param = Lookup.Get<Parameter>();
            ViewData["ActionName"] = "Delete";
            ViewData["Parameter"] = param;

            CentralizeHeader reqH = _hRepository.getCentralizeHeader(m.AccountNo);
            if (reqH.IsNull())
            {
                ScreenMessages.Submit(ScreenMessage.Error("Account number does not exist."));
                return RedirectToAction("Index");
            }

            //var branches = getBranches();
            //if (user.BranchCode != "000" && !branches.Exists(x => x.BranchCode == reqH.BranchCode))
            //{
            //    ScreenMessages.Submit(ScreenMessage.Error("Account number does not belong to your branch."));
            //    return RedirectToAction("Index");
            //}
            //reqH.IdentityCards = _hRepository.getKTPDet(reqH.AccountNo);
            reqH.Documents = _hRepository.getDocDet(reqH.NIK);
            reqH.Signatures = _hRepository.getSignDet(reqH.NIK);

            CentralizeUnitHReqView data = ConvertFrom(reqH);
            Session["SignHReqData"] = data;
            return CreateView(data);
        }

        private ViewResult CreateView(CentralizeUnitHReqView data)
        {
            List<RoleDocType> authDocs = Lookup.Get<List<RoleDocType>>();
            ViewData["DocCount"] = authDocs.Count;

            ViewData["AllDocTypeMaster"] = createAllDocTypeSelect();
            ViewData["DocTypeMaster"] = createDocTypeSelect("");
            ViewData["RegionMaster"] = createRegionSelect("");
            ViewData["DivisionMaster"] = createDivisionSelect(data.Region.IsNullOrEmpty() ? "-" : data.Region, data.Division.IsNullOrEmpty() ? "-" : data.Division);
            ViewData["DepartementMaster"] = createDepartementSelect(data.Region.IsNullOrEmpty() ? "-" : data.Region, data.Division.IsNullOrEmpty() ? "-" : data.Division, data.Departement.IsNullOrEmpty() ? "-" : data.Departement);
            ViewData["DocTypeMaster"] = createDocTypeSelect("");
            return View("Detail", data);
        }

        private IList<SelectListItem> createRegionSelect(string selected)
        {
            IList<SelectListItem> dataList = _regionRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Treemas.Credential.Model.Region, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Value);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (!selected.IsNullOrEmpty())
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }

        private IList<SelectListItem> createDivisionSelect(string Region, string selected)
        {
            IList<SelectListItem> dataList = _divisionRepository.getDivisionbyRegion(Region).ToList().ConvertAll<SelectListItem>(new Converter<Division, SelectListItem>(ConvertFrom));
            IEnumerable<SelectListItem> sortedEnum = null;
            IList<SelectListItem> sortedList = null;

            if ((selected != "-"))
            {
                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                if (dataList.Count > 0)
                {
                    sortedList.FindElement(item => item.Value == selected).Selected = true;
                }
            }
            else
            {
                SelectListItem item = new SelectListItem();
                item.Text = "Select";
                item.Value = "";
                dataList.Add(item);

                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                sortedList.FindElement(x => x.Value == "").Selected = true;
            }

            return sortedList;
        }
        private IList<SelectListItem> createDepartementSelect(string Region, string Division, string selected)
        {
            IList<SelectListItem> dataList = _departementRepository.getDepartementbyRegion(Region, Division).ToList().ConvertAll<SelectListItem>(new Converter<Departement, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = null;
            IList<SelectListItem> sortedList = null;

            if ((selected != "-"))
            {
                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                if (dataList.Count > 0)
                {
                    sortedList.FindElement(item => item.Value == selected).Selected = true;
                }
            }
            else
            {
                SelectListItem item = new SelectListItem();
                item.Text = "Select";
                item.Value = "";
                dataList.Add(item);

                sortedEnum = dataList.OrderBy(f => f.Value);
                sortedList = sortedEnum.ToList();

                sortedList.FindElement(x => x.Value == "").Selected = true;
            }

            return sortedList;
        }

        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(string Id)
        {
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                CentralizeUnitHRequest existingSHQ = _signatureHReqRepository.getCentralizeUnitHReq(Id);
                CentralizeUnitHReqView currData = (CentralizeUnitHReqView)Session["SignHReqData"];

                if (!existingSHQ.IsNull())
                {
                    if (existingSHQ.NIK.Trim() != currData.NIK.Trim())
                    {
                        ScreenMessages.Submit(ScreenMessage.Error("Delete failed, invalid or not exist account number!"));
                        return RedirectToAction("Index");
                    }

                    if (existingSHQ.RequestType == "D" && existingSHQ.IsRejected == false)
                    {
                        ScreenMessages.Submit(ScreenMessage.Error("Delete failed, account number " + Id.Trim() + " is awaiting for DELETE approval"));
                        return RedirectToAction("Index");
                    }
                    else if (existingSHQ.RequestType == "U" && existingSHQ.IsRejected == false)
                    {
                        ScreenMessages.Submit(ScreenMessage.Error("Delete failed, account number " + Id.Trim() + " is awaiting for UPDATE approval"));
                        return RedirectToAction("Index");
                    }
                }

                CentralizeHeader reqH = _hRepository.getCentralizeHeader(Id);
                //reqH.IdentityCards = _hRepository.getKTPDet(reqH.AccountNo);
                reqH.Documents = _hRepository.getDocDet(reqH.NIK);
                reqH.Signatures = _hRepository.getSignDet(reqH.NIK);

                CentralizeUnitHRequest newData = new CentralizeUnitHRequest("");
                newData.NIK = reqH.NIK;
                //newData.AccountName = reqH.AccountName;
                newData.Name = reqH.Name;
                newData.Titles = reqH.Titles;
                newData.Region = reqH.Region;
                newData.Division = reqH.Division;
                newData.Titles = reqH.Titles;
                //newData.BranchCode = reqH.BranchCode;
                newData.Note = HttpUtility.HtmlDecode(reqH.Note); ;
                newData.RequestType = "D";
                newData.RequestDate = DateTime.Now;
                newData.RequestUser = user.Username;
                newData.RequestReason = "Delete SVS";
                //newData.doc1 = byteDoc1;
                IList<CentralizeDDocReq> docs = new List<CentralizeDDocReq>();
                if (reqH.Documents != null)
                {
                    foreach (CentralizeDDoc item in reqH.Documents)
                    {
                        CentralizeDDoc rDoc = _hRepository.getDocDet(item.ID);

                        CentralizeDDocReq doc = new CentralizeDDocReq(0);
                        doc.NIK = reqH.NIK;
                        doc.DocumentType = item.DocumentType;
                        doc.FileBlob = rDoc.FileBlob;
                        doc.FileType = item.FileType;
                        doc.RequestType = "D";
                        doc.RequestDate = DateTime.Now;
                        doc.RequestUser = user.Username;
                        doc.RequestReason = "Delete SVS";
                        docs.Add(doc);
                    }
                }
                newData.Documents = docs;

                IList<CentralizeDSignReq> signs = new List<CentralizeDSignReq>();
                if (reqH.Signatures != null)
                {
                    foreach (CentralizeDSign item in reqH.Signatures)
                    {
                        CentralizeDSignReq sign = new CentralizeDSignReq(0);
                        sign.NIK = reqH.NIK;
                        sign.Signature = item.Signature;
                        sign.ImageType = item.ImageType;
                        sign.RequestType = "D";
                        sign.RequestDate = DateTime.Now;
                        sign.RequestUser = user.Username;
                        sign.RequestReason = "Delete SVS";
                        signs.Add(sign);
                    }
                }
                newData.Signatures = signs;

                message = _signatureHReqService.SaveCentralizeReq(newData);

                // Hapus detail data, biar audit trailnya ngak penuh
                newData.Documents = null;
                newData.Signatures = null;
                //newData.IdentityCards = null;
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                ScreenMessages.Submit(ScreenMessage.Success(SvsResources.Validation_Approval));
                return RedirectToAction("Index");
            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            //CollectScreenMessages(); ga perlu karena ke index otomatis collect, kalo ada ini malah ga keluar messagenya by Ivan
            return RedirectToAction("Index");
        }

        #region Dropdownlist

        private IList<SelectListItem> createBranchSelect(string selected)
        {
            IList<SelectListItem> dataList = getBranches().ConvertAll<SelectListItem>(new Converter<Branch, SelectListItem>(ConvertFrom));
            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected).Selected = true;
            }
            return dataList;
        }

        public List<Branch> getBranches()
        {
            User user = Lookup.Get<User>();
            IList<string> branchs = new List<string>();
            if (user.BranchCode == "000")
            {
                return _branchRepository.getBranchs().ToList();
            }
            else
            {
                branchs.Add(user.BranchCode);
                return _branchRepository.getBranchs(branchs).ToList();
            }
        }

        public SelectListItem ConvertFrom(Branch item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.BranchCode;
            returnItem.Value = item.BranchCode;
            return returnItem;
        }


        private IList<SelectListItem> createDocTypeSelect(string selected)
        {
            List<RoleDocType> authDocs = Lookup.Get<List<RoleDocType>>();
            List<DocumentType> docList = _docTypeRepository.FindAll().ToList();
            List<DocumentType> finalDocList = new List<DocumentType>();
            foreach (DocumentType item in docList)
            {
                var found = authDocs.FindElement(x => x.DocumentTypeId == item.Id);
                if (!found.IsNull())
                {
                    finalDocList.Add(item);
                }
            }

            IList<SelectListItem> dataList = finalDocList.ConvertAll<SelectListItem>(new Converter<DocumentType, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (!selected.IsNullOrEmpty())
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }

        private IList<SelectListItem> createAllDocTypeSelect()
        {
            List<DocumentType> docList = _docTypeRepository.FindAll().ToList();

            IList<SelectListItem> dataList = docList.ConvertAll<SelectListItem>(new Converter<DocumentType, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
            IList<SelectListItem> sortedList = sortedEnum.ToList();
            return sortedList;
        }
        public SelectListItem ConvertFrom(DocumentType item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Description;
            returnItem.Value = item.DocType;
            return returnItem;
        }

        private IList<SelectListItem> createAccountTypeSelect(string selected)
        {
            IList<SelectListItem> dataList = new List<SelectListItem>();
            dataList.Insert(0, new SelectListItem() { Value = "CS", Text = "CA/SA" });
            dataList.Insert(1, new SelectListItem() { Value = "TD", Text = "TD" });

            if (!selected.IsNullOrEmpty())
            {
                dataList.FindElement(item => item.Value == selected.ToString().Trim()).Selected = true;
            }
            return dataList;
        }

        #endregion

        public SelectListItem ConvertFrom(Treemas.Credential.Model.Region item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }

        public SelectListItem ConvertFrom(Division item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }

        public SelectListItem ConvertFrom(Departement item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.Alias;
            returnItem.Value = item.Alias;
            return returnItem;
        }

        [HttpGet]
        public ActionResult NoAuthCheckViewDocument(int ID)
        {
            JsonResult result = new JsonResult();
            CentralizeDDoc doc = _hRepository.getDocDet(ID);
            if (doc.IsNull())
            {
                return Content("Not exist or invalid document");
            }
            byte[] blob = Convert.FromBase64String(doc.FileBlob);

            string filetype = doc.FileType.Split(';')[0].Split(':')[1];
            if (filetype.Trim() == "application/pdf")
            {
                return File(blob, filetype);
            }
            else
            {
                Stream strm = new MemoryStream(Convert.FromBase64String(doc.FileBlob));
                Bitmap bmp = (Bitmap)Bitmap.FromStream(strm);

                ImageFormatConverter imgConverter = new ImageFormatConverter();
                var png = imgConverter.ConvertFromString("PNG");

                MemoryStream ms = new MemoryStream();
                bmp.Save(ms, (ImageFormat)png);

                byte[] bytImg = ms.ToArray();
                string strImg = Convert.ToBase64String(bytImg);
                return Content("<img src='data:image/PNG;base64," + strImg + "' alt='" + doc.DocumentType + "'/>");
            }
        }
    }
}