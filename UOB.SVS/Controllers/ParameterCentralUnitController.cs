﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;

namespace UOB.SVS.Controllers
{
    public class ParameterCentralUnitController : PageController
    {
        //private IParameterRepository _parameterRepository;
        private IParameterCentralUnitRepository _parameterCentralUnitRepository;
        //private IParameterAuditTrail _auditRepository;
        private IParameterAuditTrailCentralUnit _auditRepository;
        public ParameterCentralUnitController(ISessionAuthentication sessionAuthentication, IParameterCentralUnitRepository parameterCentralUnitRepository, IParameterAuditTrailCentralUnit auditRepository)
            : base(sessionAuthentication)
        {
            this._parameterCentralUnitRepository = parameterCentralUnitRepository;
            this._auditRepository = auditRepository;

            Settings.ModuleName = "ParameterCentralUnit";
            Settings.Title = "Parameter Centralize";
        }
        protected override void Startup()
        {
            ParameterCentralUnit param = _parameterCentralUnitRepository.getParameter();
            param.UserId = Encryption.Instance.DecryptText(param.UserId);
            param.Password = Encryption.Instance.DecryptText(param.Password);

            bool InChangeRequest = false;

            if (!param.IsNull()) {
                InChangeRequest  = _parameterCentralUnitRepository.InChangesRequest(param.Id);

                if (InChangeRequest)
                    ScreenMessages.Submit(ScreenMessage.Info(ParameterResources.ChangesRequestInProgress));

            }

            ViewData["InChangeRequest"] = InChangeRequest;
            ViewData["Parameter"] = param;
        }

        private string validateData(ParameterView data, string mode)
        {

            //if (data.UserId.IsNullOrEmpty())
            //    return ParameterResources.Validation_FillUserId;
            if (mode == "Create")
            {
                if (_parameterCentralUnitRepository.IsDuplicate(data.UserId))
                    return ParameterResources.Validation_DuplicateData;
            }
            return "";
        }

        // POST: Function/Edit/5
        [HttpPost]
        public ActionResult Edit(ParameterView data)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";
            try
            {
                message = validateData(data, "Edit");
                if (message.IsNullOrEmpty())
                {

                    ParameterCentralUnit paramData = _parameterCentralUnitRepository.getParameter();
                    //if (paramData.SessionTimeout == data.SessionTimeout && paramData.LoginAttempt == data.LoginAttempt &&
                    //paramData.SizeKTP == data.SizeKTP &&
                    //paramData.SizeSignature == data.SizeSignature && paramData.SizeDocument == data.SizeDocument &&
                    //paramData.KeepHistory == data.KeepHistory && paramData.KeepAuditTrail == data.KeepAuditTrail &&
                    //paramData.KeepPendingAppr == data.KeepPendingAppr && paramData.MaxDiffAccount == data.MaxDiffAccount &&
                    //paramData.KeepCloseAccount == data.KeepCloseAccount)
                    //{
                    //    ScreenMessages.Submit(ScreenMessage.Error("No changes to submitted!"));
                    //    CollectScreenMessages();
                    //    return CreateView(data);
                    //}

                    if (paramData.SizeSignature == data.SizeSignature && paramData.SizeDocument == data.SizeDocument &&
                    paramData.KeepHistory == data.KeepHistory && paramData.KeepAuditTrail == data.KeepAuditTrail &&
                    paramData.KeepPendingAppr == data.KeepPendingAppr)
                    {
                        ScreenMessages.Submit(ScreenMessage.Error("No changes to submitted!"));
                        CollectScreenMessages();
                        return CreateView(data);
                    }


                    ParameterHistoryCentralUnit HistData = new ParameterHistoryCentralUnit(0L);
                    HistData.ParamId = paramData.ParamId;
                    HistData.SizeSignature = paramData.SizeSignature;
                    HistData.SizeDocument = paramData.SizeDocument;
                    HistData.KeepAuditTrail = paramData.KeepAuditTrail;
                    HistData.KeepPendingAppr = paramData.KeepPendingAppr;
                    HistData.NewSizeSignature = data.SizeSignature;
                    HistData.NewSizeDocument = data.SizeDocument;
                    HistData.NewKeepAuditTrail = data.KeepAuditTrail;
                    HistData.NewKeepPendingAppr = data.KeepPendingAppr;
                    HistData.ApprovalStatus = ApprovalStatusState.R.ToString();
                    HistData.ChangedBy = user.Username;
                    HistData.ChangedDate = DateTime.Now;

                    _parameterCentralUnitRepository.AddHistory(HistData);

                    //paramData.SessionTimeout = (data.SessionTimeout <= 0) ? 30 : data.SessionTimeout;
                    //paramData.LoginAttempt = (data.LoginAttempt <= 0) ? 3 : data.LoginAttempt;
                    //paramData.PasswordExp = data.PasswordExp;

                    ViewData["Parameter"] = paramData;
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                //ScreenMessages.Submit(ScreenMessage.Success(ParameterResources.Validation_RequestSuccess));
                return RedirectToRoute(new { controller = "ParameterCentralUnit", action = "Index" });

            }
            ScreenMessages.Submit(ScreenMessage.Error(message));
            CollectScreenMessages();
            return CreateView(data);

        }

        private ViewResult CreateView(ParameterView data)
        {
            ParameterCentralUnit param = _parameterCentralUnitRepository.getParameter();
            bool InChangeRequest = _parameterCentralUnitRepository.InChangesRequest(param.Id);

            ViewData["InChangeRequest"] = InChangeRequest;
            ViewData["Parameter"] = param;
            return View("ParameterCentralUnit", data);
        }

    }
}