﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;

namespace UOB.SVS.Controllers
{
    public class RoleFunctionController : PageController
    {
        private IRoleRepository _roleRepository;
        private IFunctionRepository _funcRepository;
        private IRoleFunctionRepository _rolefuncRepository;
        private IFeatureRepository _featRepository;
        private IFunctionFeatureRepository _funcfeatRepository;
        private IRoleFunctionFeatureRepository _rolefuncfeatRepository;
        private IAppAuditTrailLog _auditRepository;
        private IRoleFunctionCrRepository _rolefuncCrRepository;
        private IRoleFunctionFeatureCrRepository _rolefuncfeatCrRepository;

        public IList<RoleFunction> roleFuncList;
        public IList<RoleFunctionCr> roleFuncCrList;
        public List<RoleFunctionSelectHelper> fflist;
        //int i = 0;

        public RoleFunctionController(ISessionAuthentication sessionAuthentication, IRoleRepository roleRepository, IRoleFunctionRepository rolefuncRepository,
            IFunctionRepository funcRepository, IFeatureRepository featRepository, IFunctionFeatureRepository funcfeatRepository, IRoleFunctionFeatureRepository rolefuncfeatRepository,
            IAppAuditTrailLog auditRepository, IRoleFunctionCrRepository rolefuncCrRepository, IRoleFunctionFeatureCrRepository rolefuncfeatCrRepository)
            : base(sessionAuthentication)
        {
            this._roleRepository = roleRepository;
            this._rolefuncRepository = rolefuncRepository;
            this._funcRepository = funcRepository;
            this._featRepository = featRepository;
            this._funcfeatRepository = funcfeatRepository;
            this._rolefuncfeatRepository = rolefuncfeatRepository;
            this._auditRepository = auditRepository;
            this._rolefuncCrRepository = rolefuncCrRepository;
            this._rolefuncfeatCrRepository = rolefuncfeatCrRepository;
            Settings.Title = RoleResources.PageTitleRoleFunction;
            Settings.ModuleName = "Role Function";
        }
        protected override void Startup()
        {
            //var returnpage = new { controller = "Role", action = "Index" };
            //try
            //{
            //    int Roleid = Convert.ToInt32(Request.RequestContext.RouteData.Values["Id"]);
            //    Role role = null;
            //    if (Roleid < 1)
            //        Response.RedirectToRoute(returnpage);
            //    role = _roleRepository.getRole(Roleid);
            //    if (role.IsNull())
            //        Response.RedirectToRoute(returnpage);
            //    Lookup.Remove<Role>();
            //    Lookup.Add(role);
            //    ViewData["Role"] = role;
            //    //Search();
            //}
            //catch
            //{
            //    Response.RedirectToRoute(returnpage);
            //}

            ViewData["RoleList"] = createRoleSelect("");
            fflist = new List<RoleFunctionSelectHelper>();
            //ViewData["RoleFunctionSelectHelperList"] = fflist;
            Session.Add("fflist", fflist);
            ViewData["loadState"] = (Request.QueryString.GetValues("loadState") == null) ? "0" : Request.QueryString.GetValues("loadState").FirstOrDefault();
        }
        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                //Role role = Lookup.Get<Role>();
                //if (role.IsNull())
                //{
                //    if (Request.QueryString.GetValues("Id").IsNullOrEmpty())
                //        return null;
                //    long Id = long.Parse(Request.QueryString.GetValues("Id")[0]);
                //    role = _roleRepository.getRole(Id);
                //    Lookup.Remove<Role>();
                //    Lookup.Add(role);
                //}

                // Initialization.   
                string RoleID = Request.QueryString.GetValues("RoleID")[0];
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                roleFuncList = _rolefuncRepository.getFunctions(Convert.ToInt32(RoleID));
                roleFuncCrList = _rolefuncCrRepository.getFunctions(Convert.ToInt32(RoleID));

                // Loading.   
                PagedResult<AuthorizationFunction> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _funcRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _funcRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }


                string[] functionList = new string[7];
                functionList[0] = "Function";
                functionList[1] = "Feature";
                functionList[2] = "Application";
                functionList[3] = "Menu";
                functionList[4] = "Svs";
                functionList[5] = "UserApplication";
                functionList[6] = "SignatureRequest";

                foreach (string functionid in functionList)
                {
                    AuthorizationFunction authFunction = _funcRepository.getFunction(functionid);

                    if (data.Items.Contains(authFunction))
                    {
                        data.Items = data.Items.Where(x => x.FunctionId != authFunction.FunctionId);
                        data.TotalItems -= 1;
                    }
                }

                var items = data.Items.ToList().ConvertAll<RoleFunctionView>(new Converter<AuthorizationFunction, RoleFunctionView>(ConvertFrom));

                //Kode di bawah untuk menghapus value yang di inginkan berdasarkan value value yang di inginkan di remove :)
                //items.Remove(items.SingleOrDefault(x => x.FunctionName == "Function"));                //1
                //items.Remove(items.SingleOrDefault(x => x.FunctionName == "Feature"));                 //2
                //items.Remove(items.SingleOrDefault(x => x.FunctionName == "Application"));             //3
                //items.Remove(items.SingleOrDefault(x => x.FunctionName == "Menu"));                    //4
                //items.Remove(items.SingleOrDefault(x => x.FunctionName == "Svs"));                     //5
                //items.Remove(items.SingleOrDefault(x => x.FunctionName == "UserApplication"));         //6
                //items.Remove(items.SingleOrDefault(x => x.FunctionName == "SignatureRequest"));        //7

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   

            fflist = new List<RoleFunctionSelectHelper>();
            Session.Add("fflist", fflist);

            return result;
        }
        public RoleFunctionView ConvertFrom(AuthorizationFunction item)
        {
            RoleFunctionView returnItem = new RoleFunctionView();
            var found = roleFuncList.FindElement(x => x.FunctionId == item.Id);
            var foundCr = roleFuncCrList.FindElement(x => x.FunctionId == item.Id);

            //if (found.IsNull() && foundCr.IsNull())
            if (found.IsNull())
            {
                returnItem.selected = false;
            }
            else
            {
                returnItem.selected = true;
                returnItem.Id = found.IsNull() ? foundCr.Id : found.Id;
                returnItem.RoleId = found.IsNull() ? foundCr.RoleId : found.RoleId;
            }
            returnItem.FunctionId = item.Id;
            returnItem.Name = item.Name;
            returnItem.FunctionName = item.FunctionId;
            return returnItem;
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTemp(RoleFunctionSelectHelper select)
        {
            JsonResult result = new JsonResult();
            List<RoleFunctionSelectHelper> cfflist = (List<RoleFunctionSelectHelper>)Session["fflist"];

            RoleFunctionSelectHelper ff = cfflist.Find(f => f.FunctionId == select.FunctionId);

            if (!ff.IsNull())
            {
                if (ff.selected != select.selected)
                {
                    cfflist.Remove(ff);
                }
            }
            else
            {
                cfflist.Add(select);
            }


            Session["fflist"] = cfflist;

            result = this.Json(new
            {
                message = "ok",
            }, JsonRequestBehavior.AllowGet);

            // Return info.   
            return result;
        }

        public ActionResult Create(int RoleID, RoleFunctionSelectHelper sel)
        {


            if (RoleID == 0)
            {
                return CreateTemp(sel);
            }

            else
            {
                List<RoleFunctionSelectHelper> cfflist = (List<RoleFunctionSelectHelper>)Session["fflist"];
                JsonResult result = new JsonResult();
                User user = Lookup.Get<User>();
                string message = "";
                string messageRFF = "";
                long Id = 0;
                //long IdRoleFuncFeat = 0;

                if (cfflist.Count == 0)
                {
                    result = this.Json(new
                    {
                        message = "No changes to submitted!",
                        Id = Id
                    }, JsonRequestBehavior.AllowGet);
                    return result;
                }

                if (cfflist.Count > 0)
                {
                    IList<RoleFunction> currRFs = _rolefuncRepository.getFunctions(RoleID).OrderBy(o => o.FunctionId).ToArray();
                    List<RoleFunctionSelectHelper> currRfsh = new List<RoleFunctionSelectHelper>();

                    foreach (RoleFunction currRF in currRFs)
                    {
                        RoleFunctionSelectHelper rfsh = new RoleFunctionSelectHelper();
                        rfsh.FunctionId = currRF.FunctionId;
                        rfsh.selected = true;
                        currRfsh.Add(rfsh);
                    }

                    if (cfflist.OrderBy(o => o.FunctionId).ToArray().SequenceEqual(currRfsh))
                    {
                        result = this.Json(new
                        {
                            message = "No changes to submitted!",
                            Id = Id
                        }, JsonRequestBehavior.AllowGet);
                        return result;
                    }
                }

                try
                {
                    //Role role = Lookup.Get<Role>();
                    foreach (RoleFunctionSelectHelper select in cfflist)
                    {
                        AuthorizationFunction func = null;
                        RoleFunction rolefunc = _rolefuncRepository.getRoleFunc(RoleID, select.FunctionId);
                        RoleFunctionCr rolefunccr = _rolefuncCrRepository.getRoleFunc(RoleID, select.FunctionId);

                        //IList<FunctionFeature> funcFeatlist = null;
                        if (select.selected == true)
                        {
                            if (rolefunccr.IsNull())
                            {
                                func = _funcRepository.getFunction(select.FunctionId);
                                Id = Insert(func, RoleID);
                            }
                            else
                            {
                                if (rolefunccr.ApprovalType == "D")
                                    message = DeleteCr(RoleID, select.FunctionId);
                            }
                        }
                        else
                        {
                            if (!rolefunccr.IsNull())
                            {
                                if (rolefunccr.ApprovalType == "C")
                                {
                                    message = DeleteCr(RoleID, select.FunctionId);
                                }
                            }
                            else
                            {
                                if (!rolefunc.IsNull())
                                {
                                    messageRFF = DeleteRoleFuncFeat(rolefunc.Id);
                                    message = Delete(RoleID, select.FunctionId);
                                }
                            }

                        }
                    }

                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }

                result = this.Json(new
                {
                    message = RoleResources.Validation_Success,
                    Id = Id
                }, JsonRequestBehavior.AllowGet);

                // Return info.   
                fflist = new List<RoleFunctionSelectHelper>();
                Session.Add("fflist", fflist);
                return result;
            }
        }


        private long Insert(AuthorizationFunction data, long roleId)
        {
            User user = Lookup.Get<User>();
            long returnId = -1;
            try
            {
                //RoleFunction newData = new RoleFunction(0L);
                RoleFunctionCr newData = new RoleFunctionCr(0L);
                newData.FunctionId = data.Id;
                newData.RoleId = roleId;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                newData.ApprovalType = "C";
                newData.ApprovalStatus = "N";
                newData.Approved = false;

                //_rolefuncRepository.Add(newData);
                _rolefuncCrRepository.Add(newData);
                returnId = newData.Id;
            }
            catch (Exception e)
            {
                returnId = -1;
            }
            return returnId;
        }

        private long InsertRoleFuncFeat(long rfId, long roleId, long funcId, long featId)
        {
            User user = Lookup.Get<User>();
            long returnId = -1;
            try
            {
                RoleFunctionFeature newData = new RoleFunctionFeature(0L);
                newData.FeatureId = featId;
                newData.RoleFunctionId = rfId;
                newData.FunctionId = funcId;
                newData.RoleId = roleId;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                _rolefuncfeatRepository.Add(newData);
                returnId = newData.Id;

            }
            catch (Exception e)
            {
                returnId = -1;
            }
            return returnId;
        }

        private long InsertRoleFuncFeatCr(long rfId, long roleId, long funcId, long featId)
        {
            User user = Lookup.Get<User>();
            long returnId = -1;
            try
            {
                RoleFunctionFeatureCr newData = new RoleFunctionFeatureCr(0L);
                newData.FeatureId = featId;
                newData.RoleFunctionId = rfId;
                newData.FunctionId = funcId;
                newData.RoleId = roleId;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                newData.ApprovalType = "C";
                newData.ApprovalStatus = "N";
                newData.Approved = false;

                _rolefuncfeatCrRepository.Add(newData);

                returnId = newData.Id;

            }
            catch (Exception e)
            {
                returnId = -1;
            }
            return returnId;
        }

        public string Delete(long roleId, long functionId)
        {
            string message = "";
            User user = Lookup.Get<User>();
            try
            {
                //RoleFunction newData = new RoleFunction(0L);
                RoleFunctionCr newData = new RoleFunctionCr(0L);
                newData.FunctionId = functionId;
                newData.RoleId = roleId;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                newData.ApprovalType = "D";
                newData.ApprovalStatus = "N";
                newData.Approved = false;

                _rolefuncCrRepository.Add(newData);
                //_rolefuncRepository.deleteSelected(roleId, functionId);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }

        public string DeleteCr(long roleId, long functionId)
        {
            string message = "";
            try
            {
                _rolefuncCrRepository.deleteSelected(roleId, functionId);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }

        public string DeleteRoleFuncFeat(long rolefunctionId)
        {
            string message = "";
            try
            {
                _rolefuncfeatRepository.deleteAllbyRoleFuncId(rolefunctionId);
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            return message;
        }

        private IList<SelectListItem> createRoleSelect(string selected)
        {
            IList<SelectListItem> dataList = _roleRepository.FindAll().ToList().ConvertAll<SelectListItem>(new Converter<Role, SelectListItem>(ConvertFrom));

            IEnumerable<SelectListItem> sortedEnum = dataList.OrderBy(f => f.Text);
            IList<SelectListItem> sortedList = sortedEnum.ToList();

            if (!selected.IsNullOrEmpty())
            {
                sortedList.FindElement(item => item.Value == selected).Selected = true;
            }
            return sortedList;
        }
        public SelectListItem ConvertFrom(Role item)
        {
            SelectListItem returnItem = new SelectListItem();
            returnItem.Text = item.RoleId;
            returnItem.Value = item.Id.ToString();
            return returnItem;
        }
    }
}