﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;
using LinqSpecs;

namespace UOB.SVS.Controllers
{
    public class UserApplicationController : PageController
    {
        private IUserRepository _userRepository;
        private IUserApplicationRepository _userAppRepository;
        private IApplicationRepository _appRepository;
        private IList<UserApplication> userAppList;
        private IAuditTrailLog _auditRepository;
        private IUserCrRepository _userCrRepository;
        private IAuthorizationRepository _authRepository;
        private IUserApplicationRepository _userApplicationRepository;
        private IRoleRepository _roleRepository;
        private IRoleFunctionFeatureRepository _roleFuncFeatRepository;
        private IFeatureRepository _featRepository;
        private IFunctionRepository _funcRepository;

        public UserApplicationController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository,
            IUserApplicationRepository userAppRepository, IApplicationRepository appRepository, IAuditTrailLog auditRepository,
            IUserCrRepository userCrRepository, IAuthorizationRepository authorizationRepository,
            IUserApplicationRepository userApplicationRepository, IRoleRepository roleRepository,
            IRoleFunctionFeatureRepository roleFuncFeatRepository, IFunctionRepository funcRepository,
            IFeatureRepository featRepository)
            : base(sessionAuthentication)
        {
            this._userRepository = userRepository;
            this._userAppRepository = userAppRepository;
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;
            this._userCrRepository = userCrRepository;
            this._authRepository = authorizationRepository;
            this._userApplicationRepository = userApplicationRepository;
            this._roleRepository = roleRepository;
            this._roleFuncFeatRepository = roleFuncFeatRepository;
            this._funcRepository = funcRepository;
            this._featRepository = featRepository;

            Settings.Title = "UserApplication";
            Settings.ModuleName = UserResources.PageTitle;
        }
        protected override void Startup()
        {
            var returnpage = new { controller = "User", action = "Index" };
            try
            {
                string usernameString = Convert.ToString(Request.RequestContext.RouteData.Values["Id"]);
                UserHelper userhelper = new UserHelper();
                if (usernameString.IsNullOrEmpty())
                    Response.RedirectToRoute(returnpage);

                User userData = _userRepository.GetUser(usernameString);
                if (userData.IsNull())
                    Response.RedirectToRoute(returnpage);

                userhelper.Username = userData.Username.ToString();
                Lookup.Remove<UserHelper>();
                Lookup.Add(userhelper);
                ViewData["Userhelper"] = userhelper;

            }
            catch
            {
                Response.RedirectToRoute(returnpage);
            }

        }
        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            try
            {
                // Initialization.   
                UserHelper userhelper = Lookup.Get<UserHelper>();
                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                userAppList = _userAppRepository.getUserApplication(userhelper.Username);

                // Loading.   
                PagedResult<Application> data;
                if (searchValue.IsNullOrEmpty())
                {
                    data = _appRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir);
                }
                else
                {
                    data = _appRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue);
                }

                var items = data.Items.ToList().ConvertAll<UserApplicationView>(new Converter<Application, UserApplicationView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }
            // Return info.   
            return result;
        }
        public UserApplicationView ConvertFrom(Application item)
        {
            UserApplicationView returnItem = new UserApplicationView();
            UserHelper userhelper = Lookup.Get<UserHelper>();
            int index = userAppList.FindIndex(x => x.Application == item.ApplicationId);
            if (index == -1)
            {
                returnItem.Selected = false;
                returnItem.Username = userhelper.Username;
            }
            else
            {
                returnItem.Id = userAppList[index-1].Id;
                returnItem.Selected = true;
                returnItem.Username = userhelper.Username;
            }
            returnItem.Application = item.ApplicationId;


            return returnItem;
        }

        [HttpPost]
        public ActionResult Create(UserHelper data)
        {
            JsonResult result = new JsonResult();
            string message = "";
            long Id = 0;
            try
            {

                if (data.Selected == true)
                {
                    Id=Insert(data);
                    
                }
                else
                {
                    Delete(data);
                }
            }
            catch (Exception ex)
            {
                // Info   
                Console.Write(ex);
            }

            result = this.Json(new
            {
                message = message,
                Id = Id
            }, JsonRequestBehavior.AllowGet);

            // Return info.   
            return result;
        }

        [HttpPost]
        public JsonResult Edit(string userName, string apps)
        {
            ViewData["ActionName"] = "Edit";
            User user = Lookup.Get<User>();
            string message = "";

            bool isAdmin = false;

            if (user.Username == SystemSettings.Instance.Runtime.SuperUser)
            {
                isAdmin = true;
            }
            else
            {
                isAdmin = false;
            }

            try
            {
                UserCr usrcr = _userCrRepository.GetUser(userName, ApprovalType.U);
                if (!usrcr.IsNull())
                {
                    throw new Exception(UserResources.Validation_AlreadyRequest);
                }

                User oldData = _userRepository.GetUser(userName);

                //get authorized applications
                IList<UserApplication> authorizedApplicationIdsList = _userAppRepository.getUserApplication(userName);
                string authorizedApplicationIdsString = string.Empty;
                foreach (UserApplication app in authorizedApplicationIdsList)
                {
                    authorizedApplicationIdsString += app.Application.ToString() + ",";
                }
                if (!authorizedApplicationIdsString.IsNullOrEmpty())
                {
                    authorizedApplicationIdsString = authorizedApplicationIdsString.Remove(authorizedApplicationIdsString.LastIndexOf(','), 1);
                }

                // check if there is any change on authorized apps
                //string[] appsOld = authorizedApplicationIdsString.Replace(" ", "").Split(',');
                //string[] appsNew = apps.Replace(" ", "").Split(',');

                string[] appsOld = authorizedApplicationIdsString.Split(',');
                string[] appsNew = apps.Split(',');

                var q = from a in appsOld
                        join b in appsNew on a equals b
                        select a;

                bool isAppsEqual = appsOld.Length == appsNew.Length && q.Count() == appsOld.Length;
                if (isAppsEqual)
                {
                    throw new Exception("No application changes being made!");
                }

                string authorizedRoles = _authRepository.getDistinctUserAuthorizationByUsername(userName);

                //_auditRepository.SaveAuditTrail(Settings.ModuleName, oldData, user.Username, "BeforeEdit");

                UserCr newData = new UserCr(0L);
                newData.Username = oldData.Username;
                newData.FullName = oldData.FullName;
                newData.FullNameNew = oldData.FullName;
                newData.AuthorizedApplicationIdsOld = authorizedApplicationIdsString;
                newData.AuthorizedApplicationIdsNew = apps;
                newData.AuthorizedRoleIdsOld = authorizedRoles;
                newData.AuthorizedRoleIdsNew = "";  // if any changes made, then automatically reset all roles being assigned.
                newData.RegNo = oldData.RegNo;
                newData.SessionTimeout = oldData.SessionTimeout;
                newData.LockTimeout = oldData.LockTimeout;
                newData.MaximumConcurrentLogin = oldData.MaximumConcurrentLogin;

                newData.LastLogin = oldData.LastLogin;
                newData.LoginAttempt = oldData.LoginAttempt;

                newData.InActiveDirectory = oldData.InActiveDirectory;
                newData.InActiveDirectoryNew = oldData.InActiveDirectory;
                newData.IsActive = oldData.IsActive;

                newData.IsPrinted = oldData.IsPrinted;
                newData.Approved = false;

                newData.ApprovalType = ApprovalType.U.ToString();

                newData.Password = oldData.Password;
                newData.PasswordExpirationDate = oldData.PasswordExpirationDate;
                newData.AccountValidityDate = oldData.AccountValidityDate;

                newData.CreatedDate = oldData.CreatedDate;
                newData.CreatedBy = oldData.CreatedBy;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;

                newData.BranchCode = oldData.BranchCode;
                newData.BranchCodeNew = oldData.BranchCode;

                _userCrRepository.Add(newData);

                if (isAdmin)
                {
                    User newData2 = _userRepository.GetUser(userName);
                    usrcr = _userCrRepository.GetUser(userName, ApprovalType.U);

                    newData2.FullName = usrcr.FullNameNew;
                    newData2.InActiveDirectory = usrcr.InActiveDirectoryNew;
                    newData2.Approved = usrcr.Approved;
                    newData2.ChangedBy = usrcr.ChangedBy;
                    newData2.ChangedDate = DateTime.Now; //apakah changed date = approval date

                    //Check for any application change
                    //string[] appsOld = usrcr.AuthorizedApplicationIdsOld.Replace(" ", "").Split(',');
                    //string[] appsNew = usrcr.AuthorizedApplicationIdsNew.Replace(" ", "").Split(',');

                    //string[] appsOld = usrcr.AuthorizedApplicationIdsOld.Split(',');
                    //string[] appsNew = usrcr.AuthorizedApplicationIdsNew.Split(',');

                    //var q = from a in appsOld
                    //        join b in appsNew on a equals b
                    //        select a;
                    //bool isAppsEqual = appsOld.Length == appsNew.Length && q.Count() == appsOld.Length;
                    if (!isAppsEqual)
                    {
                        newData2.UpdateMenu = true; //Will be saved later with conditional Roles changes.
                                                    // delete all authorization by username
                        foreach (string appId in appsOld)
                        {
                            UserApplication userApp = _userApplicationRepository.getUserApplication(usrcr.Username, appId);
                            if (!userApp.IsNull())
                            {
                                _userApplicationRepository.Remove(userApp);
                            }
                        }
                        // begin new insert
                        foreach (string appId in appsNew)
                        {
                            if (appId.IsNullOrEmpty())
                            {
                                continue;
                            }
                            UserApplication newUserAppData = new UserApplication(0L);
                            newUserAppData.Username = usrcr.Username;
                            newUserAppData.Application = appId;
                            newUserAppData.IsDefault = false;
                            newUserAppData.ChangedBy = null;
                            newUserAppData.ChangedDate = null;
                            newUserAppData.CreatedBy = usrcr.CreatedBy;
                            newUserAppData.CreatedDate = usrcr.CreatedDate;
                            _userApplicationRepository.Add(newUserAppData);
                        }
                    }

                    // Check for any Role change
                    //string[] rolesOld = userdata.AuthorizedRoleIdsOld.Replace(" ", "").Split(',');
                    //string[] rolesNew = userdata.AuthorizedRoleIdsNew.Replace(" ", "").Split(',');

                    string[] rolesOld = usrcr.AuthorizedRoleIdsOld.Split(',');
                    string[] rolesNew = usrcr.AuthorizedRoleIdsNew.Split(',');

                    var qRole = from a in rolesOld
                                join b in rolesNew on a equals b
                                select a;

                    bool isRolesEqual = rolesOld.Length == rolesNew.Length && qRole.Count() == rolesOld.Length;
                    if (!isRolesEqual)
                    {
                        newData2.UpdateMenu = true; //Will be saved later with conditional Roles changes.

                        // delete all authorization by username
                        foreach (string roleId in rolesOld)
                        {
                            Role role = _roleRepository.GetOne(new AdHocSpecification<Role>(s => s.RoleId == roleId));

                            if (!role.IsNull())
                            {
                                //_authRepository.delete(usrcr.Username, role._Application, roleId);
                                _authRepository.delete(usrcr.Username, role._Application);
                            }
                        }

                        // begin new insert
                        foreach (string roleId in rolesNew)
                        {
                            Role role = _roleRepository.GetOne(new AdHocSpecification<Role>(s => s.RoleId == roleId));
                            if (role.IsNull())
                            {
                                continue;
                            }

                            IList<RoleFunctionFeature> roleFuncFeat = _roleFuncFeatRepository.getRoleFuncFeaturebyRole(role.Id);
                            foreach (var item in roleFuncFeat)
                            {
                                AuthorizationFeature feat = _featRepository.getFeature(item.FeatureId);
                                AuthorizationFunction func = _funcRepository.getFunction(item.FunctionId);
                                try
                                {
                                    Authorization newAuthorizationData = new Authorization(0L);
                                    newAuthorizationData.Username = usrcr.Username;
                                    newAuthorizationData.Application = role._Application;
                                    newAuthorizationData.Role = role.RoleId;
                                    newAuthorizationData.Function = func.FunctionId;
                                    newAuthorizationData.Feature = feat.Name;
                                    newAuthorizationData.QualifierKey = "";
                                    newAuthorizationData.QualifierValue = "";
                                    newAuthorizationData.ChangedBy = user.Username;
                                    newAuthorizationData.ChangedDate = DateTime.Now;
                                    newAuthorizationData.CreatedBy = user.Username;
                                    newAuthorizationData.CreatedDate = DateTime.Now;
                                    _authRepository.Add(newAuthorizationData);
                                }
                                catch (Exception e)
                                {

                                }
                            }
                        }
                    }

                    _userCrRepository.setApproved(usrcr.Id, user.Username, DateTime.Now, ApprovalStatusState.A.ToString());
                    _userRepository.Save(newData2);
                    //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData2, user.Username, "AfterEdit");
                }
            }
            catch (Exception e)
            {
                message = e.Message;
            }

            if (message.IsNullOrEmpty())
            {
                return this.Json(ApplicationResources.Validation_Success, JsonRequestBehavior.AllowGet);
            }
            return this.Json(message, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public long Insert(UserHelper data)
        {
            User user = Lookup.Get<User>();
            long returnId = -1;
            try
            {
                UserApplication newData = new UserApplication(0L);
                newData.Username = data.Username;
                newData.Application = data.Application;
                newData.IsDefault = false;
                newData.ChangedBy = user.Username;
                newData.ChangedDate = DateTime.Now;
                newData.CreatedBy = user.Username;
                newData.CreatedDate = DateTime.Now;
                _userAppRepository.Add(newData);
                returnId = newData.Id;
                //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Create");
            }
            catch (Exception e)
            {
                returnId = -1;
            }

            return returnId;
        }

        [HttpPost]
        public ActionResult Delete(UserHelper data)
        {
            User user = Lookup.Get<User>();
            UserApplication userApplication = _userAppRepository.getUserApplication(data.Username, data.Application);
            string message = "";
            try
            {
                UserApplication newData = new UserApplication(userApplication.Id);
                _userAppRepository.deleteSelected(data.Username, data.Application);
                _userAppRepository.Remove(newData);
                //_auditRepository.SaveAuditTrail(Settings.ModuleName, newData, user.Username, "Delete");
            }
            catch (Exception e)
            {
                message = e.Message;
            }
            
            return RedirectToAction("Search");
        }

    }
}