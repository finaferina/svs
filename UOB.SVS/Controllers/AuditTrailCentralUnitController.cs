﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Web.Platform;
using Treemas.Credential.Interface;
using Treemas.Credential.Model;
using UOB.SVS.Models;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Resources;
using System.Globalization;
using System.Text.RegularExpressions;
using Microsoft.Reporting.WebForms;
using System.IO;
using SpreadsheetLight;
using System.Data;
using UOB.SVS.Model;
using UOB.SVS.Interface;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace UOB.SVS.Controllers
{
    public class AuditTrailCentralUnitController : PageController
    {
        //private IList<UserRole> userRoles;
        private IUserRepository _userRepository;
        private IApplicationRepository _appRepository;
        private IAuditTrailCentralUnitRepository _auditRepository;
        private IBranchRepository _branchRepository;
        public AuditTrailCentralUnitController(ISessionAuthentication sessionAuthentication, IUserRepository userRepository,
            IApplicationRepository appRepository, IAuditTrailCentralUnitRepository auditRepository, IBranchRepository branchRepository) : base(sessionAuthentication)
        {
            this._userRepository = userRepository;
            this._appRepository = appRepository;
            this._auditRepository = auditRepository;
            this._branchRepository = branchRepository;

            Settings.ModuleName = "AuditTrailCentralUnit";
            Settings.Title = "Audit Trail Officer";
        }
        protected override void Startup()
        {

        }

        public ActionResult Search()
        {
            JsonResult result = new JsonResult();
            //CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
            try
            {
                //AuditTrailFilter auditFilter = new AuditTrailFilter();
                ////auditFilter.FunctionName = Request.QueryString.GetValues("FunctionName")[0].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("FunctionName")[0];
                ////auditFilter.ObjectName = Request.QueryString.GetValues("ObjectName")[0].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("ObjectName")[0];
                //auditFilter.ActionName = Request.QueryString.GetValues("Action")[0].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("ActionName")[0];
                //auditFilter.ActionDateEnd = !Request.QueryString["ActionDateEnd"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("ActionDateEnd")[0], "d/M/yyyy", culture) : (DateTime?)null;
                //auditFilter.ActionDateStart = !Request.QueryString["ActionDateStart"].IsNullOrEmpty() ? DateTime.ParseExact(Request.QueryString.GetValues("ActionDateStart")[0], "d/M/yyyy", culture) : (DateTime?)null;

                string searchColumn = Request.QueryString.GetValues("searchColumn")[0];
                string searchValue = Request.QueryString.GetValues("searchValue")[0];
                string ActionDateStart = Request.QueryString.GetValues("ActionDateStart")[0];
                string ActionDateEnd = Request.QueryString.GetValues("ActionDateEnd")[0];
                string draw = Request.QueryString.GetValues("draw").FirstOrDefault();
                var sortColumn = Request.QueryString.GetValues("columns[" + Request.QueryString.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.QueryString.GetValues("order[0][dir]").FirstOrDefault();
                int startRec = Convert.ToInt32(Request.QueryString.GetValues("start").FirstOrDefault());
                int pageSize = Convert.ToInt32(Request.QueryString.GetValues("length").FirstOrDefault());
                int pageNumber = (startRec + pageSize - 1) / pageSize;

                // Loading.   
                PagedResult<AuditTrailCentralUnit> data;

                DateTime _RequestTimeStart = DateTime.MinValue;
                DateTime _RequestTimeEnd = DateTime.MinValue;

                if (!ActionDateStart.IsNullOrEmpty())
                {
                    if (!DateTime.TryParseExact(ActionDateStart, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out _RequestTimeStart))
                        throw new Exception("Input date is not valid");
                }
                if (!ActionDateEnd.IsNullOrEmpty())
                {
                    if (!DateTime.TryParseExact(ActionDateEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out _RequestTimeEnd))
                        throw new Exception("Input date is not valid");
                }

                if (_RequestTimeEnd < _RequestTimeStart)
                {
                    throw new Exception("Input date range is not valid");
                }

                if (_RequestTimeEnd == DateTime.MinValue)
                    _RequestTimeEnd = _RequestTimeStart;

                User user = Lookup.Get<User>();
                if (user.BranchCode != "000")
                {
                    //string[] UserList = getATUser(user.BranchCode);
                    data = _auditRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue, _RequestTimeStart, _RequestTimeEnd, user);
                }
                else
                {
                    data = _auditRepository.FindAllPaged(pageNumber, pageSize, sortColumn, sortColumnDir, searchColumn, searchValue, _RequestTimeStart, _RequestTimeEnd);
                }

                var items = data.Items.ToList().ConvertAll<AuditTrailView>(new Converter<AuditTrailCentralUnit, AuditTrailView>(ConvertFrom));

                result = this.Json(new
                {
                    draw = Convert.ToInt32(draw),
                    recordsTotal = data.TotalItems,
                    recordsFiltered = data.TotalItems,
                    data = items
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = this.Json(new
                {
                    draw = Convert.ToInt32(0),
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    data = new List<AppAuditTrail>(),
                    msg = ex.Message.ToString()
                }, JsonRequestBehavior.AllowGet);
                //// Info   
                //Console.Write(ex);
            }
            // Return info.   
            return result;
        }

        public string[] getATUser(string branchCode)
        {
            Branch brc = _branchRepository.getBranch(branchCode);

            string[] userList;
            List<string> lstUser = new List<string>();

            if (brc.BranchCode == brc.MainBranch)
            {
                IList<Branch> subBranch = _branchRepository.getBranchs(branchCode);
                foreach (Branch sb in subBranch)
                {
                    User usr = _userRepository.GetUserByBranch(branchCode);
                    IList<User> users = _userRepository.GetuserByBranch(branchCode);

                    for (int i = 0; i <= users.Count - 1; i++)
                    {
                        lstUser.Add(users[i].Username);
                        //userList[i] = users[i].Username.Trim();
                    }
                }

                userList = new string[lstUser.Count];
                for (int i = 0; i <= lstUser.Count - 1; i++)
                {
                    userList[i] = lstUser[i].Trim();
                }
            }
            else
            {
                User usr = _userRepository.GetUserByBranch(branchCode);
                userList = new string[1];
                userList[0] = usr.Username.Trim();
            }
            return userList;
        }

        public AuditTrailView ConvertFrom(AuditTrailCentralUnit item)
        {
            //var ar = _auditRepository
            AuditTrailView returnItem = new AuditTrailView();
            JObject objBefore = JObject.Parse(item.ObjectValueBefore);
            JObject objAfter = JObject.Parse(item.ObjectValueAfter);


            objBefore["Name"] = objBefore["Name"].ToString().IsNullOrEmpty() ? null : HttpUtility.HtmlEncode(objBefore["Name"]);
            objBefore["Note"] = objBefore["Note"].ToString().IsNullOrEmpty() ? null : HttpUtility.HtmlEncode(objBefore["Note"]);
            objAfter["Name"] = objAfter["Name"].ToString().IsNullOrEmpty() ? null : HttpUtility.HtmlEncode(objAfter["Name"]);
            objAfter["Note"] = objAfter["Note"].ToString().IsNullOrEmpty() ? null : HttpUtility.HtmlEncode(objAfter["Note"]);

            returnItem.Id = item.Id;
            returnItem.FunctionName = item.FunctionName;
            returnItem.ObjectName = item.ObjectName;
            returnItem.ObjectValueBefore = JsonConvert.SerializeObject(objBefore);
            returnItem.ObjectValueAfter = JsonConvert.SerializeObject(objAfter);
            returnItem.Action = item.Action;
            returnItem.TimeStamp = Convert.ToDateTime(item.TimeStamp).ToString("dd/MM/yyyy HH:mm:ss");
            returnItem.UserMaker = item.UserMaker;
            returnItem.UserChecker = item.UserChecker;
            returnItem.RequestDate = item.RequestDate.IsNull() ? null : Convert.ToDateTime(item.RequestDate).ToString("dd/MM/yyyy");
            returnItem.ApproveDate = item.ApproveDate.IsNull() ? null : Convert.ToDateTime(item.ApproveDate).ToString("dd/MM/yyyy");

            return returnItem;
        }

        [HttpPost]
        //public ActionResult Export(AuditTrailView data)
        //{
        //    CultureInfo culture = CultureInfo.CreateSpecificCulture("id-ID");
        //    AuditTrailFilter auditFilter = new AuditTrailFilter();
        //    auditFilter.FunctionName = data.FunctionName.IsNullOrEmpty() ? "" : data.FunctionName.Trim();// Request.QueryString.GetValues("FunctionName")[0].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("FunctionName")[0];
        //    auditFilter.ObjectName = data.ObjectName.IsNullOrEmpty() ? "" : data.ObjectName.Trim(); //Request.QueryString.GetValues("ObjectName")[0].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("ObjectName")[0];
        //    auditFilter.ActionName = data.Action.IsNullOrEmpty() ? "" : data.Action.Trim(); //Request.QueryString.GetValues("ActionName")[0].IsNullOrEmpty() ? "" : Request.QueryString.GetValues("ActionName")[0];
        //    auditFilter.ActionDateEnd = !data.TimeStampEnd.IsNullOrEmpty() ? DateTime.ParseExact(data.TimeStampEnd, "d/M/yyyy", culture) : (DateTime?)null;
        //    auditFilter.ActionDateStart = !data.TimeStampStart.IsNullOrEmpty() ? DateTime.ParseExact(data.TimeStampStart, "d/M/yyyy", culture) : (DateTime?)null;

        //    var auditList = _auditRepository.FindForExport(auditFilter);

        //    IList<AuditTrailView> items = auditList.ToList().ConvertAll<AuditTrailView>(new Converter<AuditTrail, AuditTrailView>(ConvertFrom));

        //    DataTable dt = items.ListToDataTable();
        //    List<AuditTrailView> list = new List<AuditTrailView>();

        //    dt.Columns.Remove("Id");
        //    dt.Columns.Remove("TimeStampString");
        //    dt.Columns.Remove("RequestDateString");
        //    dt.Columns.Remove("ApproveDateString");
        //    dt.Columns.Remove("TimeStampStart");
        //    dt.Columns.Remove("TimeStampEnd");

        //    MemoryStream ms = new MemoryStream();
        //    using (SLDocument sl = new SLDocument())
        //    {
        //        sl.ImportDataTable(1, 1, dt, true);
        //        sl.SelectWorksheet("Worksheet");
        //        sl.SaveAs(ms);
        //    }

        //    ms.Position = 0;
        //    string fName = "SVS_Audit_Trail.xls";
        //    return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fName);
        //}
        public ActionResult Export(string searchColumn, string searchValue, string ActionDateTimeStart, string ActionDateTimeEnd)
        {

            DateTime _RequestTimeStart = DateTime.MinValue;
            DateTime _RequestTimeEnd = DateTime.MinValue;

            if (!ActionDateTimeStart.IsNullOrEmpty())
            {
                if (!DateTime.TryParseExact(ActionDateTimeStart, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out _RequestTimeStart))
                {
                    ScreenMessages.Submit(ScreenMessage.Error("Input date is not valid!"));
                    return RedirectToAction("", "AuditTrailCentralUnit");
                }
            }
            if (!ActionDateTimeEnd.IsNullOrEmpty())
            {
                if (!DateTime.TryParseExact(ActionDateTimeEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out _RequestTimeEnd))
                {
                    ScreenMessages.Submit(ScreenMessage.Error("Input date is not valid!"));
                    return RedirectToAction("", "AuditTrailCentralUnit");
                }
            }

            if (_RequestTimeStart > _RequestTimeEnd)
            {
                ScreenMessages.Submit(ScreenMessage.Error("Input date range is not valid!"));
                return RedirectToAction("", "AuditTrailCentralUnit");
            }

            if (_RequestTimeEnd == DateTime.MinValue)
                _RequestTimeEnd = _RequestTimeStart;

            User user = Lookup.Get<User>();
            IList<AuditTrailCentralUnit> data;
            if (user.BranchCode != "000")
            {
                //string[] UserList = getATUser(user.BranchCode);
                data = _auditRepository.FindAllPagedForExport(searchColumn, searchValue, _RequestTimeStart, _RequestTimeEnd, user);
            }
            else
            {
                data = _auditRepository.FindAllPagedForExport(searchColumn, searchValue, _RequestTimeStart, _RequestTimeEnd);
            }

            //var auditList = _auditRepository.FindAllPagedForExport(searchColumn, searchValue, _RequestTimeStart, _RequestTimeEnd);
            IList<AuditTrailView> items = data.ToList().ConvertAll<AuditTrailView>(new Converter<AuditTrailCentralUnit, AuditTrailView>(ConvertFrom));

            DataTable dt = items.ListToDataTable();
            List<AuditTrailView> list = new List<AuditTrailView>();

            dt.Columns.Remove("Id");
            dt.Columns.Remove("TimeStampString");
            dt.Columns.Remove("RequestDateString");
            dt.Columns.Remove("ApproveDateString");
            dt.Columns.Remove("TimeStampStart");
            dt.Columns.Remove("TimeStampEnd");

            MemoryStream ms = new MemoryStream();
            using (SLDocument sl = new SLDocument())
            {
                sl.ImportDataTable(1, 1, dt, true);
                sl.SelectWorksheet("Worksheet");
                sl.AutoFitColumn(1, 6);
                sl.SaveAs(ms);
            }

            ms.Position = 0;
            string fName = "SVS_Audit_Trail.xls";
            return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fName);
        }

    }
}