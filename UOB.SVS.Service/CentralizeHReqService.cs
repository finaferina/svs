﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Treemas.Credential.Model;
using UOB.SVS.Interface;
using UOB.SVS.Model;

namespace UOB.SVS.Service
{
    public class CentralizeHReqService : ICentralizeHReqService
    {
        private ICentralizeUnitHReqRepository _centHReqRepo;
        private ICentralizeHRepository _centHRepo;
        private ICentralizeHHistoryRepository _centHHistoryRepo;

        public CentralizeHReqService(ICentralizeUnitHReqRepository centHReqRepo, ICentralizeHRepository centHRepo, ICentralizeHHistoryRepository centHHistoryRepo)
        {
            this._centHReqRepo = centHReqRepo;
            this._centHRepo = centHRepo;
            this._centHHistoryRepo = centHHistoryRepo;
        }

        public string SaveCentralizeReq(CentralizeUnitHRequest centHReq)
        {
            string returnMessage = "";

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {

                    _centHReqRepo.Add(centHReq);
                    if (centHReq.Documents != null)
                    {
                        foreach (CentralizeDDocReq item in centHReq.Documents)
                        {
                            _centHReqRepo.AddDocReqD(item);
                        }
                    }

                    if (centHReq.Signatures != null)
                    {
                        foreach (CentralizeDSignReq item in centHReq.Signatures)
                        {
                            _centHReqRepo.AddSignReqD(item);
                        }
                    }

                    scope.Complete();
                }
            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }
        public string SaveDocument(CentralizeDDocReq document)
        {
            string returnMessage = "";

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    _centHReqRepo.AddDocReqD(document);
                    scope.Complete();
                }
            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }
        public string SaveSign(CentralizeDSignReq sign)
        {
            string returnMessage = "";

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    _centHReqRepo.AddSignReqD(sign);
                    scope.Complete();
                }
            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }
        public string ApproveReq(CentralizeUnitHRequest centHReq, CentralizeHeader centH, CentralizeHHistory centHistory, User user)
        {
            string returnMessage = "";

            try
            {
                CentralizeHeader oldcentH = new CentralizeHeader(centH.NIK);

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Suppress))
                {
                    //if (centHReq.RequestType == "U" || centHReq.RequestType == "D")
                    //{
                    //    _centHRepo.DeleteCentralizeH(centH.NIK);
                    //    _centHRepo.DeleteSignDet(centH.NIK);
                    //    _centHRepo.DeleteDocDet(centH.NIK);

                    //}

                    if (centHReq.RequestType == "D")
                    {
                        _centHRepo.DeleteCentralizeH(centH.NIK);
                        _centHRepo.DeleteSignDet(centH.NIK);
                        _centHRepo.DeleteDocDet(centH.NIK);
                    }

                    if (centHReq.RequestType == "C")
                    {
                        _centHRepo.Save(centH);

                        foreach (CentralizeDDoc item in centH.Documents)
                        {
                            _centHRepo.AddDocDet(item);
                        }
                        foreach (CentralizeDSign item in centH.Signatures)
                        {
                            _centHRepo.AddSignDet(item);
                        }
                    }

                    if (centHReq.RequestType == "U")
                    {
                        //--------------------Sekarang Konsepnya Header Update yg detail delete dulu
                        _centHRepo.DeleteSignDet(centH.NIK);
                        _centHRepo.DeleteDocDet(centH.NIK);
                        //---------------------------------------------

                        _centHRepo.UpdateCHeader(centH);
                        ////===================Final Check ini Func untuk Bulk SP========//
                        //_signHRepo.UpdateSignatureDetail(svsH.AccountNo, svsH.Documents,svsH.IdentityCards,svsH.Signatures);
                        //---------------------------------------------

                        //==============Document_Detail=======
                        //_signHRepo.UpdateDocDet(svsH.Documents);
                        ////==============KTP_Detail=======
                        //_signHRepo.UpdateKTPDet(svsH.IdentityCards);
                        ////==============Sign_Detail=======
                        //_signHRepo.UpdateSignDet(svsH.Signatures);
                        //---------------------------------------------

                        foreach (CentralizeDDoc item in centH.Documents)
                        {
                            _centHRepo.AddDocDet(item);
                        }
                        foreach (CentralizeDSign item in centH.Signatures)
                        {
                            _centHRepo.AddSignDet(item);
                        }
                    }

                    if (centHReq.RequestType != "D")
                    {
                        _centHRepo.Save(centH);
                        foreach (CentralizeDDoc item in centH.Documents)
                        {
                            _centHRepo.AddDocDet(item);
                        }
                        foreach (CentralizeDSign item in centH.Signatures)
                        {
                            _centHRepo.AddSignDet(item);
                        }
                    }

                    _centHHistoryRepo.Save(centHistory);

                    _centHReqRepo.DeleteCentralizeUnitHReq(centHReq.NIK);
                    _centHReqRepo.DeleteSignReqD(centHReq.NIK);
                    _centHReqRepo.DeleteDocReqD(centHReq.NIK);

                    scope.Complete();
                }
            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }
        public string RejectReq(CentralizeUnitHRequest centHReq, User user)
        {
            string returnMessage = "";

            try
            {

                using (TransactionScope scope = new TransactionScope())
                {
                    CentralizeHHistory centHistory = new CentralizeHHistory(0L);
                    centHistory.NIK = centHReq.NIK;
                    centHistory.Name = centHReq.Name;
                    centHistory.Region = centHReq.Region;
                    centHistory.Division = centHReq.Division;
                    centHistory.Departement = centHReq.Departement;
                    centHistory.Titles = centHReq.Titles;
                    //centHistory.BranchCode = centHReq.BranchCode;
                    centHistory.Note = centHReq.Note;
                    centHistory.RequestType = centHReq.RequestType;
                    centHistory.RequestDate = centHReq.RequestDate;
                    centHistory.RequestUser = centHReq.RequestUser;
                    centHistory.RequestReason = centHReq.RequestReason;
                    centHistory.RejectBy = user.Username;
                    centHistory.RejectDate = DateTime.Now;

                    _centHHistoryRepo.Save(centHistory);


                    _centHReqRepo.Remove(centHReq);
                    _centHReqRepo.DeleteSignReqD(centHReq.NIK);
                    _centHReqRepo.DeleteDocReqD(centHReq.NIK);

                    scope.Complete();
                }
            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return "";
        }
        public string CancelCentralizeReq(CentralizeUnitHRequest centHReq)
        {
            string returnMessage = "";

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {

                    _centHReqRepo.Remove(centHReq);
                    _centHReqRepo.DeleteSignReqD(centHReq.NIK);
                    _centHReqRepo.DeleteDocReqD(centHReq.NIK);

                    scope.Complete();
                }
            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }
        public string UpdateCentralizeReq(CentralizeUnitHRequest centHReq)
        {
            string returnMessage = "";

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    _centHReqRepo.Save(centHReq);
                    //_signHReqRepo.DeleteKTPReqD(signHReq.AccountNo);
                    //_signHReqRepo.DeleteSignReqD(signHReq.AccountNo);
                    //_signHReqRepo.DeleteDocReqD(signHReq.AccountNo);

                    if (centHReq.Documents != null) {
                        //foreach (CentralizeDDocReq item in centHReq.Documents)
                        //{
                        //    //_centHReqRepo.DeleteDocReqD(item.ID);
                        //}
                        _centHReqRepo.DeleteDocReqD(centHReq.NIK);
                    }

                    if (centHReq.Signatures != null)
                    {
                        //foreach (CentralizeDSignReq item in centHReq.Signatures)
                        //{
                        //    _centHReqRepo.DeleteSignReqD(item.ID);
                        //}
                        _centHReqRepo.DeleteSignReqD(centHReq.NIK);
                    }

                    scope.Complete();
                }
            }
            catch (TransactionAbortedException ex)
            {
                returnMessage = ex.Message;
            }
            catch (ApplicationException ex)
            {
                returnMessage = ex.Message;
            }
            return returnMessage;
        }
    }
}
