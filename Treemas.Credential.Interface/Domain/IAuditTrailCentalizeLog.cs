﻿using System;
using Treemas.Credential.Model;
using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Interface
{
    public interface IAuditTrailCentalizeLog : IBaseRepository<AuditTrailCentralUnit>
    {
        void SaveAuditTrail(string functionName, object auditedObjectBefore, object auditedObjectAfter, string maker, DateTime? requestDate, string actionby, string action);
        void SaveAuditTrail(string functionName, object auditedObject, string actionby, string action);
        PagedResult<AuditTrailCentralUnit> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<AuditTrailCentralUnit> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<AuditTrailCentralUnit> FindAllPagedDate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue);
    }
}
