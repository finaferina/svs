﻿using System;
using Treemas.Credential.Model;
using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Interface
{
    public interface IUserAuditTrailLog : IBaseRepository<UserAuditTrail>
    {
        void SaveAuditTrail(string functionName, object auditedObjectBefore, object auditedObjectAfter, string maker, DateTime? requestDate, string actionby, string action);
        void SaveAuditTrail(string functionName, string objectName, object auditedObjectBefore, object auditedObjectAfter, string maker, DateTime? requestDate, string actionby, string action);
        PagedResult<UserAuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<UserAuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<UserAuditTrail> FindAllPagedDate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue);
    }
}
