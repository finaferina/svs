﻿using System;
using Treemas.Credential.Model;
using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Interface
{
    public interface IAppAuditTrailCentralizeLog : IBaseRepository<AppAuditTrailCentralize>
    {
        void SaveAuditTrail(string functionName, object auditedObjectBefore, object auditedObjectAfter, string maker, DateTime? requestDate, string actionby, string action);
        void SaveAuditTrail(string functionName, string objectName, object auditedObjectBefore, object auditedObjectAfter, string maker, DateTime? requestDate, string actionby, string action);
        PagedResult<AppAuditTrailCentralize> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<AppAuditTrailCentralize> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<AppAuditTrailCentralize> FindAllPagedDate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue);
    }
}
