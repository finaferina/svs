﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface IParameterCentralUnitRepository : IBaseRepository<ParameterCentralUnit>
    {
        ParameterCentralUnit getParameter();
        bool IsDuplicate(string UserId);
        bool InChangesRequest(string ParamID);
        void AddHistory(ParameterHistoryCentralUnit item);
        void SaveHistory(ParameterHistoryCentralUnit item);
        ParameterHistoryCentralUnit GetChangesRequest();
    }
}
