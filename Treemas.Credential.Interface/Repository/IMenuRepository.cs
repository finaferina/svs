﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface IMenuRepository : IBaseRepository<Menu>
    {
        IList<Menu> getApplicationMenu(string application);

        Menu getForEditMode(long id);
        Menu getForEditMode(string menuid);

        IList<AuthorizationFunction> getFunctions(IList<string> ApplicationIds);
        bool IsDuplicate(string application, string menuId);
        bool IsDuplicateForAdd(string application, string functionid);
        PagedResult<Menu> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<Menu> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        IList<Menu> FindOnlyParentMenu(string menuID);

        string GetRootMenuId(long id);
        string GetRootMenuNameByParentMenuId(string parentId);
    }
}
