﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;


namespace Treemas.Credential.Interface
{
    public interface IParameterAuditTrailRepository : IBaseRepository<ParameterAuditTrail>
    {
        void SaveAuditTrail(string functionName, object auditedObject, string actionby, string action);
        //IList<ParameterAuditTrail> FindForExport(AuditTrailFilter filter);
        PagedResult<ParameterAuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, DateTime start, DateTime end);
        PagedResult<ParameterAuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        //PagedResult<ParameterAuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, AuditTrailFilter filter);
        PagedResult<ParameterAuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<ParameterAuditTrail> FindAllPagedDate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue);
        IList<ParameterAuditTrail> FindAllPagedForExport(string searchColumn, string searchValue, DateTime ActionDateTimeStart, DateTime ActionDateTimeEnd);
    }
}
