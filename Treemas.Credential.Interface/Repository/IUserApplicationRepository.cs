﻿using System.Collections.Generic;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface IUserApplicationRepository : IBaseRepository<UserApplication>
    {
        IList<UserApplication> getUserApplication(string username);
        UserApplication getUserApplication(string username, string application);
        UserApplication getUserApplication(long id);
        int deleteSelected(string username, string application);
        UserApplication getUserApp(string username, string application);
        bool IsDuplicate(string username);
        UserApplication GetUser(string username);
        int DeleteUserApplication(string username);
    }
}
