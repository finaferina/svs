﻿using System.Collections.Generic;
using System;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface IDivisionRepository : IBaseRepository <Division>
    {
        IList<Division> getApplicationDivision(string application);
        bool IsDuplicate(string application, string divisionid);
        bool IsDuplicateByAlias(string application, string alias);
        bool IsDuplicateCR(string application, string divisionid, string approval_status);
        bool IsDuplicateCRByAlias(string application, string alias, string approval_status);
        PagedResult<Division> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<Division> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        IList<Division> FindForExport(string searchColumn, string searchValue);
        Division getDivision(long id);
        Division getDivision(string id);
        IList<Division> getDivisionbyRegion(string Region);
        void UpdateDivision(long id, string divisionid, string name, string alias, string regionid, string changedby, DateTime changeddate);
        bool IsFound(string region);
    }
}
