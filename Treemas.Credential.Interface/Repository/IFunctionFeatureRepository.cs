﻿using LinqSpecs;
using System.Collections.Generic;
using Treemas.Credential.Model;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Interface
{
    public interface IFunctionFeatureRepository : IBaseRepository<FunctionFeature>
    {
        FunctionFeature getFunctionFeature(long id);
        FunctionFeature getFunctionFeature(long functionid, long featureid);
        IList<FunctionFeature> getFunctionFeatures(long functionid);
        int deleteSelected(long functionId, long featureid);
        PagedResult<FunctionFeature> FindAllPagedbyFunction(long functionId, int pageNumber, string orderColumn, int itemsPerPage, string orderKey);
        PagedResult<FunctionFeature> FindAllPagedbyFunction(long functionId, int pageNumber, string orderColumn, int itemsPerPage, string orderKey, string searchColumn, string searchValue);
    }
}
