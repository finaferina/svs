﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;


namespace Treemas.Credential.Interface
{
    public interface IFeatureCrRepository : IBaseRepository<AuthorizationFeatureCr>
    {
        IList<AuthorizationFeatureCr> getFeatures(IList<string> ApplicationIds);
        bool IsDuplicate(string application, string featureid);
        PagedResult<AuthorizationFeatureCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter);
        AuthorizationFeatureCr getFeature(long id);
        AuthorizationFeatureCr getExistingRequest(string functionId);
        void deleteExistingRequest(string functionId);
    }

}
