﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface ICentralUnitDocTypeCrRepository : IBaseRepository<CentralUnitDocTypeCr>
    {
        IList<CentralUnitDocTypeCr> getApplicationCentralizeDoc(string application);
        bool IsDuplicate(string application, string docid);
        PagedResult<CentralUnitDocTypeCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter);
        CentralUnitDocTypeCr getCentralizeDoc(long id);
        CentralUnitDocTypeCr getCentralizeDoc(string id);
        CentralUnitDocTypeCr getExistingRequest(string docid, string reqType);
        void deleteExistingRequest(string docid);
    }
}
