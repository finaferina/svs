﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface IRegionCrRepository : IBaseRepository<RegionCr>
    {
        IList<RegionCr> getApplicationRegion(string application);
        bool IsDuplicate(string application, string regionid);
        PagedResult<RegionCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter);
        RegionCr getRegion(long id);
        RegionCr getRegion(string id);
        RegionCr getExistingRequest(string regionid, string reqType);
        void deleteExistingRequest(string regionid);
    }
}
