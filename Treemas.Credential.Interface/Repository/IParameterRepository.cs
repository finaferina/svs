﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface IParameterRepository : IBaseRepository<Parameter>
    {
        Parameter getParameter();
        bool IsDuplicate(string UserId);
        bool InChangesRequest(string ParamID);
        void AddHistory(ParameterHistory item);
        void SaveHistory(ParameterHistory item);
        ParameterHistory GetChangesRequest();
    }
}
