﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface IAuditTrailCentralUnitRepository : IBaseRepository<AuditTrailCentralUnit>
    {
        void SaveAuditTrail(string functionName, object auditedObject, string actionby, string action);
        //IList<AuditTrail> FindForExport(AuditTrailFilter filter);
        PagedResult<AuditTrailCentralUnit> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, DateTime start, DateTime end);
        PagedResult<AuditTrailCentralUnit> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, DateTime start, DateTime end, User user);
        PagedResult<AuditTrailCentralUnit> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, DateTime start, DateTime end, string[] userList);
        PagedResult<AuditTrailCentralUnit> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<AuditTrailCentralUnit> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, AuditTrailFilter filter);
        PagedResult<AuditTrailCentralUnit> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<AuditTrailCentralUnit> FindAllPagedDate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue);
        IList<AuditTrailCentralUnit> FindAllPagedForExport(string searchColumn, string searchValue, DateTime ActionDateTimeStart, DateTime ActionDateTimeEnd);
        IList<AuditTrailCentralUnit> FindAllPagedForExport(string searchColumn, string searchValue, DateTime ActionDateTimeStart, DateTime ActionDateTimeEnd, User user);
        IList<AuditTrailCentralUnit> FindAllPagedForExport(string searchColumn, string searchValue, DateTime ActionDateTimeStart, DateTime ActionDateTimeEnd, string[] userList);
    }
}
