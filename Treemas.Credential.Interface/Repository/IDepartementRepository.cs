﻿using System.Collections.Generic;
using System;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface IDepartementRepository : IBaseRepository<Departement>
    {
        IList<Departement> getApplicationDepartement(string application);
        bool IsDuplicate(string application, string departementid);
        bool IsDuplicateByAlias(string application, string alias);
        bool IsDuplicateCR(string application, string departementid, string approval_status);
        bool IsDuplicateCRByAlias(string application, string alias, string approval_status);
        PagedResult<Departement> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<Departement> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        IList<Departement> FindForExport(string searchColumn, string searchValue);
        Departement getDepartement(long id);
        Departement getDepartement(string id);
        IList<Departement> getDepartementbyRegion(string Region, string division);
        void UpdateDepartement(long id, string departementid, string name, string alias, string regionid, string divisionid, string changedby, DateTime changeddate);
        bool IsFoundRegion(string region);
        bool IsFoundDivision(string division);
    }
}
