﻿using System.Collections.Generic;
using Treemas.Credential.Model;


namespace Treemas.Credential.Interface
{
    public interface IAuthorizationRepository : IBaseRepository<Authorization>
    {
        IList<Authorization> getUserAuthorization(string username);
        IList<Authorization> getUserRoleAuthorization(string roleId);
        IList<Authorization> getUserAuthorizationList(long id);
        Authorization getUserAuthorization(long id);
        Authorization getUserAuthorizationByUsername(string username);
        string getDistinctUserAuthorizationByUsername(string username);
        string getDistinctUserAuthorizationByRole(string roleId);
        bool IsRoleExist(string roleid);
        int delete(string username, string application, string roleId);
        int delete(string username, string application);
    }
}
