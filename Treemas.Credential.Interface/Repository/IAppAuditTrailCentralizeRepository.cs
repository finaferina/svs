﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface IAppAuditTrailCentralizeRepository : IBaseRepository<AppAuditTrailCentralize>
    {
        void SaveAuditTrail(string functionName, object auditedObject, string actionby, string action);
        void SaveAuditTrail(string functionName, string objectName, object auditedObjectBefore, object auditedObjectAfter, string maker, DateTime? requestDate, string actionby, string action);
        IList<AppAuditTrailCentralize> FindForExport(string searchColumn, string searchValue, DateTime ActionDateTimeStart, DateTime ActionDateTimeEnd);
        PagedResult<AppAuditTrailCentralize> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<AppAuditTrailCentralize> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, AuditTrailFilter filter);
        PagedResult<AppAuditTrailCentralize> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, DateTime start, DateTime end);
        PagedResult<AppAuditTrailCentralize> FindAllPagedDate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue);
    }
}
