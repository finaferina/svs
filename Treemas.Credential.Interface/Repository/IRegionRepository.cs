﻿using System.Collections.Generic;
using System;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface IRegionRepository : IBaseRepository<Region>
    {
        IList<Region> getApplicationRegion(string application);
        bool IsDuplicate(string application, string regionid);
        bool IsDuplicateCR(string application, string regionid, string approval_status);
        bool IsDuplicateByAlias(string application, string Alias);
        bool IsDuplicateCRByAlias(string application, string Alias, string approval_status);
        PagedResult<Region> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<Region> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        IList<Region> FindForExport(string searchColumn, string searchValue);
        Region getRegion(long id);
        Region getRegion(string id);
        void UpdateRegion(long id, string regionid, string name, string alias, string changedby, DateTime changeddate);
    }
}
