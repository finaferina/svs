﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;


namespace Treemas.Credential.Interface
{
    public interface IParameterAuditTrailCentralUnitRepository : IBaseRepository<ParameterAuditTrailCentralUnit>
    {
        void SaveAuditTrail(string functionName, object auditedObject, string actionby, string action);
        //IList<ParameterAuditTrail> FindForExport(AuditTrailFilter filter);
        PagedResult<ParameterAuditTrailCentralUnit> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, DateTime start, DateTime end);
        PagedResult<ParameterAuditTrailCentralUnit> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        //PagedResult<ParameterAuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, AuditTrailFilter filter);
        PagedResult<ParameterAuditTrailCentralUnit> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        PagedResult<ParameterAuditTrailCentralUnit> FindAllPagedDate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue);
        IList<ParameterAuditTrailCentralUnit> FindAllPagedForExport(string searchColumn, string searchValue, DateTime ActionDateTimeStart, DateTime ActionDateTimeEnd);
    }
}
