﻿using System.Collections.Generic;
using System;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface ICentralUnitDocTypeRepository : IBaseRepository<CentralUnitDocType>
    {
        IList<CentralUnitDocType> getApplicationCentralUnitDocType(string application);
        bool IsDuplicate(string application, string docid);
        bool IsDuplicateCR(string application, string docid, string approval_status);
        bool IsDuplicateByAlias(string application, string Alias);
        bool IsDuplicateCRByAlias(string application, string Alias, string approval_status);
        PagedResult<CentralUnitDocType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        PagedResult<CentralUnitDocType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue);
        IList<CentralUnitDocType> FindForExport(string searchColumn, string searchValue);
        CentralUnitDocType getCentralUnitDocType(long id);
        CentralUnitDocType getCentralUnitDocType(string id);
        void UpdateCentralUnitDocType(long id, string docid, string name, string alias, string changedby, DateTime changeddate);
    }
}
