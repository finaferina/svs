﻿using LinqSpecs;
using System.Collections.Generic;
using Treemas.Credential.Model;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Interface
{
    public interface IRoleDocTypeRepository : IBaseRepository<RoleDocType>
    {
        IList<RoleDocType> getDocTypes(long RoleId);
        bool IsDuplicate(long roleid, long DocTypeid);
        PagedResult<RoleDocType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        RoleDocType getRoleDocType(long id);
        int deleteSelected(long roleId, long DocTypeId);
        RoleDocType getRoleDocType(long roleId,long docTypeId);
        RoleDocType getRoleDocByDocType(long docTypeId);
    }
}
