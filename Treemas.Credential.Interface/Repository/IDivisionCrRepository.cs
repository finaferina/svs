﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface IDivisionCrRepository : IBaseRepository<DivisionCr>
    {
        IList<DivisionCr> getApplicationDivision(string application);
        bool IsDuplicate(string application, string divisionid);
        PagedResult<DivisionCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter);
        DivisionCr getDivision(long id);
        DivisionCr getDivision(string id);
        DivisionCr getExistingRequest(string divisionid, string reqType);
        void deleteExistingRequest(string divisionid);
    }
}
