﻿using LinqSpecs;
using System.Collections.Generic;
using Treemas.Credential.Model;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Interface
{
    public interface IRoleFunctionFeatureRepository : IBaseRepository<RoleFunctionFeature>
    {
        IList<RoleFunctionFeature> getRoleFuncFeatures(long RoleFuncId);
        IList<RoleFunctionFeature> getRoleFuncFeatures(long FuncId, long FunctionId);
        bool IsDuplicate(long roleid, long functionid);
        PagedResult<RoleFunctionFeature> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey);
        RoleFunctionFeature getRoleFuncFeature(long id);
        RoleFunctionFeature getRoleFuncFeature(long RoleId, long FunctionId, long FeatureId);
        int deleteSelected(long roleId, long functionId, long featureId);
        int deleteAllbyRoleFuncId(long roleFunctId);
        IList<RoleFunctionFeature> getRoleFuncFeaturebyRole(long roleId);
    }
}
