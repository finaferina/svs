﻿using System.Collections.Generic;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;

namespace Treemas.Credential.Interface
{
    public interface IDepartementCrRepository : IBaseRepository<DepartementCr>
    {
        IList<DepartementCr> getApplicationDepartement(string application);
        bool IsDuplicate(string application, string departementid);
        PagedResult<DepartementCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter);
        DepartementCr getDepartement(long id);
        DepartementCr getDepartement(string id);
        DepartementCr getExistingRequest(string departementid, string reqType);
        void deleteExistingRequest(string departementid);
    }
}
