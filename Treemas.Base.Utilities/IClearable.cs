﻿namespace Treemas.Base.Utilities
{
    public interface IClearable
    {
        void Clear(object param);
    }
}

