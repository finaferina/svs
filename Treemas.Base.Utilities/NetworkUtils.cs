﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace Treemas.Base.Utilities
{

    public class NetworkUtils
    {
        private NetworkUtils()
        {
        }

        public static Stream HttpPost(string url, IDictionary<string, string> parameters)
        {
            if (!string.IsNullOrEmpty(url))
            {
                HttpWebRequest request = (HttpWebRequest) WebRequest.Create(url);
                if (request != null)
                {
                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";
                    if ((parameters != null) && (parameters.Count > 0))
                    {
                        StringBuilder builder = new StringBuilder();
                        foreach (string str in parameters.Keys)
                        {
                            builder.Append(str).Append('=').Append(parameters[str]).Append('&');
                        }
                        builder.Remove(builder.Length - 1, 1);
                        byte[] bytes = Encoding.ASCII.GetBytes(builder.ToString());
                        if ((bytes != null) && (bytes.Length > 0))
                        {
                            request.ContentLength = bytes.Length;
                            Stream requestStream = request.GetRequestStream();
                            requestStream.Write(bytes, 0, bytes.Length);
                            requestStream.Close();
                        }
                    }
                    else
                    {
                        request.ContentLength = 0L;
                    }
                    HttpWebResponse response = (HttpWebResponse) request.GetResponse();
                    if (response != null)
                    {
                        response.GetResponseStream();
                    }
                }
            }
            return null;
        }
    }
}

