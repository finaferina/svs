﻿using System;
using ServiceStack.Text;

namespace Treemas.Base.Utilities
{
    public class JSON
    {
        public static T ToObject<T>(string str)
        {
            object obj2 = null;
            if (!string.IsNullOrEmpty(str))
            {
                obj2 = JsonSerializer.DeserializeFromString<T>(str);
            }
            return (T) obj2;
        }

        public static object ToObject(string str, Type type)
        {
            if (!string.IsNullOrEmpty(str))
            {
                return JsonSerializer.DeserializeFromString(str, type);
            }
            return null;
        }

        public static string ToString(object obj)
        {
            if (obj == null)
            {
                return string.Empty;
            }
            return JsonSerializer.SerializeToString<object>(obj);
        }

        public static string ToString<T>(object obj)
        {
            if (obj == null)
            {
                return string.Empty;
            }
            return JsonSerializer.SerializeToString<T>((T) obj);
        }

        public static string ToString(object obj, Type type)
        {
            if (obj == null)
            {
                return string.Empty;
            }
            return JsonSerializer.SerializeToString(obj, type);
        }
    }
}

