﻿namespace Treemas.Base.Utilities
{
    public static class NumberExtensions
    {
        public static int IncrementIfAllowed(this int number, bool allowed)
        {
            if (allowed)
            {
                number++;
            }
            return number;
        }
    }
}

