﻿namespace Treemas.Base.Utilities
{
    public interface IEntityString
    {
        string Id { get; }
    }
}
