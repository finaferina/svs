﻿namespace Treemas.Base.Utilities
{
    public class BaseAction : IAction
    {
        private string name;

        public BaseAction(string name)
        {
            this.SetName(name);
        }

        public string GetName() => 
            this.name;

        public void SetName(string name)
        {
            this.name = name;
        }
    }
}

