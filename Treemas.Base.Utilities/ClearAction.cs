﻿using System;

namespace Treemas.Base.Utilities
{
    public class ClearAction : BaseAction, IClearAction, IAction
    {
        public ClearAction(string name, Action<object> action) : base(name)
        {
            this._Action = action;
        }

        public void Clear(object param)
        {
            if (this._Action != null)
            {
                this._Action(param);
            }
        }

        protected Action<object> _Action { get; private set; }
    }
}

