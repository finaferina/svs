﻿using System;

namespace Treemas.Base.Utilities
{
    public class CreateAction : BaseAction, ICreateAction, IAction
    {
        public CreateAction(string name, Action<object> action) : base(name)
        {
            this._Action = action;
        }

        public void Create(object param)
        {
            if (this._Action != null)
            {
                this._Action(param);
            }
        }

        protected Action<object> _Action { get; private set; }
    }
}

