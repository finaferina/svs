﻿namespace Treemas.Base.Utilities
{
    public interface ICleanAction : IAction
    {
        void Clean(object param);
    }
}

