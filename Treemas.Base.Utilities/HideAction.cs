﻿using System;

namespace Treemas.Base.Utilities
{
    public class HideAction : BaseAction, IHideAction, IAction
    {
        public HideAction(string name, Action<object> action) : base(name)
        {
            this._Action = action;
        }

        public void Hide(object param)
        {
            if (this._Action != null)
            {
                this._Action(param);
            }
        }

        protected Action<object> _Action { get; private set; }
    }
}

