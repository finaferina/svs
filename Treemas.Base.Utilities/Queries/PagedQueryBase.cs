﻿
namespace Treemas.Base.Utilities.Queries
{
    public abstract class PagedQueryBase
    {
        private int pageNumber;
        private int itemsPerPage;
        public int PageNumber
        {
            get
            {
                return (this.pageNumber == 0) ? 1 : this.pageNumber;
            }
            set
            {
                this.pageNumber = value;
            }
        }
        public int ItemsPerPage
        {
            get
            {
                return (this.itemsPerPage == 0) ? 10 : this.itemsPerPage;
            }
            set
            {
                this.itemsPerPage = value;
            }
        }

    }
}
