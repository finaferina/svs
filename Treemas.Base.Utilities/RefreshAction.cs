﻿using System;

namespace Treemas.Base.Utilities
{
    public class RefreshAction : BaseAction, IRefreshAction, IAction
    {
        public RefreshAction(string name, Action<object> action) : base(name)
        {
            this._Action = action;
        }

        public void Refresh(object param)
        {
            if (this._Action != null)
            {
                this._Action(param);
            }
        }

        private Action<object> _Action { get; set; }
    }
}

