﻿using System;

namespace Treemas.Base.Utilities
{
    public class DeleteAction : BaseAction, IDeleteAction, IAction
    {
        public DeleteAction(string name, Action<object> action) : base(name)
        {
            this._Action = action;
        }

        public void Delete(object param)
        {
            if (this._Action != null)
            {
                this._Action(param);
            }
        }

        protected Action<object> _Action { get; private set; }
    }
}

