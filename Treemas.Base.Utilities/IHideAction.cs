﻿namespace Treemas.Base.Utilities
{
    public interface IHideAction : IAction
    {
        void Hide(object param);
    }
}

