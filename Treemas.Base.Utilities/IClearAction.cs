﻿namespace Treemas.Base.Utilities
{
    using System;

    public interface IClearAction : IAction
    {
        void Clear(object param);
    }
}

