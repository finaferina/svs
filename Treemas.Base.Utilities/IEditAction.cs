﻿namespace Treemas.Base.Utilities
{
    public interface IEditAction : IAction
    {
        void Edit(object param);
    }
}

