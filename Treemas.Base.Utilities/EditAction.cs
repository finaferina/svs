﻿namespace Treemas.Base.Utilities
{
    using System;
    using System.Runtime.CompilerServices;

    public class EditAction : BaseAction, IEditAction, IAction
    {
        public EditAction(string name, Action<object> action) : base(name)
        {
            this._Action = action;
        }

        public void Edit(object param)
        {
            if (this._Action != null)
            {
                this._Action(param);
            }
        }

        protected Action<object> _Action { get; private set; }
    }
}

