﻿using System;
using LinqSpecs;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Base.Repository
{
    public interface IRepositoryController<T> : IEnumerable<T> where T : IEntity
    {
        void Add(T item);
        bool Contains(T item);
        int Count { get; }
        bool Remove(T item);
        void Save(T item);
        void SaveOrUpdateAll(IEnumerable<T> items);
        IEnumerable<T> FindAll();
        IEnumerable<T> FindAll(Specification<T> specification);
        T FindOne(Specification<T> specification);
        T GetOne(Specification<T> specification);
        Type GetSubjectType();
    }
}
