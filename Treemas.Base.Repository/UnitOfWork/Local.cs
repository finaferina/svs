﻿using System;
using System.Collections;
using System.Web;

namespace Treemas.Base.Repository.UnitOfWork
{
    public static class Local
    {
        static readonly ILocalData data = new LocalData();

        public static ILocalData Data
        {
            get { return data; }
        }

        private class LocalData : ILocalData
        {
            [ThreadStatic]
            private static Hashtable localData;
            private static readonly object localDataHashtableKey = new object();

            private static Hashtable localHashtable
            {
                get
                {
                    if (!RunningInWeb)
                    {
                        return localData ?? (localData = new Hashtable());
                    }
                    var webHashtable = HttpContext.Current.Items[localDataHashtableKey] as Hashtable;
                    if (webHashtable != null) return webHashtable;
                    webHashtable = new Hashtable();
                    HttpContext.Current.Items[localDataHashtableKey] = webHashtable;
                    return webHashtable;
                }
            }

            public object this[object key]
            {
                get { return localHashtable[key]; }
                set { localHashtable[key] = value; }
            }

            public int Count
            {
                get { return localHashtable.Count; }
            }

            public void Clear()
            {
                localHashtable.Clear();
            }

            public static bool RunningInWeb
            {
                get { return HttpContext.Current != null; }
            }
        }
    }
}
