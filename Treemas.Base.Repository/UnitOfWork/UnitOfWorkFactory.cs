﻿using System;
using System.IO;
using System.Xml;
using NHibernate;
using NHibernate.Cfg;

namespace Treemas.Base.Repository.UnitOfWork
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private const string DefaultHibernateConfig = "database.cfg.xml";

        private ISession currentSession;
        private ISessionFactory sessionFactory;
        private Configuration configuration;

        public UnitOfWorkFactory(Configuration configuration, ISessionFactory sessionFactory, ISession currentSession)
        {
            this.sessionFactory = sessionFactory;
            this.currentSession = currentSession;
            this.configuration = configuration;
        }
        public UnitOfWorkFactory(Configuration configuration, ISessionFactory sessionFactory)
        {
            this.sessionFactory = sessionFactory;
        }
        public UnitOfWorkFactory()
        {

        }

        public IUnitOfWork Create()
        {
            currentSession.FlushMode = FlushMode.Commit;
            return new UnitOfWorkImplementor(this, currentSession);
        }

        public Configuration Configuration
        {
            get
            {
                if (configuration != null)
                    return configuration;

                configuration = new Configuration();
                var hibernateConfig = DefaultHibernateConfig;

                //if not rooted, assume path from base directory
                if (Path.IsPathRooted(hibernateConfig) == false)
                    hibernateConfig = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, hibernateConfig);

                if (File.Exists(hibernateConfig))
                    configuration.Configure(new XmlTextReader(hibernateConfig));

                return configuration;
            }
        }

        public ISessionFactory SessionFactory
        {
            get { return sessionFactory ?? (sessionFactory = Configuration.BuildSessionFactory()); }
            set { sessionFactory = value; }
        }

        public ISession CurrentSession
        {
            get
            {
                if (currentSession != null) return currentSession;
                try
                {
                    currentSession = createSession();
                }
                catch (Exception)
                {
                    throw new InvalidOperationException("You are not in a unit of work.");
                }
                return currentSession;
            }
            set { currentSession = value; }
        }

        public void DisposeUnitOfWork(UnitOfWorkImplementor adapter)
        {
            CurrentSession = null;
            UnitOfWork.DisposeUnitOfWork(adapter);
        }

        private ISession createSession()
        {
            return SessionFactory.OpenSession();
        }
    }
}
