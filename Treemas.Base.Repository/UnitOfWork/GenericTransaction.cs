﻿using System;
using System.Runtime.InteropServices;
using NHibernate;

namespace Treemas.Base.Repository.UnitOfWork
{
    public class GenericTransaction : IGenericTransaction
    {
        private readonly ITransaction m_transaction;
        private IntPtr nativeResource = Marshal.AllocHGlobal(100);
        public GenericTransaction(ITransaction transaction)
        {
            m_transaction = transaction;
        }

        public void Commit()
        {
            m_transaction.Commit();
        }

        public void Rollback()
        {
            m_transaction.Rollback();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        ~GenericTransaction()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (m_transaction != null)
                {
                    m_transaction.Dispose();
                }
            }
            // free native resources if there are any.
            if (nativeResource == IntPtr.Zero) return;
            Marshal.FreeHGlobal(nativeResource);
            nativeResource = IntPtr.Zero;
        }
    }
}
