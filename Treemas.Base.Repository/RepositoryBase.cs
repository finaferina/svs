﻿using System;
using NHibernate;
using Treemas.Base.Repository.UnitOfWork;

namespace Treemas.Base.Repository
{
    public abstract class RepositoryBase
    {
        public IUnitOfWorkFactory m_unitOfWorkFactory { get; set; }

        protected virtual ISession session
        {
            get
            {
                return m_unitOfWorkFactory.CurrentSession;
            }
        }

        protected virtual TResult transact<TResult>(Func<TResult> func)
        {
            if (session.Transaction.IsActive) return func.Invoke();

            // Wrap in transaction
            TResult result;
            using (UnitOfWork.UnitOfWork.Start(m_unitOfWorkFactory))
            {
                result = func.Invoke();
                UnitOfWork.UnitOfWork.Current.TransactionalFlush();
            }
            return result;
            // Don't wrap;
        }

        protected virtual void transact(Action action)
        {
            transact<bool>(() =>
            {
                action.Invoke();
                return false;
            });
        }
    }
}
