﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;

namespace UOB.SVS.Model
{
    public class BranchAccountPrefix : Entity
    {
        public BranchAccountPrefix()
        {

        }
        public BranchAccountPrefix(long id) : base(id)
        {

        }

        protected override void validate()
        {
            throw new NotImplementedException();
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string PrefixCode { get; set; }
        public virtual string BranchCode { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }
    }

    public class BranchAccountPrefixAudit
    {        
        public virtual string PrefixCode { get; set; }
        public virtual string BranchCode { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }
    }
}
