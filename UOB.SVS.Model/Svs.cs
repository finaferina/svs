﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;

namespace UOB.SVS.Model
{
    public class Svs : Entity
    {
        protected Svs() { }
        public Svs(long id) : base(id)
        {
            this.ID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.ID };
        }

        public virtual long ID { get; set; }
        public virtual string ACCOUNT_NUMBER { get; set; }
        public virtual string CUSTOMER_NAME { get; set; }
        public virtual string NOTE { get; set; }
        public virtual string SIGNATURE { get; set; }
        public virtual string KTP { get; set; }
        public virtual string NOTE_KTP { get; set; }
        public virtual string PENGENAL { get; set; }
        public virtual string ADD_BY { get; set; }
        public virtual DateTime? DATE_ADD { get; set; }
        public virtual string UPD_BY { get; set; }
        public virtual DateTime? DATE_UPD { get; set; }
        public virtual string DEL_BY { get; set; }
        public virtual DateTime? DATE_DEL { get; set; }
        public virtual string APP_ADD_BY { get; set; }
        public virtual DateTime? DATE_APP_ADD { get; set; }
        public virtual string APP_UP_BY { get; set; }
        public virtual DateTime? DATE_APP_UPD { get; set; }
        public virtual string APP_DEL_BY { get; set; }
        public virtual DateTime? DATE_APP_DEL { get; set; }
        public virtual string NOT_APP_ADD_BY { get; set; }
        public virtual DateTime? DATE_NOT_APP_ADD { get; set; }
        public virtual string NOT_APP_UPD_BY { get; set; }
        public virtual DateTime? DATE_NOT_APP_UPD { get; set; }
        public virtual string NOT_APP_DEL_BY { get; set; }
        public virtual DateTime? DATE_NOT_APP_DELL { get; set; }
        public virtual string STATUS_FLAG { get; set; }
        public virtual string STATUS_AKTIF { get; set; }
        public virtual string OLD_NOTE { get; set; }
        public virtual string OLD_NOTE_KTP { get; set; }
        public virtual string FSIGNATURE { get; set; }
        public virtual string FKTP { get; set; }
        public virtual string FNOTE { get; set; }
        public virtual string ZSIGNATURE { get; set; }
        public virtual string ZOLD_SIGNATURE { get; set; }
        public virtual string ZKTP { get; set; }
        public virtual string ZOLD_KTP { get; set; }
        public virtual string OLD_SIGNATURE { get; set; }
        public virtual string OLD_KTP { get; set; }
        public virtual string OLD_ACC_NUMBER { get; set; }
    }
}
