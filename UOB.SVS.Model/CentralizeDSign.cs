﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;

namespace UOB.SVS.Model
{
    public class CentralizeDSign : Entity
    {
        protected CentralizeDSign() { }
        public CentralizeDSign(long id) : base(id)
        {
            this.ID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.NIK };
        }

        public virtual long ID { get; set; }
        public virtual string NIK { get; set; }
        public virtual string Signature { get; set; }
        public virtual string ImageType { get; set; }
        public virtual int Seq { get; set; }
    }
}
