﻿namespace UOB.SVS.Model
{
    public class ErrorMessage
    {
        public string ERROR_CODE { get; set; }
        public string ERROR_MESSAGE { get; set; }
    }
}