﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;

namespace UOB.SVS.Model
{
    public class CentralizeDDoc : Entity
    {
        protected CentralizeDDoc() { }
        public CentralizeDDoc(long id) : base(id)
        {
            this.ID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.NIK };
        }

        public virtual long ID { get; set; }
        public virtual string NIK { get; set; }
        public virtual string FileBlob { get; set; }
        public virtual string FileType { get; set; }
        public virtual string DocumentType { get; set; }
        public virtual int Seq { get; set; }
    }
}
