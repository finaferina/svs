﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;
using Treemas.Credential.Model;

namespace UOB.SVS.Model
{
    public class DocumentTypeCr : Entity
    {
        protected DocumentTypeCr() { }
        public DocumentTypeCr(long id) : base(id)
        {
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string DocType { get; set; }
        public virtual long DocTypeId { get; set; }
        public virtual string Description { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }

        public virtual bool Approved { get; set; }
        public virtual string ApprovedBy { get; set; }
        public virtual string ApprovalType { get; set; }
        public virtual ApprovalType ApprovalTypeEnum
        {
            get
            {
                ApprovalType currentState = (ApprovalType)Enum.Parse(typeof(ApprovalType), this.ApprovalType);
                return currentState;
            }
        }

        public virtual DateTime? ApprovedDate { get; set; }
        public virtual string ApprovalStatus { get; set; }
    }
}
