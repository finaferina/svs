﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;

namespace UOB.SVS.Model
{
    public class AccountMaster: Entity
    {
        protected AccountMaster() { }
        public AccountMaster(long id) : base(id)
        {
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string AccountNo { get; set; }
        public virtual string AccountName { get; set; }
        public virtual string AccountType { get; set; }
        public virtual string CIFNo{ get; set; }
        public virtual string BranchCode { get; set; }
        public virtual string Status { get; set; }
        public virtual DateTime? DateOpened { get; set; }
        public virtual DateTime? DateClosed { get; set; }
    }
}
