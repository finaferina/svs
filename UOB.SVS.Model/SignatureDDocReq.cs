﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;

namespace UOB.SVS.Model
{
    public class SignatureDDocReq : Entity
    {
        protected SignatureDDocReq() { }
        public SignatureDDocReq(long id) : base(id)
        {
            this.ID = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.ID };
        }

        public virtual long ID { get; set; }
        public virtual string AccountNo { get; set; }
        public virtual string FileBlob { get; set; }
        public virtual string FileType { get; set; }
        public virtual string DocumentType { get; set; }
        public virtual string RequestType { get; set; }
        public virtual DateTime? RequestDate { get; set; }
        public virtual string RequestUser { get; set; }
        public virtual string RequestReason { get; set; }
        public virtual int FileSeq { get; set; }
    }
}
