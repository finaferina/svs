﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;

namespace UOB.SVS.Model
{
    public class SignatureHeader : EntityString
    {
        protected SignatureHeader() { }
        public SignatureHeader(string id) : base(id)
        {
            this.AccountNo = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.AccountNo };
        }

        public virtual string AccountNo { get; set; }
        public virtual string AccountName { get; set; }
        public virtual string AccountType { get; set; }
        public virtual string CIFNumber { get; set; }
        public virtual string BranchCode { get; set; }
        
        //[AllowHtml]
        public virtual string Note { get; set; }
        public virtual IList<SignatureDDoc> Documents { get; set; }
        public virtual IList<SignatureDSign> Signatures { get; set; }
        public virtual IList<SignatureDKTP> IdentityCards { get; set; }
    }
    public class SignatureHeaderAudit
    {
        public virtual string Id { get; set; }
        public virtual string AccountNo { get; set; }
        public virtual string AccountName { get; set; }
        public virtual string AccountType { get; set; }
        public virtual string CIFNumber { get; set; }
        public virtual string BranchCode { get; set; }
        public virtual string Note { get; set; }
        public virtual string Documents { get; set; }
        public virtual string Signatures { get; set; }
        public virtual string IdentityCards { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
    }
}
