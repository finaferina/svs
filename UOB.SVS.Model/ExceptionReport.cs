﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace UOB.SVS.Model
{
    public class ExceptionReport : Entity
    {
        protected ExceptionReport(){}
        public ExceptionReport(int id) : base(id)
        {
         
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        //public virtual int Id { get; set; }
        public virtual string AccountNo { get; set; }
        public virtual string AccountName { get; set; }
        public virtual string CIFNumber { get; set; }
        public virtual DateTime? OpenDate { get; set; }
        public virtual DateTime? CloseDate { get; set; }
        public virtual string Reason { get; set; }
        public virtual string RequestType { get; set; }
        public virtual string BranchCode { get; set; }
        public virtual DateTime? ActionDate { get; set; }

    }
}
