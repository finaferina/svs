﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UOB.SVS.Model
{
    public class SVSReqFilter
    {
        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public string RequestType { get; set; }
        public IList<Branch> Branches{ get; set; }
        public string CIFNumber { get; set; }
        public string ApprovalType { get; set; }
        public DateTime? TanggalStart { get; set; }
        public DateTime? TanggalEnd { get; set; }
        public string RequestUser { get; set; }
    }
}
