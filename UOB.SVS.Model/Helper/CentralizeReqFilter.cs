﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UOB.SVS.Model.Helper
{
    public class CentralizeReqFilter
    {
        public string NIK { get; set; }
        public string Name { get; set; }
        public string Titles { get; set; }
        public string Region { get; set; }
        public string Division { get; set; }
        public string Departement { get; set; }
        public string RequestType { get; set; }
        public string ApprovalType { get; set; }
        public DateTime? TanggalStart { get; set; }
        public DateTime? TanggalEnd { get; set; }
        public string RequestUser { get; set; }
    }
}
