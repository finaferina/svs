﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;
using Treemas.Credential.Model;
using System.ComponentModel.DataAnnotations;

namespace UOB.SVS.Model
{
    public class CentralizeUnitHRequest : EntityString
    {
        protected CentralizeUnitHRequest() { }
        public CentralizeUnitHRequest(string id) : base(id)
        {
            this.NIK = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.NIK };
        }

        public virtual string NIK { get; set; }
        public virtual string Name { get; set; }
        public virtual string DocType { get; set; }
        public virtual string Region { get; set; }
        public virtual string Division { get; set; }
        public virtual string Departement { get; set; }
        public virtual string Titles { get; set; }
        public virtual string Note { get; set; }
        public virtual string RequestType { get; set; }
        public virtual string RequestUser { get; set; }
        public virtual DateTime? RequestDate { get; set; }
        public virtual string RequestReason { get; set; }
        public virtual bool IsRejected { get; set; }
        public virtual IList<CentralizeDDocReq> Documents { get; set; }
        public virtual IList<CentralizeDSignReq> Signatures { get; set; }
    }
}
