﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;

namespace UOB.SVS.Model
{
    public class SignatureHHistory : Entity
    {
        protected SignatureHHistory() { }
        public SignatureHHistory(long id) : base(id)
        {
            this.RecordId = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.RecordId };
        }

        public virtual long RecordId { get; set; }
        public virtual string AccountNo { get; set; }
        public virtual string AccountName { get; set; }
        public virtual string AccountType { get; set; }
        public virtual string CIFNumber { get; set; }
        public virtual string BranchCode { get; set; }
        public virtual string Note { get; set; }
        public virtual string RequestType { get; set; }
        public virtual DateTime? RequestDate { get; set; }
        public virtual string RequestUser { get; set; }
        public virtual string RequestReason { get; set; }
        public virtual string ApproveBy { get; set; }
        public virtual DateTime? ApproveDate { get; set; }
        public virtual DateTime? RejectDate { get; set; }
        public virtual string RejectBy { get; set; }
        public virtual string RejectReason { get; set; }
    }
}
