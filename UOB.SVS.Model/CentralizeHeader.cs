﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;

namespace UOB.SVS.Model
{
    public class CentralizeHeader : EntityString
    {
        protected CentralizeHeader() { }
        public CentralizeHeader(string id) : base(id)
        {
            this.NIK = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.NIK };
        }

        public virtual string NIK { get; set; }
        public virtual string Name { get; set; }
        public virtual string Region { get; set; }
        public virtual string Division { get; set; }
        public virtual string Departement { get; set; }
        public virtual string Titles { get; set; }
        public virtual string Note { get; set; }
        public virtual string CreateUser { get; set; }
        public virtual DateTime? CreateDate { get; set; }
        public virtual IList<CentralizeDDoc> Documents { get; set; }
        public virtual IList<CentralizeDSign> Signatures { get; set; }
    }
    public class CentralizeHeaderAudit
    {
        public virtual string Id { get; set; }
        public virtual string NIK { get; set; }
        public virtual string Name { get; set; }
        public virtual string Region { get; set; }
        public virtual string Division { get; set; }
        public virtual string Departement { get; set; }
        public virtual string Titles { get; set; }
        public virtual string Note { get; set; }
        public virtual string Documents { get; set; }
        public virtual string Signatures { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
    }
}
