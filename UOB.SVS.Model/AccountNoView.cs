﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;

namespace UOB.SVS.Model
{
    public class AccountNoView : EntityString
    {
        protected AccountNoView() { }
        public AccountNoView(string id) : base(id)
        {
            this.AccountNo = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.AccountNo };
        }

        public virtual string AccountNo { get; set; }
    }
}
