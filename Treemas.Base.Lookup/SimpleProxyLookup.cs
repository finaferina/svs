﻿using System;
using System.Collections.Generic;

namespace Treemas.Base.Lookup
{

    [Serializable]
    public class SimpleProxyLookup : SimpleProxyLookupEventBroadcaster, IProxyLookup, IProxyLookupEventBroadcaster
    {
        private IList<ILookup> lookups;
        private string name;

        public SimpleProxyLookup() : this("SimpleProxyLookup")
        {
        }

        public SimpleProxyLookup(string name)
        {
            this.name = name;
            this.lookups = new List<ILookup>();
        }

        public void AddLookup(ILookup lookup)
        {
            if (!this.lookups.Contains(lookup))
            {
                this.lookups.Add(lookup);
                ProxyLookupEvent evt = new ProxyLookupEvent {
                    Broadcaster = this,
                    Lookup = lookup,
                    Type = ProxyLookupEventType.Lookup_Added
                };
                base.BroadcastEvent(evt);
            }
        }

        public T Get<T>()
        {
            foreach (ILookup lookup in this.lookups)
            {
                T local = lookup.Get<T>();
                if (local != null)
                {
                    return local;
                }
            }
            object obj2 = null;
            return (T) obj2;
        }

        public IList<T> GetAll<T>()
        {
            List<T> list = new List<T>();
            foreach (ILookup lookup in this.lookups)
            {
                IList<T> all = lookup.GetAll<T>();
                if (all != null)
                {
                    list.AddRange(all);
                }
            }
            if (list.Count > 0)
            {
                return list;
            }
            return null;
        }

        public ILookup GetLookup(string name)
        {
            foreach (ILookup lookup in this.lookups)
            {
                if (lookup.GetName().Equals(name))
                {
                    return lookup;
                }
            }
            return null;
        }

        public string GetName() => 
            this.name;

        public void Remove<T>()
        {
            foreach (ILookup lookup in this.lookups)
            {
                lookup.Remove<T>();
            }
        }

        public void Remove(object obj)
        {
            foreach (ILookup lookup in this.lookups)
            {
                lookup.Remove(obj);
            }
        }

        public void Remove<T>(Predicate<T> matchedCondition)
        {
            foreach (ILookup lookup in this.lookups)
            {
                lookup.Remove<T>(matchedCondition);
            }
        }

        public void RemoveLookup(string name)
        {
            ILookup lookup = this.GetLookup(name);
            this.RemoveLookup(lookup);
        }

        public void RemoveLookup(ILookup lookup)
        {
            this.lookups.Remove(lookup);
            ProxyLookupEvent evt = new ProxyLookupEvent {
                Broadcaster = this,
                Lookup = lookup,
                Type = ProxyLookupEventType.Lookup_Removed
            };
            base.BroadcastEvent(evt);
        }
    }
}

