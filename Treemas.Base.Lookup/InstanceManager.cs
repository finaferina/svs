﻿using System;
using System.Collections.Generic;

namespace Treemas.Base.Lookup
{
    public class InstanceManager
    {
        private IDictionary<Type, Type> implementationMap = new Dictionary<Type, Type>();
        private ILookup lookup = new SimpleLookup();

        public void AttachToProxyLookup(IProxyLookup proxyLookup)
        {
            proxyLookup.AddLookup(this.lookup);
        }

        public void DetachFromProxyLookup(IProxyLookup proxyLookup)
        {
            proxyLookup.RemoveLookup(this.lookup);
        }

        public T GetInstance<T>() => 
            this.GetInstance<T>(null);

        public T GetInstance<T>(params object[] arguments)
        {
            T local = this.lookup.Get<T>();
            if (local != null)
            {
                return local;
            }
            Type key = typeof(T);
            if (!this.implementationMap.ContainsKey(key))
            {
                return local;
            }
            if ((arguments != null) && (arguments.Length > 0))
            {
                return (T) Activator.CreateInstance(key, arguments);
            }
            return (T) Activator.CreateInstance(key);
        }

        public void Register<T>(Type implementationType)
        {
            if (implementationType != null)
            {
                Type key = typeof(T);
                if (this.implementationMap.ContainsKey(key))
                {
                    this.implementationMap[key] = implementationType;
                }
                else
                {
                    this.implementationMap.Add(key, implementationType);
                }
            }
        }

        public void RegisterSingleton<T>(Type implementationType, params object[] arguments)
        {
            IList<T> all = this.lookup.GetAll<T>();
            if (all != null)
            {
                foreach (T local in all)
                {
                    this.lookup.Remove(local);
                }
            }
            Type type = Type.GetType(implementationType.AssemblyQualifiedName);
            object obj2 = null;
            if ((arguments != null) && (arguments.Length > 0))
            {
                obj2 = (T) Activator.CreateInstance(type, arguments);
            }
            else
            {
                obj2 = (T) Activator.CreateInstance(type);
            }
            if (obj2 != null)
            {
                this.lookup.Add(obj2);
            }
        }

        public void Remove<T>()
        {
            Type key = typeof(T);
            if (this.implementationMap.ContainsKey(key))
            {
                this.implementationMap.Remove(key);
            }
        }

        public void RemoveSingleton<T>()
        {
            this.lookup.Remove<T>();
        }
    }
}

