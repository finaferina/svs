﻿using System;

namespace Treemas.Base.Lookup
{
    public interface ILookupEventListener
    {
        void LookupChanged(LookupEvent evt);
    }
}

