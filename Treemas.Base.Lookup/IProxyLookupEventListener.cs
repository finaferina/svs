﻿using System;

namespace Treemas.Base.Lookup
{
    public interface IProxyLookupEventListener
    {
        void ProxyLookupChanged(ProxyLookupEvent evt);
    }
}

