﻿using System;
using System.Collections.Generic;

namespace Treemas.Base.Lookup
{
    [Serializable]
    public class SimpleProxyLookupEventBroadcaster : IProxyLookupEventBroadcaster
    {
        private List<Action<ProxyLookupEvent>> actions = new List<Action<ProxyLookupEvent>>();
        private List<IProxyLookupEventListener> listeners = new List<IProxyLookupEventListener>();

        public void AddEventListener(Action<ProxyLookupEvent> action)
        {
            if ((action != null) && !this.actions.Contains(action))
            {
                this.actions.Add(action);
            }
        }

        public void AddEventListener(IProxyLookupEventListener listener)
        {
            if (!this.listeners.Contains(listener))
            {
                this.listeners.Add(listener);
            }
        }

        protected void BroadcastEvent(ProxyLookupEvent evt)
        {
            if (this.listeners.Count > 0)
            {
                IList<IProxyLookupEventListener> list;
                IList<Action<ProxyLookupEvent>> list2;
                lock (this.listeners)
                {
                    list = this.listeners.AsReadOnly();
                }
                if (list != null)
                {
                    foreach (IProxyLookupEventListener listener in list)
                    {
                        listener.ProxyLookupChanged(evt);
                    }
                }
                lock (this.actions)
                {
                    list2 = this.actions.AsReadOnly();
                }
                if (list2 != null)
                {
                    foreach (Action<ProxyLookupEvent> action in list2)
                    {
                        action(evt);
                    }
                }
            }
        }

        public void RemoveEventListener(Action<ProxyLookupEvent> action)
        {
            if (action != null)
            {
                this.actions.Remove(action);
            }
        }

        public void RemoveEventListener(IProxyLookupEventListener listener)
        {
            this.listeners.Remove(listener);
        }
    }
}

