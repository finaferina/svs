﻿
using Treemas.Base.Lookup;

namespace Treemas.Base.Web.Platform
{
    public interface ISSOSessionStorage
    {
        void Delete(string id);
        ILookup Load(string id);
        void Save(string id, ILookup lookup);        
    }
}
