﻿
namespace Treemas.Base.Web.Platform
{
    public class DeploymentSettings
    {
        public DeploymentSettings()
        {
            this.Context = new DeploymentContextSettings();
        }

        public DeploymentContextSettings Context { get; set; }

        public string HomeFolderLocation { get; set; }
    }
}
