﻿using System;
using Treemas.Base.Lookup;

namespace Treemas.Base.Web.Platform
{
    [Serializable]
    internal class SSOSessionPersistence
    {
        public ILookup Data { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
    }
}
