﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Treemas.Base.Lookup;
using Treemas.Base.Utilities;

namespace Treemas.Base.Web.Platform
{
    public static class PlatformExtensions
    {
        public static IEnumerable<KeyValuePair<string, object>> ExtractKeyValueList(this object obj, string propName)
        {
            if (obj != null)
            {
                PropertyInfo property = obj.GetType().GetProperty(propName);
                if (property != null)
                {
                    foreach (KeyValuePair<string, object> iteratorVariable1 in (List<KeyValuePair<string, object>>)property.GetValue(obj, null))
                    {
                        yield return iteratorVariable1;
                    }
                }
            }
        }

        public static T Get<T>(this ViewDataDictionary viewData, string key)
        {
            if ((!viewData.IsNull() && !key.IsNullOrEmpty()) && viewData.ContainsKey(key))
            {
                return (T)viewData[key];
            }
            return default(T);
        }

        public static bool IsNullOrEmpty(this MvcHtmlString str)
        {
            if (!str.IsNull())
            {
                return string.IsNullOrEmpty(str.ToString());
            }
            return true;
        }

        public static ILookup Lookup(this HttpSessionStateBase session)
        {
            if (!session.IsNull())
            {
                return (ILookup)session["__tms_Lookup__"];
            }
            return null;
        }

        public static MvcHtmlString PrintProperty(this object obj, string propName)
        {
            string str;
            return obj.PrintProperty(propName, out str);
        }

        public static MvcHtmlString PrintProperty(this object obj, string propName, out string value)
        {
            string str = string.Empty;
            value = string.Empty;
            if (obj != null)
            {
                PropertyInfo property = obj.GetType().GetProperty(propName);
                if (property != null)
                {
                    value = Convert.ToString(property.GetValue(obj, null));
                    str = value;
                }
            }
            return new MvcHtmlString(str);
        }

        public static MvcHtmlString ToHtmlAttribute(this object obj, string propName) =>
            obj.ToHtmlAttribute(propName, false);

        public static MvcHtmlString ToHtmlAttribute(this object obj, string propName, bool autoGenerate)
        {
            string str;
            return obj.ToHtmlAttribute(propName, autoGenerate, out str);
        }

        public static MvcHtmlString ToHtmlAttribute(this object obj, string propName, bool autoGenerate, out string value)
        {
            string str = string.Empty;
            value = string.Empty;
            if (obj != null)
            {
                PropertyInfo property = obj.GetType().GetProperty(propName);
                if (property != null)
                {
                    value = Convert.ToString(property.GetValue(obj, null));
                    str = $"{property.Name}= {value}";
                    //str = $"{property.Name}="{ value}"";
                }
                else if (autoGenerate)
                {
                    value = Guid.NewGuid().ToString();
                    str = $"{propName}= {value}";
                    //str = $"{propName}="{ value}"";
                }
            }
            else
            {
                value = Guid.NewGuid().ToString();
                str = $"{propName}= {value}";
                //str = $"{propName}="{ value}"";
            }
            return new MvcHtmlString(str);
        }

        public static MvcHtmlString ToJsMapPair(this object obj, string propName) =>
            obj.ToJsMapPair(propName, true);

        public static MvcHtmlString ToJsMapPair(this object obj, string propName, bool asString) =>
            obj.ToJsMapPair(propName, asString, null);

        public static MvcHtmlString ToJsMapPair(this object obj, string propName, bool asString, string defaultValue)
        {
            string str;
            return obj.ToJsMapPair(propName, asString, defaultValue, out str);
        }

        public static MvcHtmlString ToJsMapPair(this object obj, string propName, bool asString, string defaultValue, out string value)
        {
            string str = string.Empty;
            value = string.Empty;
            if (obj != null)
            {
                PropertyInfo property = obj.GetType().GetProperty(propName);
                if (property != null)
                {
                    value = Convert.ToString(property.GetValue(obj, null));
                }
                else if (defaultValue != null)
                {
                    value = defaultValue;
                }
                if ((property != null) || (defaultValue != null))
                {
                    if (!asString)
                    {
                        str = $"{propName}: {value}";
                    }
                    else
                    {
                        str = $"{propName}: {value}";
                    }
                }
            }
            return new MvcHtmlString(str);
        }

        public static MvcHtmlString ToMvcHtmlString(this string str) =>
            str.ToMvcHtmlString(string.Empty);

        public static MvcHtmlString ToMvcHtmlString(this string str, string fallbackString) =>
            new MvcHtmlString((str != null) ? str : fallbackString);

        public static void Write(this HtmlHelper helper, string str)
        {
            helper.Write(new MvcHtmlString(str));
        }

        public static void Write(this HtmlHelper helper, MvcHtmlString str)
        {
            if (!helper.IsNull())
            {
                helper.ViewContext.Writer.Write(str);
            }
        }

        public static void WriteLine(this HtmlHelper helper, string str)
        {
            helper.WriteLine(new MvcHtmlString(str));
        }

        public static void WriteLine(this HtmlHelper helper, MvcHtmlString str)
        {
            if (!helper.IsNull())
            {
                helper.ViewContext.Writer.WriteLine(str);
            }
        }
    }
}
