﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treemas.Base.Web.Platform
{
    public class BrowserSettings
    {
        public BrowserSettings()
        {
            this.BlockedAgents = new List<BrowserAgent>();
        }

        public IList<BrowserAgent> BlockedAgents { get; private set; }

        public string BlockingControllerName { get; set; }

        public bool EnableRestriction { get; set; }
    }
}
