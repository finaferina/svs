﻿using Treemas.Base.Configuration;

namespace Treemas.Base.Web.Platform
{
    public class DataUploadSettings
    {
        public string ValidationResultPath
        {
            get
            {
                IConfigurationBinder binder = ApplicationConfigurationCabinet.Instance.GetBinder(ApplicationConstants.Instance.DataUploadName);
                ConfigurationItem configuration = binder.GetConfiguration(ApplicationConstants.Instance.DataUploadRootFolder);
                ConfigurationItem item2 = binder.GetConfiguration(ApplicationConstants.Instance.DataUploadValidationResultFolder);
                return (SystemSettings.Instance.Deployment.HomeFolderLocation + @"\" + configuration.Value + @"\" + item2.Value);
            }
        }
    }
}
