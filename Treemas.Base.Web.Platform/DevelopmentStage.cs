﻿namespace Treemas.Base.Web.Platform
{
    public class DevelopmentStage
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
