﻿namespace Treemas.Base.Web.Platform
{
    public class AuthorizationRuleState
    {
        public bool IsAuthorized { get; set; }
        public bool IsValid { get; set; }
    }
}
