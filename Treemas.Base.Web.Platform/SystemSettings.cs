﻿using System;

namespace Treemas.Base.Web.Platform
{
    public class SystemSettings
    {
        private static readonly SystemSettings instance = new SystemSettings();

        private SystemSettings()
        {
            this.Deployment = new DeploymentSettings();
            this.Security = new SecuritySettings();
            this.Development = new DevelopmentSettings();
            this.Menu = new MenuSettings();
            this.Logging = new LogSettings();
            this.Runtime = new RuntimeSettings();
            this.DataUpload = new DataUploadSettings();
            this.UOBISetting = new UOBIEncryptionSetting();

            this.Name = "Treemas Framework";
            this.Alias = "TMS";
            this.OwnerName = "Treemas Solusi Utama";
            this.OwnerAlias = "Treemas";
        }

        public string Alias { get; set; }
        public LogSettings Logging { get; private set; }
        public DataUploadSettings DataUpload { get; private set; }
        public DevelopmentSettings Development { get; private set; }
        public DeploymentSettings Deployment { get; private set; }
        public MenuSettings Menu { get; private set; }
        public static SystemSettings Instance =>
            instance;
        public string Name { get; set; }
        public string OwnerAlias { get; set; }
        public string OwnerName { get; set; }
        public RuntimeSettings Runtime { get; private set; }
        public SecuritySettings Security { get;  private set; }
        public UOBIEncryptionSetting UOBISetting { get; private set; }
    }

}
