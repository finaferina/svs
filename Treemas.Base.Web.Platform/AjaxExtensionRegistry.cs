﻿using System;
using System.Collections.Generic;

namespace Treemas.Base.Web.Platform
{
    public class AjaxExtensionRegistry
    {
        private IDictionary<string, IAjaxExtension> extensions = new Dictionary<string, IAjaxExtension>();
        private static readonly AjaxExtensionRegistry instance = new AjaxExtensionRegistry();

        private AjaxExtensionRegistry()
        {
        }

        public void Add(IAjaxExtension extension)
        {
            string name = extension.GetName();
            if (!string.IsNullOrEmpty(name))
            {
                if (this.extensions.ContainsKey(name))
                {
                    this.extensions[name] = extension;
                }
                else
                {
                    this.extensions.Add(name, extension);
                }
            }
        }

        public IAjaxExtension Get(string name)
        {
            if (this.extensions.ContainsKey(name))
            {
                return this.extensions[name];
            }
            return null;
        }

        public bool HasExtension(string name) =>
            this.extensions.ContainsKey(name);

        public void Remove(string name)
        {
            if (this.extensions.ContainsKey(name))
            {
                this.extensions.Remove(name);
            }
        }

        public void Remove(IAjaxExtension extension)
        {
            this.Remove(extension.GetName());
        }

        public static AjaxExtensionRegistry Instance =>
            instance;
    }
}
