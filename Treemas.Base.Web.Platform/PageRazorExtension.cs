﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;
using System.Web.Mvc;
using Treemas.Credential.Model;
using Treemas.Base.Utilities;


namespace Treemas.Base.Web.Platform
{
    public class PageRazorExtension : BaseRazorExtension
    {

        public MvcHtmlString BaseUrl => new MvcHtmlString((string)this.Helper.ViewData["_tmsBaseUrl"]);
        public MvcHtmlString ControllerName => new MvcHtmlString((string)this.Helper.ViewData["_tmsControllerName"]);
        public MvcHtmlString ModuleName => new MvcHtmlString((string)this.Helper.ViewData["_tmsModuleName"]);
        public IDictionary<string, string> ForwardedParameters => ((IDictionary<string, string>)this.Helper.ViewData["_tmsForwardedParameters"]);
        public MvcHtmlString HomeUrl => new MvcHtmlString(this.BaseUrl + "/" + SystemSettings.Instance.Runtime.HomeController);
        public MvcHtmlString PortalUrl = new MvcHtmlString(SystemSettings.Instance.Runtime.PortalUrl + "/" + SystemSettings.Instance.Runtime.SwitchController);
        public MvcHtmlString LoginActionUrl => new MvcHtmlString(string.Concat(new object[] { this.BaseUrl, "/", SystemSettings.Instance.Security.LoginController, "/Index" }));
        public MvcHtmlString AjaxLoginActionUrl => new MvcHtmlString(string.Concat(new object[] { this.BaseUrl, "/", SystemSettings.Instance.Security.LoginController, "/AjaxLogin" }));
        public MvcHtmlString LoginUrl => new MvcHtmlString(this.BaseUrl + "/" + SystemSettings.Instance.Security.LoginController);
        public MvcHtmlString LogoutActionUrl => new MvcHtmlString(string.Concat(new object[] { this.BaseUrl, "/", SystemSettings.Instance.Security.LoginController, "/Logout" }));
        public MvcHtmlString Title => new MvcHtmlString((string)this.Helper.ViewData["_tmsScreenTitle"]);
        public MvcHtmlString SourcePage => new MvcHtmlString(this.ForwardedParameters.ContainsKey("tmsSrcPage") ? this.ForwardedParameters["tmsSrcPage"] : string.Empty);
        public MvcHtmlString SourcePageUrl
        {
            get
            {
                string homeController = this.SourcePage.ToString();
                if (string.IsNullOrEmpty(homeController))
                {
                    homeController = SystemSettings.Instance.Runtime.HomeController;
                }
                return new MvcHtmlString(this.BaseUrl + "/" + homeController);
            }
        }
        
        public Treemas.Credential.Model.User User
        {
            get
            {
                Treemas.Credential.Model.User user = (Treemas.Credential.Model.User)this.Helper.ViewData["_tmsAuthenticatedUser"];
                if (user != null)
                {
                    return user;
                }
                return null;
            }
        }
        public MvcHtmlString ScreenID => new MvcHtmlString((string)this.Helper.ViewData["_tmsScreenID"]);

        public IList<ScreenMessage> ScreenMessages
        {
            get
            {
                object obj2 = this.Helper.ViewData["_tmsScreenMessages"];
                if (!obj2.IsNull())
                {
                    return (IList<ScreenMessage>)obj2;
                }
                return null;
            }
        }

        public MvcHtmlString Forward(string controllerName) =>
           this.Forward(controllerName, delegate (IDictionary<string, object> param) {
           });

        public MvcHtmlString Forward(string controllerName, Action<IDictionary<string, object>> paramAction) =>
            this.Forward(controllerName, null, paramAction);

        public MvcHtmlString Forward(string controllerName, object routeValues) =>
            this.Forward(controllerName, null, routeValues);

        public MvcHtmlString Forward(string controllerName, string actionName) =>
            this.Forward(controllerName, actionName, delegate (IDictionary<string, object> param) {
            });

        public MvcHtmlString Forward(string controllerName, string actionName, Action<IDictionary<string, object>> paramAction)
        {
            IDictionary<string, object> dictionary = new Dictionary<string, object>();
            paramAction(dictionary);
            dictionary.Add("orig", this.ControllerName);
            StringBuilder builder = new StringBuilder();
            foreach (string str in dictionary.Keys)
            {
                builder.Append($"_p{str}={Convert.ToString(dictionary[str])}&");
            }
            builder.Remove(builder.Length - 1, 1);
            if (!(string.IsNullOrEmpty(controllerName) || string.IsNullOrEmpty(actionName)))
            {
                builder.Insert(0, this.GetActionUrl(controllerName, actionName));
            }
            else if (!string.IsNullOrEmpty(controllerName))
            {
                builder.Insert(0, $"{this.GetControllerUrl(controllerName)}?");
            }
            return new MvcHtmlString(builder.ToString());
        }

        public MvcHtmlString Forward(string controllerName, string actionName, object routeValues)
        {
            string str = new { _porig = this.ControllerName }.ToUrlQueryString(null);
            str = str.Substring(1, str.Length - 1);
            str = '&' + str;
            if (!string.IsNullOrEmpty(actionName))
            {
                return new MvcHtmlString(routeValues.ToUrlQueryString(string.Concat(new object[] { this.BaseUrl, "/", controllerName, "/", actionName })) + str);
            }
            return new MvcHtmlString(routeValues.ToUrlQueryString((this.BaseUrl + "/" + controllerName)) + str);
        }

        public MvcHtmlString ForwardToAction(string actionName, Action<IDictionary<string, object>> paramAction) =>
            this.Forward(this.ControllerName.ToString(), actionName, paramAction);

        public MvcHtmlString ForwardToAction(string actionName, object routeValues) =>
            this.Forward(this.ControllerName.ToString(), actionName, routeValues);

        public MvcHtmlString GetActionUrl(string actionName) =>
            new MvcHtmlString(this.ScreenUrl + "/" + actionName);

        public MvcHtmlString GetActionUrl(string actionName, object routeValues) =>
            new MvcHtmlString(routeValues.ToUrlQueryString(this.ScreenUrl + "/" + actionName));

        public MvcHtmlString GetActionUrl(string controllerName, string actionName, object routeValues) =>
            new MvcHtmlString(routeValues.ToUrlQueryString(string.Concat(new object[] { this.BaseUrl, "/", controllerName, "/", actionName })));

        public MvcHtmlString GetControllerUrl(string controllerName) =>
            this.GetControllerUrl(controllerName, null);

        public MvcHtmlString GetControllerUrl(string controllerName, object routeValues) =>
            new MvcHtmlString(routeValues.ToUrlQueryString(this.BaseUrl + "/" + controllerName));

        public string GetForwardedParameterValue(string key)
        {
            IDictionary<string, string> forwardedParameters = this.ForwardedParameters;
            if (forwardedParameters.ContainsKey(key))
            {
                return forwardedParameters[key];
            }
            return null;
        }

        public MvcHtmlString GetPageUrl() =>
            this.GetControllerUrl(this.ControllerName.ToString());

        public MvcHtmlString GetPageUrl(object routeValues) =>
            this.GetControllerUrl(this.ControllerName.ToString(), routeValues);

        public bool HasForwardedParameter(string key) =>
            this.ForwardedParameters.ContainsKey(key);
        public MvcHtmlString ScreenUrl =>
           new MvcHtmlString(this.BaseUrl + "/" + this.ControllerName);

        public MvcHtmlString SessionLockActionUrl =>
            new MvcHtmlString(string.Concat(new object[] { this.BaseUrl, "/", SystemSettings.Instance.Security.LoginController, "/Lock" }));

        public MvcHtmlString SessionUnlockActionUrl =>
            new MvcHtmlString(string.Concat(new object[] { this.BaseUrl, "/", SystemSettings.Instance.Security.LoginController, "/Unlock" }));

    }
}
