﻿
namespace Treemas.Base.Web.Platform
{
    public enum ScreenMessageSeverity
    {
        Info,
        Success,
        Warning,
        Error
    }
}
