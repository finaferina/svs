﻿namespace Treemas.Base.Web.Platform
{
    public class LogSettings
    {
        public LogSettings()
        {
            this.LoggingPoolName = "Logging-Pool";
        }

        public bool Enabled { get; set; }

        public string FolderLocation { get; set; }

        public string LoggingPoolName { get; set; }
    }
}
