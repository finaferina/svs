﻿using System.Web.Mvc;

namespace Treemas.Base.Web.Platform
{
    public class PlatformUtilities : BaseRazorExtension
    {
        private HtmlHelper _helper;
        private string _InitializationCache { get; set; }
        public PlatformUtilities()
        {
            this.Page = new PageRazorExtension();
            this.Authorization = new AuthorizationRazorExtension();
            this.Application = new ApplicationRazorExtension();
        }
        public PageRazorExtension Page { get; private set; }
        public ApplicationRazorExtension Application { get; private set; }
        public AuthorizationRazorExtension Authorization { get; private set; }
        public override HtmlHelper Helper
        {
            get { return this._helper; }
            set
            {
                this._helper = value;
                this.Page.Helper = value;
                this.Authorization.Helper = value;
                this.Application.Helper = value;
            }
        }
    }
}
