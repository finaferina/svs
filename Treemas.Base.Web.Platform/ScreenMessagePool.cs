﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.Base.Web.Platform
{
    [Serializable]
    public class ScreenMessagePool : IClearable
    {
        private List<ScreenMessage> messageList = new List<ScreenMessage>();

        public void Clear(object param)
        {
            this.messageList.Clear();
        }

        public IList<ScreenMessage> Pull()
        {
            List<ScreenMessage> list = new List<ScreenMessage>(this.messageList);
            this.Clear(null);
            return list;
        }

        public void Remove(ScreenMessage message)
        {
            this.messageList.Remove(message);
        }

        public void Submit(params ScreenMessage[] message)
        {
            if (!message.IsNullOrEmpty<ScreenMessage>())
            {
                foreach (ScreenMessage message2 in message)
                {
                    if (!this.messageList.Contains(message2))
                    {
                        this.messageList.Add(message2);
                    }
                }
            }
        }
    }
}
