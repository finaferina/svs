﻿using System.Collections.Generic;

namespace Treemas.Base.Web.Platform
{
    public interface ILocalizedWordCollection
    {
        IDictionary<string, string> GetAll();
        string GetDefaultCode();
        IList<string> GetKeys();
        void SetDefaultCode(string code);
        string Translate(string key);
    }
}
