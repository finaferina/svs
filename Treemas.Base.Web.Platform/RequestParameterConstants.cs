﻿
namespace Treemas.Base.Web.Platform
{
    public class RequestParameterConstants
    {
        public string Name =>
            "name";

        public string Select =>
            "select";

        public string Value =>
            "value";
    }
}
