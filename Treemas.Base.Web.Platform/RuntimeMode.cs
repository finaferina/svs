﻿
namespace Treemas.Base.Web.Platform
{
    public enum RuntimeMode
    {
        Online,
        Offline,
        Maintenance
    }
}
