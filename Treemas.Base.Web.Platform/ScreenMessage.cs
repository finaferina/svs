﻿using System;


namespace Treemas.Base.Web.Platform
{
    [Serializable]
    public class ScreenMessage
    {
        public ScreenMessage() : this(string.Empty, string.Empty, ScreenMessageSeverity.Info, string.Empty)
        {
        }

        public ScreenMessage(string name) : this(name, string.Empty, ScreenMessageSeverity.Info, string.Empty)
        {
        }

        public ScreenMessage(string name, string text) : this(name, text, ScreenMessageSeverity.Info, string.Empty)
        {
        }

        public ScreenMessage(ScreenMessageSeverity severity, string text) : this(Guid.NewGuid().ToString(), text, severity, string.Empty )
        {
        }

        public ScreenMessage(string name, string text, ScreenMessageSeverity severity, string skin)
        {
            this.Name = name;
            this.Text = text;
            this.Severity = severity;
            this.Skin = (skin.Equals(string.Empty )) ? "info" : skin ;
        }

        public static ScreenMessage Error(string text) =>
            new ScreenMessage(Guid.NewGuid().ToString(), text, ScreenMessageSeverity.Error, "danger");

        public static ScreenMessage Info(string text) =>
            new ScreenMessage(Guid.NewGuid().ToString(), text, ScreenMessageSeverity.Info, "info");

        public static ScreenMessage Success(string text) =>
            new ScreenMessage(Guid.NewGuid().ToString(), text, ScreenMessageSeverity.Success, "primary");

        public static ScreenMessage Warning(string text) =>
            new ScreenMessage(Guid.NewGuid().ToString(), text, ScreenMessageSeverity.Warning, "warning");

        public string Name { get; set; }

        public ScreenMessageSeverity Severity { get; set; }

        public string Text { get; set; }

        public string Skin { get; set; }
    }
}
