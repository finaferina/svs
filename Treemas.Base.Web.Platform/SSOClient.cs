﻿using System;
using Treemas.Base.Utilities;
using Treemas.Base.Web.Service;

namespace Treemas.Base.Web.Platform
{
    public class SSOClient : IDisposable
    {
        private static SSOClient instance;
        private SSOClient()
        {
            this.ServiceFactory = new ServiceClientFactory<IWebService>(SystemSettings.Instance.Security.SSOServiceUrl);
        }

        public void Dispose()
        {
            if (this.ServiceFactory != null)
            {
                this.ServiceFactory.Dispose();
            }
        }

        public static SSOClient Instance
        {
            get
            {
                if (instance.IsNull())
                {
                    instance = new SSOClient();
                }
                return instance;
            }
        }

        private ServiceClientFactory<IWebService> ServiceFactory { get; set; }

        public string GetLoggedInUser(string id)
        {
            ServiceParameter parameter = new ServiceParameter().Define<ServiceParameter>(delegate (ServiceParameter p)
            {
                p.Command = "GetLoggedInUser";
                p.Parameters.Add<string>("id", id);
            });
            IWebService service = this.ServiceFactory.Create();
            ServiceRuntimeResult result = service.Execute(parameter.ToRuntime());
            if (!result.IsNull())
            {
                ServiceResult result2 = ServiceResult.Create(result);
                if (!((result2.IsNull() || (result2.Status != ServiceStatus.Success)) || result2.Data.IsNull()))
                {
                    return result2.Data.Get<string>("username");
                }
            }
            service.Dispose();
            return null;
        }

        public bool IsAlive(string id)
        {
            bool flag = false;
            ServiceParameter parameter = new ServiceParameter().Define<ServiceParameter>(delegate (ServiceParameter p) {
                p.Command = "IsAlive";
                p.Parameters.Add<string>("id", id);
            });
            IWebService service = this.ServiceFactory.Create();
            ServiceRuntimeResult result = service.Execute(parameter.ToRuntime());
            if (!result.IsNull())
            {
                ServiceResult result2 = ServiceResult.Create(result);
                flag = !result2.IsNull() && (result2.Status == ServiceStatus.Confirmed);
            }
            service.Dispose();
            return flag;
        }

        public bool IsLocked(string id)
        {
            bool flag = false;
            ServiceParameter parameter = new ServiceParameter().Define<ServiceParameter>(delegate (ServiceParameter p) {
                p.Command = "IsLocked";
                p.Parameters.Add<string>("id", id);
            });
            IWebService service = this.ServiceFactory.Create();
            ServiceRuntimeResult result = service.Execute(parameter.ToRuntime());
            if (!result.IsNull())
            {
                ServiceResult result2 = ServiceResult.Create(result);
                if (!((result2.IsNull() || (result2.Status != ServiceStatus.Success)) || result2.Data.IsNull()))
                {
                    flag = result2.Data.Get<bool>("is_locked");
                }
            }
            service.Dispose();
            return flag;
        }

        public bool IsUserAuthentic(string username, string password)
        {
            ServiceParameter parameter = new ServiceParameter().Define<ServiceParameter>(delegate (ServiceParameter p)
            {
                p.Command = "IsUserAuthentic";
                p.Parameters.Add<string>("username", username);
                p.Parameters.Add<string>("password", password);
            });
            IWebService service = this.ServiceFactory.Create();
            ServiceRuntimeResult result = service.Execute(parameter.ToRuntime());
            if (!result.IsNull())
            {
                ServiceResult result2 = ServiceResult.Create(result);
                if (!result2.IsNull() && (result2.Status == ServiceStatus.Success))
                {
                    return result2.Data.Get<bool>("IsUserAuthentic");
                }
            }
            service.Dispose();
            return false;
        }

        public bool IsUserLocked(string username, string password)
        {
            ServiceParameter parameter = new ServiceParameter().Define<ServiceParameter>(delegate (ServiceParameter p)
            {
                p.Command = "IsUserLocked";
                p.Parameters.Add<string>("username", username);
                p.Parameters.Add<string>("password", password);
            });
            IWebService service = this.ServiceFactory.Create();
            ServiceRuntimeResult result = service.Execute(parameter.ToRuntime());
            if (!result.IsNull())
            {
                ServiceResult result2 = ServiceResult.Create(result);
                if (!result2.IsNull() && (result2.Status == ServiceStatus.Success))
                {
                    return result2.Data.Get<bool>("IsUserLocked");
                }
            }
            service.Dispose();
            return false;
        }

        public string IsLoggedIn(string username, string hostname, string hostIP, string browser, string browserVersion, bool isMobile)
        {
            string str = null;
            ServiceParameter parameter = new ServiceParameter().Define<ServiceParameter>(delegate (ServiceParameter p) {
                p.Command = "IsLoggedIn";
                p.Parameters.Add<string>("username", username);
                p.Parameters.Add<string>("hostname", hostname);
                p.Parameters.Add<string>("host_ip", hostIP);
                p.Parameters.Add<string>("browser", browser);
                p.Parameters.Add<string>("browser_version", browserVersion);
                p.Parameters.Add<bool>("is_mobile", isMobile);
            });
            IWebService service = this.ServiceFactory.Create();
            ServiceRuntimeResult result = service.Execute(parameter.ToRuntime());
            if (!result.IsNull())
            {
                ServiceResult result2 = ServiceResult.Create(result);
                if (!((result2.IsNull() || (result2.Status != ServiceStatus.Confirmed)) || result2.Data.IsNull()))
                {
                    str = result2.Data.Get<string>("id");
                }
            }
            service.Dispose();
            return str;
        }

        public bool Lock(string id)
        {
            bool flag = false;
            ServiceParameter parameter = new ServiceParameter().Define<ServiceParameter>(delegate (ServiceParameter p) {
                p.Command = "Lock";
                p.Parameters.Add<string>("id", id);
            });
            IWebService service = this.ServiceFactory.Create();
            ServiceRuntimeResult result = service.Execute(parameter.ToRuntime());
            if (!result.IsNull())
            {
                ServiceResult result2 = ServiceResult.Create(result);
                flag = !result2.IsNull() && (result2.Status == ServiceStatus.Success);
            }
            service.Dispose();
            return flag;
        }

        public string Login(string username, string password, string hostname, string hostIP, string browser, string browserVersion, bool isMobile)
        {
            string str = null;
            ServiceParameter parameter = new ServiceParameter().Define<ServiceParameter>(delegate (ServiceParameter p) {
                p.Command = "Login";
                p.Parameters.Add<string>("username", username);
                p.Parameters.Add<string>("password", password);
                p.Parameters.Add<string>("hostname", hostname);
                p.Parameters.Add<string>("host_ip", hostIP);
                p.Parameters.Add<string>("browser", browser);
                p.Parameters.Add<string>("browser_version", browserVersion);
                p.Parameters.Add<bool>("is_mobile", isMobile);
            });
            IWebService service = this.ServiceFactory.Create();
            ServiceRuntimeResult result = service.Execute(parameter.ToRuntime());
            if (!result.IsNull())
            {
                ServiceResult result2 = ServiceResult.Create(result);
                if (!((result2.IsNull() || (result2.Status != ServiceStatus.Success)) || result2.Data.IsNull()))
                {
                    str = result2.Data.Get<string>("id");
                }
            }
            service.Dispose();
            return str;
        }

        public bool Logout(string id)
        {
            bool flag = false;
            ServiceParameter parameter = new ServiceParameter().Define<ServiceParameter>(delegate (ServiceParameter p) {
                p.Command = "Logout";
                p.Parameters.Add<string>("id", id);
            });
            IWebService service = this.ServiceFactory.Create();
            ServiceRuntimeResult result = service.Execute(parameter.ToRuntime());
            if (!result.IsNull())
            {
                ServiceResult result2 = ServiceResult.Create(result);
                flag = !result2.IsNull() && (result2.Status == ServiceStatus.Success);
            }
            service.Dispose();
            return flag;
        }

        public bool MarkActive(string id)
        {
            bool flag = false;
            ServiceParameter parameter = new ServiceParameter().Define<ServiceParameter>(delegate (ServiceParameter p) {
                p.Command = "MarkActive";
                p.Parameters.Add<string>("id", id);
            });
            IWebService service = this.ServiceFactory.Create();
            ServiceRuntimeResult result = service.Execute(parameter.ToRuntime());
            if (!result.IsNull())
            {
                ServiceResult result2 = ServiceResult.Create(result);
                flag = !result2.IsNull() && (result2.Status == ServiceStatus.Success);
            }
            service.Dispose();
            return flag;
        }

        public bool Unlock(string id)
        {
            bool flag = false;
            ServiceParameter parameter = new ServiceParameter().Define<ServiceParameter>(delegate (ServiceParameter p) {
                p.Command = "Unlock";
                p.Parameters.Add<string>("id", id);
            });
            IWebService service = this.ServiceFactory.Create();
            ServiceRuntimeResult result = service.Execute(parameter.ToRuntime());
            if (!result.IsNull())
            {
                ServiceResult result2 = ServiceResult.Create(result);
                flag = !result2.IsNull() && (result2.Status == ServiceStatus.Success);
            }
            service.Dispose();
            return flag;
        }

    }
}
