﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Repository
{
    public class FunctionFeatureRepository : RepositoryController<FunctionFeature>, IFunctionFeatureRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public FunctionFeatureRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public FunctionFeature getFunctionFeature(long id)
        {
            return transact(() => session.QueryOver<FunctionFeature>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }
        
        public IList<FunctionFeature> getFunctionFeatures(long functionid)
        {
            return transact(() => session.QueryOver<FunctionFeature>()
                                                .Where(f => f.FunctionId == functionid).List());
        }

        public int deleteSelected(long functionId, long featureId)
        {
            string hqlSyntax = "delete FunctionFeature where FunctionId = :FunctionId and FeatureId = :FeatureId";
            return session.CreateQuery(hqlSyntax)
                    .SetInt64("FunctionId", functionId)
                    .SetInt64("FeatureId", featureId)
                    .ExecuteUpdate();
        }


        public PagedResult<FunctionFeature> FindAllPagedbyFunction(long functionId, int pageNumber, string orderColumn, int itemsPerPage, string orderKey)
        {

            PagedResult<FunctionFeature> paged = new PagedResult<FunctionFeature>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<FunctionFeature>()
                                 .Where(x => x.FunctionId==functionId)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<FunctionFeature>()
                                 .Where(x => x.FunctionId == functionId)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            paged.TotalItems = transact(() =>
                          session.QueryOver<FunctionFeature>()
                                 .Where(x => x.FunctionId == functionId)
                                .RowCount());

            return paged;
        }

        public FunctionFeature getFunctionFeature(long functionid, long featureid)
        {
            return transact(() => session.QueryOver<FunctionFeature>()
                                                .Where(f => f.FunctionId == functionid && f.FeatureId == featureid).SingleOrDefault());
        }

        public PagedResult<FunctionFeature> FindAllPagedbyFunction(long functionId, int pageNumber, string orderColumn, int itemsPerPage, string orderKey, string searchColumn, string searchValue)
        {
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            PagedResult<FunctionFeature> paged = new PagedResult<FunctionFeature>(pageNumber, itemsPerPage);

            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<FunctionFeature>()
                                 .Where(x => x.FunctionId == functionId)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<FunctionFeature>()
                                 .Where(x => x.FunctionId == functionId)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            paged.TotalItems = transact(() =>
                          session.QueryOver<FunctionFeature>()
                                 .Where(x => x.FunctionId == functionId)
                                .RowCount());

            return paged;
        }
    }
}
