﻿using System;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Collections.Generic;
using NHibernate.Criterion;
using LinqSpecs;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.Base.Globals;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;

namespace Treemas.Credential.Repository
{
    public class UserCrRepository : RepositoryController<UserCr>, IUserCrRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public UserCrRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public void Delete(int id)
        {
            Remove(new UserCr(id));
        }
        public UserCr GetUser(string username)
        {
            UserCr _user = this.FindOne(new AdHocSpecification<UserCr>(s => s.Username == username && s.Approved == false));
            return _user;
        }
        public UserCr GetUser(string username, ApprovalType type)
        {
            UserCr _user = this.FindOne(new AdHocSpecification<UserCr>(s => s.Username == username && s.Approved == false && s.ApprovalType == type.ToString()));
            return _user;
        }
        public UserCr GetUserforAdmin(string username, ApprovalType type)
        {
            UserCr _user = this.FindOne(new AdHocSpecification<UserCr>(s => s.Username == username && s.Approved == true && s.ApprovalType == type.ToString()));
            return _user;
        }
        public UserCr GetUserById(long id)
        {
            return transact(() => session.QueryOver<UserCr>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }
        public UserCr GetUserForPrint(long id)
        {
            return transact(() => session.QueryOver<UserCr>()
                                                .Where(f => f.Id == id && f.IsPrinted == false).SingleOrDefault());
        }
        public IList<UserCr> GetUsers()
        {
            return null;
        }
        public PagedResult<UserCr> GetUsers(int pageNumber, int itemsPerPage)
        {
            return null;
        }
        public IList<UserCr> SearchLikeName(string key)
        {
            return null;
        }
        public PagedResult<UserCr> SearchLikeName(string key, int pageNumber, int itemsPerPage)
        {
            return null;
        }
        public long GetUserCount()
        {
            long num = 0L;
            num = Count;
            return num;
        }

        public bool IsDuplicate(string username)
        {
            int rowCount = transact(() => session.QueryOver<UserCr>()
                                                .Where(f => f.Username == username).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }


        public IList<UserCr> FindExport(AuditTrailFilter filter)
        {
            SimpleExpression dateStart = Restrictions.Ge("CreatedDate", filter.ActionDateStart);
            SimpleExpression dateEnd = Restrictions.Le("CreatedDate", filter.ActionDateEnd);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.ActionDateStart.IsNull()) conjuction.Add(dateStart);
            if (!filter.ActionDateEnd.IsNull()) conjuction.Add(dateEnd);
            SimpleExpression specification;

            IList<UserCr> items = new List<UserCr>();
            if (filter.searchValue != "")
            {
                specification = Restrictions.Like(filter.searchColumn, filter.searchValue, MatchMode.Anywhere);
                items = transact(() => session.QueryOver<UserCr>()
                                                        .Where(specification)
                                                        .And(conjuction).List());
            }
            else
            {
                items = transact(() => session.QueryOver<UserCr>()
                                                        .Where(conjuction).List());
            }


            return items;
        }

        public PagedResult<UserCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, AuditTrailFilter filter)
        {
            PagedResult<UserCr> paged = new PagedResult<UserCr>(pageNumber, itemsPerPage);
            SimpleExpression dateStart = Restrictions.Ge("CreatedDate", filter.ActionDateStart);
            SimpleExpression dateEnd = Restrictions.Le("CreatedDate", filter.ActionDateEnd);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.ActionDateStart.IsNull()) conjuction.Add(dateStart);
            if (!filter.ActionDateEnd.IsNull()) conjuction.Add(dateEnd);

            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<UserCr>()
                                                    .Where(conjuction)
                                                    .OrderBy(Projections.Property(orderColumn)).Asc
                                                    .Skip((pageNumber) * itemsPerPage)
                                                    .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<UserCr>()
                                                     .Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                         session.QueryOver<UserCr>()
                                .Where(conjuction)
                                .RowCount());
            return paged;
        }

        public PagedResult<UserCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, AuditTrailFilter filter)
        {
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            SimpleExpression dateStart = Restrictions.Ge("CreatedDate", filter.ActionDateStart);
            SimpleExpression dateEnd = Restrictions.Le("CreatedDate", filter.ActionDateEnd);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.ActionDateStart.IsNull()) conjuction.Add(dateStart);
            if (!filter.ActionDateEnd.IsNull()) conjuction.Add(dateEnd);

            PagedResult<UserCr> paged = new PagedResult<UserCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<UserCr>()
                                 .Where(specification)
                                 .And(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<UserCr>()
                                 .Where(specification)
                                 .And(conjuction)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }


            paged.TotalItems = transact(() =>
                         session.QueryOver<UserCr>()
                                .Where(specification)
                                .And(conjuction)
                                .RowCount());
            return paged;
        }

        public PagedResult<UserCr> FindAllPagedDate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue)
        {

            PagedResult<UserCr> paged = new PagedResult<UserCr>(pageNumber, itemsPerPage);
            if (searchColumn.StringEqualsIgnoreCase("PasswordExpirationDate"))
            {
                if (orderKey.ToLower() == "asc")
                {
                    paged.Items = transact(() =>
                              session.QueryOver<UserCr>()
                                     .Where(f => f.PasswordExpirationDate == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() =>
                              session.QueryOver<UserCr>()
                                     .Where(f => f.PasswordExpirationDate == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                paged.TotalItems = transact(() =>
                        session.QueryOver<UserCr>()
                               .Where(f => f.PasswordExpirationDate == searchValue).RowCount());
            }
            else
            {
                if (orderKey.ToLower() == "asc")
                {
                    paged.Items = transact(() =>
                              session.QueryOver<UserCr>()
                                     .Where(f => f.AccountValidityDate == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() =>
                              session.QueryOver<UserCr>()
                                     .Where(f => f.AccountValidityDate == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                paged.TotalItems = transact(() =>
                        session.QueryOver<UserCr>()
                               .Where(f => f.AccountValidityDate == searchValue).RowCount());
            }
            return paged;
        }

        public void UpdateLoginAttempt(string username, int loginAttempt, bool active, DateTime? lastlogin)
        {
            if (loginAttempt == 0 && !lastlogin.IsNull())
            {
                string hqlSyntax = "Update UserCr Set LastLogin=:LastLogin, LoginAttempt = :LoginAttempt, IsActive = :IsActive Where UserName = :UserName";
                session.CreateQuery(hqlSyntax)
                        .SetInt32("LoginAttempt", loginAttempt)
                        .SetString("UserName", username)
                        .SetBoolean("IsActive", active)
                        .SetDateTime("LastLogin", DateTime.Now)
                        .ExecuteUpdate();
            }
            else
            {
                string hqlSyntax = "Update UserCr Set LoginAttempt = :LoginAttempt, IsActive = :IsActive Where UserName = :UserName";
                session.CreateQuery(hqlSyntax)
                        .SetInt32("LoginAttempt", loginAttempt)
                        .SetString("UserName", username)
                        .SetBoolean("IsActive", active)
                        .ExecuteUpdate();
            }
        }

        public void SetUserToInactive(string username)
        {
            string hqlSyntax = "Update UserCr Set IsActive = :IsActive, ChangedDate = :ChangedDate Where UserName = :UserName";
            session.CreateQuery(hqlSyntax)
                    .SetBoolean("IsActive", false)
                    .SetDateTime("ChangedDate", DateTime.Now)
                    .SetString("UserName", username)
                    .ExecuteUpdate();
        }

        public int deleteUserCr(string username, long id)
        {

            string hqlSyntax = "delete UserCr where UserName = :UserName and Id = :id";
            int deletedEntities = session.CreateQuery(hqlSyntax)
                    .SetString("UserName", username)
                    .SetInt64("id", id)
                    .ExecuteUpdate();

            return deletedEntities;
        }

        public int deleteAll(string username)
        {

            string hqlSyntax = "delete Authorization where Username = :Username";
            int deletedEntities = session.CreateQuery(hqlSyntax)
                    .SetString("Username", username)
                    .ExecuteUpdate();

            string hqlSyntaxUserApp = "delete UserApplication where Username = :Username";
            int deletedEntities2 = session.CreateQuery(hqlSyntaxUserApp)
                    .SetString("Username", username)
                    .ExecuteUpdate();

            return deletedEntities;
        }

        public void resetPassword(long id, string password)
        {
            string hqlSyntax = "update UserCr set Password = :password, LoginAttempt = 0, IsPrinted = false where Id = :id";
            session.CreateQuery(hqlSyntax)
                .SetString("password", Encryption.Instance.EncryptText(password))
                .SetInt64("id", id)
                .ExecuteUpdate();
        }

        public void resetPassword(long id, string password, DateTime expiredDate)
        {
            string hqlSyntax = "update UserCr set LastLogin=:LastLogin, Password = :password, PasswordExpirationDate = :PasswordExpirationDate , IsPrinted = false where Id = :id";
            session.CreateQuery(hqlSyntax)
                .SetString("password", Encryption.Instance.EncryptText(password))
                .SetDateTime("PasswordExpirationDate", expiredDate)
                .SetDateTime("LastLogin", DateTime.Now)
                .SetInt64("id", id)
                .ExecuteUpdate();
        }

        public IList<PasswordHistory> GetPasswordHistory(string username)
        {
            return transact(() => session.QueryOver<PasswordHistory>()
                                    .Where(f => f.Username == username)
                                    .OrderBy(x => x.Id).Asc
                                    .List());
        }

        public void SavePasswordHistory(PasswordHistory passwordHistory, PasswordHistory deletedHistory)
        {
            transact(() =>
            {
                session.Save(passwordHistory);
                if (!deletedHistory.IsNull())
                    session.Delete(deletedHistory);
            });
        }
        public void setApproved(long id, string approvedby, DateTime approveddate, string approvalstatus)
        {
            string hqlSyntax = "update UserCr set Approved = true, IsActive = 1, ApprovedBy = :approvedby, ApprovedDate = :approveddate, ApprovalStatus = :approvalstatus where Id = :id";
            session.CreateQuery(hqlSyntax)
                .SetInt64("id", id)
                .SetDateTime("approveddate", approveddate)
                .SetString("approvedby", approvedby)
                .SetString("approvalstatus", approvalstatus)
                .ExecuteUpdate();
        }
        public void setActive(long id, DateTime passexpdate, DateTime accvaldate)
        {
            string hqlSyntax = "update UserCr set IsActive = true, AccountValidityDate = :accvaldate, PasswordExpirationDate = :passexpdate, LoginAttempt = 0, LastLogin = null where Id = :id";
            session.CreateQuery(hqlSyntax)
                .SetInt64("id", id)
                .SetDateTime("passexpdate", passexpdate)
                .SetDateTime("accvaldate", accvaldate)
                .ExecuteUpdate();
        }
        public void setPrinted(long id)
        {
            string hqlSyntax = "update UserCr set IsPrinted = true where Id = :id";
            session.CreateQuery(hqlSyntax)
                .SetInt64("id", id)
                .ExecuteUpdate();
        }
        public void setUpdateMenu(long id, bool update)
        {
            string hqlSyntax = "update UserCr set UpdateMenu = :UpdateMenu where Id = :id";
            session.CreateQuery(hqlSyntax)
                .SetBoolean("UpdateMenu", update)
                .SetInt64("id", id)
                .ExecuteUpdate();
        }
        public void setUpdateMenu(string username, bool update)
        {
            string hqlSyntax = "update UserCr set UpdateMenu = :UpdateMenu where Username = :Username";
            session.CreateQuery(hqlSyntax)
                .SetBoolean("UpdateMenu", update)
                .SetString("Username", username)
                .ExecuteUpdate();
        }
        public void setUpdateMenu(bool update)
        {
            string hqlSyntax = "update UserCr set UpdateMenu = :UpdateMenu ";
            session.CreateQuery(hqlSyntax)
                .SetBoolean("UpdateMenu", update)
                .ExecuteUpdate();
        }
        public PagedResult<UserCr> FindAllPagedApproval(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string user)
        {
            if (orderColumn == "RolesNew")
            {
                orderColumn = "AuthorizedRoleIdsNew";
            }
            AbstractCriterion createdby;
            createdby = Restrictions.Not(Restrictions.Eq("CreatedBy", user));
            AbstractCriterion ChangedBy;
            ChangedBy = Restrictions.Not(Restrictions.Eq("ChangedBy", user));

            PagedResult<UserCr> paged = new PagedResult<UserCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<UserCr>()
                .Where(x => x.Approved == false)
                .And(createdby)
                .And(ChangedBy)
                .OrderBy(Projections.Property(orderColumn)).Asc
                .Skip((pageNumber) * itemsPerPage)
                .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<UserCr>()
                .Where(x => x.Approved == false)
                .And(createdby)
                .And(ChangedBy)
                .OrderBy(Projections.Property(orderColumn)).Desc
                .Skip((pageNumber) * itemsPerPage)
                .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                        session.QueryOver<UserCr>()
                               .Where(x => x.Approved == false)
                               .And(createdby)
                               .And(ChangedBy)
                               .RowCount());
            return paged;
        }

        public PagedResult<UserCr> FindAllPagedApproval(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string user)
        {

            if (orderColumn == "RolesNew")
            {
                orderColumn = "AuthorizedRoleIdsNew";
            }
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
            AbstractCriterion createdby;
            createdby = Restrictions.Not(Restrictions.Eq("CreatedBy", user));
            AbstractCriterion ChangedBy;
            ChangedBy = Restrictions.Not(Restrictions.Eq("ChangedBy", user));

            PagedResult<UserCr> paged = new PagedResult<UserCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<UserCr>()
                                 .Where(x => x.Approved == false)//.And(x => x.CreatedBy.Trim() != user.Trim()).And(x => x.ChangedBy.Trim() != user.Trim())
                                 .And(specification)
                                 .And(createdby)
                                 .And(ChangedBy)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<UserCr>()
                                 .Where(x => x.Approved == false)//.And(x => x.CreatedBy.Trim() != user.Trim()).And(x => x.ChangedBy.Trim() != user.Trim())
                                 .And(specification)
                                 .And(createdby)
                                 .And(ChangedBy)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }


            paged.TotalItems = transact(() =>
                         session.QueryOver<UserCr>()
                                .Where(x => x.Approved == false)//.And(x => x.CreatedBy.Trim() != user.Trim()).And(x => x.ChangedBy.Trim() != user.Trim())
                                .And(specification)
                                .And(createdby)
                                .And(ChangedBy)
                                .RowCount());
            return paged;
        }

        public PagedResult<UserCr> FindAllPagedDateApproval(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue, string user)
        {

            if (orderColumn == "RolesNew")
            {
                orderColumn = "AuthorizedRoleIdsNew";
            }

            AbstractCriterion createdby;
            createdby = Restrictions.Not(Restrictions.Eq("CreatedBy", user));
            AbstractCriterion ChangedBy;
            ChangedBy = Restrictions.Not(Restrictions.Eq("ChangedBy", user));

            PagedResult<UserCr> paged = new PagedResult<UserCr>(pageNumber, itemsPerPage);
            if (searchColumn.StringEqualsIgnoreCase("PasswordExpirationDate"))
            {
                if (orderKey.ToLower() == "asc")
                {
                    paged.Items = transact(() => session.QueryOver<UserCr>().Where(f => f.PasswordExpirationDate == searchValue).And(createdby).And(ChangedBy)
                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() => session.QueryOver<UserCr>().Where(f => f.PasswordExpirationDate == searchValue).And(createdby).And(ChangedBy)
                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                paged.TotalItems = transact(() =>session.QueryOver<UserCr>().Where(f => f.PasswordExpirationDate == searchValue).And(f => f.Approved == false).RowCount());
            }
            else
            {
                if (orderKey.ToLower() == "asc")
                {
                    paged.Items = transact(() => session.QueryOver<UserCr>().Where(f => f.AccountValidityDate == searchValue).And(createdby).And(ChangedBy)
                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() => session.QueryOver<UserCr>().Where(f => f.AccountValidityDate == searchValue).And(createdby).And(ChangedBy)
                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                paged.TotalItems = transact(() => session.QueryOver<UserCr>().Where(f => f.AccountValidityDate == searchValue).And(createdby).And(ChangedBy).RowCount());
            }
            return paged;
        }

        public IList<UserRole> getDistinctUserRoles(IList<UserCr> UserCr)
        {
            string hqlSyntax = "SELECT DISTINCT Username, Role FROM Authorization Where Username in (:usernames) ";
            var result = session.CreateQuery(hqlSyntax).SetParameterList("usernames", UserCr.Select<UserCr, string>(x => x.Username).ToArray()).List();

            List<UserRole> roles = new List<UserRole>();
            foreach (object[] item in result)
            {
                UserRole role = new UserRole();
                role.Username = item.GetValue(0).ToString().Trim();
                role.Role = item.GetValue(1).ToString().Trim();
                roles.Add(role);
            }

            return roles;
        }
    }
}
