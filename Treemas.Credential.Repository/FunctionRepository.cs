﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Repository
{
    public class FunctionRepository : RepositoryController<AuthorizationFunction>, IFunctionRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public FunctionRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public AuthorizationFunction getFunction(long id)
        {
            return transact(() => session.QueryOver<AuthorizationFunction>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }

        public IList<AuthorizationFunction> getFunctions(IList<string> ApplicationIds)
        {
            return transact(() => 
                   session.QueryOver<AuthorizationFunction>()
                          .WhereRestrictionOn(val => val.Application)
                          .IsIn(ApplicationIds.ToArray()).List());
        }
        public bool IsDuplicate(string application, string functionid)
        {
            int rowCount = transact(() => session.QueryOver<AuthorizationFunction>()
                                                .Where(f => f.FunctionId == functionid && f.Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<AuthorizationFunction> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<AuthorizationFunction> paged = new PagedResult<AuthorizationFunction>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<AuthorizationFunction>()
                                                     .OrderByDescending(GetOrderByExpression<AuthorizationFunction>(orderColumn)).ToList());
                                                     //.Skip((pageNumber) * itemsPerPage)
                                                     //.Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<AuthorizationFunction>()
                                                     .OrderBy(GetOrderByExpression<AuthorizationFunction>(orderColumn)).ToList());
                                                     //.Skip((pageNumber) * itemsPerPage)
                                                     //.Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<AuthorizationFunction> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            PagedResult<AuthorizationFunction> paged = new PagedResult<AuthorizationFunction>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<AuthorizationFunction>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 //.Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<AuthorizationFunction>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 //.Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }


            paged.TotalItems = transact(() =>
                         session.QueryOver<AuthorizationFunction>()
                                .Where(specification)
                                .RowCount());
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public AuthorizationFunction getFunction(string functionid)
        {
            return transact(() => session.QueryOver<AuthorizationFunction>()
                                                .Where(f => f.FunctionId == functionid).SingleOrDefault());
        }
    }
}
