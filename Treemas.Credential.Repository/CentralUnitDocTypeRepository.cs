﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;
using NHibernate.Criterion;

namespace Treemas.Credential.Repository
{
    public class CentralUnitDocTypeRepository : RepositoryController<CentralUnitDocType>, ICentralUnitDocTypeRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public CentralUnitDocTypeRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public CentralUnitDocType getCentralUnitDocType(long id)
        {
            return transact(() => session.QueryOver<CentralUnitDocType>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }
        public IList<CentralUnitDocType> getApplicationCentralUnitDocType(string application)
        {
            IEnumerable<CentralUnitDocType> _applicationCentralUnitDocType = this.FindAll(new AdHocSpecification<CentralUnitDocType>(s => s._Application == application));
            return _applicationCentralUnitDocType.ToList();
        }

        public bool IsDuplicate(string application, string Docid)
        {
            int rowCount = transact(() => session.QueryOver<CentralUnitDocType>()
                                                .Where(f => f.DocId == Docid && f._Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public bool IsDuplicateCR(string application, string Docid, string approval_status = "N")
        {
            int rowCount = transact(() => session.QueryOver<CentralUnitDocTypeCr>()
                                                .Where(f => f.DocId == Docid && f._Application == application && f.ApprovalStatus == approval_status).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public bool IsDuplicateByAlias(string application, string Alias)
        {
            int rowCount = transact(() => session.QueryOver<CentralUnitDocType>()
                                                .Where(f => f.Alias == Alias && f._Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public bool IsDuplicateCRByAlias(string application, string Alias, string approval_status)
        {
            int rowCount = transact(() => session.QueryOver<CentralUnitDocTypeCr>()
                                                .Where(f => f.Alias == Alias && f._Application == application && f.ApprovalStatus == approval_status).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<CentralUnitDocType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<CentralUnitDocType> paged = new PagedResult<CentralUnitDocType>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<CentralUnitDocType>()
                                                     .OrderByDescending(GetOrderByExpression<CentralUnitDocType>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<CentralUnitDocType>()
                                                     .OrderBy(GetOrderByExpression<CentralUnitDocType>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<CentralUnitDocType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            PagedResult<CentralUnitDocType> paged = new PagedResult<CentralUnitDocType>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<CentralUnitDocType>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<CentralUnitDocType>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }


            paged.TotalItems = transact(() =>
                         session.QueryOver<CentralUnitDocType>()
                                .Where(specification)
                                .RowCount());
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public CentralUnitDocType getCentralUnitDocType(string id)
        {
            return transact(() => session.QueryOver<CentralUnitDocType>()
                                                 .Where(f => f.DocId == id).SingleOrDefault());
        }

        public IList<CentralUnitDocType> FindForExport(string searchColumn, string searchValue)
        {
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            return transact(() =>
                          session.QueryOver<CentralUnitDocType>()
                                 .Where(specification)
                                 .List());
        }

        public void UpdateCentralUnitDocType(long id, string docid, string name, string alias, string changedby, DateTime changeddate)
        {
            string hqlSyntax = "update CentralUnitDocType set DocId = :DOCID, Name = :NAME, Alias = :ALIAS, ChangedBy = :CHANGEDBY, ChangedDate = :CHANGEDDATE where Id = :id";
            session.CreateQuery(hqlSyntax)
                .SetString("DOCID", docid)
                .SetString("NAME", name)
                .SetString("ALIAS", alias)
                .SetString("CHANGEDBY", changedby)
                .SetDateTime("CHANGEDDATE", changeddate)
                .SetInt64("id", id)
                .ExecuteUpdate();
        }
    }
}
