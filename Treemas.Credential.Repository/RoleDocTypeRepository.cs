﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LinqSpecs;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;
using Treemas.Base.Utilities;

namespace Treemas.Credential.Repository
{
    public class RoleDocTypeRepository : RepositoryController<RoleDocType>, IRoleDocTypeRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public RoleDocTypeRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public RoleDocType getRoleDocType(long id)
        {
            return transact(() => session.QueryOver<RoleDocType>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }

        public IList<RoleDocType> getDocTypes(long RoleId)
        {
            IEnumerable<RoleDocType> _roleDocType = this.FindAll(new AdHocSpecification<RoleDocType>(s => s.RoleId == RoleId));
            return _roleDocType.ToList();
        }

        //public IList<RoleDocType> getRolebyFuncID(long funcId)
        //{
        //    IEnumerable<RoleDocType> _applicationRole = this.FindAll(new AdHocSpecification<RoleDocType>(s => s.DocTypeId == funcId));
        //    return _applicationRole.ToList();
        //}
        public bool IsDuplicate(long roleid, long DocTypeid)
        {
            int rowCount = transact(() => session.QueryOver<RoleDocType>()
                                                .Where(f => f.DocumentTypeId == DocTypeid && f.RoleId == roleid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<RoleDocType> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<RoleDocType> paged = new PagedResult<RoleDocType>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<RoleDocType>()
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<RoleDocType>()
                                                     .OrderBy(x => typeof(RoleDocType).GetProperty(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public int deleteSelected(long roleId, long DocTypeId)
        {
            string hqlSyntax = "delete RoleDocType where RoleId = :RoleId and DocumentTypeId = :DocumentTypeId";
            return session.CreateQuery(hqlSyntax)
                    .SetInt64("RoleId", roleId)
                    .SetInt64("DocumentTypeId", DocTypeId)
                    .ExecuteUpdate();
        }
        
        public RoleDocType getRoleDocType(long roleId, long docTypeId)
        {
            return transact(() => session.QueryOver<RoleDocType>()
                                                .Where(f => f.RoleId == roleId && f.DocumentTypeId == docTypeId).SingleOrDefault());
        }

        public RoleDocType getRoleDocByDocType(long docTypeId)
        {
           return this.FindOne(new AdHocSpecification<RoleDocType>(s => s.DocumentTypeId == docTypeId));
        }
    }
}
