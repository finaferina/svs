﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using NHibernate.Criterion;

namespace Treemas.Credential.Repository
{
    public class ApplicationCrRepository : RepositoryController<ApplicationCr>, IApplicationCrRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public ApplicationCrRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public ApplicationCr getChangesRequest()
        {
            return transact(() => session.QueryOver<ApplicationCr>().Where(f => f.ApprovalStatus == ApprovalStatusState.R.ToString()).SingleOrDefault());
        }

        public PagedResult<ApplicationCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter)
        {
            SimpleExpression status = Restrictions.Eq("ApprovalStatus", filter.ApprovalStatus);
            SimpleExpression apprtype = Restrictions.Eq("ApprovalType", filter.ApprovalType);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.ApprovalStatus.Trim().IsNullOrEmpty()) conjuction.Add(status);
            if (!filter.ApprovalType.Trim().IsNullOrEmpty()) conjuction.Add(apprtype);

            PagedResult<ApplicationCr> paged = new PagedResult<ApplicationCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<ApplicationCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<ApplicationCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<ApplicationCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<ApplicationCr> specification;

            switch (searchColumn)
            {
                case "ApplicationId":
                    specification = new AdHocSpecification<ApplicationCr>(s => s.ApplicationId.Contains(searchValue));
                    break;
                case "Name":
                    specification = new AdHocSpecification<ApplicationCr>(s => s.Name.Contains(searchValue));
                    break;
                case "Type":
                    specification = new AdHocSpecification<ApplicationCr>(s => s.Type.Contains(searchValue));
                    break;
                case "Runtime":
                    specification = new AdHocSpecification<ApplicationCr>(s => s.Runtime.Contains(searchValue));
                    break;
                case "Description":
                    specification = new AdHocSpecification<ApplicationCr>(s => s.Description.Contains(searchValue));
                    break;
                case "CSSColor":
                    specification = new AdHocSpecification<ApplicationCr>(s => s.CSSColor.Contains(searchValue));
                    break;
                case "Icon":
                    specification = new AdHocSpecification<ApplicationCr>(s => s.Icon.Contains(searchValue));
                    break;
                default:
                    specification = new AdHocSpecification<ApplicationCr>(s => s.ApplicationId.Contains(searchValue));
                    break;
            }

            PagedResult<ApplicationCr> paged = new PagedResult<ApplicationCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<ApplicationCr>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<ApplicationCr>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<ApplicationCr>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<ApplicationCr>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = Count;
            return paged;
        }

        public ApplicationCr getApplication(long id)
        {
            return transact(() => session.QueryOver<ApplicationCr>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }

        public IList<ApplicationCr> getApplications(IList<string> Ids)
        {
            return transact(() => session.QueryOver<ApplicationCr>()
                        .WhereRestrictionOn(val => val.ApplicationId)
                        .IsIn(Ids.ToArray()).List());
        }

        public bool IsDuplicate(string applicationid)
        {
            int rowCount = transact(() => session.QueryOver<ApplicationCr>()
                                                 .Where(f => f.ApplicationId == applicationid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public ApplicationCr getExistingRequest(string AppID)
        {
            return transact(() => session.QueryOver<ApplicationCr>()
                                                .Where(f => f.ApplicationId == AppID && f.ApprovalType == "U" && f.ApprovalStatus == "N").SingleOrDefault());
        }

        public void deleteExistingRequest(string AppID)
        {
            string hqlDelete = "delete SEC_APPLICATION_CR where APPLICATION_ID = :APPLICATION_ID and APPROVAL_TYPE = 'U' and APPROVAL_STATUS = 'N' ";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("APPLICATION_ID", AppID)
                    .ExecuteUpdate();
        }
    }
}
