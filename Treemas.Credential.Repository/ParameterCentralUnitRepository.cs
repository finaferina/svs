﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Repository
{
    public class ParameterCentralUnitRepository : RepositoryControllerString<ParameterCentralUnit>, IParameterCentralUnitRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public ParameterCentralUnitRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public ParameterCentralUnit getParameter()
        {
            return transact(() => session.QueryOver<ParameterCentralUnit>().Take(1).SingleOrDefault());
        }

        public bool IsDuplicate(string ID)
        {
            int rowCount = transact(() => session.QueryOver<ParameterCentralUnit>()
                                                .Where(f => f.UserId == ID).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public bool InChangesRequest(string ParamID)
        {
            int rowCount = transact(() => session.QueryOver<ParameterHistoryCentralUnit>()
                                                .Where(f => f.ApprovalStatus == ApprovalStatusState.R.ToString() &&
                                                        f.ParamId == ParamID).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public ParameterHistoryCentralUnit GetChangesRequest()
        {
            return transact(() => session.QueryOver<ParameterHistoryCentralUnit>().Where(f => f.ApprovalStatus == ApprovalStatusState.R.ToString()).Take(1).SingleOrDefault());
        }
        public void AddHistory(ParameterHistoryCentralUnit item)
        {
            try
            {
                transact(() => session.Save(item));
            }
            catch (Exception e)
            {
                throw new RepositoryException(e.Message, e);
            }
        }
        public void SaveHistory(ParameterHistoryCentralUnit item)
        {
            try
            {
                transact(() => session.Update(item));
            }
            catch (Exception e)
            {
                throw new RepositoryException(e.Message, e);
            }
        }
    }
}
