﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using NHibernate.Criterion;

namespace Treemas.Credential.Repository
{
    public class RegionCrRepository : RepositoryController<RegionCr>, IRegionCrRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public RegionCrRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public void deleteExistingRequest(string regionid)
        {
            string hqlDelete = "delete SEC_REGION_CR where REGION_ID = :REGION_ID and APPROVAL_TYPE = 'U' and APPROVAL_STATUS = 'N' ";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("REGION_ID", regionid)
                    .ExecuteUpdate();
        }

        public PagedResult<RegionCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter)
        {
            SimpleExpression status = Restrictions.Eq("ApprovalStatus", filter.ApprovalStatus);
            SimpleExpression apprtype = Restrictions.Eq("ApprovalType", filter.ApprovalType);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.ApprovalStatus.Trim().IsNullOrEmpty()) conjuction.Add(status);
            if (!filter.ApprovalType.Trim().IsNullOrEmpty()) conjuction.Add(apprtype);

            PagedResult<RegionCr> paged = new PagedResult<RegionCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<RegionCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<RegionCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public IList<RegionCr> getApplicationRegion(string application)
        {
            throw new NotImplementedException();
        }

        public RegionCr getExistingRequest(string regionid, string reqType)
        {
            return transact(() => session.QueryOver<RegionCr>()
                                                            .Where(f => f.RegionId == regionid && f.ApprovalType == reqType && f.ApprovalStatus == "N").SingleOrDefault());
        }

        public RegionCr getRegion(string id)
        {
            return transact(() => session.QueryOver<RegionCr>()
                                                .Where(f => f.RegionId == id).SingleOrDefault());
        }

        public RegionCr getRegion(long id)
        {
            return transact(() => session.QueryOver<RegionCr>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }

        public bool IsDuplicate(string application, string regionid)
        {
            int rowCount = transact(() => session.QueryOver<RegionCr>()
                                                .Where(f => f.RegionId == regionid && f._Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
    }
}
