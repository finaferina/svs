﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;
using NHibernate.Criterion;

namespace Treemas.Credential.Repository
{
    public class RoleRepository : RepositoryController<Role>, IRoleRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public RoleRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public Role getRole(long id)
        {
            return transact(() => session.QueryOver<Role>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }
        public IList<Role> getApplicationRole(string application)
        {
            IEnumerable<Role> _applicationRole = this.FindAll(new AdHocSpecification<Role>(s => s._Application == application));
            return _applicationRole.ToList();
        }

        public bool IsDuplicate(string application, string Roleid)
        {
            int rowCount = transact(() => session.QueryOver<Role>()
                                                .Where(f => f.RoleId == Roleid && f._Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<Role> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<Role> paged = new PagedResult<Role>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<Role>()
                                                     .OrderByDescending(GetOrderByExpression<Role>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<Role>()
                                                     .OrderBy(GetOrderByExpression<Role>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<Role> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            PagedResult<Role> paged = new PagedResult<Role>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<Role>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<Role>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }


            paged.TotalItems = transact(() =>
                         session.QueryOver<Role>()
                                .Where(specification)
                                .RowCount());
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public Role getRole(string id)
        {
            return transact(() => session.QueryOver<Role>()
                                                 .Where(f => f.RoleId == id).SingleOrDefault());
        }

        public IList<Role> FindForExport(string searchColumn, string searchValue)
        {
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            return transact(() =>
                          session.QueryOver<Role>()
                                 .Where(specification)
                                 .List());
        }
    }
}
