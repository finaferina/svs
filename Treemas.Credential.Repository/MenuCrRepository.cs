﻿using System;
using System.Linq;
using System.Collections.Generic;
using LinqSpecs;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.Base.Globals;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using System.Linq.Expressions;
using NHibernate.Linq;
using NHibernate.Criterion;

namespace Treemas.Credential.Repository
{
    public class MenuCrRepository : RepositoryController<MenuCr>, IMenuCrRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public MenuCrRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public IList<MenuCr> getApplicationMenu(string application)
        {
            return transact(() =>
                  session.QueryOver<MenuCr>()
                         .Where(val => val.Application == application)
                         .And(val => val.MenuLevel == 0)
                         .And(val => val.Active == true)
                         .OrderBy(val => val.MenuOrder).Asc
                         .List());

            //IEnumerable<Menu> _applicationRole = this.FindAll(new AdHocSpecification<Menu>(s => s.Application == application && s.MenuLevel == 0 && s.Active == true)).OrderBy("");
            //return _applicationRole.ToList();
        }

        public MenuCr getForEditMode(long id)
        {
            return transact(() => session.QueryOver<MenuCr>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }

        public IList<AuthorizationFunction> getFunctions(IList<string> ApplicationIds)
        {
            return transact(() =>
                   session.QueryOver<AuthorizationFunction>()
                          .WhereRestrictionOn(val => val.Application)
                          .IsIn(ApplicationIds.ToArray()).List());
        }

        public bool IsDuplicate(string application, string menuId)
        {
            int rowCount = transact(() => session.QueryOver<MenuCr>()
                                                .Where(f => f.MenuId == menuId && f.Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public bool IsDuplicateForAdd(string application, string joinId)
        {
            int rowCount = transact(() => session.QueryOver<MenuCr>()
                                                .Where(f => f.MenuId == joinId && f.Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public PagedResult<MenuCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter)
        {
            SimpleExpression status = Restrictions.Eq("ApprovalStatus", filter.ApprovalStatus);
            SimpleExpression apprtype = Restrictions.Eq("ApprovalType", filter.ApprovalType);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.ApprovalStatus.Trim().IsNullOrEmpty()) conjuction.Add(status);
            if (!filter.ApprovalType.Trim().IsNullOrEmpty()) conjuction.Add(apprtype);

            PagedResult<MenuCr> paged = new PagedResult<MenuCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<MenuCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<MenuCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public string GetRootMenuId(long id)
        {
            string menuId = transact(() => session.QueryOver<MenuCr>()
                                               .Where(f => f.Id == id).SingleOrDefault().MenuId);
            //if (menuId.Length>3)
            //{
            //   return menuId.Substring(0, 4);
            //}
            return menuId;
        }

        public IList<MenuCr> FindOnlyParentMenu(string menuId)
        {
            IEnumerable<MenuCr> _applicationRole = transact(() =>
                          session.QueryOver<MenuCr>()
                                 .Where(s => s.MenuId == menuId && s.MenuLevel == 0 && s.Active == true).List());
            return _applicationRole.ToList();
        }

        public string GetRootMenuNameByParentMenuId(string parentId)
        {
            return transact(() => session.QueryOver<MenuCr>()
                                               .Where(f => f.Id == Convert.ToInt16(parentId)).SingleOrDefault().MenuName);
        }

        public string GetRootMenuId(string id)
        {
            string menuId = transact(() => session.QueryOver<MenuCr>()
                                               .Where(f => f.MenuId == id).SingleOrDefault().MenuId);
            //if (menuId.Length>3)
            //{
            //   return menuId.Substring(0, 4);
            //}
            return menuId;
        }
    }
}
