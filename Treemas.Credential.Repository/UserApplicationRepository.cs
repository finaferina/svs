﻿using System.Linq;
using LinqSpecs;
using System.Collections.Generic;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;

namespace Treemas.Credential.Repository
{
    public class UserApplicationRepository : RepositoryController<UserApplication>, IUserApplicationRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public UserApplicationRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public IList<UserApplication> getUserApplication(string username)
        {   
            IEnumerable<UserApplication> _userApplication = this.FindAll(new AdHocSpecification<UserApplication>(s => s.Username == username));
            return _userApplication.ToList();
        }
        public UserApplication getUserApplication(string username, string application)
        {
            return GetOne(new AdHocSpecification<UserApplication>(s => s.Username.Equals(username) && s.Application.Equals(application)));
        }

        public UserApplication getUserApplication(long id)
        {
            return GetOne(new AdHocSpecification<UserApplication>(s => s.Id == id));
        }
        public int deleteSelected(string username, string application)
        {
            string hqlSyntax = "delete Authorization where Username = :username and Application = :application";
            return session.CreateQuery(hqlSyntax)
                    .SetString("username", username)
                    .SetString("application", application)
                    .ExecuteUpdate();
        }
        public UserApplication getUserApp(string username, string application)
        {
            return FindOne(new AdHocSpecification<UserApplication>(s => s.Username.Equals(username) && s.Application.Equals(application)));
        }
        public bool IsDuplicate(string username)
        {
            int rowCount = transact(() => session.QueryOver<UserApplication>()
                                                .Where(f => f.Username == username).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public UserApplication GetUser(string username)
        {
            UserApplication _user = this.FindOne(new AdHocSpecification<UserApplication>(s => s.Username == username));
            return _user;
        }
        public int DeleteUserApplication(string username)
        {
            string hqlSyntaxUserApp = "delete UserApplication where Username = :Username";
            int deletedEntities2 = session.CreateQuery(hqlSyntaxUserApp)
                    .SetString("Username", username)
                    .ExecuteUpdate();

            return deletedEntities2;
        }

    }
}
