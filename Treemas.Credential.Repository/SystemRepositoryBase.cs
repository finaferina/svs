﻿using System.Data;
using System.Reflection;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Connection;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Mapping.ByCode;
using Treemas.Base.Utilities;
using Treemas.Base.Repository.UnitOfWork;

namespace Treemas.Credential.Repository
{
    public class SystemRepositoryBase
    {
        private volatile static SystemRepositoryBase uniqueInstance;
        private static object syncLock = new object();
        public static Configuration configuration { get; set; }
        public static ISessionFactory sessionFactory { get; private set; }
        private static HbmMapping _mapping;
        private static IConnection _connection = null;

        public SystemRepositoryBase()
        {
            var conn = Connection.GetConnectionSetting();
            configuration = new Configuration();
            configuration.DataBaseIntegration(dbi =>
            {
                //dbi.Dialect<MsSql2012Dialect>();
                dbi.Dialect<MsSql2005Dialect>();
                dbi.Driver<SqlClientDriver>();
                dbi.ConnectionProvider<DriverConnectionProvider>();
                dbi.ConnectionString = string.Format(@"Data Source={0};Database={1};Uid={2};Pwd={3};", conn.DBInstance, conn.DBName, conn.DBUser, conn.DBPassword);
                //dbi.ConnectionString = string.Format("Data Source=192.168.2.15,4444;Database=SVS;Uid=sa;Pwd=tr33m4s!;");
                //dbi.ConnectionString = string.Format("Data Source=NTSVSSQLIDUV11;Database=SVS;Uid=id.app.svs.local;Pwd=Am6H$Oi1J+;");
                dbi.IsolationLevel = IsolationLevel.ReadCommitted;
                dbi.Timeout = 15;
            });
            configuration.AddDeserializedMapping(Mapping, null);
            sessionFactory = configuration.BuildSessionFactory();
        }

        private static SystemRepositoryBase instance()
        {
            if (uniqueInstance == null)
            {
                // Lock area where instance is created
                lock (syncLock)
                {
                    if (uniqueInstance == null)
                    {
                        uniqueInstance = new SystemRepositoryBase();
                    }
                }
            }
            return uniqueInstance;
        }
        public static SystemRepositoryBase Instance => instance();
        public ISession currentSession
        {
            get
            {
                return sessionFactory.OpenSession();
            }
        }

        public IStatelessSession statelessSession
        {
            get
            {
                return sessionFactory.OpenStatelessSession(); 
            }
        }
        public IUnitOfWorkFactory UnitOfWork
        {
            get
            {
                return new UnitOfWorkFactory(configuration, sessionFactory, currentSession);
            }
        }
        public IConnection Connection
        {
            get
            {
                if (_connection == null)
                {
                    _connection = new SystemConnection();
                }
                return _connection;
            }
        }
        public HbmMapping Mapping
        {
            get
            {
                if (_mapping == null)
                {
                    _mapping = CreateMapping();
                }
                return _mapping;
            }
        }
        private HbmMapping CreateMapping()
        {
            var mapper = new ModelMapper();
            //Add mapping classes to the model mapper
            var types = Assembly.Load("Treemas.Credential.Repository").GetTypes();
            mapper.AddMappings(types);

            //Create and return a HbmMapping of the model mapping in code
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }
    }
}
