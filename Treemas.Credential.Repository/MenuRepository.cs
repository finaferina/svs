﻿using System;
using System.Linq;
using System.Collections.Generic;
using LinqSpecs;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.Base.Globals;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using System.Linq.Expressions;
using NHibernate.Linq;
using NHibernate.Criterion;

namespace Treemas.Credential.Repository
{
    public class MenuRepository : RepositoryController<Menu>, IMenuRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public MenuRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public IList<Menu> getApplicationMenu(string application)
        {
            return transact(() =>
                  session.QueryOver<Menu>()
                         .Where(val => val.Application == application)
                         .And(val => val.MenuLevel == 0)
                         .And(val => val.Active == true)
                         .OrderBy(val => val.MenuOrder).Asc
                         .List());

            //IEnumerable<Menu> _applicationRole = this.FindAll(new AdHocSpecification<Menu>(s => s.Application == application && s.MenuLevel == 0 && s.Active == true)).OrderBy("");
            //return _applicationRole.ToList();
        }

        public Menu getForEditMode(long id)
        {
            return transact(() => session.QueryOver<Menu>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }

        public IList<AuthorizationFunction> getFunctions(IList<string> ApplicationIds)
        {
            return transact(() =>
                   session.QueryOver<AuthorizationFunction>()
                          .WhereRestrictionOn(val => val.Application)
                          .IsIn(ApplicationIds.ToArray()).List());
        }

        public bool IsDuplicate(string application, string menuId)
        {
            int rowCount = transact(() => session.QueryOver<Menu>()
                                                .Where(f => f.MenuId == menuId && f.Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public bool IsDuplicateForAdd(string application, string joinId)
        {
            int rowCount = transact(() => session.QueryOver<Menu>()
                                                .Where(f => f.MenuId == joinId && f.Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public PagedResult<Menu> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<Menu> paged = new PagedResult<Menu>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<Menu>()
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<Menu>()
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<Menu> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            SimpleExpression specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            PagedResult<Menu> paged = new PagedResult<Menu>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<Menu>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<Menu>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }


            paged.TotalItems = transact(() =>
                         session.QueryOver<Menu>()
                                .Where(specification).RowCount());
            return paged;
        }

        public string GetRootMenuId(long id)
        {
            string menuId = transact(() => session.QueryOver<Menu>()
                                               .Where(f => f.Id == id).SingleOrDefault().MenuId);
            //if (menuId.Length>3)
            //{
            //   return menuId.Substring(0, 4);
            //}
            return menuId;
        }

        public IList<Menu> FindOnlyParentMenu(string menuId)
        {
            IEnumerable<Menu> _applicationRole = transact(() =>
                          session.QueryOver<Menu>()
                                 .Where(s => s.MenuId == menuId && s.MenuLevel == 0 && s.Active == true).List());
            return _applicationRole.ToList();
        }

        public string GetRootMenuNameByParentMenuId(string parentId)
        {
            return transact(() => session.QueryOver<Menu>()
                                               .Where(f => f.Id == Convert.ToInt16(parentId)).SingleOrDefault().MenuName);
        }

        public Menu getForEditMode(string menuid)
        {
            return transact(() => session.QueryOver<Menu>()
                                                .Where(f => f.MenuId == menuid).SingleOrDefault());
        }
    }
}
