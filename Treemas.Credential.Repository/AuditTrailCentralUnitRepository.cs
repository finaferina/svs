﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;
using Treemas.Base.Utilities;
using Newtonsoft.Json;
using NHibernate.Transform;

namespace Treemas.Credential.Repository
{
    public class AuditTrailCentralUnitRepository : RepositoryController<AuditTrailCentralUnit>, IAuditTrailCentralUnitRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        private IAuditTrailCentralUnitRepository _auditRepository;
        public AuditTrailCentralUnitRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public void SaveAuditTrail(string functionName, object auditedObject, string actionby, string action)
        {
            AuditTrailCentralUnit audit = new AuditTrailCentralUnit(0L);
            audit.FunctionName = functionName;
            audit.ObjectName = auditedObject.GetType().Name;
            audit.ObjectValue = JsonConvert.SerializeObject(auditedObject);
            audit.Action = action;
            audit.ActionBy = actionby;
            audit.ActionDateTime = DateTime.Now;
            _auditRepository.Add(audit);
        }

        public PagedResult<AuditTrailCentralUnit> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<AuditTrailCentralUnit> paged = new PagedResult<AuditTrailCentralUnit>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<AuditTrailCentralUnit>()
                                                    .OrderBy(Projections.Property(orderColumn)).Asc
                                                    .Skip((pageNumber) * itemsPerPage)
                                                    .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<AuditTrailCentralUnit>()
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<AuditTrailCentralUnit> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            ICriterion specification;

            if (searchColumn == "REQUEST_TIME")
            {
                DateTime reqDate;
                DateTime.TryParseExact(searchValue, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out reqDate);
                DateTime tempDate = reqDate.AddDays(1);
                specification = Restrictions.Between(searchColumn, reqDate, tempDate);
            }
            else
            {
                specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
            }

            PagedResult<AuditTrailCentralUnit> paged = new PagedResult<AuditTrailCentralUnit>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<AuditTrailCentralUnit>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<AuditTrailCentralUnit>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }


            paged.TotalItems = transact(() =>
                         session.QueryOver<AuditTrailCentralUnit>()
                                .Where(specification).RowCount());
            return paged;
        }

        public PagedResult<AuditTrailCentralUnit> FindAllPagedDate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue)
        {

            PagedResult<AuditTrailCentralUnit> paged = new PagedResult<AuditTrailCentralUnit>(pageNumber, itemsPerPage);
            if (searchColumn.StringEqualsIgnoreCase("ActionDateTime"))
            {
                if (orderKey.ToLower() == "asc")
                {
                    paged.Items = transact(() =>
                              session.QueryOver<AuditTrailCentralUnit>()
                                     .Where(f => f.ActionDateTime == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() =>
                              session.QueryOver<AuditTrailCentralUnit>()
                                     .Where(f => f.ActionDateTime == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                paged.TotalItems = transact(() =>
                        session.QueryOver<AuditTrailCentralUnit>()
                               .Where(f => f.ActionDateTime == searchValue).RowCount());
            }

            return paged;
        }

        public PagedResult<AuditTrailCentralUnit> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, AuditTrailFilter filter)
        {
            //SimpleExpression FunctionName = Restrictions.Like("FunctionName", filter.FunctionName, MatchMode.Anywhere);
            //SimpleExpression ObjectName = Restrictions.Like("ObjectName", filter.ObjectName, MatchMode.Anywhere);
            SimpleExpression ActionName = Restrictions.Like("Action", filter.ActionName, MatchMode.Anywhere);
            SimpleExpression ActionDateStart = Restrictions.Ge("TimeStamp", filter.ActionDateStart);
            SimpleExpression ActionDateEnd = Restrictions.Le("TimeStamp", filter.ActionDateEnd);

            Conjunction conjuction = Restrictions.Conjunction();
            //if (!filter.FunctionName.IsNullOrEmpty()) conjuction.Add(FunctionName);
            //if (!filter.ObjectName.IsNullOrEmpty()) conjuction.Add(ObjectName);
            if (!filter.ActionName.IsNullOrEmpty()) conjuction.Add(ActionName);
            if (!filter.ActionDateStart.IsNull()) conjuction.Add(ActionDateStart);
            if (!filter.ActionDateEnd.IsNull()) conjuction.Add(ActionDateEnd);

            PagedResult<AuditTrailCentralUnit> paged = new PagedResult<AuditTrailCentralUnit>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<AuditTrailCentralUnit>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<AuditTrailCentralUnit>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() => session.QueryOver<AuditTrailCentralUnit>().Where(conjuction).RowCount());
            return paged;
        }

        public PagedResult<AuditTrailCentralUnit> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, DateTime start, DateTime end)
        {
            //SimpleExpression specification;

            //if (searchValue.IsNullOrEmpty())
            //{
            //    specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
            //}
            //else
            //{
            //    specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Exact);
            //}

            //Conjunction conjuction = Restrictions.Conjunction();
            //ICriterion timestamp = null;

            //if (start != DateTime.MinValue && end != DateTime.MinValue)
            //{
            //    string sStart = start.ToString("dd/M/yyyy");
            //    string sEnd = end.ToString("dd/M/yyyy");

            //    var dateStart = DateTime.ParseExact(sStart, "dd/M/yyyy", CultureInfo.InvariantCulture);
            //    var dateEnd = DateTime.ParseExact(sEnd, "dd/M/yyyy", CultureInfo.InvariantCulture);

            //    timestamp = Restrictions.Between("TimeStamp", dateStart, dateEnd);
            //}

            //if (searchValue != null) conjuction.Add(specification);
            //if (start != DateTime.MinValue) conjuction.Add(timestamp);

            ICriterion specification;
            Conjunction conjuction = Restrictions.Conjunction();

            if (!searchValue.IsNullOrEmpty())
            {
                specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
                conjuction.Add(specification);
            }
            else
            {
                specification = Restrictions.IsNotEmpty("ID");
                //conjuction.Add(specification);
            }

            if (start != DateTime.MinValue && end != DateTime.MinValue)
            {
                //specification = Restrictions.Between("TimeStamp", start.ToString("MM/dd/yyyy 00:00:01"), end.ToString("MM/dd/yyyy 23:59:59"));
                ICriterion specification2 = Restrictions.Between("TimeStamp", start, end.AddDays(1));
                conjuction.Add(specification2);
            }

            PagedResult<AuditTrailCentralUnit> paged = new PagedResult<AuditTrailCentralUnit>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                //paged.Items = transact(() => session.QueryOver<AuditTrail>().Where(specification)
                //                                     .OrderBy(Projections.Property(orderColumn)).Asc
                //                                     .Skip((pageNumber) * itemsPerPage)
                //                                     .Take(itemsPerPage).List());
                paged.Items = transact(() => session.QueryOver<AuditTrailCentralUnit>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<AuditTrailCentralUnit>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() => session.QueryOver<AuditTrailCentralUnit>().Where(conjuction).RowCount());
            return paged;
        }

        public IList<AuditTrailCentralUnit> FindAllPagedForExport(string searchColumn, string searchValue, DateTime ActionDateTimeStart, DateTime ActionDateTimeEnd)
        {
            Conjunction conjuction = Restrictions.Conjunction();
            ICriterion specification;
            if (!searchValue.IsNullOrEmpty())
            {
                specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
                conjuction.Add(specification);
            }
            if (ActionDateTimeStart != DateTime.MinValue && ActionDateTimeEnd != DateTime.MinValue)
            {
                ICriterion specification2 = Restrictions.Between("TimeStamp", ActionDateTimeStart, ActionDateTimeEnd.AddDays(1));
                conjuction.Add(specification2);
            }
            return transact(() => session.QueryOver<AuditTrailCentralUnit>().Where(conjuction).List());

            //return transact(() => session.QueryOver<ParameterAuditTrail>().Where(conjuction).List());
        }

        public PagedResult<AuditTrailCentralUnit> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, DateTime start, DateTime end, string[] userList)
        {

            ICriterion specification;
            Conjunction conjuction = Restrictions.Conjunction();

            if (!searchValue.IsNullOrEmpty())
            {
                specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
                conjuction.Add(specification);
            }
            else
            {
                specification = Restrictions.IsNotEmpty("ID");
            }

            if (start != DateTime.MinValue && end != DateTime.MinValue)
            {
                ICriterion specification2 = Restrictions.Between("TimeStamp", start, end.AddDays(1));
                conjuction.Add(specification2);
            }

            PagedResult<AuditTrailCentralUnit> paged = new PagedResult<AuditTrailCentralUnit>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<AuditTrailCentralUnit>().Where(conjuction).And(x => x.UserChecker.IsIn(userList)).And(x => x.UserMaker.IsIn(userList))
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<AuditTrailCentralUnit>().Where(conjuction).And(x => x.UserChecker.IsIn(userList)).And(x => x.UserMaker.IsIn(userList))
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() => session.QueryOver<AuditTrailCentralUnit>().Where(conjuction).And(x => x.UserChecker.IsIn(userList)).And(x => x.UserMaker.IsIn(userList)).RowCount());
            return paged;
        }

        public IList<AuditTrailCentralUnit> FindAllPagedForExport(string searchColumn, string searchValue, DateTime ActionDateTimeStart, DateTime ActionDateTimeEnd, string[] userList)
        {
            Conjunction conjuction = Restrictions.Conjunction();
            ICriterion specification;
            if (!searchValue.IsNullOrEmpty())
            {
                specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
                conjuction.Add(specification);
            }
            if (ActionDateTimeStart != DateTime.MinValue && ActionDateTimeEnd != DateTime.MinValue)
            {
                ICriterion specification2 = Restrictions.Between("TimeStamp", ActionDateTimeStart, ActionDateTimeEnd.AddDays(1));
                conjuction.Add(specification2);
            }
            return transact(() => session.QueryOver<AuditTrailCentralUnit>().Where(conjuction).And(x => x.UserChecker.IsIn(userList)).And(x => x.UserMaker.IsIn(userList)).List());
        }

        public PagedResult<AuditTrailCentralUnit> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, DateTime start, DateTime end, User user)
        {
            string sql = "";
            string sqlbranch = "";
            string sqlorder = "";
            PagedResult<AuditTrailCentralUnit> paged = new PagedResult<AuditTrailCentralUnit>(pageNumber, itemsPerPage);

            if (user.BranchCode.Trim() != "000")
            {
                sqlbranch = "AND((JSON_VALUE(OBJECT_VALUE_BEFORE, '$.BranchCode')) IN(SELECT BRANCH_CODE FROM BRANCH WHERE MAIN_BRANCH = '" + user.BranchCode.Trim() + "' OR BRANCH_CODE = '" + user.BranchCode.Trim() + "')" +
                  " OR(JSON_VALUE(OBJECT_VALUE_AFTER, '$.BranchCode')) IN(SELECT BRANCH_CODE FROM BRANCH WHERE MAIN_BRANCH = '" + user.BranchCode.Trim() + "' OR BRANCH_CODE = '" + user.BranchCode.Trim() + "'))";
            }

            if (orderKey.ToLower() == "asc")
            { sqlorder = " ASC "; }
            else
            { sqlorder = " DESC "; }

            sql = "SELECT " +
                  "      TimeStamp = TIMESTAMP" +
                  "    , FunctionName = FUNCTION_NAME" +
                  "    , ObjectName = OBJECT_NAME" +
                  "    , ObjectValueBefore = OBJECT_VALUE_BEFORE" +
                  "    , ObjectValueAfter = OBJECT_VALUE_AFTER" +
                  "    , Action = ACTION" +
                  "    , UserMaker = USER_MAKER" +
                  "    , RequestDate = REQUEST_DATE" +
                  "    , UserChecker = USER_CHECKER" +
                  "    , ApproveDate = APPROVE_DATE " +
                  "FROM SEC_AUDIT_TRAILCENTRALUNIT " +
                  "WHERE TIMESTAMP >= '" + start.ToString("MM/dd/yyyy") + "' AND TIMESTAMP <= '" + end.AddDays(1).ToString("MM/dd/yyyy") + "' " + sqlbranch +
                " order by " + Projections.Property(orderColumn).ToString() + sqlorder;
                //" OFFSET " + pageNumber * 10 + " ROWS" +
                //" FETCH NEXT " + itemsPerPage + " ROWS ONLY";

            NHibernate.IQuery myQuery = session.CreateSQLQuery(sql).SetResultTransformer(Transformers.AliasToBean<AuditTrailCentralUnit>());
            paged.Items = myQuery.List<AuditTrailCentralUnit>().Take(itemsPerPage);
            paged.TotalItems = myQuery.List<AuditTrailCentralUnit>().Count;
            paged.PageNumber = pageNumber;
            
            return paged;
        }

        public IList<AuditTrailCentralUnit> FindAllPagedForExport(string searchColumn, string searchValue, DateTime start, DateTime end, User user)
        {
            string sql = "";
            string sqlbranch = "";

            if (user.BranchCode.Trim() != "000")
            {
                sqlbranch = "AND((JSON_VALUE(OBJECT_VALUE_BEFORE, '$.BranchCode')) IN(SELECT BRANCH_CODE FROM BRANCH WHERE MAIN_BRANCH = '" + user.BranchCode.Trim() + "' OR BRANCH_CODE = '" + user.BranchCode.Trim() + "')" +
                  " OR(JSON_VALUE(OBJECT_VALUE_AFTER, '$.BranchCode')) IN(SELECT BRANCH_CODE FROM BRANCH WHERE MAIN_BRANCH = '" + user.BranchCode.Trim() + "' OR BRANCH_CODE = '" + user.BranchCode.Trim() + "'))";
            }

            sql = "SELECT " +
                  "      TimeStamp = TIMESTAMP" +
                  "    , FunctionName = FUNCTION_NAME" +
                  "    , ObjectName = OBJECT_NAME" +
                  "    , ObjectValueBefore = OBJECT_VALUE_BEFORE" +
                  "    , ObjectValueAfter = OBJECT_VALUE_AFTER" +
                  "    , Action = ACTION" +
                  "    , UserMaker = USER_MAKER" +
                  "    , RequestDate = REQUEST_DATE" +
                  "    , UserChecker = USER_CHECKER" +
                  "    , ApproveDate = APPROVE_DATE " +
                  "FROM SEC_AUDIT_TRAILCENTRALUNIT " +
                  "WHERE TIMESTAMP >= '" + start.ToString("MM/dd/yyyy") + "' AND TIMESTAMP <= '" + end.AddDays(1).ToString("MM/dd/yyyy") + "' " + sqlbranch;

            NHibernate.IQuery myQuery = session.CreateSQLQuery(sql).SetResultTransformer(Transformers.AliasToBean<AuditTrailCentralUnit>());
            IList<AuditTrailCentralUnit> result = myQuery.List<AuditTrailCentralUnit>();
            return result;
        }
    }
}
