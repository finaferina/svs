﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Repository
{
    public class ApplicationRepository : RepositoryController<Application>, IApplicationRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public ApplicationRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
       
        public Application getApplication(long id)
        {
            return transact(() => session.QueryOver<Application>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }

        public IList<Application> getApplications(IList<string> Ids)
        {
            return transact(() => session.QueryOver<Application>()
                        .WhereRestrictionOn(val => val.ApplicationId)
                        .IsIn(Ids.ToArray()).List());
        }
        public bool IsDuplicate(string applicationid)
        {
            int rowCount = transact(() => session.QueryOver<Application>()
                                                .Where(f => f.ApplicationId == applicationid).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<Application> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<Application> paged = new PagedResult<Application>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<Application>()
                                                     .OrderByDescending(GetOrderByExpression<Application>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<Application>()
                                                     .OrderBy(GetOrderByExpression<Application>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<Application> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<Application> specification;

            switch (searchColumn)
            {
                case "ApplicationId":
                    specification = new AdHocSpecification<Application>(s => s.ApplicationId.Contains(searchValue));
                    break;
                case "Name":
                    specification = new AdHocSpecification<Application>(s => s.Name.Contains(searchValue));
                    break;
                case "Type":
                    specification = new AdHocSpecification<Application>(s => s.Type.Contains(searchValue));
                    break;
                case "Runtime":
                    specification = new AdHocSpecification<Application>(s => s.Runtime.Contains(searchValue));
                    break;
                case "Description":
                    specification = new AdHocSpecification<Application>(s => s.Description.Contains(searchValue));
                    break;
                case "CSSColor":
                    specification = new AdHocSpecification<Application>(s => s.CSSColor.Contains(searchValue));
                    break;
                case "Icon":
                    specification = new AdHocSpecification<Application>(s => s.Icon.Contains(searchValue));
                    break;
                default:
                    specification = new AdHocSpecification<Application>(s => s.ApplicationId.Contains(searchValue));
                    break;
            }

            PagedResult<Application> paged = new PagedResult<Application>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<Application>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<Application>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<Application>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<Application>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = Count;
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public Application getApplication(string AppID)
        {
            return transact(() => session.QueryOver<Application>()
                                                .Where(f => f.ApplicationId == AppID).SingleOrDefault());
        }
    }
}
