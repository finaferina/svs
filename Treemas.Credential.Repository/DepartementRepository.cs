﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;
using NHibernate.Criterion;

namespace Treemas.Credential.Repository
{
    public class DepartementRepository : RepositoryController<Departement>, IDepartementRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public DepartementRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public Departement getDepartement(long id)
        {
            return transact(() => session.QueryOver<Departement>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }
        public IList<Departement> getApplicationDepartement(string application)
        {
            IEnumerable<Departement> _applicationDepartement = this.FindAll(new AdHocSpecification<Departement>(s => s._Application == application));
            return _applicationDepartement.ToList();
        }

        public bool IsDuplicate(string application, string Departementid)
        {
            int rowCount = transact(() => session.QueryOver<Departement>()
                                                .Where(f => f.DepartementId == Departementid && f._Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public bool IsDuplicateByAlias(string application, string alias)
        {
            int rowCount = transact(() => session.QueryOver<Departement>()
                                                .Where(f => f.Alias == alias && f._Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public bool IsDuplicateCR(string application, string Departementid, string approval_status = "N")
        {
            int rowCount = transact(() => session.QueryOver<DepartementCr>()
                                                .Where(f => f.DepartementId == Departementid && f._Application == application && f.ApprovalStatus == approval_status).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public bool IsDuplicateCRByAlias(string application, string alias, string approval_status)
        {
            int rowCount = transact(() => session.QueryOver<DepartementCr>()
                                                .Where(f => f.Alias == alias && f._Application == application && f.ApprovalStatus == approval_status).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public PagedResult<Departement> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<Departement> paged = new PagedResult<Departement>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<Departement>()
                                                     .OrderByDescending(GetOrderByExpression<Departement>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<Departement>()
                                                     .OrderBy(GetOrderByExpression<Departement>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<Departement> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            PagedResult<Departement> paged = new PagedResult<Departement>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<Departement>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<Departement>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }


            paged.TotalItems = transact(() =>
                         session.QueryOver<Departement>()
                                .Where(specification)
                                .RowCount());
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public Departement getDepartement(string id)
        {
            return transact(() => session.QueryOver<Departement>()
                                                 .Where(f => f.DepartementId == id).SingleOrDefault());
        }

        public IList<Departement> getDepartementbyRegion(string region, string division = "")
        {
            if (division == "")
            {
                SimpleExpression specification;
                specification = Restrictions.Like("Region", region, MatchMode.Anywhere);

                return transact(() =>
                              session.QueryOver<Departement>()
                                     .Where(specification)
                                     .List());
            }
            else {
                SimpleExpression FilterReg = Restrictions.Eq("Region", region);
                SimpleExpression FilterDivision = Restrictions.Eq("Division", division);

                Conjunction conjuction = Restrictions.Conjunction();
                if (!region.Trim().Equals("")) conjuction.Add(FilterReg);
                if (!division.Trim().Equals("")) conjuction.Add(FilterDivision);

                return transact(() =>
                              session.QueryOver<Departement>()
                                     .Where(conjuction)
                                     .List());
            }
          
        }

        public IList<Departement> FindForExport(string searchColumn, string searchValue)
        {
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            return transact(() =>
                          session.QueryOver<Departement>()
                                 .Where(specification)
                                 .List());
        }

        public void UpdateDepartement(long id, string departementid, string name, string alias, string region, string division, string changedby, DateTime changeddate)
        {
            string hqlSyntax = "update Departement set DepartementId = :DEPARTEMENTID, Name = :NAME, Alias = :ALIAS, Region = :REGION, Division = :DIVISION, ChangedBy = :CHANGEDBY, ChangedDate = :CHANGEDDATE where Id = :id";
            session.CreateQuery(hqlSyntax)
                .SetString("DEPARTEMENTID", departementid)
                .SetString("NAME", name)
                .SetString("ALIAS", alias)
                .SetString("REGION", region)
                .SetString("DIVISION", division)
                .SetString("CHANGEDBY", changedby)
                .SetDateTime("CHANGEDDATE", changeddate)
                .SetInt64("id", id)
                .ExecuteUpdate();
        }
        public bool IsFoundRegion(string region)
        {
            int rowCount = transact(() => session.QueryOver<Departement>()
                                                .Where(f => f.Region == region).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public bool IsFoundDivision(string division)
        {
            int rowCount = transact(() => session.QueryOver<Departement>()
                                                .Where(f => f.Division == division).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
    }
}
