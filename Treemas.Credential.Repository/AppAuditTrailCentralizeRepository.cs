﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;
using Treemas.Base.Utilities;
using Newtonsoft.Json;

namespace Treemas.Credential.Repository
{
    public class AppAuditTrailCentralizeRepository : RepositoryController<AppAuditTrailCentralize>, IAppAuditTrailCentralizeRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        private IAppAuditTrailCentralizeRepository _auditRepository;

        public AppAuditTrailCentralizeRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            //this._auditRepository = auditRepository;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public void SaveAuditTrail(string functionName, object auditedObject, string actionby, string action)
        {
            AppAuditTrailCentralize audit = new AppAuditTrailCentralize(0L);
            audit.FunctionName = functionName;
            audit.ObjectName = auditedObject.GetType().Name;
            audit.ObjectValue = JsonConvert.SerializeObject(auditedObject);
            audit.Action = action;
            audit.ActionBy = actionby;
            audit.ActionDateTime = DateTime.Now;
            _auditRepository.Add(audit);
        }
        public void SaveAuditTrail(string functionName, string objectName, object auditedObjectBefore, object auditedObjectAfter, string maker, DateTime? requestDate, string actionby, string action)
        {
            AppAuditTrailCentralize audit = new AppAuditTrailCentralize(0L);
            audit.TimeStamp = DateTime.Now;
            audit.FunctionName = functionName;
            audit.ObjectName = objectName;
            audit.ObjectValueBefore = JsonConvert.SerializeObject(auditedObjectBefore);
            audit.ObjectValueAfter = JsonConvert.SerializeObject(auditedObjectAfter);
            audit.UserMaker = maker;
            audit.RequestDate = requestDate;
            audit.Action = action;
            audit.UserChecker = actionby;
            audit.ApproveDate = DateTime.Now;
            _auditRepository.Add(audit);
        }

        public PagedResult<AppAuditTrailCentralize> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<AppAuditTrailCentralize> paged = new PagedResult<AppAuditTrailCentralize>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<AppAuditTrailCentralize>()
                                                    .OrderBy(Projections.Property(orderColumn)).Asc
                                                    .Skip((pageNumber) * itemsPerPage)
                                                    .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<AppAuditTrailCentralize>()
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<AppAuditTrailCentralize> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, DateTime start, DateTime end)
        {
            ICriterion specification;
            Conjunction conjuction = Restrictions.Conjunction();

            if (!searchValue.IsNullOrEmpty())
            {
                specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
                conjuction.Add(specification);
            }
            else
            {
                specification = Restrictions.IsNotEmpty("ID");
                //conjuction.Add(specification);
            }

            if (start != DateTime.MinValue && end != DateTime.MinValue)
            {
                ICriterion specification2 = Restrictions.Between("TimeStamp", start, end.AddDays(1));
                conjuction.Add(specification2);
            }

            PagedResult<AppAuditTrailCentralize> paged = new PagedResult<AppAuditTrailCentralize>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<AppAuditTrailCentralize>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<AppAuditTrailCentralize>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() => session.QueryOver<AppAuditTrailCentralize>().Where(conjuction).RowCount());
            return paged;
        }

        public PagedResult<AppAuditTrailCentralize> FindAllPagedDate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue)
        {

            PagedResult<AppAuditTrailCentralize> paged = new PagedResult<AppAuditTrailCentralize>(pageNumber, itemsPerPage);
            if (searchColumn.StringEqualsIgnoreCase("ActionDateTime"))
            {
                if (orderKey.ToLower() == "asc")
                {
                    paged.Items = transact(() =>
                              session.QueryOver<AppAuditTrailCentralize>()
                                     .Where(f => f.ActionDateTime == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() =>
                              session.QueryOver<AppAuditTrailCentralize>()
                                     .Where(f => f.ActionDateTime == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                paged.TotalItems = transact(() =>
                        session.QueryOver<AppAuditTrailCentralize>()
                               .Where(f => f.ActionDateTime == searchValue).RowCount());
            }

            return paged;
        }

        public PagedResult<AppAuditTrailCentralize> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, AuditTrailFilter filter)
        {
            SimpleExpression FunctionName = Restrictions.Like("FunctionName", filter.FunctionName, MatchMode.Anywhere);
            SimpleExpression ObjectName = Restrictions.Like("ObjectName", filter.ObjectName, MatchMode.Anywhere);
            SimpleExpression ActionName = Restrictions.Like("Action", filter.ActionName, MatchMode.Anywhere);
            SimpleExpression ActionDateStart = Restrictions.Ge("TimeStamp", filter.ActionDateStart);
            SimpleExpression ActionDateEnd = Restrictions.Le("TimeStamp", filter.ActionDateEnd);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.FunctionName.IsNullOrEmpty()) conjuction.Add(FunctionName);
            if (!filter.ObjectName.IsNullOrEmpty()) conjuction.Add(ObjectName);
            if (!filter.ActionName.IsNullOrEmpty()) conjuction.Add(ActionName);
            if (!filter.ActionDateStart.IsNull()) conjuction.Add(ActionDateStart);
            if (!filter.ActionDateEnd.IsNull()) conjuction.Add(ActionDateEnd);

            PagedResult<AppAuditTrailCentralize> paged = new PagedResult<AppAuditTrailCentralize>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<AppAuditTrailCentralize>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<AppAuditTrailCentralize>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() => session.QueryOver<AppAuditTrailCentralize>().Where(conjuction).RowCount());
            return paged;
        }

        public IList<AppAuditTrailCentralize> FindForExport(string searchColumn, string searchValue, DateTime ActionDateTimeStart, DateTime ActionDateTimeEnd)
        {
            Conjunction conjuction = Restrictions.Conjunction();
            ICriterion specification;
            if (!searchValue.IsNullOrEmpty())
            {
                specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
                conjuction.Add(specification);
            }
            if (ActionDateTimeStart != DateTime.MinValue && ActionDateTimeEnd != DateTime.MinValue)
            {
                ICriterion specification2 = Restrictions.Between("TimeStamp", ActionDateTimeStart, ActionDateTimeEnd.AddDays(1));
                conjuction.Add(specification2);
            }
            return transact(() => session.QueryOver<AppAuditTrailCentralize>().Where(conjuction).List());

        }
    }
}
