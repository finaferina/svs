﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using NHibernate.Criterion;

namespace Treemas.Credential.Repository
{
    public class CentralUnitDocTypeCrRepository : RepositoryController<CentralUnitDocTypeCr>, ICentralUnitDocTypeCrRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public CentralUnitDocTypeCrRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public void deleteExistingRequest(string docid)
        {
            string hqlDelete = "delete SEC_CENTRALUNITDOCTYPE_CR where DOC_ID = :DOC_ID and APPROVAL_TYPE = 'U' and APPROVAL_STATUS = 'N' ";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("DOC_ID", docid)
                    .ExecuteUpdate();
        }

        public PagedResult<CentralUnitDocTypeCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter)
        {
            SimpleExpression status = Restrictions.Eq("ApprovalStatus", filter.ApprovalStatus);
            SimpleExpression apprtype = Restrictions.Eq("ApprovalType", filter.ApprovalType);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.ApprovalStatus.Trim().IsNullOrEmpty()) conjuction.Add(status);
            if (!filter.ApprovalType.Trim().IsNullOrEmpty()) conjuction.Add(apprtype);

            PagedResult<CentralUnitDocTypeCr> paged = new PagedResult<CentralUnitDocTypeCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<CentralUnitDocTypeCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<CentralUnitDocTypeCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public IList<CentralUnitDocTypeCr> getApplicationCentralizeDoc(string application)
        {
            throw new NotImplementedException();
        }

        public CentralUnitDocTypeCr getExistingRequest(string docid, string reqType)
        {
            return transact(() => session.QueryOver<CentralUnitDocTypeCr>()
                                                            .Where(f => f.DocId == docid && f.ApprovalType == reqType && f.ApprovalStatus == "N").SingleOrDefault());
        }

        public CentralUnitDocTypeCr getCentralizeDoc(string id)
        {
            return transact(() => session.QueryOver<CentralUnitDocTypeCr>()
                                                .Where(f => f.DocId == id).SingleOrDefault());
        }

        public CentralUnitDocTypeCr getCentralizeDoc(long id)
        {
            return transact(() => session.QueryOver<CentralUnitDocTypeCr>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }

        public bool IsDuplicate(string application, string docid)
        {
            int rowCount = transact(() => session.QueryOver<CentralUnitDocTypeCr>()
                                                .Where(f => f.DocId == docid && f._Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
    }
}
