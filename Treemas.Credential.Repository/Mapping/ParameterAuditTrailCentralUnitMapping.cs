﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class ParameterAuditTrailCentralUnitMapping : ClassMapping<ParameterAuditTrailCentralUnit>
    {
        public ParameterAuditTrailCentralUnitMapping()
        {
            this.Table("SEC_AUDIT_TRAIL_PARAMCENTRALUNIT");
            Id<long>(
                x => x.Id,
                map =>
                {
                    map.Column("ID");
                    map.Generator(Generators.Identity);
                });

            Property<DateTime>(x => x.TimeStamp,
                map =>
                {
                    map.Column("TIMESTAMP");
                });

            Property<string>(x => x.FunctionName,
                map =>
                {
                    map.Column("FUNCTION_NAME");
                });

            Property<string>(x => x.ObjectName,
                map =>
                {
                    map.Column("OBJECT_NAME");
                });

            Property<string>(x => x.ObjectValueBefore,
                map =>
                {
                    map.Column("OBJECT_VALUE_BEFORE");
                    map.Length(20000000);
                });

            Property<string>(x => x.ObjectValueAfter,
                map =>
                {
                    map.Column("OBJECT_VALUE_AFTER");
                    map.Length(20000000);
                });

            Property<string>(x => x.Action,
                map =>
                {
                    map.Column("ACTION");
                });

            Property<string>(x => x.UserMaker,
                map =>
                {
                    map.Column("USER_MAKER");
                });

            Property<DateTime?>(x => x.RequestDate,
                map =>
                {
                    map.Column("REQUEST_DATE");
                });

            Property<string>(x => x.UserChecker,
                map =>
                {
                    map.Column("USER_CHECKER");
                });

            Property<DateTime?>(x => x.ApproveDate,
                map =>
                {
                    map.Column("APPROVE_DATE");
                });

        }
    }
}
