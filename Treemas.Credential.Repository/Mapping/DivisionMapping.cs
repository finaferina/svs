﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class DivisionMapping : ClassMapping<Division>
    {
        public DivisionMapping()
        {
            this.Table("SEC_DIVISION");
            Id<long>(x => x.Id,
                map =>
                {
                    map.Column("ID");
                    map.Generator(Generators.Identity);
                });

            Property<string>(x => x._Application,
                map =>
                {
                    map.Column("APPLICATION");
                    map.Update(false);
                });

            Property<string>(x => x.DivisionId,
                map =>
                {
                    map.Column("DIVISION_ID");
                    map.Update(false);
                });

            //Property<string>(x => x.TransId,
            //    map =>
            //    {
            //        map.Column("TRANS_ID");
            //        map.Update(false);
            //    });

            Property<string>(x => x.Name,
                map =>
                {
                    map.Column("NAME");
                });

            Property<string>(x => x.Alias,
                map =>
                {
                    map.Column("ALIAS");
                });

            Property<string>(x => x.Region,
                map =>
                {
                    map.Column("REGION_ID");
                });

            Property<string>(x => x.CreatedBy,
                map =>
                {
                    map.Column("CREATED_BY");
                    map.Update(false);
                });

            Property<DateTime?>(x => x.CreatedDate,
                map =>
                {
                    map.Column("CREATED_DATE");
                    map.Update(false);
                });

            Property<string>(x => x.ChangedBy,
                map =>
                {
                    map.Column("CHANGED_BY");
                });

            Property<DateTime?>(x => x.ChangedDate,
               map =>
               {
                   map.Column("CHANGED_DATE");
               });
            
        }
    }
}
