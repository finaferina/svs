﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;


namespace Treemas.Credential.Repository.Mapping
{
    public class ParameterHistoryCentralUnitMapping : ClassMapping<ParameterHistoryCentralUnit>
    {
        public ParameterHistoryCentralUnitMapping()
        {
            this.Table("PARAMETERCENTRALUNIT_CR");
            Id<long>(
              x => x.Id,
              map =>
              {
                  map.Column("ID");
                  map.Generator(Generators.Identity);
              });

            Property<string>(x => x.ParamId,
               map => { map.Column("PARAM_ID"); });

            Property<string>(x => x.UserId,
               map => { map.Column("USER_ID");  });

            Property<string>(x => x.Password,
                map => { map.Column("PASSWORD"); });

            Property<string>(x => x.NewUserId,
               map => { map.Column("USER_ID_NEW"); });

            Property<string>(x => x.NewPassword,
                map => { map.Column("PASSWORD_NEW"); });

            Property<DateTime?>(x => x.ChangedDate,
                map => { map.Column("CHANGED_DATE"); });

            Property<string>(x => x.ChangedBy,
                map => { map.Column("CHANGED_BY"); });

            Property<DateTime?>(x => x.ApprovedDate,
                map => { map.Column("APPROVED_DATE"); });

            Property<string>(x => x.ApprovedBy,
                map => { map.Column("APPROVED_BY"); });

            Property<string>(x => x.ApprovalStatus,
                map => { map.Column("APPROVAL_STATUS"); });

            Property<int>(x => x.SizeSignature,
                map => { map.Column("SIZE_SIGNATURE"); });

            Property<int>(x => x.SizeDocument,
                map => { map.Column("SIZE_DOCUMENT"); });

            Property<int>(x => x.KeepAuditTrail,
                map => { map.Column("KEEP_AUDIT_TRAIL"); });

            Property<int>(x => x.KeepPendingAppr,
                map => { map.Column("KEEP_PENDING_APPROVAL"); });

            Property<int>(x => x.NewKeepPendingAppr,
                map => { map.Column("KEEP_PENDING_APPROVAL_NEW"); });

            Property<int>(x => x.NewSizeSignature,
                map => { map.Column("SIZE_SIGNATURE_NEW"); });

            Property<int>(x => x.NewSizeDocument,
                map => { map.Column("SIZE_DOCUMENT_NEW"); });

            Property<int>(x => x.NewKeepAuditTrail,
                map => { map.Column("KEEP_AUDIT_TRAIL_NEW"); });
        }
    }
}
