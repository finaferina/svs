﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class FeatureMapping : ClassMapping<AuthorizationFeature>
    {
        public FeatureMapping()
        {
            this.Table("SEC_FEATURE");
            Id<long>(x => x.Id,
                map =>
                {
                    map.Column("ID");
                    map.Generator(Generators.Identity);
                });

            Property<string>(x => x.FeatureId,
                map =>
                {
                    map.Column("FEATURE_ID");
                    map.Update(false);
                });

            Property<string>(x => x.Application,
                map =>
                {
                    map.Column("APPLICATION");
                    map.Update(false);
                });

            Property<string>(x => x.Name,
                map =>
                {
                    map.Column("NAME");
                });

            Property<string>(x => x.Description,
                map =>
                {
                    map.Column("DESCRIPTION");
                });

            Property<DateTime?>(x => x.ChangedDate,
                map =>
                {
                    map.Column("CHANGED_DATE");
                });

            Property<DateTime?>(x => x.CreatedDate,
                map =>
                {
                    map.Column("CREATED_DATE");
                    map.Update(false);
                });

            Property<string>(x => x.ChangedBy,
                map =>
                {
                    map.Column("CHANGED_BY");
                });

            Property<string>(x => x.CreatedBy,
                map =>
                {
                    map.Column("CREATED_BY");
                    map.Update(false);
                });

        }
    }
}
