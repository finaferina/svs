﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;


namespace Treemas.Credential.Repository.Mapping
{
    public class ParameterHistoryMapping : ClassMapping<ParameterHistory>
    {
        public ParameterHistoryMapping()
        {
            this.Table("PARAMETER_CR");
            Id<long>(
              x => x.Id,
              map =>
              {
                  map.Column("ID");
                  map.Generator(Generators.Identity);
              });

            Property<string>(x => x.ParamId,
               map => { map.Column("PARAM_ID"); });

            Property<string>(x => x.UserId,
               map => { map.Column("USER_ID");  });

            Property<string>(x => x.Password,
                map => { map.Column("PASSWORD"); });

            Property<string>(x => x.StaggingUrl,
                map => { map.Column("STAGGING_URL"); });

            Property<int>(x => x.LoginAttempt,
                map => { map.Column("LOGIN_ATTEMPT"); });

            Property<int>(x => x.SessionTimeout,
                map => { map.Column("SESSION_TIMEOUT"); });

            Property<string>(x => x.PasswordExp,
                map => { map.Column("PASSWORD_EXPIRED_DATE"); });

            Property<string>(x => x.NewUserId,
               map => { map.Column("USER_ID_NEW"); });

            Property<string>(x => x.NewPassword,
                map => { map.Column("PASSWORD_NEW"); });

            Property<string>(x => x.NewStaggingUrl,
                map => { map.Column("STAGGING_URL_NEW"); });

            Property<int>(x => x.NewLoginAttempt,
                map => { map.Column("LOGIN_ATTEMPT_NEW"); });

            Property<int>(x => x.NewSessionTimeout,
                map => { map.Column("SESSION_TIMEOUT_NEW"); });

            Property<string>(x => x.NewPasswordExp,
                map => { map.Column("PASSWORD_EXPIRED_DATE_NEW"); });

            Property<DateTime?>(x => x.ChangedDate,
                map => { map.Column("CHANGED_DATE"); });

            Property<string>(x => x.ChangedBy,
                map => { map.Column("CHANGED_BY"); });

            Property<DateTime?>(x => x.ApprovedDate,
                map => { map.Column("APPROVED_DATE"); });

            Property<string>(x => x.ApprovedBy,
                map => { map.Column("APPROVED_BY"); });

            Property<string>(x => x.ApprovalStatus,
                map => { map.Column("APPROVAL_STATUS"); });

            Property<string>(x => x.DomainServer,
                map => { map.Column("DOMAIN_SERVER"); });

            Property<string>(x => x.NewDomainServer,
                map => { map.Column("DOMAIN_SERVER_NEW"); });


            Property<int>(x => x.SizeKTP,
                map => { map.Column("SIZE_KTP"); });

            Property<int>(x => x.SizeSignature,
                map => { map.Column("SIZE_SIGNATURE"); });

            Property<int>(x => x.SizeDocument,
                map => { map.Column("SIZE_DOCUMENT"); });

            Property<int?>(x => x.KeepHistory,
                map => { map.Column("KEEP_HISTORY"); });

            Property<int>(x => x.KeepAuditTrail,
                map => { map.Column("KEEP_AUDIT_TRAIL"); });

            Property<int>(x => x.KeepPendingAppr,
                map => { map.Column("KEEP_PENDING_APPROVAL"); });

            Property<decimal>(x => x.MaxDiffAccount,
                map => { map.Column("MAX_DIFF_ACCOUNT"); });


            Property<int>(x => x.NewSizeKTP,
                map => { map.Column("SIZE_KTP_NEW"); });

            Property<int>(x => x.NewSizeSignature,
                map => { map.Column("SIZE_SIGNATURE_NEW"); });

            Property<int>(x => x.NewSizeDocument,
                map => { map.Column("SIZE_DOCUMENT_NEW"); });

            Property<int?>(x => x.NewKeepHistory,
                map => { map.Column("KEEP_HISTORY_NEW"); });

            Property<int>(x => x.NewKeepAuditTrail,
                map => { map.Column("KEEP_AUDIT_TRAIL_NEW"); });

            Property<int>(x => x.NewKeepPendingAppr,
                map => { map.Column("KEEP_PENDING_APPR_NEW"); });

            Property<decimal>(x => x.NewMaxDiffAccount,
                map => { map.Column("MAX_DIFF_ACCOUNT_NEW"); });

            Property<int>(x => x.KeepCloseAccount,
               Map => { Map.Column("KEEP_CLOSED_ACCOUNT"); });
            Property<int>(x => x.NewKeepCloseAccount,
               Map => { Map.Column("KEEP_CLOSED_ACCOUNT_NEW"); });
        }
    }
}
