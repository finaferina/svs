﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class UserMapping : ClassMapping<User>
    {
        public UserMapping()
        {
            this.Table("SEC_USER");
            Id<long>(x => x.Id,
                map =>
                {
                    map.Column("ID");
                    map.Generator(Generators.Identity);
                });

            Property<string>(x => x.Username,
                map =>
                {
                    map.Column("USERNAME"); map.Update(false);
                });

            Property<string>(x => x.FullName,
                map =>
                {
                    map.Column("FULL_NAME");
                });

            //Property<string>(x => x.LastName,
            //    map =>
            //    {
            //        map.Column("LAST_NAME");
            //    });

            Property<string>(x => x.Password,
                map =>
                {
                    map.Column("PASSWORD");
                    //map.Update(false);
                });
            Property<string>(x => x.RegNo,
               map =>
               {
                   map.Column("REG_NO");
               });
            Property<bool>(x => x.IsActive,
                map =>
                {
                    map.Column("IS_ACTIVE"); map.Update(false);
                });

            Property<int>(x => x.SessionTimeout ,
                map =>
                {
                    map.Column("SESSION_TIMEOUT");
                });
            Property<int>(x => x.LockTimeout,
               map =>
               {
                   map.Column("LOCK_TIMEOUT");
               });
            Property<int>(x => x.MaximumConcurrentLogin,
              map =>
              {
                  map.Column("MAX_CONCURRENT_LOGIN");
              });

            Property<bool>(x => x.InActiveDirectory ,
                map =>
                {
                    map.Column("IN_ACTIVE_DIRECTORY");
                });
            
            Property<DateTime>(x => x.PasswordExpirationDate,
                map =>
                {
                    map.Column("PASSWORD_EXPIRATION_DATE"); map.Update(false);
                });
            Property<DateTime>(x => x.AccountValidityDate,
               map =>
               {
                   map.Column("ACCOUNT_VALIDITY_DATE"); map.Update(false);
               });

            Property<int>(x => x.LoginAttempt,
               map =>
               {
                   map.Column("LOGIN_ATTEMPT");
               });

            Property<DateTime?>(x => x.LastLogin,
               map =>
               {
                   map.Column("LAST_LOGIN");
                   map.Update(true); map.Insert(false);
               });

            Property<bool>(x => x.IsPrinted,
              map =>
              {
                  map.Column("IS_PRINTED"); map.Update(false);
              });

            Property<bool>(x => x.Approved,
              map =>
              {
                  map.Column("APPROVED"); map.Update(false);
              });

            Property<bool>(x => x.UpdateMenu,
               map =>
               {
                   map.Column("UPDATE_MENU");
               });

            Property<DateTime?>(x => x.ChangedDate,
               map =>
               {
                   map.Column("CHANGED_DATE"); 
               });

            Property<DateTime?>(x => x.CreatedDate,
                map =>
                {
                    map.Column("CREATED_DATE");
                    map.Update(false);
                });

            Property<string>(x => x.ChangedBy,
                map =>
                {
                    map.Column("CHANGED_BY");
                });

            Property<string>(x => x.CreatedBy,
                map =>
                {
                    map.Column("CREATED_BY");
                    map.Update(false);
                });
            Property<string>(x => x.BranchCode, map => { map.Column("BRANCH_CODE"); });
        }
    }
}
