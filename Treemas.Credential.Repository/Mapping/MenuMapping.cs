﻿using System;
using System.Collections.Generic;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class MenuMapping : ClassMapping<Menu>
    {
        public MenuMapping()
        {
            this.Table("SEC_MENU");
            Id<long>(x => x.Id, map => { map.Column("ID"); map.Generator(Generators.Identity); });
            
            Property<string>(x => x.MenuId, map => { map.Column("MENU_ID");  });
            Property<string>(x => x.MenuName, map => { map.Column("MENU_NAME"); });
            Property<string>(x => x.ParentMenu, map => { map.Column("PARENT_MENU_ID"); map.Update(false); });
            Property<string>(x => x.Application, map => { map.Column("APPLICATION"); map.Update(false); });
            Property<string>(x => x.FunctionId, map => { map.Column("FUNCTION_ID"); });
            Property<string>(x => x.MenuUrl, map => { map.Column("MENU_URL"); });
            Property<int>(x => x.MenuLevel, map => { map.Column("MENU_LEVEL"); });
            Property<int>(x => x.MenuOrder, map => { map.Column("MENU_ORDER"); });
            Property<string>(x => x.MenuUrl, map => { map.Column("MENU_URL"); });
            Property<string>(x => x.MenuIcon, map => { map.Column("MENU_ICON"); });
            Property<bool>(x => x.Active, map => { map.Column("ACTIVE"); });

            Property<DateTime?>(x => x.ChangedDate, map => { map.Column("CHANGED_DATE");});

            Property<DateTime?>(x => x.CreatedDate, map => { map.Column("CREATED_DATE");
                map.Update(false);
                });

            Property<string>(x => x.ChangedBy, map => {
                    map.Column("CHANGED_BY");
                });

            Property<string>(x => x.CreatedBy, map => {
                    map.Column("CREATED_BY");
                    map.Update(false);
                });

            Bag(x => x.ChildMenu, bag => {
                bag.Inverse(true); // Is collection inverse?
                bag.Cascade(Cascade.None); //set cascade strategy
                bag.Lazy(CollectionLazy.NoLazy);
                bag.Key(k => k.Column(col => col.Name("PARENT_MENU_ID"))); //foreign key in Comment table
                bag.OrderBy("MENU_ORDER");
            }, a => a.OneToMany());

        }
    }
}
