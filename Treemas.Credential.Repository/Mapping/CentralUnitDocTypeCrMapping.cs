﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using Treemas.Credential.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class CentralUnitDocTypeCrMapping : ClassMapping<CentralUnitDocTypeCr>
    {
        public CentralUnitDocTypeCrMapping()
        {
            this.Table("SEC_CENTRALUNITDOCTYPE_CR");
            Id<long>(x => x.Id,
                map =>
                {
                    map.Column("ID");
                    map.Generator(Generators.Identity);
                });

            Property<string>(x => x._Application,
                map =>
                {
                    map.Column("APPLICATION");
                    map.Update(false);
                });

            Property<string>(x => x.DocId,
                map =>
                {
                    map.Column("DOC_ID");
                    map.Update(false);
                });

            Property<string>(x => x.Name,
                map =>
                {
                    map.Column("NAME");
                });

            Property<string>(x => x.Alias,
                map =>
                {
                    map.Column("ALIAS");
                });

            Property<string>(x => x.CreatedBy,
                map =>
                {
                    map.Column("CREATED_BY");
                    map.Update(false);
                });

            Property<DateTime?>(x => x.CreatedDate,
                map =>
                {
                    map.Column("CREATED_DATE");
                    map.Update(false);
                });

            Property<string>(x => x.ChangedBy,
                map =>
                {
                    map.Column("CHANGED_BY");
                });

            Property<DateTime?>(x => x.ChangedDate,
               map =>
               {
                   map.Column("CHANGED_DATE");
               });

            Property<bool>(x => x.Approved, map => { map.Column("APPROVED"); });

            Property<string>(x => x.ApprovedBy, map => { map.Column("APPROVED_BY"); });

            Property<DateTime?>(x => x.ApprovedDate, map => { map.Column("APPROVED_DATE"); });

            Property<string>(x => x.ApprovalType, map => { map.Column("APPROVAL_TYPE"); });

            Property<string>(x => x.ApprovalStatus, map => { map.Column("APPROVAL_STATUS"); });
        }
    }
}
