﻿using Treemas.Base.Utilities;
using Treemas.Base.Configuration;
using UOBISecurity;

namespace Treemas.Credential.Repository
{
    public class SystemConnection : IConnection
    {
        private ConnectionParam connectionSetting;
        public SystemConnection()
        {
            connectionSetting = new ConnectionParam();
            IConfigurationBinder binder = SystemConfigurationCabinet.Instance.GetBinder("Database");
            CompositeConfigurationItem items = (CompositeConfigurationItem)binder.GetConfiguration("System");

            Encryptor enc = new Encryptor();
            ApplicationRegistryHandler appReg = new ApplicationRegistryHandler();

            string keyReg;
            string appName = items.GetItem("AppName").Value;
            string uobikey = items.GetItem("UOBIKey").Value;
            string config = items.GetItem("DBConfig").Value;
            appName = appName.Trim().Replace(" ", "");
            //keyReg = "409E95B50611DA5D3F13AD0DD3B676CB";
            keyReg = appReg.ReadFromRegistry(@"Software\" + appName, "Key");
            var _ConDetails = enc.Decrypt(config, keyReg, uobikey);

            //var _dec = enc.Encrypt("AppName=UOBSVS;Server=.\\SQLEXPRESS2016;DBName=SVS;UserID=tms;Password=tr33m4s!;Timeout=0", keyReg, uobikey);

            //var _dec = enc.Encrypt("AppName=UOBSVS;Server=IDITG12030;DBName=SVS;UserID=tms;Password=tr33m4s!;Timeout=0", keyReg, uobikey);

            //var _enc = enc.Encrypt("AppName=UOBKTPInquiry;Server=ntdkcsqliduv1;DBName=Dukcapil2;UserID=id.app.dukcapil;Password=P@ssw0rd1;Timeout=0", keyReg, uobikey);
            //var _dec = enc.Decrypt("GtHt1INQOaEZKC2l8C4ODpOjycUXxvFK5x+UfjpPH44GrFFT+fRTzoKaS++QE5s25F0ZkQEhAfOElyxurTPRORlUfhdAlN+a574nkDnyCCZfWYFjSMQVIfcryQG1ODype1zSMhqxuevEh+L81Vy7CA==", keyReg, uobikey);
            //var _enc = enc.Encrypt("AppName=UOBSVS;Server=localhost;DBName=SVS;UserID=sa;Password=seni2221;Timeout=0", keyReg, uobikey);
            //var _enc1 = enc.Encrypt("AppName=UOBSVS;Server=LAPTOP-8N7447UC;DBName=SVS;UserID=sa;Password=seni2221;Timeout=0", keyReg, uobikey);

            string[] dbsetting = _ConDetails.Split(';');
            string[] dbsettingItem = dbsetting[1].Split('=');

            //ConfigurationItem item = items.GetItem("DBInstance");
            connectionSetting.DBInstance = dbsettingItem[1];

            //item = items.GetItem("DBUser");
            dbsettingItem = dbsetting[3].Split('=');
            connectionSetting.DBUser = dbsettingItem[1];

            //item = items.GetItem("DBPassword");
            dbsettingItem = dbsetting[4].Split('=');
            connectionSetting.DBPassword = dbsettingItem[1];

            //item = items.GetItem("DBName");
            dbsettingItem = dbsetting[2].Split('=');
            connectionSetting.DBName = dbsettingItem[1];
        }
        public ConnectionParam GetConnectionSetting()
        {
            return connectionSetting;
        }
    }
}
