﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;

namespace Treemas.Credential.Repository
{
    public class ParameterRepository : RepositoryControllerString<Parameter>, IParameterRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public ParameterRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public Parameter getParameter()
        {
            return transact(() => session.QueryOver<Parameter>().Take(1).SingleOrDefault());
        }
        public bool IsDuplicate(string ID)
        {
            int rowCount = transact(() => session.QueryOver<Parameter>()
                                                .Where(f => f.UserId == ID).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public bool InChangesRequest(string ParamID)
        {
            int rowCount = transact(() => session.QueryOver<ParameterHistory>()
                                                .Where(f => f.ApprovalStatus == ApprovalStatusState.R.ToString() &&
                                                        f.ParamId == ParamID).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public ParameterHistory GetChangesRequest()
        {
            return transact(() => session.QueryOver<ParameterHistory>().Where(f => f.ApprovalStatus == ApprovalStatusState.R.ToString()).Take(1).SingleOrDefault());
        }
        public void AddHistory(ParameterHistory item)
        {
            try
            {
                transact(() => session.Save(item));
            }
            catch (Exception e)
            {
                throw new RepositoryException(e.Message, e);
            }
        }
        public void SaveHistory(ParameterHistory item)
        {
            try
            {
                transact(() => session.Update(item));
            }
            catch (Exception e)
            {
                throw new RepositoryException(e.Message, e);
            }
        }
    }
}
