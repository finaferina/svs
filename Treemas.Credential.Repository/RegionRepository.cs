﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;
using NHibernate.Criterion;

namespace Treemas.Credential.Repository
{
    public class RegionRepository : RepositoryController<Region>, IRegionRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public RegionRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public Region getRegion(long id)
        {
            return transact(() => session.QueryOver<Region>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }
        public IList<Region> getApplicationRegion(string application)
        {
            IEnumerable<Region> _applicationRegion = this.FindAll(new AdHocSpecification<Region>(s => s._Application == application));
            return _applicationRegion.ToList();
        }

        public bool IsDuplicate(string application, string Regionid)
        {
            int rowCount = transact(() => session.QueryOver<Region>()
                                                .Where(f => f.RegionId == Regionid && f._Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public bool IsDuplicateCR(string application, string Regionid, string approval_status = "N")
        {
            int rowCount = transact(() => session.QueryOver<RegionCr>()
                                                .Where(f => f.RegionId == Regionid && f._Application == application && f.ApprovalStatus == approval_status).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public bool IsDuplicateByAlias(string application, string Alias)
        {
            int rowCount = transact(() => session.QueryOver<Region>()
                                                .Where(f => f.Alias == Alias && f._Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public bool IsDuplicateCRByAlias(string application, string Alias, string approval_status)
        {
            int rowCount = transact(() => session.QueryOver<RegionCr>()
                                                .Where(f => f.Alias == Alias && f._Application == application && f.ApprovalStatus == approval_status).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public PagedResult<Region> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<Region> paged = new PagedResult<Region>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<Region>()
                                                     .OrderByDescending(GetOrderByExpression<Region>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<Region>()
                                                     .OrderBy(GetOrderByExpression<Region>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<Region> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            PagedResult<Region> paged = new PagedResult<Region>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<Region>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<Region>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }


            paged.TotalItems = transact(() =>
                         session.QueryOver<Region>()
                                .Where(specification)
                                .RowCount());
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public Region getRegion(string id)
        {
            return transact(() => session.QueryOver<Region>()
                                                 .Where(f => f.RegionId == id).SingleOrDefault());
        }

        public IList<Region> FindForExport(string searchColumn, string searchValue)
        {
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            return transact(() =>
                          session.QueryOver<Region>()
                                 .Where(specification)
                                 .List());
        }

        public void UpdateRegion(long id, string regionid, string name, string alias, string changedby, DateTime changeddate)
        {
            string hqlSyntax = "update Region set RegionId = :REGIONID, Name = :NAME, Alias = :ALIAS, ChangedBy = :CHANGEDBY, ChangedDate = :CHANGEDDATE where Id = :id";
            session.CreateQuery(hqlSyntax)
                .SetString("REGIONID", regionid)
                .SetString("NAME", name)
                .SetString("ALIAS", alias)
                .SetString("CHANGEDBY", changedby)
                .SetDateTime("CHANGEDDATE", changeddate)
                .SetInt64("id", id)
                .ExecuteUpdate();
        }

    }
}
