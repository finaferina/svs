﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using NHibernate.Linq;
using Treemas.Base.Repository;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;
using NHibernate.Criterion;

namespace Treemas.Credential.Repository
{
    public class DivisionRepository : RepositoryController<Division>, IDivisionRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public DivisionRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public Division getDivision(long id)
        {
            return transact(() => session.QueryOver<Division>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }
        public IList<Division> getApplicationDivision(string application)
        {
            IEnumerable<Division> _applicationDivision = this.FindAll(new AdHocSpecification<Division>(s => s._Application == application));
            return _applicationDivision.ToList();
        }

        public bool IsDuplicate(string application, string Divisionid)
        {
            int rowCount = transact(() => session.QueryOver<Division>()
                                                .Where(f => f.DivisionId == Divisionid && f._Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public bool IsDuplicateByAlias(string application, string Alias)
        {
            int rowCount = transact(() => session.QueryOver<Division>()
                                                .Where(f => f.Alias == Alias && f._Application == application).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public bool IsDuplicateCR(string application, string Divisionid, string approval_status = "N")
        {
            int rowCount = transact(() => session.QueryOver<DivisionCr>()
                                                .Where(f => f.DivisonId == Divisionid && f._Application == application && f.ApprovalStatus == approval_status).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public bool IsDuplicateCRByAlias(string application, string Alias, string approval_status)
        {
            int rowCount = transact(() => session.QueryOver<DivisionCr>()
                                                .Where(f => f.Alias == Alias && f._Application == application && f.ApprovalStatus == approval_status).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public PagedResult<Division> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<Division> paged = new PagedResult<Division>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<Division>()
                                                     .OrderByDescending(GetOrderByExpression<Division>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<Division>()
                                                     .OrderBy(GetOrderByExpression<Division>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<Division> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            PagedResult<Division> paged = new PagedResult<Division>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<Division>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<Division>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }


            paged.TotalItems = transact(() =>
                         session.QueryOver<Division>()
                                .Where(specification)
                                .RowCount());
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public Division getDivision(string id)
        {
            return transact(() => session.QueryOver<Division>()
                                                 .Where(f => f.DivisionId == id).SingleOrDefault());
        }
        public IList<Division> getDivisionbyRegion(string Region)
        {
            SimpleExpression specification;
            specification = Restrictions.Like("Region", Region, MatchMode.Anywhere);

            return transact(() =>
                          session.QueryOver<Division>()
                                 .Where(specification)
                                 .List());
        }

        public IList<Division> FindForExport(string searchColumn, string searchValue)
        {
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            return transact(() =>
                          session.QueryOver<Division>()
                                 .Where(specification)
                                 .List());
        }

        public void UpdateDivision(long id, string divisionid, string name, string alias, string region, string changedby, DateTime changeddate)
        {
            string hqlSyntax = "update Division set DivisionId = :DIVISIONID, Name = :NAME, Alias = :ALIAS, Region = :REGION, ChangedBy = :CHANGEDBY, ChangedDate = :CHANGEDDATE  where Id = :id";
            session.CreateQuery(hqlSyntax)
                .SetString("DIVISIONID", divisionid)
                .SetString("NAME", name)
                .SetString("ALIAS", alias)
                .SetString("REGION", region)
                .SetString("CHANGEDBY", changedby)
                .SetDateTime("CHANGEDDATE", changeddate)
                .SetInt64("id", id)
                .ExecuteUpdate();
        }

        public bool IsFound(string region)
        {
            int rowCount = transact(() => session.QueryOver<Division>()
                                                .Where(f => f.Region == region).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
    }
}
