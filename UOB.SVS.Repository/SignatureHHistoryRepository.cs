﻿using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using UOB.SVS.Model;
using UOB.SVS.Repository;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System;
using Treemas.Base.Utilities;
using NHibernate.Transform;
using Treemas.Credential.Model;

namespace UOB.SVS.Repository
{
    public class SignatureHHistoryRepository : RepositoryController<SignatureHHistory>, ISignatureHHistoryRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public SignatureHHistoryRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public SignatureHHistory getSignatureHHistory(string id)
        {
            return transact(() => session.QueryOver<SignatureHHistory>()
                                                .Where(f => f.AccountNo == id).SingleOrDefault());
        }

        public SignatureHHistory getSignatureHHistoryCreate(string id)
        {
            string sql = "SELECT TOP 1 RecordId, AccountNo, AccountName, AccountType, CIFNumber, BranchCode, Note, RequestType, RequestDate, RequestUser, RequestReason, ApproveBy, ApproveDate, RejectBy, RejectDate, RejectReason " +
                "FROM dbo.SIGNATURE_HEADER_HISTORY " +
                "WHERE AccountNo = '" + id + "' AND RequestType = 'C' " +
                "ORDER BY RecordId DESC";
            NHibernate.IQuery myQuery = session.CreateSQLQuery(sql).SetResultTransformer(Transformers.AliasToBean<SignatureHHistory>());

            SignatureHHistory hist = new SignatureHHistory(0L);
            IList<SignatureHHistory> hists = myQuery.List<SignatureHHistory>();
            if (hists.Count > 0)
                hist = hists[0];
            return hist;
        }
        public IList<SignatureHHistory> getSignatureHHistorys(IList<string> SignatureHHistoryIds)
        {
            return transact(() =>
                   session.QueryOver<SignatureHHistory>()
                          .WhereRestrictionOn(val => val.AccountNo)
                          .IsIn(SignatureHHistoryIds.ToArray()).List());
        }
        public bool IsDuplicate(string accno)
        {
            int rowCount = transact(() => session.QueryOver<SignatureHHistory>()
                                                .Where(f =>f.AccountNo == accno).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<SignatureHHistory> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<SignatureHHistory> paged = new PagedResult<SignatureHHistory>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<SignatureHHistory>()
                                                     .OrderByDescending(GetOrderByExpression<SignatureHHistory>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<SignatureHHistory>()
                                                     .OrderBy(GetOrderByExpression<SignatureHHistory>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<SignatureHHistory> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, DateTime start, DateTime end, User user, string[] subBranch)
        {
            ICriterion specification;
            Conjunction conjuction = Restrictions.Conjunction();

            if (!searchValue.IsNullOrEmpty())
            {
                specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
                conjuction.Add(specification);
            }
            else
            {
                specification = Restrictions.IsNotEmpty("RecordId");
            }

            if (start != DateTime.MinValue && end != DateTime.MinValue)
            {
                ICriterion specification2 = Restrictions.Between("RequestDate", start, end.AddDays(1));
                conjuction.Add(specification2);
            }

            PagedResult<SignatureHHistory> paged = new PagedResult<SignatureHHistory>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                if (user.BranchCode != "000")
                {
                    paged.Items = transact(() => session.QueryOver<SignatureHHistory>()
                                                       .Where(conjuction)
                                                       .And(x => x.BranchCode.IsIn(subBranch))
                                                       .And(s => s.RequestType == "P")
                                                       .OrderBy(Projections.Property(orderColumn)).Asc
                                                       .Skip((pageNumber) * itemsPerPage)
                                                       .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() => session.QueryOver<SignatureHHistory>().Where(conjuction).And(s => s.RequestType == "P")
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
                }
            }
            else
            {
                if (user.BranchCode != "000")
                {
                    paged.Items = transact(() => session.QueryOver<SignatureHHistory>()
                                                       .Where(conjuction)
                                                       .And(x => x.BranchCode.IsIn(subBranch))
                                                       .And(s => s.RequestType == "P")
                                                       .OrderBy(Projections.Property(orderColumn)).Desc
                                                       .Skip((pageNumber) * itemsPerPage)
                                                       .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() => session.QueryOver<SignatureHHistory>().Where(conjuction).And(s => s.RequestType == "P")
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
                }
            }
            if (user.BranchCode != "000")
            {
                paged.TotalItems = transact(() => session.QueryOver<SignatureHHistory>()
                                                        .Where(conjuction)
                                                        .And(x => x.BranchCode.IsIn(subBranch))
                                                        .And(s => s.RequestType == "P").RowCount());
            }
            else
            {
                paged.TotalItems = transact(() => session.QueryOver<SignatureHHistory>().Where(conjuction).And(s => s.RequestType == "P").RowCount());
            }
            return paged;
        }
        public PagedResult<SignatureHHistory> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<SignatureHHistory> specification;

            switch (searchColumn)
            {
                case "AccountNumber":
                    specification = new AdHocSpecification<SignatureHHistory>(s => s.AccountNo.Contains(searchValue));
                    break;
                case "AccountName":
                    specification = new AdHocSpecification<SignatureHHistory>(s => s.AccountName.Contains(searchValue));
                    break;
                case "Note":
                    specification = new AdHocSpecification<SignatureHHistory>(s => s.Note.Contains(searchValue));
                    break;                
                default:
                    specification = new AdHocSpecification<SignatureHHistory>(s => s.AccountNo.Contains(searchValue));
                    break;
            }

            PagedResult<SignatureHHistory> paged = new PagedResult<SignatureHHistory>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<SignatureHHistory>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<SignatureHHistory>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<SignatureHHistory>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<SignatureHHistory>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<SignatureHHistory>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = Count;
            return paged;
        }
        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public IList<SignatureHHistory> FindforExport(string searchColumn, string searchValue, DateTime start, DateTime end)
        {
            Conjunction conjuction = Restrictions.Conjunction();
            ICriterion specification;
            if (!searchValue.IsNullOrEmpty())
            {
                specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
                conjuction.Add(specification);
            }
            if (start != DateTime.MinValue && end != DateTime.MinValue)
            {
                ICriterion specification2 = Restrictions.Between("RequestDate", start, end.AddDays(1));
                conjuction.Add(specification2);
            }
            return transact(() => session.QueryOver<SignatureHHistory>().Where(conjuction).And(s => s.RequestType == "P").List());
        }
    }
}
