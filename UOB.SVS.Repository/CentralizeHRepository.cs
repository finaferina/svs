﻿using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using UOB.SVS.Model;
using UOB.SVS.Repository;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System;
using Treemas.Base.Utilities;
using UOBISecurity;
using Treemas.Base.Configuration;
using NHibernate.Transform;
using System.Web.Configuration;
using UOB.SVS.Model.Helper;

namespace UOB.SVS.Repository
{
    public class CentralizeHRepository : RepositoryControllerString<CentralizeHeader>, ICentralizeHRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        Encryptor enc = new Encryptor();
        ApplicationRegistryHandler appReg = new ApplicationRegistryHandler();
        bool EncryptData = Convert.ToBoolean(WebConfigurationManager.AppSettings["EncryptData"]);

        string keyReg = "";
        string appName = "";
        string uobikey = "";

        public CentralizeHRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;

            IConfigurationBinder binder = SystemConfigurationCabinet.Instance.GetBinder("Database");
            CompositeConfigurationItem items = (CompositeConfigurationItem)binder.GetConfiguration("System");
            appName = items.GetItem("AppName").Value;
            uobikey = items.GetItem("UOBIKey").Value;

            appName = appName.Trim().Replace(" ", "");
            keyReg = appReg.ReadFromRegistry(@"Software\" + appName, "Key");
        }
        public CentralizeDDoc getDocDet(long Id)
        {
            return transact(() => session.QueryOver<CentralizeDDoc>().Where(f => f.Id == Id).SingleOrDefault());
        }
        public PagedResult<CentralizeHeader> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, CentralizeReqFilter filter)
        {
            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.NIK.IsNullOrEmpty())
            {
                if(filter.NIK != "All") { 
                    SimpleExpression NIK = Restrictions.Like("NIK", "%"+filter.NIK+"%");
                    conjuction.Add(NIK);
                }
            }
            if (!filter.Name.IsNullOrEmpty())
            {
                if (filter.Name != "All")
                {
                    SimpleExpression Name = Restrictions.Like("Name", "%"+filter.Name+"%");
                    conjuction.Add(Name);
                }
            }
            if (!filter.Titles.IsNullOrEmpty())
            {
                if (filter.Titles != "All")
                {
                    SimpleExpression Titles = Restrictions.Like("Titles", "%"+filter.Titles+ "%");
                    conjuction.Add(Titles);
                }
            }
            if (!filter.Region.IsNullOrEmpty())
            {
                if (filter.Region != "All")
                {
                    SimpleExpression Region = Restrictions.Eq("Region", filter.Region);
                    conjuction.Add(Region);
                }
            }
            if (!filter.Division.IsNullOrEmpty())
            {
                if (filter.Division != "All")
                {
                    SimpleExpression Division = Restrictions.Eq("Division", filter.Division);
                    conjuction.Add(Division);
                }
            }
            if (!filter.Departement.IsNullOrEmpty())
            {
                if (filter.Departement != "All")
                {
                    SimpleExpression Departement = Restrictions.Eq("Departement", filter.Departement);
                    conjuction.Add(Departement);
                }
            }

            PagedResult<CentralizeHeader> paged = new PagedResult<CentralizeHeader>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<CentralizeHeader>()
                                             .Where(conjuction)
                                             .OrderBy(Projections.Property(orderColumn)).Asc
                                             .Skip((pageNumber) * itemsPerPage)
                                             .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<CentralizeHeader>()
                                             .Where(conjuction)
                                             .OrderBy(Projections.Property(orderColumn)).Desc
                                             .Skip((pageNumber) * itemsPerPage)
                                             .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() => session.QueryOver<CentralizeHeader>()
                                            .Where(conjuction).RowCount());
            return paged;
        }

        public PagedResult<CentralizeHeader> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            Conjunction conjuction = Restrictions.Conjunction();

            PagedResult<CentralizeHeader> paged = new PagedResult<CentralizeHeader>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<CentralizeHeader>()
                                             .Where(conjuction)
                                             .OrderBy(Projections.Property(orderColumn)).Asc
                                             .Skip((pageNumber) * itemsPerPage)
                                             .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<CentralizeHeader>()
                                             .Where(conjuction)
                                             .OrderBy(Projections.Property(orderColumn)).Desc
                                             .Skip((pageNumber) * itemsPerPage)
                                             .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() => session.QueryOver<CentralizeHeader>()
                                            .Where(conjuction).RowCount());
            return paged;
        }

        public CentralizeHeader getCentralizeHeader(string id, string name, string titles, string region, string division, string departement)
        {
            return transact(() => session.QueryOver<CentralizeHeader>()
                                                .Where(f => f.NIK == id && f.Region == region
                                                            && f.Division == division && f.Departement == departement).SingleOrDefault());
        }

        public CentralizeHeader getCentralizeHeaderNik(string nik)
        {
            return transact(() => session.QueryOver<CentralizeHeader>()
                                                .Where(f => f.NIK == nik).SingleOrDefault());
        }

        public IList<CentralizeDDoc> getDocDet(string nik)
        {
            string sqlSelect = "SELECT ID,FileBlob=FILE_BLOB, NIK = NIK, FileType = FILE_TYPE, DocumentType = DOCUMENT_TYPE, Seq = FILE_SEQ " +
                "FROM dbo.CENTRALIZE_DETAIL_DOC " +
                "WHERE NIK = '" + nik + "' " +
                "ORDER BY FILE_SEQ";

            NHibernate.IQuery myQuery = session.CreateSQLQuery(sqlSelect).SetResultTransformer(Transformers.AliasToBean<CentralizeDDoc>());
            IList<CentralizeDDoc> docs = myQuery.List<CentralizeDDoc>();

            return docs;
        }

        public IList<CentralizeDSign> getSignDet(string nik)
        {
            IList<CentralizeDSign> req = transact(() => session.QueryOver<CentralizeDSign>().Where(f => f.NIK == nik && f.Signature != null).OrderBy(o => o.Seq).Asc.List());

            foreach (CentralizeDSign sign in req)
            {
                sign.Signature = sign.Signature;
            }

            return req;
        }
        public void DeleteCentralizeH(string ID)
        {
            string hqlDelete = "delete CentralizeHeader where NIK = :nik";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("nik", ID)
                    .ExecuteUpdate();
        }
        public void DeleteSignDet(string ID)
        {
            string hqlDelete = "delete CentralizeDSign where NIK = :nik";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("nik", ID)
                    .ExecuteUpdate();
        }
        public void DeleteDocDet(string ID)
        {
            string hqlDelete = "delete CentralizeDDoc where NIK = :nik";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("nik", ID)
                    .ExecuteUpdate();
        }
        public void AddSignDet(CentralizeDSign item)
        {
            transact(() => session.SaveOrUpdate(item));
        }
        public void AddDocDet(CentralizeDDoc item)
        {
            transact(() => session.SaveOrUpdate(item));
        }
        public CentralizeHeader getCentralizeHeader(string id)
        {
            return transact(() => session.QueryOver<CentralizeHeader>()
                                                .Where(f => f.NIK == id).SingleOrDefault());
        }

        public void UpdateCHeader(CentralizeHeader item)
        {
            transact(() => session.Update(item));
        }

        public void AddDocDet(SignatureDDoc item)
        {
            transact(() => session.SaveOrUpdate(item));
        }

        public void AddSignDet(SignatureDSign item)
        {
            transact(() => session.SaveOrUpdate(item));
        }
    }
}
