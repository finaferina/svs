﻿using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using UOB.SVS.Model;
using UOB.SVS.Repository;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System;

namespace UOB.SVS.Repository
{
    public class SvsRepository : RepositoryController<Svs>, ISvsRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public SvsRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public Svs getSvs(long id)
        {
            return transact(() => session.QueryOver<Svs>()
                                                .Where(f => f.ID == id).SingleOrDefault());
        }

        public IList<Svs> getSvss(IList<string> SvsIds)
        {
            return transact(() =>
                   session.QueryOver<Svs>()
                          .WhereRestrictionOn(val => val.ACCOUNT_NUMBER)
                          .IsIn(SvsIds.ToArray()).List());
        }
        public bool IsDuplicate(string accno, long id)
        {
            int rowCount = transact(() => session.QueryOver<Svs>()
                                                .Where(f => f.ID == id && f.ACCOUNT_NUMBER == accno).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<Svs> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<Svs> paged = new PagedResult<Svs>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<Svs>()
                                                     .OrderByDescending(GetOrderByExpression<Svs>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<Svs>()
                                                     .OrderBy(GetOrderByExpression<Svs>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<Svs> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<Svs> specification;

            switch (searchColumn)
            {
                case "Account Number":
                    specification = new AdHocSpecification<Svs>(s => s.ACCOUNT_NUMBER.Contains(searchValue));
                    break;
                case "Customer Name":
                    specification = new AdHocSpecification<Svs>(s => s.CUSTOMER_NAME.Contains(searchValue));
                    break;
                case "Note":
                    specification = new AdHocSpecification<Svs>(s => s.NOTE.Contains(searchValue));
                    break;                
                default:
                    specification = new AdHocSpecification<Svs>(s => s.ACCOUNT_NUMBER.Contains(searchValue));
                    break;
            }

            PagedResult<Svs> paged = new PagedResult<Svs>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<Svs>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<Svs>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<Svs>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<Svs>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<Svs>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = Count;
            return paged;
        }
        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

    }
}
