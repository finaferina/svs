﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Interface;
using UOB.SVS.Model;

namespace UOB.SVS.Repository
{
    public class BranchAccountPrefixRepository : RepositoryController<BranchAccountPrefix>, IBranchAccountPrefixRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public BranchAccountPrefixRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public int DeleteAccountPrefix(string BranchCode, string PrefixCode)
        {
            string hqlSyntax = "delete BranchAccountPrefix where BranchCode = :BranchCode and PrefixCode = :PrefixCode";
            return session.CreateQuery(hqlSyntax)
                    .SetString("BranchCode", BranchCode)
                    .SetString("PrefixCode", PrefixCode)
                    .ExecuteUpdate();
        }

        public PagedResult<BranchAccountPrefix> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            throw new NotImplementedException();
        }

        public PagedResult<BranchAccountPrefix> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string branchCode)
        {
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            PagedResult<BranchAccountPrefix> paged = new PagedResult<BranchAccountPrefix>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<BranchAccountPrefix>()
                                 .Where(specification).And(b => b.BranchCode == branchCode)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<BranchAccountPrefix>()
                                 .Where(specification).And(b => b.BranchCode == branchCode)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                         session.QueryOver<BranchAccountPrefix>()
                                .Where(specification).And(b => b.BranchCode == branchCode)
                                .RowCount());
            return paged;
        }

        public BranchAccountPrefix getAccountPrefix(long id)
        {
            return transact(() => session.QueryOver<BranchAccountPrefix>().Where(f => f.Id == id).SingleOrDefault());
        }

        public BranchAccountPrefix getAccountPrefix(string PrefixCode)
        {
            return transact(() => session.QueryOver<BranchAccountPrefix>().Where(f => f.PrefixCode == PrefixCode).SingleOrDefault());
        }

        public BranchAccountPrefix getAccountPrefix(string PrefixCode, string BranchCode)
        {
            return transact(() => session.QueryOver<BranchAccountPrefix>().Where(f => f.PrefixCode == PrefixCode && f.BranchCode == BranchCode).SingleOrDefault());
        }

        public IList<BranchAccountPrefix> getAccountPrefixes()
        {
            return transact(() => session.QueryOver<BranchAccountPrefix>().List());
        }

        public IList<BranchAccountPrefix> getAccountPrefixes(string BranchCode)
        {
            return transact(() => session.QueryOver<BranchAccountPrefix>().Where(f => f.BranchCode == BranchCode).List());
        }

        public IList<BranchAccountPrefix> getAccountPrefixes(List<string> Branches)
        {
            return transact(() => session.QueryOver<BranchAccountPrefix>().Where(val => val.BranchCode.IsIn(Branches.ToArray())).List());
        }

        public bool IsDuplicate(string PrefixCode)
        {
            int rowCount = transact(() => session.QueryOver<BranchAccountPrefix>()
                                                .Where(f => f.PrefixCode == PrefixCode).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
    }
}
