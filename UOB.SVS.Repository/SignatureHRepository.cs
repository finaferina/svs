﻿using System.Linq;
using System.Collections.Generic;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using UOB.SVS.Model;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System;
using Treemas.Base.Utilities;
using UOBISecurity;
using Treemas.Base.Configuration;
using NHibernate.Transform;
using System.Web.Configuration;
using System.Data;
using System.Reflection;
using System.Data.SqlClient;

namespace UOB.SVS.Repository
{
    public class SignatureHRepository : RepositoryControllerString<SignatureHeader>, ISignatureHRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        Encryptor enc = new Encryptor();
        ApplicationRegistryHandler appReg = new ApplicationRegistryHandler();
        bool EncryptData = Convert.ToBoolean(WebConfigurationManager.AppSettings["EncryptData"]);

        string keyReg = "";
        string appName = "";
        string uobikey = "";
        string connetionUOBencrypt = "";
        string connetionUOB = "";

        public SignatureHRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;

            IConfigurationBinder binder = SystemConfigurationCabinet.Instance.GetBinder("Database");
            CompositeConfigurationItem items = (CompositeConfigurationItem)binder.GetConfiguration("System");
            appName = items.GetItem("AppName").Value;
            uobikey = items.GetItem("UOBIKey").Value;
            connetionUOBencrypt = items.GetItem("DBConfig").Value;

            appName = appName.Trim().Replace(" ", "");
            keyReg = appReg.ReadFromRegistry(@"Software\" + appName, "Key");
            connetionUOB = enc.Decrypt(connetionUOBencrypt, keyReg, uobikey);
        }
        public SignatureHeader getSignatureHeader(string id)
        {
            return transact(() => session.QueryOver<SignatureHeader>()
                                                .Where(f => f.AccountNo == id).SingleOrDefault());
        }

        public IList<SignatureHeader> getSignatureHeaders(IList<string> SignatureHeaderIds)
        {
            return transact(() =>
                   session.QueryOver<SignatureHeader>()
                          .WhereRestrictionOn(val => val.AccountNo)
                          .IsIn(SignatureHeaderIds.ToArray()).List());
        }
        public bool IsDuplicate(string accno)
        {
            int rowCount = transact(() => session.QueryOver<SignatureHeader>()
                                                .Where(f =>f.AccountNo == accno).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<SignatureHeader> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey,SVSReqFilter filter)
        {
            //SimpleExpression tanggalStart = Restrictions.Ge("TanggalFPDO", filter.TanggalStart);
            //SimpleExpression tanggalEnd = Restrictions.Le("TanggalFPDO", filter.TanggalEnd);
            Conjunction conjuction = Restrictions.Conjunction();

            //if (!filter.TanggalStart.IsNull()) conjuction.Add(tanggalStart);
            //if (!filter.TanggalEnd.IsNull()) conjuction.Add(tanggalEnd);
            if (!filter.AccountNo.IsNullOrEmpty())
            {
                //SimpleExpression accNo = Restrictions.Like("AccountNo", filter.AccountNo, MatchMode.Start);
                SimpleExpression accNo = Restrictions.Eq("AccountNo", filter.AccountNo);
                conjuction.Add(accNo);
            }
            if (!filter.AccountName.IsNullOrEmpty())
            {
                //SimpleExpression accName = Restrictions.Like("AccountName", filter.AccountName, MatchMode.Anywhere);
                SimpleExpression accName = Restrictions.Eq("AccountName", filter.AccountName);
                conjuction.Add(accName);
            }
            if (!filter.CIFNumber.IsNullOrEmpty())
            {
                //SimpleExpression cifNumber = Restrictions.Like("CIFNumber", filter.CIFNumber, MatchMode.Anywhere);
                SimpleExpression cifNumber = Restrictions.Eq("CIFNumber", filter.CIFNumber);
                conjuction.Add(cifNumber);
            }
            if (!filter.Branches.IsNullOrEmpty())
            {
                conjuction.Add(Restrictions.In("BranchCode", filter.Branches.Select(x => x.BranchCode).ToArray()));
            }

            PagedResult<SignatureHeader> paged = new PagedResult<SignatureHeader>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<SignatureHeader>()
                                             .Where(conjuction)
                                             .OrderBy(Projections.Property(orderColumn)).Asc
                                             .Skip((pageNumber) * itemsPerPage)
                                             .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<SignatureHeader>()
                                             .Where(conjuction)
                                             .OrderBy(Projections.Property(orderColumn)).Desc
                                             .Skip((pageNumber) * itemsPerPage)
                                             .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() => session.QueryOver<SignatureHeader>()
                                            .Where(conjuction).RowCount());
            return paged;
        }
        public PagedResult<SignatureHeader> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<SignatureHeader> specification;

            switch (searchColumn)
            {
                case "AccountNo":
                    specification = new AdHocSpecification<SignatureHeader>(s => s.AccountNo.Contains(searchValue));
                    break;
                case "CIFNumber":
                    specification = new AdHocSpecification<SignatureHeader>(s => s.CIFNumber.Contains(searchValue));
                    break;               
                default:
                    specification = new AdHocSpecification<SignatureHeader>(s => s.AccountNo.Contains(searchValue));
                    break;
            }

            PagedResult<SignatureHeader> paged = new PagedResult<SignatureHeader>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<SignatureHeader>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<SignatureHeader>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<SignatureHeader>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<SignatureHeader>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<SignatureHeader>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = Count;
            return paged;
        }
        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public void DeleteSignatureH(string ID)
        {
            string hqlDelete = "delete SignatureHeader where AccountNo = :accountNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("accountNo", ID)
                    .ExecuteUpdate();
        }

        public void AddKTPDet(SignatureDKTP item)
        {
            //if (EncryptData)
            //{
            //    item.KTP = enc.Encrypt(item.KTP, keyReg, uobikey); //UOB security encrypt
            //}
            transact(() => session.SaveOrUpdate(item));
        }


        public IList<SignatureDKTP> getKTPDet(string accountNo)
        {
            IList<SignatureDKTP> req = transact(() => session.QueryOver<SignatureDKTP>().Where(f => f.AccountNo == accountNo && f.KTP != null).OrderBy(o => o.Seq).Asc.List());

            foreach (SignatureDKTP ktp in req)
            {
                //try
                //{
                //    ktp.KTP = enc.Decrypt(ktp.KTP, keyReg, uobikey); //UOB security encrypt
                //}
                //catch
                //{
                 ktp.KTP = ktp.KTP;
                //}
            }
            return req;
        }

        public void DeleteKTPDet(string ID)
        {
            string hqlDelete = "delete SignatureDKTP where AccountNo = :accountNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("accountNo", ID)
                    .ExecuteUpdate();
        }

        public void AddSignDet(SignatureDSign item)
        {
            //if (EncryptData)
            //{
            //    item.Signature = enc.Encrypt(item.Signature, keyReg, uobikey); //UOB security encrypt
            //}
            transact(() => session.SaveOrUpdate(item));
        }

        public IList<SignatureDSign> getSignDet(string accountNo)
        {
            IList<SignatureDSign> req = transact(() => session.QueryOver<SignatureDSign>().Where(f => f.AccountNo == accountNo && f.Signature != null).OrderBy(o => o.Seq).Asc.List());

            foreach (SignatureDSign sign in req)
            {
                //try
                //{
                //    sign.Signature = enc.Decrypt(sign.Signature, keyReg, uobikey); //UOB security encrypt
                //}
                //catch
                //{
                 sign.Signature = sign.Signature;
                //}
            }

            return req;
        }

        public void DeleteSignDet(string ID)
        {
            string hqlDelete = "delete SignatureDSign where AccountNo = :accountNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("accountNo", ID)
                    .ExecuteUpdate();
        }

        public void UpdateSgHeader(SignatureHeader item)
        {
            transact(() => session.Update(item));

            //string sqlQ = "update SignatureHeader set AccountName = :accountName" +
            //    ",AccountType = :accountType" +
            //    ",CIFNumber = :cifNumber" +
            //    ",BranchCode = :branchCode " +
            //    ",Note = :note " +
            //    "where AccountNo = :accountNo";
            //int deletedEntities = session.CreateQuery(sqlQ)
            //        .SetString("accountNo", (item.AccountNo.IsNullOrEmpty() ? "" : item.AccountNo))
            //        .SetString("accountName", item.AccountName)
            //        .SetString("accountType", item.AccountType)
            //        .SetString("cifNumber", item.CIFNumber)
            //        .SetString("branchCode", item.BranchCode)
            //        .SetString("note", (item.Note.IsNullOrEmpty()? "" : item.Note))
            //        .ExecuteUpdate();
        }

        public void UpdateSignatureDetail(string AccountNo
            , IList<SignatureDDoc> docs
            , IList<SignatureDKTP> ktps
            , IList<SignatureDSign> signs) {
            
            DataTable dtDosc = ToDataTable(docs);
            DataTable dtktps = ToDataTable(ktps);
            DataTable dtsigns = ToDataTable(signs);
            //---------------------------------------------------
            //connetionUOB = connetionUOB.Replace("AppName", "Application Name");
            //connetionUOB = connetionUOB.Replace("DBName", "Database");
            //connetionUOB = connetionUOB.Replace("UserID", "User Id");
            //----------------------------------------------------

            //AppName = UOBSVS; Server = ANDINID; DBName = SVS; UserID = sa; Password = Rajawal1db; Timeout = 0
            //string ConStr = "Application Name = UOBSVS; Server = ANDINID; Database = SVS; User ID = sa; Password = Rajawal1db; Timeout = 0";
            //connetionUOB = "Data Source=ANDINID;Initial Catalog = SVS; User ID = sa; Password = Rajawal1db";
            connetionUOB = connetionUOB.Replace("AppName=UOBSVS;", "");
            connetionUOB = connetionUOB.Replace("Server", "Data Source");
            connetionUOB = connetionUOB.Replace("DBName", "Initial Catalog");
            connetionUOB = connetionUOB.Replace("UserID", "User Id");
            connetionUOB = connetionUOB.Replace("Timeout=0", "");

            SqlConnection conn = new SqlConnection(connetionUOB);
            try
            {
                //code
                conn.Open();
                SqlCommand cmdProc = new SqlCommand("spUpdateSignatureDetail", conn);
                cmdProc.CommandType = CommandType.StoredProcedure;
                cmdProc.Parameters.AddWithValue("@Account_No", AccountNo);
                cmdProc.Parameters.AddWithValue("@dtDosc", dtDosc);
                cmdProc.Parameters.AddWithValue("@dtktps", dtktps);
                cmdProc.Parameters.AddWithValue("@dtsigns", dtsigns);
                cmdProc.ExecuteNonQuery();
                //strMsg = "Saved successfully.";

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                //conn.Close();
                conn.Dispose();
            }
        }
        public void UpdateDocDet(SignatureDDoc item)
        {
            transact(() => session.SaveOrUpdate(item));
        }
        public void UpdateDocListDet(IList<SignatureDDoc> item)
        {
            //string sqlQ = "update SignatureDDoc set FileBlob = :fileBlob" +
            //   ",FileType = :fileType" +
            //   ",DocumentType = :documentType " +
            //   "where AccountNo = :accountNo and Seq = :seq";
            //int deletedEntities = session.CreateQuery(sqlQ)
            //        .SetString("accountNo", item.AccountNo)
            //        .SetInt32("seq", item.Seq)
            //        .SetString("fileBlob", item.FileBlob)
            //        .SetString("fileType", item.FileType)
            //        .SetString("documentType", item.DocumentType)
            //        .ExecuteUpdate();

            DataTable dt = ToDataTable(item);

            //using (SqlConnection conn = new SqlConnection(connetionUOB))
            //AppName = UOBSVS; Server = ANDINID; DBName = SVS; UserID = sa; Password = Rajawal1db; Timeout = 0
            //using (SqlConnection conn = new SqlConnection("Server=ANDINID;Database=SVS;User Id=sa;Password=Rajawal1db;"))
            //{
            //    conn.Open();
            //    //fnConOpen();//Function for opening connection
            //    SqlCommand cmdProc = new SqlCommand("spEmpDetails", conn);
            //    cmdProc.CommandType = CommandType.StoredProcedure;
            //    cmdProc.Parameters.AddWithValue("@Type", "InsertDetails");
            //    cmdProc.Parameters.AddWithValue("@Details", dt);
            //    cmdProc.ExecuteNonQuery();
            //    //strMsg = "Saved successfully.";
            //    conn.Close();
            //}

            SqlConnection conn = new SqlConnection("Server=ANDINID;Database=SVS;User Id=sa;Password=Rajawal1db;");
            try
            {
                //code
                conn.Open();
                SqlCommand cmdProc = new SqlCommand("spDocUpd", conn);
                cmdProc.CommandType = CommandType.StoredProcedure;
                cmdProc.Parameters.AddWithValue("@Details", dt);
                cmdProc.ExecuteNonQuery();
                //strMsg = "Saved successfully.";
                
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
            finally
            {
                //conn.Close();
                conn.Dispose();
            }
        }
        public void UpdateKTPDet(SignatureDKTP item)
        {
            transact(() => session.SaveOrUpdate(item));
        }
        public void UpdateKTPListDet(IList<SignatureDKTP> item)
        {
            //string sqlQ = "update SignatureDKTP set KTP = :ktp" +
            //    ",ImageType = :imageType " +
            //    "where AccountNo = :accountNo and Seq = :seq";
            //int deletedEntities = session.CreateQuery(sqlQ)
            //        .SetString("accountNo", item.AccountNo)
            //        .SetInt32("seq", item.Seq)
            //        .SetString("ktp", item.KTP)
            //        .SetString("imageType", item.ImageType)
            //        .ExecuteUpdate();
            //transact(() => session.Update(item));
        }

        public void UpdateSignDet(SignatureDSign item)
        {
            transact(() => session.Update(item));
        }
        public void UpdateSignListDet(IList<SignatureDSign> item)
        {
            //string sqlQ = "update SignatureDSign set SIGNATURE = :signature" +
            //  ",ImageType = :imageType" +
            //  " where AccountNo = :accountNo and Seq = :seq";
            //int deletedEntities = session.CreateQuery(sqlQ)
            //        .SetString("accountNo", item.AccountNo)
            //        .SetInt32("seq", item.Seq)
            //        .SetString("signature", item.Signature)
            //        .SetString("imageType", item.ImageType)
            //        .ExecuteUpdate();
            //transact(() => session.Update(item));
        }

        public void AddDocDet(SignatureDDoc item)
        {
            //if (EncryptData)
            //{
               
            //    item.FileBlob = enc.Encrypt(item.FileBlob, keyReg, uobikey); //UOB security encrypt
            //}
            transact(() => session.SaveOrUpdate(item));
        }

        public IList<SignatureDDoc> getDocDet(string accountNo)
        {
            string sqlSelect = "SELECT ID, AccountNo = ACCOUNT_NUMBER, FileType = FILE_TYPE, DocumentType = DOCUMENT_TYPE, Seq = FILE_SEQ " +
                "FROM dbo.SIGNATURE_DETAIL_DOC " +
                "WHERE ACCOUNT_NUMBER = '" + accountNo + "' " +
                "ORDER BY FILE_SEQ";

            NHibernate.IQuery myQuery = session.CreateSQLQuery(sqlSelect).SetResultTransformer(Transformers.AliasToBean<SignatureDDoc>());
            IList<SignatureDDoc> docs = myQuery.List<SignatureDDoc>();            

            return docs;
        }

        public void DeleteDocDet(string ID)
        {
            string hqlDelete = "delete SignatureDDoc where AccountNo = :accountNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("accountNo", ID)
                    .ExecuteUpdate();
        }

        public SignatureDDoc getDocDet(long Id)
        {
            return transact(() => session.QueryOver<SignatureDDoc>().Where(f => f.Id == Id).SingleOrDefault());
        }

        public DataTable ToDataTable<T>(IList<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                if (prop.Name != "Id")
                    dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length-1];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    if(Props[i].Name != "Id")
                        values[(i == Props.Length - 1) ? i-1:i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
    }
}
