﻿using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using UOB.SVS.Model;
using UOB.SVS.Repository;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System;
using NHibernate.Transform;

namespace UOB.SVS.Repository
{
    public class AccountPrefixCrRepository : RepositoryController<AccountPrefixCr>, IAccountPrefixCrRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;

        public AccountPrefixCrRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        //public PagedResult<AccountPrefixCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        //{
        //    throw new NotImplementedException();
        //}

        //public PagedResult<AccountPrefixCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        //{
        //    SimpleExpression specification;
        //    specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

        //    PagedResult<AccountPrefixCr> paged = new PagedResult<AccountPrefixCr>(pageNumber, itemsPerPage);
        //    if (orderKey.ToLower() == "asc")
        //    {
        //        paged.Items = transact(() =>
        //                  session.QueryOver<AccountPrefixCr>()
        //                         .Where(specification)
        //                         .OrderBy(Projections.Property(orderColumn)).Asc
        //                         .Skip((pageNumber) * itemsPerPage)
        //                         .Take(itemsPerPage).List());
        //    }
        //    else
        //    {
        //        paged.Items = transact(() =>
        //                  session.QueryOver<AccountPrefixCr>()
        //                         .Where(specification)
        //                         .OrderBy(Projections.Property(orderColumn)).Desc
        //                         .Skip((pageNumber) * itemsPerPage)
        //                         .Take(itemsPerPage).List());
        //    }


        //    paged.TotalItems = transact(() =>
        //                 session.QueryOver<AccountPrefixCr>()
        //                        .Where(specification)
        //                        .RowCount());
        //    return paged;
        //}

        //public PagedResult<AccountPrefixCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string branchCode)
        //{
        //    SimpleExpression specification;
        //    specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

        //    PagedResult<AccountPrefixCr> paged = new PagedResult<AccountPrefixCr>(pageNumber, itemsPerPage);
        //    string sql = "";
        //    string sqlorder = "";

        //    if (orderKey.ToLower() == "asc")
        //    { sqlorder = " ASC "; }
        //    else
        //    { sqlorder = " DESC "; }

        //    sql = "SELECT " +
        //        "   PrefixCode = ACCOUNT_PREFIX_CODE " +
        //        "   ,Description = DESCRIPTION " +
        //        "FROM BRANCH_ACCOUNT_PREFIX " +
        //        "WHERE BRANCH_CODE = '" + branchCode + "' AND ACCOUNT_PREFIX_CODE LIKE '" + searchValue + "%' " +
        //        "UNION ALL " +
        //        "SELECT " +
        //        "    PrefixCode = ACCOUNT_PREFIX_CODE " +
        //        "    ,Description = DESCRIPTION " +
        //        "FROM ACCOUNT_PREFIX " +
        //        "WHERE ACCOUNT_PREFIX_CODE NOT IN (SELECT ACCOUNT_PREFIX_CODE FROM BRANCH_ACCOUNT_PREFIX) " +
        //        //"AND ACCOUNT_PREFIX_CODE NOT IN (SELECT ACCOUNT_PREFIX_CODE FROM BRANCH_ACCOUNT_PREFIX_CR WHERE APPROVED = '0' AND APPROVAL_STATUS = 'N') " +
        //        "AND ACCOUNT_PREFIX_CODE LIKE '%' " +
        //        "order by " + Projections.Property(orderColumn).ToString() + sqlorder +
        //        " OFFSET " + pageNumber * 10 + " ROWS " +
        //        "FETCH NEXT " + itemsPerPage + " ROWS ONLY";

        //    NHibernate.IQuery myQuery = session.CreateSQLQuery(sql).SetResultTransformer(Transformers.AliasToBean<AccountPrefixCr>());
        //    paged.Items = myQuery.List<AccountPrefixCr>().Take(itemsPerPage);
        //    paged.TotalItems = myQuery.List<AccountPrefixCr>().Count;
        //    paged.PageNumber = pageNumber;

        //    return paged;
        //}

        public AccountPrefixCr getAccountPrefixCr(long id)
        {
            return transact(() => session.QueryOver<AccountPrefixCr>().Where(f => f.Id == id).SingleOrDefault());
        }

        public AccountPrefixCr getAccountPrefixCr(string PrefixCode)
        {
            return transact(() => session.QueryOver<AccountPrefixCr>().Where(f => f.PrefixCode == PrefixCode).SingleOrDefault());
        }

        public IList<AccountPrefixCr> getAccountPrefixCres()
        {
            return transact(() => session.QueryOver<AccountPrefixCr>().List());
        }

        public bool IsDuplicate(string PrefixCode)
        {
            int rowCount = transact(() => session.QueryOver<AccountPrefixCr>()
                                                .Where(f => f.PrefixCode == PrefixCode).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public AccountPrefixCr getExistingRequest(string ApprovalType, string PrefixCode)
        {
            return transact(() => session.QueryOver<AccountPrefixCr>().Where(f => f.PrefixCode == PrefixCode && f.ApprovalType == ApprovalType && f.ApprovalStatus == "N").SingleOrDefault());
        }
    }
}
