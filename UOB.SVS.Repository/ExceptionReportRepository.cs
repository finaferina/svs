﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Treemas.Base.Repository;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using Treemas.Credential.Model;
using UOB.SVS.Interface;
using UOB.SVS.Model;

namespace UOB.SVS.Repository
{
    public class ExceptionReportRepository : RepositoryController<ExceptionReport>, IExceptionReportRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public ExceptionReportRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public IList<ExceptionReport> FindAllExport(string searchColumn, string searchValue, string reportType, DateTime start, DateTime end, User user, string[] subBranch)
        {
            ICriterion specification;
            Conjunction conjuction = Restrictions.Conjunction();

            if (!searchValue.IsNullOrEmpty())
            {
                specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
                conjuction.Add(specification);
            }

            if (reportType == "P")
            {
                if (start != DateTime.MinValue && end != DateTime.MinValue)
                {
                    ICriterion specification2 = Restrictions.Between("ActionDate", start, end.AddDays(1));
                    conjuction.Add(specification2);
                }
            }

            ICriterion specification3 = Restrictions.Eq("RequestType", reportType);
            conjuction.Add(specification3);

            if (user.BranchCode != "000")
            {
                return transact(() => session.QueryOver<ExceptionReport>()
                                                   .Where(conjuction)
                                                   .And(x => x.BranchCode.IsIn(subBranch)).List());
            }
            else
            {
                return transact(() => session.QueryOver<ExceptionReport>().Where(conjuction).List());
            }

        }

        public PagedResult<ExceptionReport> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string reportType, DateTime start, DateTime end, User user, string[] subBranch)
        {
            ICriterion specification;
            Conjunction conjuction = Restrictions.Conjunction();

            if (!searchValue.IsNullOrEmpty())
            {
                specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
                conjuction.Add(specification);
            }

            if (reportType == "P")
            {
                if (start != DateTime.MinValue && end != DateTime.MinValue)
                {
                    ICriterion specification2 = Restrictions.Between("ActionDate", start, end.AddDays(1));
                    conjuction.Add(specification2);
                }
            }

            ICriterion specification3 = Restrictions.Eq("RequestType", reportType);
            conjuction.Add(specification3);

            PagedResult<ExceptionReport> paged = new PagedResult<ExceptionReport>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                if (user.BranchCode != "000")
                {
                    paged.Items = transact(() => session.QueryOver<ExceptionReport>()
                                                       .Where(conjuction)
                                                       .And(x => x.BranchCode.IsIn(subBranch))
                                                       .OrderBy(Projections.Property(orderColumn)).Asc
                                                       .Skip((pageNumber) * itemsPerPage)
                                                       .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() => session.QueryOver<ExceptionReport>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
                }
            }
            else
            {
                if (user.BranchCode != "000")
                {
                    paged.Items = transact(() => session.QueryOver<ExceptionReport>()
                                                       .Where(conjuction)
                                                       .And(x => x.BranchCode.IsIn(subBranch))
                                                       .OrderBy(Projections.Property(orderColumn)).Desc
                                                       .Skip((pageNumber) * itemsPerPage)
                                                       .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() => session.QueryOver<ExceptionReport>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
                }
            }
            if (user.BranchCode != "000")
            {
                paged.TotalItems = transact(() => session.QueryOver<ExceptionReport>()
                                                        .Where(conjuction)
                                                        .And(x => x.BranchCode.IsIn(subBranch)).RowCount());
            }
            else
            {
                paged.TotalItems = transact(() => session.QueryOver<ExceptionReport>().Where(conjuction).RowCount());
            }
            return paged;
        }
        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
