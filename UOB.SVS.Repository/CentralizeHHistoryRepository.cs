﻿using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using UOB.SVS.Model;
using UOB.SVS.Repository;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System;
using Treemas.Base.Utilities;
using NHibernate.Transform;
using Treemas.Credential.Model;

namespace UOB.SVS.Repository
{
    public class CentralizeHHistoryRepository : RepositoryController<CentralizeHHistory>, ICentralizeHHistoryRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public CentralizeHHistoryRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public CentralizeHHistory getCentralizeHHistory(string id)
        {
            return transact(() => session.QueryOver<CentralizeHHistory>()
                                                .Where(f => f.NIK == id).SingleOrDefault());
        }

        public CentralizeHHistory getCentralizeHHistoryCreate(string id)
        {
            string sql = "SELECT TOP 1 RecordId, NIK, Name, Region, Division, Departement, Titles, Note, RequestType, RequestDate, RequestUser, RequestReason, ApproveBy, ApproveDate, RejectBy, RejectDate, RejectReason " +
                "FROM dbo.CENTRALIZE_HEADER_HISTORY " +
                "WHERE NIK = '" + id + "' AND RequestType = 'C' " +
                "ORDER BY RecordId DESC";
            NHibernate.IQuery myQuery = session.CreateSQLQuery(sql).SetResultTransformer(Transformers.AliasToBean<CentralizeHHistory>());

            CentralizeHHistory hist = new CentralizeHHistory(0L);
            IList<CentralizeHHistory> hists = myQuery.List<CentralizeHHistory>();
            if (hists.Count > 0)
                hist = hists[0];
            return hist;
        }
        public IList<CentralizeHHistory> getCentralizeHHistorys(IList<string> CentralizeHHistoryIds)
        {
            return transact(() =>
                   session.QueryOver<CentralizeHHistory>()
                          .WhereRestrictionOn(val => val.NIK)
                          .IsIn(CentralizeHHistoryIds.ToArray()).List());
        }
        public bool IsDuplicate(string nik)
        {
            int rowCount = transact(() => session.QueryOver<CentralizeHHistory>()
                                                .Where(f => f.NIK == nik).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<CentralizeHHistory> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<CentralizeHHistory> paged = new PagedResult<CentralizeHHistory>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<CentralizeHHistory>()
                                                     .OrderByDescending(GetOrderByExpression<CentralizeHHistory>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<CentralizeHHistory>()
                                                     .OrderBy(GetOrderByExpression<CentralizeHHistory>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<CentralizeHHistory> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, DateTime start, DateTime end, User user, string[] subBranch)
        {
            ICriterion specification;
            Conjunction conjuction = Restrictions.Conjunction();

            if (!searchValue.IsNullOrEmpty())
            {
                specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
                conjuction.Add(specification);
            }
            else
            {
                specification = Restrictions.IsNotEmpty("RecordId");
            }

            if (start != DateTime.MinValue && end != DateTime.MinValue)
            {
                ICriterion specification2 = Restrictions.Between("RequestDate", start, end.AddDays(1));
                conjuction.Add(specification2);
            }

            PagedResult<CentralizeHHistory> paged = new PagedResult<CentralizeHHistory>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                if (user.BranchCode != "000")
                {
                    paged.Items = transact(() => session.QueryOver<CentralizeHHistory>()
                                                       .Where(conjuction)
                                                       //.And(x => x.BranchCode.IsIn(subBranch))
                                                       .And(s => s.RequestType == "P")
                                                       .OrderBy(Projections.Property(orderColumn)).Asc
                                                       .Skip((pageNumber) * itemsPerPage)
                                                       .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() => session.QueryOver<CentralizeHHistory>().Where(conjuction).And(s => s.RequestType == "P")
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
                }
            }
            else
            {
                if (user.BranchCode != "000")
                {
                    paged.Items = transact(() => session.QueryOver<CentralizeHHistory>()
                                                       .Where(conjuction)
                                                       //.And(x => x.BranchCode.IsIn(subBranch))
                                                       .And(s => s.RequestType == "P")
                                                       .OrderBy(Projections.Property(orderColumn)).Desc
                                                       .Skip((pageNumber) * itemsPerPage)
                                                       .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() => session.QueryOver<CentralizeHHistory>().Where(conjuction).And(s => s.RequestType == "P")
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
                }
            }
            if (user.BranchCode != "000")
            {
                paged.TotalItems = transact(() => session.QueryOver<CentralizeHHistory>()
                                                        .Where(conjuction)
                                                        //.And(x => x.BranchCode.IsIn(subBranch))
                                                        .And(s => s.RequestType == "P").RowCount());
            }
            else
            {
                paged.TotalItems = transact(() => session.QueryOver<CentralizeHHistory>().Where(conjuction).And(s => s.RequestType == "P").RowCount());
            }
            return paged;
        }
        public PagedResult<CentralizeHHistory> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<CentralizeHHistory> specification;

            switch (searchColumn)
            {
                case "AccountNumber":
                    specification = new AdHocSpecification<CentralizeHHistory>(s => s.NIK.Contains(searchValue));
                    break;
                case "AccountName":
                    specification = new AdHocSpecification<CentralizeHHistory>(s => s.Name.Contains(searchValue));
                    break;
                case "Note":
                    specification = new AdHocSpecification<CentralizeHHistory>(s => s.Note.Contains(searchValue));
                    break;
                default:
                    specification = new AdHocSpecification<CentralizeHHistory>(s => s.NIK.Contains(searchValue));
                    break;
            }

            PagedResult<CentralizeHHistory> paged = new PagedResult<CentralizeHHistory>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<CentralizeHHistory>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<CentralizeHHistory>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<CentralizeHHistory>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<CentralizeHHistory>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<CentralizeHHistory>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = Count;
            return paged;
        }
        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public IList<CentralizeHHistory> FindforExport(string searchColumn, string searchValue, DateTime start, DateTime end)
        {
            Conjunction conjuction = Restrictions.Conjunction();
            ICriterion specification;
            if (!searchValue.IsNullOrEmpty())
            {
                specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
                conjuction.Add(specification);
            }
            if (start != DateTime.MinValue && end != DateTime.MinValue)
            {
                ICriterion specification2 = Restrictions.Between("RequestDate", start, end.AddDays(1));
                conjuction.Add(specification2);
            }
            return transact(() => session.QueryOver<CentralizeHHistory>().Where(conjuction).And(s => s.RequestType == "P").List());
        }
    }
}
