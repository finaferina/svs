﻿using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using UOB.SVS.Model;
using UOB.SVS.Repository;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System;

namespace UOB.SVS.Repository
{
    public class BranchRepository : RepositoryController<Branch>, IBranchRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public BranchRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public Branch getBranch(string branchCode)
        {
            return transact(() => session.QueryOver<Branch>()
                                                .Where(f => f.BranchCode == branchCode).SingleOrDefault());
        }
        public Branch getBranch(long id)
        {
            Branch br = transact(() => session.QueryOver<Branch>().Where(f => f.Id == id).SingleOrDefault());
            if (br.BranchCode == br.MainBranch)
                br.isMainBranch = true;
            return br;
        }

        public IList<Branch> getBranchs(IList<string> branchCode)
        {
            return transact(() =>
                   session.QueryOver<Branch>()
                          .Where(val => val.BranchCode.IsIn(branchCode.ToArray()) || val.MainBranch.IsIn(branchCode.ToArray()))
                          .List());
        }

        public IList<Branch> getBranchs(string branchCode)
        {
            return transact(() =>
                   session.QueryOver<Branch>()
                          .Where(val => val.MainBranch == branchCode)
                          .List());
        }

        public bool IsDuplicate(string branchCode)
        {
            int rowCount = transact(() => session.QueryOver<Branch>()
                                                .Where(f => f.BranchCode == branchCode).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }
        public PagedResult<Branch> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<Branch> paged = new PagedResult<Branch>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<Branch>()
                                                     .OrderByDescending(GetOrderByExpression<Branch>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<Branch>()
                                                     .OrderBy(GetOrderByExpression<Branch>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<Branch> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            PagedResult<Branch> paged = new PagedResult<Branch>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<Branch>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<Branch>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }


            paged.TotalItems = transact(() =>
                         session.QueryOver<Branch>()
                                .Where(specification)
                                .RowCount());
            return paged;
        }
        
        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public IList<Branch> getBranchs()
        {
            return transact(() =>
                   session.QueryOver<Branch>().List());
        }
    }
}
