﻿using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using UOB.SVS.Model;
using UOB.SVS.Repository;
using UOB.SVS.Interface;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using System;
using Treemas.Credential.Model;
using System.Data;
using NHibernate.Transform;
using Treemas.Base.Configuration;
using UOBISecurity;
using System.Web.Configuration;

namespace UOB.SVS.Repository
{
    public class SignatureHReqRepository : RepositoryControllerString<SignatureHRequest>, ISignatureHReqRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        Encryptor enc = new Encryptor();
        ApplicationRegistryHandler appReg = new ApplicationRegistryHandler();
        bool EncryptData = Convert.ToBoolean(WebConfigurationManager.AppSettings["EncryptData"]);

        string keyReg = "";
        string appName = "";
        string uobikey = "";

        public SignatureHReqRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;

            IConfigurationBinder binder = SystemConfigurationCabinet.Instance.GetBinder("Database");
            CompositeConfigurationItem items = (CompositeConfigurationItem)binder.GetConfiguration("System");
            appName = items.GetItem("AppName").Value;
            uobikey = items.GetItem("UOBIKey").Value;

            appName = appName.Trim().Replace(" ", "");
            keyReg = appReg.ReadFromRegistry(@"Software\" + appName, "Key");
        }
        public SignatureHRequest getSignatureHReq(string id)
        {
            return transact(() => session.QueryOver<SignatureHRequest>()
                                                .Where(f => f.AccountNo == id).SingleOrDefault());
        }

        public IList<SignatureHRequest> getSignatureHReqList(IList<string> SignatureHReqIds)
        {
            return transact(() =>
                   session.QueryOver<SignatureHRequest>()
                          .WhereRestrictionOn(val => val.AccountNo)
                          .IsIn(SignatureHReqIds.ToArray()).List());
        }
        public bool IsDuplicate(string accno)
        {
            int rowCount = transact(() => session.QueryOver<SignatureHRequest>()
                                                .Where(f => f.AccountNo == accno && f.IsRejected == false).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public PagedResult<SignatureHRequest> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, SVSReqFilter filter, User user)
        {
            Conjunction conjuction = Restrictions.Conjunction();
            SimpleExpression reqType = Restrictions.Eq("RequestType", filter.RequestType);

            if (!filter.RequestType.IsNullOrEmpty()) conjuction.Add(reqType);

            if (!filter.AccountNo.IsNullOrEmpty())
            {
                SimpleExpression accNo = Restrictions.Like("AccountNo", filter.AccountNo, MatchMode.Anywhere);
                conjuction.Add(accNo);
            }
            if (!filter.AccountName.IsNullOrEmpty())
            {
                SimpleExpression accName = Restrictions.Like("AccountName", filter.AccountName, MatchMode.Anywhere);
                conjuction.Add(accName);
            }
            //if (!filter.Branches.IsNullOrEmpty())
            //{
            //    conjuction.Add(Restrictions.In("BranchCode", filter.Branches.Select(x => x.BranchCode).ToArray()));
            //}

            if (filter.RequestType.IsNullOrEmpty()) filter.RequestType = ""; ;

            if (filter.AccountNo.IsNullOrEmpty())
            {
                filter.AccountNo = "";
            }
            if (filter.AccountName.IsNullOrEmpty())
            {
                filter.AccountName = "";
            }

            if (!filter.ApprovalType.IsNullOrEmpty() && filter.ApprovalType.ToLower() == "approvesvs")
            {
                SimpleExpression isRejected = Restrictions.Eq("IsRejected", 0);
                conjuction.Add(isRejected);
            }
            if (!filter.RequestUser.IsNullOrEmpty())
            {
                SimpleExpression requestBy = Restrictions.Eq("RequestUser", filter.RequestUser);
                conjuction.Add(requestBy);
            }

            switch (orderColumn)
            {
                case "RequestTypeString":
                    orderColumn = "RequestType";
                    break;
                case "RequestDateString":
                    orderColumn = "RequestDate";
                    break;
                default:
                    break;
            }

            PagedResult<SignatureHRequest> paged = new PagedResult<SignatureHRequest>(pageNumber, itemsPerPage);

            //Alias
            SignatureHRequest a = null;
            User b = null;



            //.SetResultTransformer(Transformers.AliasToBean<EmployeeView>())


            string sql = "";
            string sqlbranch = "";
            string sqlorder = "";
            if (user.BranchCode.Trim() != "000")
            {
                sqlbranch = "AND BranchCode IN(SELECT BRANCH_CODE " +
                    " FROM BRANCH " +
                    " WHERE BRANCH_CODE = '" + user.BranchCode.Trim() + "' OR MAIN_BRANCH = '" + user.BranchCode.Trim() + "') ";
            }

            if (orderKey.ToLower() == "asc")
            { sqlorder = " ASC "; }
            else
            { sqlorder = " DESC "; }

            sql = "SELECT AccountNo " +
                    ", AccountName" +
                    ", AccountType" +
                    ", CIFNumber" +
                    ", BranchCode" +
                    ", Note" +
                    ", RequestType" +
                    ", RequestDate" +
                    ", RequestUser" +
                    ", RequestReason" +
                    ", IsRejected " +
                "FROM SIGNATURE_HEADER_REQUEST " +
                "WHERE AccountNo LIKE '%" + filter.AccountNo.Trim() + "%' AND AccountName LIKE '%" + filter.AccountName + "%' AND RequestType LIKE '%" + filter.RequestType + "%' AND IsRejected = 0 " + sqlbranch +
                " order by " + Projections.Property(orderColumn).ToString() + sqlorder +
                " OFFSET " + pageNumber * 10 + " ROWS" +
                " FETCH NEXT " + itemsPerPage + " ROWS ONLY";


            //         sql = "select AccountNo,AccountName,AccountType,CIFNumber,BranchCode,Note"+
            //               ",RequestType,RequestDate,RequestUser,RequestReason,IsRejected"+
            //               " from SIGNATURE_HEADER_REQUEST a"+
            //               " inner join (select * from SEC_USER"+
            //                            " where BRANCH_CODE in (select a.BRANCH_CODE from BRANCH a"+
            //                            " inner join SEC_USER b on a.BRANCH_CODE = b.BRANCH_CODE"+
            //                            " where USERNAME = '" + user.Username + "')" +
            //" ) b on a.RequestUser = b.USERNAME"+
            //               " where AccountNo like '%"+ filter.AccountNo.Trim() + "%' and AccountName like '%"+ filter.AccountName + "%' and RequestType like '%" + filter.RequestType + "%' and IsRejected = 0" +
            //               " order by " + Projections.Property(orderColumn).ToString() + " asc "+
            //               " OFFSET "+ pageNumber*10 +" ROWS"+
            //               " FETCH NEXT "+ itemsPerPage +" ROWS ONLY";

            NHibernate.IQuery myQuery = session.CreateSQLQuery(sql).SetResultTransformer(Transformers.AliasToBean<SignatureHRequest>());
            paged.Items = myQuery.List<SignatureHRequest>().Take(itemsPerPage);

            //}
            //else
            //{
            //    sql = "SELECT AccountNo " +
            //            ", AccountName" +
            //            ", AccountType" +
            //            ", CIFNumber" +
            //            ", BranchCode" +
            //            ", Note" +
            //            ", RequestType" +
            //            ", RequestDate" +
            //            ", RequestUser" +
            //            ", RequestReason" +
            //            ", IsRejected" +
            //        "FROM SIGNATURE_HEADER_REQUEST " +
            //        "WHERE AccountNo LIKE '%" + filter.AccountNo.Trim() + "%' AND AccountName LIKE '%" + filter.AccountName + "%' AND RequestType LIKE '%" + filter.RequestType + "%' AND IsRejected = 0 " + sqlbranch +
            //        " order by " + Projections.Property(orderColumn).ToString() + " desc " +
            //        " OFFSET " + pageNumber * 10 + " ROWS" +
            //        " FETCH NEXT " + itemsPerPage + " ROWS ONLY";

            //    NHibernate.IQuery myQuery = session.CreateSQLQuery(sql).SetResultTransformer(Transformers.AliasToBean<SignatureHRequest>());

            //    paged.Items = myQuery.List<SignatureHRequest>().Take(itemsPerPage);

            //}

            string sqlCount = "SELECT AccountNo " +
                    ", AccountName" +
                    ", AccountType" +
                    ", CIFNumber" +
                    ", BranchCode" +
                    ", Note" +
                    ", RequestType" +
                    ", RequestDate" +
                    ", RequestUser" +
                    ", RequestReason" +
                    ", IsRejected " +
                "FROM SIGNATURE_HEADER_REQUEST " +
                "WHERE AccountNo LIKE '%" + filter.AccountNo.Trim() + "%' AND AccountName LIKE '%" + filter.AccountName + "%' AND RequestType LIKE '%" + filter.RequestType + "%' AND IsRejected = 0 " + sqlbranch;

            NHibernate.IQuery myQuery2 = session.CreateSQLQuery(sqlCount).SetResultTransformer(Transformers.AliasToBean<SignatureHRequest>());
            paged.TotalItems = myQuery2.List<SignatureHRequest>().Count();
            return paged;
        }

        public PagedResult<SignatureHRequest> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, SVSReqFilter filter)
        {
            Conjunction conjuction = Restrictions.Conjunction();
            SimpleExpression reqType = Restrictions.Eq("RequestType", filter.RequestType);
            if (!filter.RequestType.IsNullOrEmpty()) conjuction.Add(reqType);

            if (!filter.AccountNo.IsNullOrEmpty())
            {
                SimpleExpression accNo = Restrictions.Like("AccountNo", filter.AccountNo, MatchMode.Start);
                conjuction.Add(accNo);
            }
            if (!filter.AccountName.IsNullOrEmpty())
            {
                SimpleExpression accName = Restrictions.Like("AccountName", filter.AccountName, MatchMode.Anywhere);
                conjuction.Add(accName);
            }
            if (!filter.Branches.IsNullOrEmpty())
            {
                conjuction.Add(Restrictions.In("BranchCode", filter.Branches.Select(x => x.BranchCode).ToArray()));
            }

            if (!filter.ApprovalType.IsNullOrEmpty() && filter.ApprovalType.ToLower() == "approvesvs")
            {
                SimpleExpression isRejected = Restrictions.Eq("IsRejected", false);
                conjuction.Add(isRejected);
            }

            if (!filter.RequestUser.IsNullOrEmpty())
            {
                SimpleExpression requestBy = Restrictions.Eq("RequestUser", filter.RequestUser);
                conjuction.Add(requestBy);
            }

            if (orderColumn == "RequestTypeString") orderColumn = "RequestType";
            if (orderColumn == "RequestDateString") orderColumn = "RequestDate";

            PagedResult<SignatureHRequest> paged = new PagedResult<SignatureHRequest>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<SignatureHRequest>()
                                             .Where(conjuction)
                                             .OrderBy(Projections.Property(orderColumn)).Asc
                                             .Skip((pageNumber) * itemsPerPage)
                                             .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<SignatureHRequest>()
                                             .Where(conjuction)
                                             .OrderBy(Projections.Property(orderColumn)).Desc
                                             .Skip((pageNumber) * itemsPerPage)
                                             .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() => session.QueryOver<SignatureHRequest>()
                                            .Where(conjuction).RowCount());
            return paged;
        }

        public PagedResult<SignatureHRequest> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<SignatureHRequest> paged = new PagedResult<SignatureHRequest>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<SignatureHRequest>()
                                                     .OrderByDescending(GetOrderByExpression<SignatureHRequest>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<SignatureHRequest>()
                                                     .OrderBy(GetOrderByExpression<SignatureHRequest>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = paged.Items.Count();
            return paged;
        }
        public PagedResult<SignatureHRequest> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<SignatureHRequest> specification;

            switch (searchColumn)
            {
                case "AccountNumber":
                    specification = new AdHocSpecification<SignatureHRequest>(s => s.AccountNo.Contains(searchValue));
                    break;
                case "AccountName":
                    specification = new AdHocSpecification<SignatureHRequest>(s => s.AccountName.Contains(searchValue));
                    break;
                case "Note":
                    specification = new AdHocSpecification<SignatureHRequest>(s => s.Note.Contains(searchValue));
                    break;
                default:
                    specification = new AdHocSpecification<SignatureHRequest>(s => s.AccountNo.Contains(searchValue));
                    break;
            }

            PagedResult<SignatureHRequest> paged = new PagedResult<SignatureHRequest>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<SignatureHRequest>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<SignatureHRequest>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<SignatureHRequest>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<SignatureHRequest>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<SignatureHRequest>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = paged.Items.Count();
            return paged;
        }

        public PagedResult<SignatureHRequest> FindAllPagedCreate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, SVSReqFilter filter)
        {
            Conjunction conjuction = Restrictions.Conjunction();
            SimpleExpression reqType = Restrictions.Eq("RequestType", filter.RequestType);
            if (!filter.RequestType.IsNullOrEmpty()) conjuction.Add(reqType);

            if (!filter.AccountNo.IsNullOrEmpty())
            {
                SimpleExpression accNo = Restrictions.Like("AccountNo", filter.AccountNo, MatchMode.Start);
                conjuction.Add(accNo);
            }
            if (!filter.AccountName.IsNullOrEmpty())
            {
                SimpleExpression accName = Restrictions.Like("AccountName", filter.AccountName, MatchMode.Anywhere);
                conjuction.Add(accName);
            }
            if (!filter.Branches.IsNullOrEmpty())
            {
                conjuction.Add(Restrictions.In("BranchCode", filter.Branches.Select(x => x.BranchCode).ToArray()));
            }

            if (!filter.RequestUser.IsNullOrEmpty())
            {
                SimpleExpression requestBy = Restrictions.Eq("RequestUser", filter.RequestUser);
                conjuction.Add(requestBy);
            }

            PagedResult<SignatureHRequest> paged = new PagedResult<SignatureHRequest>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<SignatureHRequest>()
                                             .Where(conjuction)
                                             .And(x => x.RequestDate.Value.Date == DateTime.Now.Date)
                                             .OrderBy(Projections.Property(orderColumn)).Asc
                                             .Skip((pageNumber) * itemsPerPage)
                                             .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<SignatureHRequest>()
                                             .Where(conjuction)
                                             .And(x => x.RequestDate.Value.Date == DateTime.Now.Date)
                                             .OrderBy(Projections.Property(orderColumn)).Desc
                                             .Skip((pageNumber) * itemsPerPage)
                                             .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() => session.QueryOver<SignatureHRequest>()
                                            .Where(conjuction)
                                             .And(x => x.RequestDate.Value.Date == DateTime.Now.Date)
                                             .RowCount());
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public void DeleteSignatureHReq(string ID)
        {
            string hqlDelete = "delete SignatureHRequest where AccountNo = :accountNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("accountNo", ID)
                    .ExecuteUpdate();
        }

        public void AddKTPReqD(SignatureDKTPReq item)
        {
            //if (EncryptData)
            //{
            //    //item.KTP = Encryption.Instance.EncryptText(item.KTP); //TMS encrypt
            //    item.KTP = enc.Encrypt(item.KTP, keyReg, uobikey); //UOB security encrypt
            //}
            transact(() => session.Save(item));
        }

        public IList<SignatureDKTPReq> getKTPReqD(string accountNo)
        {
            IList<SignatureDKTPReq> req = transact(() => session.QueryOver<SignatureDKTPReq>().Where(f => f.AccountNo == accountNo && f.KTP != null).OrderBy(o => o.FileSeq).Asc.List());

            foreach(SignatureDKTPReq ktp in req)
            {
                //try
                //{
                //    //ktp.KTP = Encryption.Instance.DecryptText(ktp.KTP); //TMS encrypt
                //    ktp.KTP = enc.Decrypt(ktp.KTP, keyReg, uobikey); //UOB security encrypt
                //}
                //catch
                //{
                 ktp.KTP = ktp.KTP;
                //}
            }

            return req;
        }

        public void DeleteKTPReqD(string ID)
        {
            string hqlDelete = "delete SignatureDKTPReq where AccountNo = :accountNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("accountNo", ID)
                    .ExecuteUpdate();
        }

        public void DeleteKTPReqD(long ID)
        {
            string hqlDelete = "delete SignatureDKTPReq where ID = :ID";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetDouble("ID", ID)
                    .ExecuteUpdate();
        }

        public void AddSignReqD(SignatureDSignReq item)
        {
            //if (EncryptData)
            //{
            //    //item.Signature = Encryption.Instance.EncryptText(item.Signature);//TMS encrypt
            //    item.Signature = enc.Encrypt(item.Signature, keyReg, uobikey); //UOB security encrypt
            //}
            transact(() => session.Save(item));
        }

        public IList<SignatureDSignReq> getSignReqD(string accountNo)
        {
            IList<SignatureDSignReq> req = transact(() => session.QueryOver<SignatureDSignReq>().Where(f => f.AccountNo == accountNo && f.Signature != null).OrderBy(o => o.FileSeq).Asc.List());
            foreach (SignatureDSignReq sign in req)
            {
                //try
                //{
                //    //sign.Signature = Encryption.Instance.DecryptText(sign.Signature); //TMS encrypt
                //    sign.Signature = enc.Decrypt(sign.Signature, keyReg, uobikey); //UOB security encrypt
                //}
                //catch
                //{
                  sign.Signature = sign.Signature;
                //}
            }
            return req;
        }

        public void DeleteSignReqD(string ID)
        {
            string hqlDelete = "delete SignatureDSignReq where AccountNo = :accountNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("accountNo", ID)
                    .ExecuteUpdate();
        }

        public void DeleteSignReqD(long ID)
        {
            string hqlDelete = "delete SignatureDSignReq where ID = :ID";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetDouble("ID", ID)
                    .ExecuteUpdate();
        }

        public void AddDocReqD(SignatureDDocReq item)
        {
            //if (EncryptData)
            //{
            //    item.FileBlob = enc.Encrypt(item.FileBlob.ToString(), keyReg, uobikey); //UOB security encrypt
            //}
            transact(() => session.Save(item));
        }

        public IList<SignatureDDocReq> getDocReqD(string accountNo)
        {
            string sqlSelect = "SELECT ID, AccountNo = ACCOUNT_NUMBER, FileType = FILE_TYPE, DocumentType = DOCUMENT_TYPE, RequestType = RequestType, " +
                "RequestDate = RequestDate, RequestUser = RequestUser, RequestReason = RequestReason, FileSeq = FILE_SEQ " +
                "FROM dbo.SIGNATURE_DETAIL_DOC_REQ " +
                "WHERE ACCOUNT_NUMBER = '" + accountNo + "' " +
                "ORDER BY FILE_SEQ";

            NHibernate.IQuery myQuery = session.CreateSQLQuery(sqlSelect).SetResultTransformer(Transformers.AliasToBean<SignatureDDocReq>());

            IList<SignatureDDocReq> req = myQuery.List<SignatureDDocReq>();

            return req;
        }

        public void DeleteDocReqD(string ID)
        {
            string hqlDelete = "delete SignatureDDocReq where AccountNo = :accountNo";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("accountNo", ID)
                    .ExecuteUpdate();
        }

        public void DeleteDocReqD(long ID)
        {
            string hqlDelete = "delete SignatureDDocReq where ID = :ID";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetDouble("ID", ID)
                    .ExecuteUpdate();
        }

        public SignatureDDocReq getDocReqD(long ID)
        {
            SignatureDDocReq ret = session.QueryOver<SignatureDDocReq>().Where(d => d.Id == ID).SingleOrDefault();

            //try
            //{
            //    ret.FileBlob = enc.Decrypt(ret.FileBlob, keyReg, uobikey); //UOB security encrypt
            //}
            //catch
            //{
            ret.FileBlob = ret.FileBlob;
            //}

            return ret;
        }
    }
}
