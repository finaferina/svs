﻿using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using UOB.SVS.Model;
using UOB.SVS.Repository;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System;
using Treemas.Credential.Model;
using Treemas.Base.Utilities;

namespace UOB.SVS.Repository
{
    public class DocumentTypeCrRepository : RepositoryController<DocumentTypeCr>, IDocumentTypeCrRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public DocumentTypeCrRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        public DocumentTypeCr getDocType(string docTypeId)
        {
            return transact(() => session.QueryOver<DocumentTypeCr>()
                                                .Where(f => f.DocType == docTypeId).SingleOrDefault());
        }
        public DocumentTypeCr getDocType(long id)
        {
            return transact(() => session.QueryOver<DocumentTypeCr>()
                                                .Where(f => f.Id == id).SingleOrDefault());
        }

        public IList<DocumentTypeCr> getDocTypes(IList<string> docTypeId)
        {
            return transact(() =>
                   session.QueryOver<DocumentTypeCr>()
                          .WhereRestrictionOn(val => val.DocType)
                          .IsIn(docTypeId.ToArray()).List());
        }
        public bool IsDuplicate(string docTypeId)
        {
            int rowCount = transact(() => session.QueryOver<DocumentTypeCr>()
                                                .Where(f =>f.DocType == docTypeId).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public DocumentTypeCr getDocTypeCr(long docTypeId, ApprovalType type)
        {
            DocumentTypeCr _docTypeCr = this.FindOne(new AdHocSpecification<DocumentTypeCr>(s => s.DocTypeId == docTypeId && s.Approved == false && s.ApprovalType == type.ToString()));
            return _docTypeCr;
        }

        public bool IsExist(string docType, ApprovalType type)
        {

            int rowCount = transact(() => session.QueryOver<DocumentTypeCr>()
                                                .Where(s => s.DocType == docType && s.Approved == false && s.ApprovalType == type.ToString()).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
            //DocumentTypeCr _docTypeCr = this.FindOne(new AdHocSpecification<DocumentTypeCr>(s => s.DocTypeId == docTypeId && s.Approved == false && s.ApprovalStatus == "N"));
            //return _docTypeCr;
        }

        public PagedResult<DocumentTypeCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<DocumentTypeCr> paged = new PagedResult<DocumentTypeCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<DocumentTypeCr>()
                                                     .OrderByDescending(GetOrderByExpression<DocumentTypeCr>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<DocumentTypeCr>()
                                                     .OrderBy(GetOrderByExpression<DocumentTypeCr>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<DocumentTypeCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<DocumentTypeCr> specification;

            switch (searchColumn)
            {
                case "DocType":
                    specification = new AdHocSpecification<DocumentTypeCr>(s => s.DocType.Contains(searchValue));
                    break;
                case "Description":
                    specification = new AdHocSpecification<DocumentTypeCr>(s => s.Description.Contains(searchValue));
                    break;              
                default:
                    specification = new AdHocSpecification<DocumentTypeCr>(s => s.DocType.Contains(searchValue));
                    break;
            }

            PagedResult<DocumentTypeCr> paged = new PagedResult<DocumentTypeCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<DocumentTypeCr>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<DocumentTypeCr>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<DocumentTypeCr>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<DocumentTypeCr>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<DocumentTypeCr>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = Count;
            return paged;
        }
        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public PagedResult<DocumentTypeCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter)
        {
            SimpleExpression status = Restrictions.Eq("ApprovalStatus", filter.ApprovalStatus);
            SimpleExpression apprtype = Restrictions.Eq("ApprovalType", filter.ApprovalType);

            Conjunction conjuction = Restrictions.Conjunction();
            if (!filter.ApprovalStatus.Trim().IsNullOrEmpty()) conjuction.Add(status);
            if (!filter.ApprovalType.Trim().IsNullOrEmpty()) conjuction.Add(apprtype);

            PagedResult<DocumentTypeCr> paged = new PagedResult<DocumentTypeCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<DocumentTypeCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<DocumentTypeCr>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = paged.Items.Count();
            return paged;
        }
    }
}
