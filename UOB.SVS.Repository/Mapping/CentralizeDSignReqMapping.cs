﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using UOB.SVS.Model;
using NHibernate;

namespace UOB.SVS.Repository.Mapping
{
    public class CentralizeDSignReqMapping : ClassMapping<CentralizeDSignReq>
    {
        public CentralizeDSignReqMapping()
        {
            Schema("dbo");
            this.Table("CENTRALIZE_DETAIL_SIGN_REQ");
            //Lazy(true);
            Id<long>(x => x.Id, map => { map.Column("ID"); map.Generator(Generators.Identity); });
            Property<string>(x => x.NIK, map => { map.Column("NIK"); });
            Property<string>(x => x.Signature, map => { map.Column("SIGNATURE"); map.Length(4000000); });
            Property<string>(x => x.ImageType, map => { map.Column("ImageType"); });
            Property<string>(x => x.RequestType, map => { map.Column("RequestType"); });
            Property<DateTime?>(x => x.RequestDate, map => { map.Column("RequestDate"); });
            Property<string>(x => x.RequestUser, map => { map.Column("RequestUser"); });
            Property<string>(x => x.RequestReason, map => { map.Column("RequestReason"); });
            Property<int>(x => x.FileSeq, map => { map.Column("FILE_SEQ"); });
        }
    }
}
