﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using UOB.SVS.Model;
using NHibernate;

namespace UOB.SVS.Repository.Mapping
{
    public class CentralizeHeaderMapping : ClassMapping<CentralizeHeader>
    {
        public CentralizeHeaderMapping()
        {
            Schema("dbo");
            this.Table("CENTRALIZE_HEADER");
            //Lazy(true);
            Id<string>(x => x.NIK, map => { map.Column("NIK"); });
            Property<string>(x => x.Name, map => { map.Column("Name"); });
            Property<string>(x => x.Region, map => { map.Column("Region"); });
            Property<string>(x => x.Division, map => { map.Column("Division"); });
            Property<string>(x => x.Departement, map => { map.Column("Departement"); });
            Property<string>(x => x.Titles, map => { map.Column("Titles"); });
            Property<string>(x => x.Note, map => { map.Column("Note"); });
            Property<string>(x => x.CreateUser, map => { map.Column("Create_User"); });
            Property<DateTime?>(x => x.CreateDate, map => { map.Column("Create_Date"); });
        }
    }
}
