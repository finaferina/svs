﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using UOB.SVS.Model;
using NHibernate;

namespace UOB.SVS.Repository.Mapping
{
    public class CentralizeDDocReqMapping : ClassMapping<CentralizeDDocReq>
    {
        public CentralizeDDocReqMapping()
        {
            Schema("dbo");
            this.Table("CENTRALIZE_DETAIL_DOC_REQ");
            //Lazy(true);
            Id<long>(x => x.Id, map => { map.Column("ID"); map.Generator(Generators.Identity); });
            Property<string>(x => x.NIK, map => { map.Column("NIK"); });
            Property<string>(x => x.FileBlob, map => { map.Column("FILE_BLOB"); map.Length(40000000); });
            Property<string>(x => x.FileType, map => { map.Column("FILE_TYPE"); });
            Property<string>(x => x.DocumentType, map => { map.Column("DOCUMENT_TYPE"); });
            Property<string>(x => x.RequestType, map => { map.Column("RequestType"); });
            Property<DateTime?>(x => x.RequestDate, map => { map.Column("RequestDate"); });
            Property<string>(x => x.RequestUser, map => { map.Column("RequestUser"); });
            Property<string>(x => x.RequestReason, map => { map.Column("RequestReason"); });
            Property<int>(x => x.FileSeq, map => { map.Column("FILE_SEQ"); });
        }
    }
}
