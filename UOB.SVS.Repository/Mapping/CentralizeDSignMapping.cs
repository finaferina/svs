﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using UOB.SVS.Model;
using NHibernate;

namespace UOB.SVS.Repository.Mapping
{
    public class CentralizeDSignMapping : ClassMapping<CentralizeDSign>
    {
        public CentralizeDSignMapping()
        {
            Schema("dbo");
            this.Table("CENTRALIZE_DETAIL_SIGN");
            //Lazy(true);
            Id<long>(x => x.Id, map => { map.Column("ID"); map.Generator(Generators.Identity); });
            Property<string>(x => x.NIK, map => { map.Column("NIK"); });
            Property<string>(x => x.Signature, map => { map.Column("SIGNATURE"); map.Length(4000000); });
            Property<string>(x => x.ImageType, map => { map.Column("ImageType"); });
            Property<int>(x => x.Seq, map => { map.Column("FILE_SEQ"); });
        }
    }
}
