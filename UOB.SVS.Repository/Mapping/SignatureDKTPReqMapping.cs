﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using UOB.SVS.Model;
using NHibernate;

namespace UOB.SVS.Repository.Mapping
{
    public class SignatureDKTPReqMapping : ClassMapping<SignatureDKTPReq>
    {
        public SignatureDKTPReqMapping()
        {
            Schema("dbo");
            this.Table("SIGNATURE_DETAIL_KTP_REQ");
            //Lazy(true);
            Id<long>(x => x.Id, map => { map.Column("ID"); map.Generator(Generators.Identity); });
            Property<string>(x => x.AccountNo, map => { map.Column("ACCOUNT_NUMBER"); });
            Property<string>(x => x.KTP, map => { map.Column("KTP"); map.Length(4000000);});
            Property<string>(x => x.ImageType, map => { map.Column("ImageType"); });
            Property<string>(x => x.RequestType, map => { map.Column("RequestType"); });
            Property<DateTime?>(x => x.RequestDate, map => { map.Column("RequestDate"); });
            Property<string>(x => x.RequestUser, map => { map.Column("RequestUser"); });
            Property<string>(x => x.RequestReason, map => { map.Column("RequestReason"); });
            Property<int>(x => x.FileSeq, map => { map.Column("FILE_SEQ"); });
        }
    }
}
