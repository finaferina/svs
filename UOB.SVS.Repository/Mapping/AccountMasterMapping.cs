﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using UOB.SVS.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class AccountMasterMapping : ClassMapping<AccountMaster>
    {
        public AccountMasterMapping()
        {
            this.Table("ACCOUNT_MASTER");

            Id<long>(x => x.Id, map => { map.Column("ID"); map.Generator(Generators.Identity); });
            Property<string>(x => x.AccountNo, map => { map.Column("ACC_NO"); });
            Property<string>(x => x.AccountName, map => { map.Column("ACC_NAME"); });
            Property<string>(x => x.BranchCode, map => { map.Column("BRANCH_CODE"); });
            Property<string>(x => x.AccountType, map => { map.Column("ACC_TYPE"); });
            Property<string>(x => x.CIFNo, map => { map.Column("CIF_NO"); });
            Property<DateTime?>(x => x.DateOpened, map => { map.Column("DATE_OPENED"); });
            Property<DateTime?>(x => x.DateClosed, map => { map.Column("DATE_CLOSED"); });
            Property<string>(x => x.Status, map => { map.Column("STATUS"); });
        }
    }
}
