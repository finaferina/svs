﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using UOB.SVS.Model;
using NHibernate;

namespace UOB.SVS.Repository.Mapping
{
    public class VApprovalMapping : ClassMapping<VApproval>
    {
        public VApprovalMapping()
        {
            Schema("dbo");
            this.Table("V_Approval");
            //Lazy(true);
            Id<string>(x => x.Id, map => { map.Column("Id"); });
            Property<string>(x => x.ApprovalFuntion, map => { map.Column("ApprovalFuntion"); });
            Property<string>(x => x.RequestType, map =>
            {
                map.Column("RequestType");
            });
            Property<string>(x => x.ApprovalStatus, map =>
            {
                map.Column("ApprovalStatus");
            });
            Property<string>(x => x.MakerUser, map =>
            {
                map.Column("MakerUser");
            });
            Property<DateTime?>(x => x.MakerDate, map => { map.Column("MakerDate"); });
        }
    }
}
