﻿using NHibernate.Mapping.ByCode.Conformist;
using UOB.SVS.Model;
using NHibernate.Mapping.ByCode;
using System;

namespace UOB.SVS.Repository.Mapping
{
    public class ExceptionReportMapping : ClassMapping<ExceptionReport>
    {
        public ExceptionReportMapping()
        {
            Schema("dbo");
            this.Table("ExceptionReportView");
            //Id<long>(x => x.Id, map => { map.Column("Id"); map.Generator(Generators.Identity); });
            Property<string>(x => x.AccountNo, map => { map.Column("AccountNo"); });
            Property<string>(x => x.AccountName, map => { map.Column("AccountName"); });
            Property<string>(x => x.CIFNumber, map => { map.Column("CIFNumber"); });
            Property<DateTime?>(x => x.OpenDate, map => { map.Column("OpenDate"); });
            Property<DateTime?>(x => x.CloseDate, map => { map.Column("CloseDate"); });
            Property<string>(x => x.Reason, map => { map.Column("Reason"); });
            Property<string>(x => x.RequestType, map => { map.Column("RequestType"); });
            Property<string>(x => x.BranchCode, map => { map.Column("BranchCode"); });
            Property<DateTime?>(x => x.ActionDate, map => { map.Column("ActionDate"); });
        }
    }
}
