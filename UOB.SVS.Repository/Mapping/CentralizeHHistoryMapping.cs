﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using UOB.SVS.Model;
using NHibernate;

namespace UOB.SVS.Repository.Mapping
{
    public class CentralizeHHistoryMapping : ClassMapping<CentralizeHHistory>
    {
        public CentralizeHHistoryMapping()
        {
            Schema("dbo");
            this.Table("CENTRALIZE_HEADER_HISTORY");
            //Lazy(true);
            Id<long>(x => x.RecordId, map => { map.Column("RecordId"); map.Generator(Generators.Identity); });
            Property<string>(x => x.NIK, map => { map.Column("NIK"); });
            Property<string>(x => x.Name, map => { map.Column("Name"); });
            Property<string>(x => x.Region, map => { map.Column("Region"); });
            Property<string>(x => x.Division, map => { map.Column("Division"); });
            Property<string>(x => x.Departement, map => { map.Column("Departement"); });
            Property<string>(x => x.Titles, map => { map.Column("Titles"); });
            //Property<string>(x => x.BranchCode, map => { map.Column("BranchCode"); });
            Property<string>(x => x.Note, map => { map.Column("Note"); });
            Property<string>(x => x.RequestType, map => { map.Column("RequestType"); });
            Property<DateTime?>(x => x.RequestDate, map => { map.Column("RequestDate"); });
            Property<string>(x => x.RequestUser, map => { map.Column("RequestUser"); });
            Property<string>(x => x.RequestReason, map => { map.Column("RequestReason"); });
            Property<DateTime?>(x => x.ApproveDate, map => { map.Column("ApproveDate"); });
            Property<string>(x => x.ApproveBy, map => { map.Column("ApproveBy"); });
            Property<DateTime?>(x => x.RejectDate, map => { map.Column("RejectDate"); });
            Property<string>(x => x.RejectBy, map => { map.Column("RejectBy"); });
            Property<string>(x => x.RejectReason, map => { map.Column("RejectReason"); });

        }
    }
}
