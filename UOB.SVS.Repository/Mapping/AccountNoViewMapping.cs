﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using UOB.SVS.Model;
using NHibernate;

namespace UOB.SVS.Repository.Mapping
{
    public class AccountNoViewMapping : ClassMapping<AccountNoView>
    {
        public AccountNoViewMapping()
        {
            Schema("dbo");
            this.Table("AccountNoView");
            //Lazy(true);
            Property<string>(x => x.AccountNo, map => { map.Column("AccountNo"); });
        }
    }
}
