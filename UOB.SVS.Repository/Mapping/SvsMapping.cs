﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using UOB.SVS.Model;
using NHibernate;

namespace UOB.SVS.Repository.Mapping
{
    public class SvsMapping : ClassMapping<Svs>
    {
        public SvsMapping()
        {
            Schema("dbo");
            this.Table("SIGNATURE");
            Lazy(true);
            Id(x => x.ID, map => map.Generator(Generators.Identity));
            Property(x => x.ACCOUNT_NUMBER);
            Property(x => x.CUSTOMER_NAME);
            Property(x => x.NOTE);
            //Property<string>(x => x.SIGNATURE, map => { map.Type(NHibernateUtil.Binary); });
            Property<string>(x => x.SIGNATURE, map => { map.Length(1000000); });
            Property(x => x.KTP);
            Property(x => x.NOTE_KTP);
            Property(x => x.PENGENAL);
            Property(x => x.ADD_BY);
            Property(x => x.DATE_ADD);
            Property(x => x.UPD_BY);
            Property(x => x.DATE_UPD);
            Property(x => x.DEL_BY);
            Property(x => x.DATE_DEL);
            Property(x => x.APP_ADD_BY);
            Property(x => x.DATE_APP_ADD);
            Property(x => x.APP_UP_BY);
            Property(x => x.DATE_APP_UPD);
            Property(x => x.APP_DEL_BY);
            Property(x => x.DATE_APP_DEL);
            Property(x => x.NOT_APP_ADD_BY);
            Property(x => x.DATE_NOT_APP_ADD);
            Property(x => x.NOT_APP_UPD_BY);
            Property(x => x.DATE_NOT_APP_UPD);
            Property(x => x.NOT_APP_DEL_BY);
            Property(x => x.DATE_NOT_APP_DELL);
            Property(x => x.STATUS_FLAG);
            Property(x => x.STATUS_AKTIF);
            Property(x => x.OLD_NOTE);
            Property(x => x.OLD_NOTE_KTP);
            Property(x => x.FSIGNATURE);
            Property(x => x.FKTP);
            Property(x => x.FNOTE);
            Property(x => x.ZSIGNATURE);
            Property(x => x.ZOLD_SIGNATURE);
            Property(x => x.ZKTP);
            Property(x => x.ZOLD_KTP);
            Property(x => x.OLD_SIGNATURE);
            Property(x => x.OLD_KTP);
            Property(x => x.OLD_ACC_NUMBER);
        }
    }
}
