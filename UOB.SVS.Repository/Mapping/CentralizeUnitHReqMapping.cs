﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using UOB.SVS.Model;
using NHibernate;

namespace UOB.SVS.Repository.Mapping
{
    public class CentralizeUnitHReqMapping : ClassMapping<CentralizeUnitHRequest>
    {
        public CentralizeUnitHReqMapping()
        {
            Schema("dbo");
            this.Table("CENTRALIZE_UNIT_HEADER_REQUEST");
            //Lazy(true);
            Id<string>(x => x.NIK, map => { map.Column("[NIK]"); });
            Property<string>(x => x.Name, map => { map.Column("[Name]"); });
            Property<string>(x => x.DocType, map => { map.Column("[DocType]"); });
            Property<string>(x => x.Region, map => { map.Column("[Region]"); });
            Property<string>(x => x.Division, map => { map.Column("[Division]"); });
            Property<string>(x => x.Departement, map => { map.Column("[Departement]"); });
            Property<string>(x => x.Titles, map => { map.Column("[Title]"); });
            Property<string>(x => x.Note, map => { map.Column("[Note]"); });
            Property<string>(x => x.RequestType, map => { map.Column("[RequestType]"); });
            Property<string>(x => x.RequestUser, map => { map.Column("[RequestUser]"); });
            Property<DateTime?>(x => x.RequestDate, map => { map.Column("[RequestDate]"); });
            Property<string>(x => x.RequestReason, map => { map.Column("[RequestReason]"); });
            Property<bool>(x => x.IsRejected, map => { map.Column("[IsRejected]"); });
        }
    }
}
