﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using UOB.SVS.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class BranchCrMapping : ClassMapping<BranchCr>
    {
        public BranchCrMapping()
        {
            this.Table("BRANCH_CR");

            Id<long>(x => x.Id, map => { map.Column("ID"); map.Generator(Generators.Identity); });
            Property<string>(x => x.BranchCode, map => { map.Column("BRANCH_CODE"); });
            Property<string>(x => x.Description, map => { map.Column("DESCRIPTION"); });
            Property<string>(x => x.MainBranch, map => { map.Column("MAIN_BRANCH"); });
            //Property<string>(x => x.LogicalBranch, map => { map.Column("LOGICAL_BRANCH"); });
            Property<DateTime?>(x => x.ChangedDate, map => { map.Column("CHANGED_DATE"); });
            Property<DateTime?>(x => x.CreatedDate, map => { map.Column("CREATED_DATE"); map.Update(false); });
            Property<string>(x => x.ChangedBy, map => { map.Column("CHANGED_BY"); });
            Property<string>(x => x.CreatedBy, map => { map.Column("CREATED_BY"); map.Update(false); });
            Property<bool>(x => x.Approved, map => { map.Column("APPROVED"); });
            Property<string>(x => x.ApprovedBy, map => { map.Column("APPROVED_BY"); });
            Property<DateTime?>(x => x.ApprovedDate, map => { map.Column("APPROVED_DATE"); });
            Property<string>(x => x.ApprovalType, map => { map.Column("APPROVAL_TYPE"); });
            Property<string>(x => x.ApprovalStatus, map => { map.Column("APPROVAL_STATUS"); });
        }
    }
}
