﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using UOB.SVS.Model;
using NHibernate;

namespace UOB.SVS.Repository.Mapping
{
    public class SignatureDKTPMapping : ClassMapping<SignatureDKTP>
    {
        public SignatureDKTPMapping()
        {
            Schema("dbo");
            this.Table("SIGNATURE_DETAIL_KTP");
            //Lazy(true);
            Id<long>(x => x.Id, map => { map.Column("ID"); map.Generator(Generators.Identity); });
            Property<string>(x => x.AccountNo, map => { map.Column("ACCOUNT_NUMBER"); });
            Property<string>(x => x.KTP, map => { map.Column("KTP"); map.Length(4000000); });
            Property<string>(x => x.ImageType, map => { map.Column("ImageType"); });
            Property<int>(x => x.Seq, map => { map.Column("FILE_SEQ"); });
        }
    }
}
