﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using UOB.SVS.Model;

namespace UOB.SVS.Repository.Mapping
{
    public class BranchAccountPrefixMappingCr : ClassMapping<BranchAccountPrefixCr>
    {
        public BranchAccountPrefixMappingCr()
        {
            this.Table("BRANCH_ACCOUNT_PREFIX_CR");

            Id<long>(x => x.Id, map => { map.Column("ID"); map.Generator(Generators.Identity); });
            Property<string>(x => x.PrefixCode, map => { map.Column("ACCOUNT_PREFIX_CODE"); });
            Property<string>(x => x.BranchCode, map => { map.Column("BRANCH_CODE"); });
            Property<DateTime?>(x => x.CreatedDate, map => { map.Column("CREATED_DATE"); map.Update(false); });
            Property<string>(x => x.ChangedBy, map => { map.Column("CHANGED_BY"); });
            Property<DateTime?>(x => x.ChangedDate, map => { map.Column("CHANGED_DATE"); map.Update(false); });
            Property<string>(x => x.CreatedBy, map => { map.Column("CREATED_BY"); map.Update(false); });
            Property<bool>(x => x.Approved, map => { map.Column("APPROVED"); });
            Property<string>(x => x.ApprovedBy, map => { map.Column("APPROVED_BY"); });
            Property<DateTime?>(x => x.ApprovedDate, map => { map.Column("APPROVED_DATE"); });
            Property<string>(x => x.ApprovalType, map => { map.Column("APPROVAL_TYPE"); });
            Property<string>(x => x.ApprovalStatus, map => { map.Column("APPROVAL_STATUS"); });
        }
    }
}
