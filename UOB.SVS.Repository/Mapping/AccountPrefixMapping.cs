﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using UOB.SVS.Model;


namespace UOB.SVS.Repository.Mapping
{
    public class AccountPrefixMapping : ClassMapping<AccountPrefix>
    {
        public AccountPrefixMapping()
        {
            this.Table("ACCOUNT_PREFIX");

            Id<long>(x => x.Id, map => { map.Column("ID"); map.Generator(Generators.Identity); });
            Property<string>(x => x.PrefixCode, map => { map.Column("ACCOUNT_PREFIX_CODE"); });
            Property<string>(x => x.Description, map => { map.Column("DESCRIPTION"); });
            Property<DateTime?>(x => x.CreatedDate, map => { map.Column("CREATED_DATE"); map.Update(false); });
            Property<string>(x => x.ChangedBy, map => { map.Column("CHANGED_BY"); });
            Property<DateTime?>(x => x.ChangedDate, map => { map.Column("CHANGED_DATE"); map.Update(false); });
            Property<string>(x => x.CreatedBy, map => { map.Column("CREATED_BY"); map.Update(false); });
        }
    }
}
