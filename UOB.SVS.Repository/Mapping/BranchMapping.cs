﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using UOB.SVS.Model;

namespace Treemas.Credential.Repository.Mapping
{
    public class BranchMapping : ClassMapping<Branch>
    {
        public BranchMapping()
        {
            this.Table("BRANCH");

            Id<long>(x => x.Id, map => { map.Column("ID"); map.Generator(Generators.Identity); });
            Property<string>(x => x.BranchCode, map => { map.Column("BRANCH_CODE"); });
            Property<string>(x => x.Description, map => { map.Column("DESCRIPTION"); });
            Property<string>(x => x.MainBranch, map => { map.Column("MAIN_BRANCH"); });
            //Property<string>(x => x.LogicalBranch, map => { map.Column("LOGICAL_BRANCH"); });
            Property<DateTime?>(x => x.CreatedDate, map => { map.Column("CREATED_DATE"); map.Update(false); });
            Property<string>(x => x.ChangedBy, map => { map.Column("CHANGED_BY"); });
            Property<string>(x => x.CreatedBy, map => { map.Column("CREATED_BY"); map.Update(false); });
            Property<DateTime?>(x => x.ChangedDate, map => { map.Column("CHANGED_DATE"); map.Update(false); });
        }
    }
}
