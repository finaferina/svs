﻿using System;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;
using UOB.SVS.Model;
using NHibernate;

namespace UOB.SVS.Repository.Mapping
{
    public class SignatureDDocMapping : ClassMapping<SignatureDDoc>
    {
        public SignatureDDocMapping()
        {
            Schema("dbo");
            this.Table("SIGNATURE_DETAIL_DOC");
            //Lazy(true);
            Id<long>(x => x.Id, map => { map.Column("ID"); map.Generator(Generators.Identity); });
            Property<string>(x => x.AccountNo, map => { map.Column("ACCOUNT_NUMBER"); });
            Property<string>(x => x.FileBlob, map => { map.Column("FILE_BLOB"); map.Length(40000000); });
            Property<string>(x => x.FileType, map => { map.Column("FILE_TYPE"); });
            Property<string>(x => x.DocumentType, map => { map.Column("DOCUMENT_TYPE"); });
            Property<int>(x => x.Seq, map => { map.Column("FILE_SEQ"); });

        }
    }
}
