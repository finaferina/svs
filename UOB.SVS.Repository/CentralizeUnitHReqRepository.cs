﻿using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using UOB.SVS.Model;
using UOB.SVS.Model.Helper;
using UOB.SVS.Repository;
using UOB.SVS.Interface;
using Treemas.Base.Utilities;
using Treemas.Base.Utilities.Queries;
using System;
using Treemas.Credential.Model;
using System.Data;
using NHibernate.Transform;
using Treemas.Base.Configuration;
using UOBISecurity;
using System.Web.Configuration;

namespace UOB.SVS.Repository
{
    public class CentralizeUnitHReqRepository : RepositoryControllerString<CentralizeUnitHRequest>, ICentralizeUnitHReqRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        Encryptor enc = new Encryptor();
        ApplicationRegistryHandler appReg = new ApplicationRegistryHandler();
        bool EncryptData = Convert.ToBoolean(WebConfigurationManager.AppSettings["EncryptData"]);

        string keyReg = "";
        string appName = "";
        string uobikey = "";

        public CentralizeUnitHReqRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;

            IConfigurationBinder binder = SystemConfigurationCabinet.Instance.GetBinder("Database");
            CompositeConfigurationItem items = (CompositeConfigurationItem)binder.GetConfiguration("System");
            appName = items.GetItem("AppName").Value;
            uobikey = items.GetItem("UOBIKey").Value;

            appName = appName.Trim().Replace(" ", "");
            keyReg = appReg.ReadFromRegistry(@"Software\" + appName, "Key");
        }
        public CentralizeUnitHRequest getCentralizeUnitHReq(string id)
        {
            return transact(() => session.QueryOver<CentralizeUnitHRequest>()
                                                .Where(f => f.NIK == id).SingleOrDefault());
        }

        public IList<CentralizeUnitHRequest> getCentralizeUnitHReqList(IList<string> SignatureHReqIds)
        {
            return transact(() =>
                   session.QueryOver<CentralizeUnitHRequest>()
                          .WhereRestrictionOn(val => val.NIK)
                          .IsIn(SignatureHReqIds.ToArray()).List());
        }
        public bool IsDuplicate(string accno)
        {
            int rowCount = transact(() => session.QueryOver<CentralizeUnitHRequest>()
                                                .Where(f => f.NIK == accno && f.IsRejected == false).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public PagedResult<CentralizeUnitHRequest> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, CentralizeReqFilter filter, User user)
        {
            Conjunction conjuction = Restrictions.Conjunction();
            SimpleExpression reqType = Restrictions.Eq("RequestType", filter.RequestType);

            if (!filter.RequestType.IsNullOrEmpty()) conjuction.Add(reqType);

            if (!filter.NIK.IsNullOrEmpty())
            {
                SimpleExpression nik = Restrictions.Like("NIK", filter.NIK, MatchMode.Anywhere);
                conjuction.Add(nik);
            }
            if (!filter.Name.IsNullOrEmpty())
            {
                SimpleExpression Name = Restrictions.Like("Name", filter.Name, MatchMode.Anywhere);
                conjuction.Add(Name);
            }
            if (filter.RequestType.IsNullOrEmpty()) filter.RequestType = ""; ;

            if (filter.NIK.IsNullOrEmpty())
            {
                filter.NIK = "";
            }
            if (filter.Name.IsNullOrEmpty())
            {
                filter.Name = "";
            }

            if (!filter.ApprovalType.IsNullOrEmpty() && filter.ApprovalType.ToLower() == "approvecentralize")
            {
                SimpleExpression isRejected = Restrictions.Eq("IsRejected", 0);
                conjuction.Add(isRejected);
            }
            if (!filter.RequestUser.IsNullOrEmpty())
            {
                SimpleExpression requestBy = Restrictions.Eq("RequestUser", filter.RequestUser);
                conjuction.Add(requestBy);
            }

            switch (orderColumn)
            {
                case "RequestTypeString":
                    orderColumn = "RequestType";
                    break;
                case "RequestDateString":
                    orderColumn = "RequestDate";
                    break;
                default:
                    break;
            }

            PagedResult<CentralizeUnitHRequest> paged = new PagedResult<CentralizeUnitHRequest>(pageNumber, itemsPerPage);

            //Alias
            CentralizeUnitHRequest a = null;
            User b = null;

            string sql = "";
            string sqlbranch = "";
            string sqlorder = "";

            //if (user.BranchCode.Trim() != "000")
            //{
            //    sqlbranch = "AND BranchCode IN(SELECT BRANCH_CODE " +
            //        " FROM BRANCH " +
            //        " WHERE BRANCH_CODE = '" + user.BranchCode.Trim() + "' OR MAIN_BRANCH = '" + user.BranchCode.Trim() + "') ";
            //}

            if (orderKey.ToLower() == "asc")
            { sqlorder = " ASC "; }
            else
            { sqlorder = " DESC "; }

            sql = "SELECT NIK " +
                    ", Name" +
                    ", DocType" +
                    ", Region" +
                    ", Division" +
                    ", Departement" +
                    ", Title as Titles" +
                    ", Note" +
                    ", RequestType" +
                    ", RequestDate" +
                    ", RequestUser" +
                    ", RequestReason" +
                    ", IsRejected " +
                "FROM CENTRALIZE_UNIT_HEADER_REQUEST " +
                "WHERE NIK LIKE '%" + filter.NIK.Trim() + "%' AND Name LIKE '%" + filter.Name + "%' AND RequestType LIKE '%" + filter.RequestType + "%' AND IsRejected = 0 " + sqlbranch +
                " order by " + Projections.Property(orderColumn).ToString() + sqlorder +
                " OFFSET " + pageNumber * 10 + " ROWS" +
                " FETCH NEXT " + itemsPerPage + " ROWS ONLY";


            NHibernate.IQuery myQuery = session.CreateSQLQuery(sql).SetResultTransformer(Transformers.AliasToBean<CentralizeUnitHRequest>());
            paged.Items = myQuery.List<CentralizeUnitHRequest>().Take(itemsPerPage);

            string sqlCount = "SELECT NIK " +
                    ", Name" +
                    ", DocType" +
                    ", Region" +
                    ", Division" +
                    ", Departement" +
                    ", Title as Titles" +
                    ", Note" +
                    ", RequestType" +
                    ", RequestDate" +
                    ", RequestUser" +
                    ", RequestReason" +
                    ", IsRejected " +
                "FROM CENTRALIZE_UNIT_HEADER_REQUEST " +
                "WHERE NIK LIKE '%" + filter.NIK.Trim() + "%' AND Name LIKE '%" + filter.Name + "%' AND RequestType LIKE '%" + filter.RequestType + "%' AND IsRejected = 0 " + sqlbranch;

            NHibernate.IQuery myQuery2 = session.CreateSQLQuery(sqlCount).SetResultTransformer(Transformers.AliasToBean<CentralizeUnitHRequest>());
            paged.TotalItems = myQuery2.List<CentralizeUnitHRequest>().Count();
            return paged;
        }

        public PagedResult<CentralizeUnitHRequest> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, CentralizeReqFilter filter)
        {
            Conjunction conjuction = Restrictions.Conjunction();
            SimpleExpression reqType = Restrictions.Eq("RequestType", filter.RequestType);
            if (!filter.RequestType.IsNullOrEmpty()) conjuction.Add(reqType);

            if (!filter.NIK.IsNullOrEmpty())
            {
                SimpleExpression accNo = Restrictions.Like("NIK", filter.NIK, MatchMode.Start);
                conjuction.Add(accNo);
            }
            if (!filter.Name.IsNullOrEmpty())
            {
                SimpleExpression accName = Restrictions.Like("Name", filter.Name, MatchMode.Anywhere);
                conjuction.Add(accName);
            }
            //if (!filter.Branches.IsNullOrEmpty())
            //{
            //    conjuction.Add(Restrictions.In("BranchCode", filter.Branches.Select(x => x.BranchCode).ToArray()));
            //}

            if (!filter.ApprovalType.IsNullOrEmpty() && filter.ApprovalType.ToLower() == "approvecentralize")
            {
                SimpleExpression isRejected = Restrictions.Eq("IsRejected", false);
                conjuction.Add(isRejected);
            }

            if (!filter.RequestUser.IsNullOrEmpty())
            {
                SimpleExpression requestBy = Restrictions.Eq("RequestUser", filter.RequestUser);
                conjuction.Add(requestBy);
            }

            if (orderColumn == "RequestTypeString") orderColumn = "RequestType";
            if (orderColumn == "RequestDateString") orderColumn = "RequestDate";

            PagedResult<CentralizeUnitHRequest> paged = new PagedResult<CentralizeUnitHRequest>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<CentralizeUnitHRequest>()
                                             .Where(conjuction)
                                             .OrderBy(Projections.Property(orderColumn)).Asc
                                             .Skip((pageNumber) * itemsPerPage)
                                             .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<CentralizeUnitHRequest>()
                                             .Where(conjuction)
                                             .OrderBy(Projections.Property(orderColumn)).Desc
                                             .Skip((pageNumber) * itemsPerPage)
                                             .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() => session.QueryOver<CentralizeUnitHRequest>()
                                            .Where(conjuction).RowCount());
            return paged;
        }

        public PagedResult<CentralizeUnitHRequest> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<CentralizeUnitHRequest> paged = new PagedResult<CentralizeUnitHRequest>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<CentralizeUnitHRequest>()
                                                     .OrderByDescending(GetOrderByExpression<CentralizeUnitHRequest>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<CentralizeUnitHRequest>()
                                                     .OrderBy(GetOrderByExpression<CentralizeUnitHRequest>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = paged.Items.Count();
            return paged;
        }
        public PagedResult<CentralizeUnitHRequest> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<CentralizeUnitHRequest> specification;

            switch (searchColumn)
            {
                case "AccountNumber":
                    specification = new AdHocSpecification<CentralizeUnitHRequest>(s => s.NIK.Contains(searchValue));
                    break;
                case "AccountName":
                    specification = new AdHocSpecification<CentralizeUnitHRequest>(s => s.NIK.Contains(searchValue));
                    break;
                case "Note":
                    specification = new AdHocSpecification<CentralizeUnitHRequest>(s => s.Note.Contains(searchValue));
                    break;
                default:
                    specification = new AdHocSpecification<CentralizeUnitHRequest>(s => s.NIK.Contains(searchValue));
                    break;
            }

            PagedResult<CentralizeUnitHRequest> paged = new PagedResult<CentralizeUnitHRequest>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<CentralizeUnitHRequest>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<CentralizeUnitHRequest>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<CentralizeUnitHRequest>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<CentralizeUnitHRequest>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<CentralizeUnitHRequest>()
                                .Where(specification.ToExpression()).Count());

            paged.TotalItems = paged.Items.Count();
            return paged;
        }

        public PagedResult<CentralizeUnitHRequest> FindAllPagedCreate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, CentralizeReqFilter filter)
        {
            Conjunction conjuction = Restrictions.Conjunction();
            SimpleExpression reqType = Restrictions.Eq("RequestType", filter.RequestType);
            if (!filter.RequestType.IsNullOrEmpty()) conjuction.Add(reqType);

            if (!filter.NIK.IsNullOrEmpty())
            {
                SimpleExpression accNo = Restrictions.Like("NIK", filter.NIK, MatchMode.Start);
                conjuction.Add(accNo);
            }
            if (!filter.Name.IsNullOrEmpty())
            {
                SimpleExpression accName = Restrictions.Like("Name", filter.Name, MatchMode.Anywhere);
                conjuction.Add(accName);
            }
            //if (!filter.Branches.IsNullOrEmpty())
            //{
            //    conjuction.Add(Restrictions.In("BranchCode", filter.Branches.Select(x => x.BranchCode).ToArray()));
            //}

            if (!filter.RequestUser.IsNullOrEmpty())
            {
                SimpleExpression requestBy = Restrictions.Eq("RequestUser", filter.RequestUser);
                conjuction.Add(requestBy);
            }

            PagedResult<CentralizeUnitHRequest> paged = new PagedResult<CentralizeUnitHRequest>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<CentralizeUnitHRequest>()
                                             .Where(conjuction)
                                             .And(x => x.RequestDate.Value.Date == DateTime.Now.Date)
                                             .OrderBy(Projections.Property(orderColumn)).Asc
                                             .Skip((pageNumber) * itemsPerPage)
                                             .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<CentralizeUnitHRequest>()
                                             .Where(conjuction)
                                             .And(x => x.RequestDate.Value.Date == DateTime.Now.Date)
                                             .OrderBy(Projections.Property(orderColumn)).Desc
                                             .Skip((pageNumber) * itemsPerPage)
                                             .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() => session.QueryOver<CentralizeUnitHRequest>()
                                            .Where(conjuction)
                                             .And(x => x.RequestDate.Value.Date == DateTime.Now.Date)
                                             .RowCount());
            return paged;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public void DeleteCentralizeUnitHReq(string ID)
        {
            string hqlDelete = "delete CentralizeUnitHRequest where NIK = :NIK";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("NIK", ID)
                    .ExecuteUpdate();
        }



        public void AddSignReqD(CentralizeDSignReq item)
        {
            transact(() => session.Save(item));
        }

        public IList<CentralizeDSignReq> getSignReqD(string nik)
        {
            IList<CentralizeDSignReq> req = transact(() => session.QueryOver<CentralizeDSignReq>().Where(f => f.NIK == nik && f.Signature != null).OrderBy(o => o.FileSeq).Asc.List());
            foreach (CentralizeDSignReq sign in req)
            {
                sign.Signature = sign.Signature;
            }
            return req;
        }

        public void DeleteSignReqD(string ID)
        {
            string hqlDelete = "delete CentralizeDSignReq where NIK = :nik";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("nik", ID)
                    .ExecuteUpdate();
        }

        public void DeleteSignReqD(long ID)
        {
            string hqlDelete = "delete CentralizeDSignReq where ID = :ID";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetDouble("ID", ID)
                    .ExecuteUpdate();
        }

        public void AddDocReqD(CentralizeDDocReq item)
        {
            transact(() => session.Save(item));
        }

        public IList<CentralizeDDocReq> getDocReqD(string nik)
        {
            string sqlSelect = "SELECT ID, NIK = NIK, FileType = FILE_TYPE, DocumentType = DOCUMENT_TYPE, RequestType = RequestType, " +
                "RequestDate = RequestDate, RequestUser = RequestUser, RequestReason = RequestReason,FileBlob=FILE_BLOB, FileSeq = FILE_SEQ " +
                "FROM dbo.CENTRALIZE_DETAIL_DOC_REQ " +
                "WHERE NIK = '" + nik + "' " +
                "ORDER BY FILE_SEQ";

            NHibernate.IQuery myQuery = session.CreateSQLQuery(sqlSelect).SetResultTransformer(Transformers.AliasToBean<CentralizeDDocReq>());

            IList<CentralizeDDocReq> req = myQuery.List<CentralizeDDocReq>();

            return req;
        }

        public void DeleteDocReqD(string ID)
        {
            string hqlDelete = "delete CentralizeDDocReq where NIK = :nik";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetString("nik", ID)
                    .ExecuteUpdate();
        }

        public void DeleteDocReqD(long ID)
        {
            string hqlDelete = "delete CentralizeDDocReq where ID = :ID";
            int deletedEntities = session.CreateQuery(hqlDelete)
                    .SetDouble("ID", ID)
                    .ExecuteUpdate();
        }

        public CentralizeDDocReq getDocReqD(long ID)
        {
            CentralizeDDocReq ret = session.QueryOver<CentralizeDDocReq>().Where(d => d.Id == ID).SingleOrDefault();
            ret.FileBlob = ret.FileBlob;

            return ret;
        }
    }
}
