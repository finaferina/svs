﻿using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using UOB.SVS.Model;
using UOB.SVS.Repository;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System;
using Treemas.Credential.Model;
using Treemas.Base.Utilities;

namespace UOB.SVS.Repository
{
    public class VApprovalRepository : RepositoryControllerString<VApproval>, IVApprovalRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public VApprovalRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }
        
        public PagedResult<VApproval> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<VApproval> paged = new PagedResult<VApproval>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<VApproval>()
                                                     .OrderByDescending(GetOrderByExpression<VApproval>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<VApproval>()
                                                     .OrderBy(GetOrderByExpression<VApproval>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }
        public PagedResult<VApproval> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            Specification<VApproval> specification;

            switch (searchColumn)
            {
                case "DocType":
                    specification = new AdHocSpecification<VApproval>(s => s.ApprovalStatus.Contains(searchValue));
                    break;
                case "Description":
                    specification = new AdHocSpecification<VApproval>(s => s.ApprovalStatus.Contains(searchValue));
                    break;              
                default:
                    specification = new AdHocSpecification<VApproval>(s => s.ApprovalFuntion.Contains(searchValue));
                    break;
            }

            PagedResult<VApproval> paged = new PagedResult<VApproval>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.Query<VApproval>()
                                 .Where(specification.ToExpression())
                                 .OrderByDescending(GetOrderByExpression<VApproval>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() =>
                          session.Query<VApproval>()
                                 .Where(specification.ToExpression())
                                 .OrderBy(GetOrderByExpression<VApproval>(orderColumn))
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).ToList());
            }


            paged.TotalItems = transact(() =>
                         session.Query<VApproval>()
                                .Where(specification.ToExpression()).Count());

            //paged.TotalItems = Count;
            return paged;
        }
        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }

        public PagedResult<VApproval> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter)
        {
            //SimpleExpression status = Restrictions.Eq("ApprovalStatus", filter.ApprovalStatus);
            SimpleExpression apprtype = Restrictions.Eq("RequestType", filter.ApprovalType);
            SimpleExpression apprfunc = Restrictions.Eq("ApprovalFuntion", filter.ApprovalFuntion);

            Conjunction conjuction = Restrictions.Conjunction();
            //if (!filter.ApprovalStatus.Trim().IsNullOrEmpty()) conjuction.Add(status);
            if (!filter.ApprovalType.Trim().IsNullOrEmpty()) conjuction.Add(apprtype);
            if (!filter.ApprovalFuntion.Trim().IsNullOrEmpty()) conjuction.Add(apprfunc);

            PagedResult<VApproval> paged = new PagedResult<VApproval>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<VApproval>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<VApproval>().Where(conjuction)
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() => session.QueryOver<VApproval>().Where(conjuction).RowCount());
            return paged;
        }

        public PagedResult<VApproval> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, ApplicationCRFilter filter, User user, string[] arrFeature)
        {
            //SimpleExpression status = Restrictions.Eq("ApprovalStatus", filter.ApprovalStatus);
            SimpleExpression apprtype = Restrictions.Eq("RequestType", filter.ApprovalType);
            SimpleExpression apprfunc = Restrictions.Eq("ApprovalFuntion", filter.ApprovalFuntion);

            Conjunction conjuction = Restrictions.Conjunction();
            //if (!filter.ApprovalStatus.Trim().IsNullOrEmpty()) conjuction.Add(status);
            if (!filter.ApprovalType.Trim().IsNullOrEmpty()) conjuction.Add(apprtype);
            if (!filter.ApprovalFuntion.Trim().IsNullOrEmpty()) conjuction.Add(apprfunc);

            PagedResult<VApproval> paged = new PagedResult<VApproval>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                if (user.Username == "Administrator")
                {
                    paged.Items = transact(() => session.QueryOver<VApproval>().Where(conjuction).And(x => x.ApprovalFuntion.IsIn(arrFeature))
                                                         .OrderBy(Projections.Property(orderColumn)).Asc
                                                         //.Skip((pageNumber) * itemsPerPage)
                                                         .Take(itemsPerPage)
                                                         .List());
                }
                else
                {
                    paged.Items = transact(() => session.QueryOver<VApproval>().Where(conjuction).And(x => x.MakerUser != user.Username).And(x => x.ApprovalFuntion.IsIn(arrFeature))
                                                         .OrderBy(Projections.Property(orderColumn)).Asc
                                                         //.Skip((pageNumber) * itemsPerPage)
                                                         .Take(itemsPerPage)
                                                         .List());
                }
            }
            else
            {
                if (user.Username == "Administrator")
                {
                    paged.Items = transact(() => session.QueryOver<VApproval>().Where(conjuction).And(x => x.ApprovalFuntion.IsIn(arrFeature))
                                                         .OrderBy(Projections.Property(orderColumn)).Desc
                                                         //.Skip((pageNumber) * itemsPerPage)
                                                         .Take(itemsPerPage)
                                                         .List());
                }
                else
                {
                    paged.Items = transact(() => session.QueryOver<VApproval>().Where(conjuction).And(x => x.MakerUser != user.Username).And(x => x.ApprovalFuntion.IsIn(arrFeature))
                                                         .OrderBy(Projections.Property(orderColumn)).Desc
                                                         //.Skip((pageNumber) * itemsPerPage)
                                                         .Take(itemsPerPage)
                                                         .List());
                }
            }

            paged.TotalItems = transact(() => session.QueryOver<VApproval>().Where(conjuction).And(x => x.MakerUser != user.Username).And(x => x.ApprovalFuntion.IsIn(arrFeature)).RowCount());
            return paged;
        }

        public VApproval GetApproval(string id)
        {
            return transact(() => session.QueryOver<VApproval>()
                                                .Where(f => f.Id == id).SingleOrDefault());

        }
    }
}
