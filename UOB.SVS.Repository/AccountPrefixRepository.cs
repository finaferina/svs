﻿using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using LinqSpecs;
using NHibernate.Linq;
using NHibernate.Criterion;
using Treemas.Base.Repository;
using UOB.SVS.Model;
using UOB.SVS.Repository;
using UOB.SVS.Interface;
using Treemas.Base.Utilities.Queries;
using System;
using NHibernate.Transform;

namespace UOB.SVS.Repository
{
    public class AccountPrefixRepository : RepositoryController<AccountPrefix>, IAccountPrefixRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;

        public AccountPrefixRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public PagedResult<AccountPrefix> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<AccountPrefix> paged = new PagedResult<AccountPrefix>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.Query<AccountPrefix>()
                                                     .OrderByDescending(GetOrderByExpression<AccountPrefix>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }
            else
            {
                paged.Items = transact(() => session.Query<AccountPrefix>()
                                                     .OrderBy(GetOrderByExpression<AccountPrefix>(orderColumn))
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).ToList());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<AccountPrefix> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            PagedResult<AccountPrefix> paged = new PagedResult<AccountPrefix>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<AccountPrefix>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<AccountPrefix>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }


            paged.TotalItems = transact(() =>
                         session.QueryOver<AccountPrefix>()
                                .Where(specification)
                                .RowCount());
            return paged;
        }

        public PagedResult<AccountPrefix> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string branchCode)
        {
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            PagedResult<AccountPrefix> paged = new PagedResult<AccountPrefix>(pageNumber, itemsPerPage);
            string sql = "";
            string sqlCount = "";
            string sqlorder = "";

            if (orderKey.ToLower() == "asc")
            { sqlorder = " ASC "; }
            else
            { sqlorder = " DESC "; }

            sql = "SELECT " +
                "   PrefixCode = ACCOUNT_PREFIX_CODE " +
                "   ,Description = DESCRIPTION " +
                "FROM BRANCH_ACCOUNT_PREFIX " +
                "WHERE BRANCH_CODE = '" + branchCode + "' AND ACCOUNT_PREFIX_CODE LIKE '" + searchValue + "%' " +
                "UNION ALL " +
                "SELECT " +
                "    PrefixCode = ACCOUNT_PREFIX_CODE " +
                "    ,Description = DESCRIPTION " +
                "FROM ACCOUNT_PREFIX " +
                "WHERE ACCOUNT_PREFIX_CODE NOT IN (SELECT ACCOUNT_PREFIX_CODE FROM BRANCH_ACCOUNT_PREFIX) " +
                "AND ACCOUNT_PREFIX_CODE LIKE '" + searchValue + "%' " +
                "order by " + Projections.Property(orderColumn).ToString() + sqlorder +
                " OFFSET " + pageNumber * 10 + " ROWS " +
                "FETCH NEXT " + itemsPerPage + " ROWS ONLY";

            sqlCount = "SELECT " +
               "   PrefixCode = ACCOUNT_PREFIX_CODE " +
               "   ,Description = DESCRIPTION " +
               "FROM BRANCH_ACCOUNT_PREFIX " +
               "WHERE BRANCH_CODE = '" + branchCode + "' AND ACCOUNT_PREFIX_CODE LIKE '" + searchValue + "%' " +
               "UNION ALL " +
               "SELECT " +
               "    PrefixCode = ACCOUNT_PREFIX_CODE " +
               "    ,Description = DESCRIPTION " +
               "FROM ACCOUNT_PREFIX " +
               "WHERE ACCOUNT_PREFIX_CODE NOT IN (SELECT ACCOUNT_PREFIX_CODE FROM BRANCH_ACCOUNT_PREFIX) " +
               "AND ACCOUNT_PREFIX_CODE LIKE '" + searchValue + "%' ";

            NHibernate.IQuery myQuery = session.CreateSQLQuery(sql).SetResultTransformer(Transformers.AliasToBean<AccountPrefix>());
            NHibernate.IQuery myCountQuery = session.CreateSQLQuery(sqlCount).SetResultTransformer(Transformers.AliasToBean<AccountPrefix>());

            paged.Items = myQuery.List<AccountPrefix>().Take(itemsPerPage);
            paged.TotalItems = myCountQuery.List<AccountPrefix>().Count;
            paged.PageNumber = pageNumber;

            return paged;
        }

        public AccountPrefix getAccountPrefix(long id)
        {
            return transact(() => session.QueryOver<AccountPrefix>().Where(f => f.Id == id).SingleOrDefault());
        }

        public AccountPrefix getAccountPrefix(string PrefixCode)
        {
            return transact(() => session.QueryOver<AccountPrefix>().Where(f => f.PrefixCode == PrefixCode).SingleOrDefault());
        }

        public IList<AccountPrefix> getAccountPrefixes()
        {
            return transact(() => session.QueryOver<AccountPrefix>().List());
        }

        public bool IsDuplicate(string PrefixCode)
        {
            int rowCount = transact(() => session.QueryOver<AccountPrefix>()
                                                .Where(f => f.PrefixCode == PrefixCode).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        private Func<T, object> GetOrderByExpression<T>(string sortColumn)
        {
            Func<T, object> orderByExpr = null;
            if (!String.IsNullOrEmpty(sortColumn))
            {
                Type sponsorResultType = typeof(T);

                if (sponsorResultType.GetProperties().Any(prop => prop.Name == sortColumn))
                {
                    System.Reflection.PropertyInfo pinfo = sponsorResultType.GetProperty(sortColumn);
                    orderByExpr = (data => pinfo.GetValue(data, null));
                }
            }
            return orderByExpr;
        }
    }
}
