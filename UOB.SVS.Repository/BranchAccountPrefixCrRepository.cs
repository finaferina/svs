﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Treemas.Base.Repository;
using Treemas.Base.Utilities.Queries;
using UOB.SVS.Interface;
using UOB.SVS.Model;

namespace UOB.SVS.Repository
{
    public class BranchAccountPrefixCrRepository : RepositoryController<BranchAccountPrefixCr>, IBranchAccountPrefixCrRepository
    {
        private readonly SystemRepositoryBase systemRepositoryBase;
        public BranchAccountPrefixCrRepository()
        {
            this.systemRepositoryBase = SystemRepositoryBase.Instance;
            m_unitOfWorkFactory = systemRepositoryBase.UnitOfWork;
        }

        public int deleteRequest(string BranchCode, string PrefixCode)
        {
            string hqlSyntax = "delete BranchAccountPrefixCr where BranchCode = :BranchCode and PrefixCode = :PrefixCode and Approved = :Approved";
            return session.CreateQuery(hqlSyntax)
                    .SetString("BranchCode", BranchCode)
                    .SetString("PrefixCode", PrefixCode)
                    .SetBoolean("Approved", false)
                    .ExecuteUpdate();
        }

        public PagedResult<BranchAccountPrefixCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            throw new NotImplementedException();
        }

        public PagedResult<BranchAccountPrefixCr> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue, string branchCode)
        {
            SimpleExpression specification;
            specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);

            PagedResult<BranchAccountPrefixCr> paged = new PagedResult<BranchAccountPrefixCr>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<BranchAccountPrefixCr>()
                                 .Where(specification).And(b => b.BranchCode == branchCode)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<BranchAccountPrefixCr>()
                                 .Where(specification).And(b => b.BranchCode == branchCode)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }

            paged.TotalItems = transact(() =>
                         session.QueryOver<BranchAccountPrefixCr>()
                                .Where(specification).And(b => b.BranchCode == branchCode)
                                .RowCount());
            return paged;
        }

        public BranchAccountPrefixCr getAccountPrefix(long id)
        {
            return transact(() => session.QueryOver<BranchAccountPrefixCr>().Where(f => f.Id == id).SingleOrDefault());
        }

        public BranchAccountPrefixCr getAccountPrefix(string PrefixCode)
        {
            return transact(() => session.QueryOver<BranchAccountPrefixCr>().Where(f => f.PrefixCode == PrefixCode).SingleOrDefault());
        }
        public BranchAccountPrefixCr getAccountPrefixExisting(string PrefixCode)
        {
            return transact(() => session.QueryOver<BranchAccountPrefixCr>().Where(f => f.PrefixCode == PrefixCode && f.ApprovalStatus == "N").SingleOrDefault());
        }

        public BranchAccountPrefixCr getAccountPrefix(string PrefixCode, string BranchCode)
        {
            return transact(() => session.QueryOver<BranchAccountPrefixCr>().Where(f => f.PrefixCode == PrefixCode && f.BranchCode == BranchCode && f.ApprovalStatus == "N").SingleOrDefault());
        }

        public IList<BranchAccountPrefixCr> getAccountPrefixes()
        {
            return transact(() => session.QueryOver<BranchAccountPrefixCr>().List());
        }

        public IList<BranchAccountPrefixCr> getAccountPrefixes(string BranchCode, string CreatedUser)
        {
            return transact(() => session.QueryOver<BranchAccountPrefixCr>().Where(f => f.BranchCode == BranchCode && f.CreatedBy == CreatedUser && f.ApprovalStatus == "N").List());
        }

        public IList<BranchAccountPrefixCr> getAccountPrefixes(List<string> Branches)
        {
            return transact(() => session.QueryOver<BranchAccountPrefixCr>().Where(val => val.BranchCode.IsIn(Branches.ToArray())).List());
        }

        public IList<BranchAccountPrefixCr> getAccountPrefixesRequest(string PrefixCode)
        {
            return transact(() => session.QueryOver<BranchAccountPrefixCr>().Where(f => f.PrefixCode == PrefixCode && f.Approved == false).List());
        }

        public bool IsDuplicate(string PrefixCode)
        {
            int rowCount = transact(() => session.QueryOver<BranchAccountPrefixCr>()
                                                .Where(f => f.PrefixCode == PrefixCode).RowCount());
            if (rowCount > 0)
                return true;
            else
                return false;
        }

        public IList<BranchAccountPrefixCr> getAccountPrefixesRequest(List<string> Branches)
        {
            return transact(() => session.QueryOver<BranchAccountPrefixCr>().Where(f => f.BranchCode.IsIn(Branches.ToArray()) && f.Approved == false).List());
        }
    }
}
