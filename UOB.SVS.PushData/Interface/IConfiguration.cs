﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UOB.SVS.PushData
{
    public interface IConfiguration
    {
        ConnectionParam GetConnectionSetting();
        JobParam GetJobParam();
    }
}
