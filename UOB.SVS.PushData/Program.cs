﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;

namespace UOB.SVS.PushData
{
    class Program
    {
        private static IConfiguration _config = null;

        private static string strFileName = "";
        private static string strFileLocation;
        private static string strFileSuccess;
        private static string strFileError;

        static void Main(string[] args)
        {
            GetJobConfiguration();
            RunPushJob();
        }

        private static IConfiguration JobConfiguration
        {
            get
            {
                if (_config == null)
                {
                    _config = new SystemConfiguration();
                }
                return _config;
            }
        }

        private static void GetJobConfiguration()
        {
            var config = JobConfiguration.GetJobParam();
            strFileLocation = config.FileLocation;
            strFileSuccess = config.FileSuccess;
            strFileError = config.FileError;
        }

        private static void RunPushJob()
        {
            DirectoryInfo di = new DirectoryInfo(strFileLocation);
            foreach (FileInfo fi in di.GetFiles())
            {
                if (File.Exists(fi.FullName))
                {
                    if (!IsFileLocked(fi.FullName))
                    {
                        string[] strArgs = new string[1];
                        strArgs[0] = fi.FullName;

                        PushData.FileName = fi.FullName;
                        PushData.SuccessFileDest = strFileSuccess;
                        PushData.ErrorFileDest = strFileError;

                        string pushResult = PushData.ProcessPushData();

                        strFileName = fi.Name + "_" + DateTime.Now.ToString("yyyyMMddhhmmss");

                        if (pushResult == "Success") {
                            MoveToFolder(strFileSuccess, fi.FullName, strFileName);
                        }
                        else {
                            MoveToFolder(strFileError, fi.FullName, strFileName);
                        }
                    }
                }
            }
        }

        public static bool IsFileLocked(string filePath)
        {
            // check whether file is read only
            bool isReadOnly = ((File.GetAttributes(filePath) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly);
            return isReadOnly;
        }

        public static void CreateNewFolder(string Path)
        {
            if (!Directory.Exists(Path))
            {
                Directory.CreateDirectory(Path);
            }
        }

        public static void MoveToFolder(string DestPath, string SourcePath, string FileName)
        {
            if (File.Exists(SourcePath))
            {
                if (!Directory.Exists(DestPath))
                {
                    CreateNewFolder(DestPath);
                }
                Directory.Move(SourcePath, DestPath + FileName);
            }
        }

    }
}
