﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UOB.SVS.PushData
{
    public class PushData
    {
        private static IConfiguration _config = null;
        private static string strConnectionString = "";
        public static string FileName { get; set; }
        public static string SuccessFileDest { get; set; }
        public static string ErrorFileDest { get; set; }
        private string FileHistoryName { get; set; }

        private static IConfiguration JobConfiguration
        {
            get
            {
                if (_config == null)
                {
                    _config = new SystemConfiguration();
                }
                return _config;
            }
        }

        public static string ProcessPushData()
        {
            strConnectionString = GetConnectionString();
            DateTime beginTime = DateTime.Now;

            WriteJobLog("Processing", "", beginTime);

            // clear all file attributes
            File.SetAttributes(FileName, FileAttributes.Normal);

            // set just only archive and read only attributes (no other attribute will set)
            File.SetAttributes(FileName, FileAttributes.Archive | FileAttributes.ReadOnly);


            CultureInfo CI = CultureInfo.CreateSpecificCulture("id-ID");

            string[] Data = (File.ReadAllLines(FileName, Encoding.UTF8)).Distinct().ToArray();

            SqlConnection sqlConn = new SqlConnection(strConnectionString);
            SqlTransaction sqlTrans = null;
            SqlCommand sqlComm = new SqlCommand();

            sqlConn.Open();
            sqlTrans = sqlConn.BeginTransaction();
            sqlComm.Connection = sqlConn;
            sqlComm.Transaction = sqlTrans;

            sqlComm.CommandText = "TRUNCATE TABLE ACCOUNT_MASTER";
            sqlComm.ExecuteNonQuery();
            try
            {
                foreach (string data in Data)
                {
                    string[] accData = data.Split(';');

                    sqlComm.Parameters.Clear();
                    sqlComm.Parameters.Add("@ACC_NO", SqlDbType.VarChar, 20).Value = accData[0].Trim();
                    sqlComm.Parameters.Add("@ACC_NAME", SqlDbType.VarChar, 100).Value = accData[1].Trim();
                    sqlComm.Parameters.Add("@ACC_TYPE", SqlDbType.Char, 10).Value = accData[3].Trim();
                    sqlComm.Parameters.Add("@CIF_NO", SqlDbType.VarChar, 20).Value = accData[4].Trim();
                    sqlComm.Parameters.Add("@BRANCH_CODE", SqlDbType.VarChar, 3).Value =accData[5].Trim();
                    sqlComm.Parameters.Add("@STATUS", SqlDbType.VarChar, 50).Value = accData[6].Trim();
                    sqlComm.Parameters.Add("@DATE_OPENED", SqlDbType.Date).Value = DateTime.Parse(accData[7].Trim(), CI);
                    sqlComm.CommandText = "INSERT INTO dbo.ACCOUNT_MASTER (ACC_NO,	ACC_NAME, ACC_TYPE, CIF_NO, BRANCH_CODE, STATUS, DATE_OPENED) " +
                        "VALUES(@ACC_NO, @ACC_NAME, @ACC_TYPE, @CIF_NO, @BRANCH_CODE, @STATUS, @DATE_OPENED)";
                    sqlComm.ExecuteNonQuery();

                }
                WriteJobLog("Success", Data.Length.ToString() + " Inserted to Account Master", beginTime);

                sqlTrans.Commit();
                sqlComm.Dispose();
                sqlTrans.Dispose();
                sqlConn.Close();
                sqlConn.Dispose();

                return "Success";
            }
            catch (Exception ex)
            {
                WriteJobLog("Failed", ex.Message.ToString(), beginTime);
                sqlTrans.Rollback();
                sqlComm.Dispose();
                sqlTrans.Dispose();
                sqlConn.Close();
                sqlConn.Dispose();

                return "Failed";
            }
        }

        static string GetConnectionString()
        {
            var conn = JobConfiguration.GetConnectionSetting();
            return "Data Source=" + conn.DBInstance + ";Initial Catalog=" + conn.DBName + ";Persist Security Info=True;User ID=" + conn.DBUser + ";Password=" + conn.DBPassword + ";";
        }

        static void WriteJobLog(string status, string remark, DateTime StartTime)
        {
            SqlConnection sqlConn = new SqlConnection(strConnectionString);
            SqlCommand sqlComm = new SqlCommand();

            sqlConn.Open();
            sqlComm.Connection = sqlConn;

            sqlComm.CommandText = "SELECT JOB_ID, PROCESS_DATE FROM dbo.EOD_JOB " +
                "WHERE DATEDIFF(D, PROCESS_DATE, GETDATE()) = 0";
            SqlDataReader sqlReader = sqlComm.ExecuteReader();

            if (!sqlReader.HasRows)
            {
                sqlReader.Close();

                sqlComm.Parameters.Clear();
                sqlComm.Parameters.Add("@JOB_ID", SqlDbType.VarChar, 20).Value = "PUSH_DATA";
                sqlComm.Parameters.Add("@JOB_NAME", SqlDbType.VarChar, 50).Value = "Push Data Account Master";
                sqlComm.Parameters.Add("@STATUS", SqlDbType.Char, 20).Value = status;
                sqlComm.Parameters.Add("@REMARK", SqlDbType.VarChar, 500).Value = remark;
                sqlComm.Parameters.Add("@BEGIN_TIME", SqlDbType.DateTime).Value = StartTime;
                sqlComm.Parameters.Add("@END_TIME", SqlDbType.DateTime).Value = DateTime.Now;
                sqlComm.CommandText = "INSERT INTO DBO.EOD_JOB(JOB_ID, JOB_NAME, STATUS, REMARK, PROCESS_DATE, BEGIN_TIME, END_TIME) " +
                    "VALUES(@JOB_ID, @JOB_NAME, @STATUS, @REMARK, GETDATE(), @BEGIN_TIME, @END_TIME)";
                sqlComm.ExecuteNonQuery();
            }
            else
            {
                sqlReader.Close();

                sqlComm.Parameters.Clear();
                sqlComm.Parameters.Add("@JOB_ID", SqlDbType.VarChar, 20).Value = "PUSH_DATA";
                sqlComm.Parameters.Add("@STATUS", SqlDbType.Char, 20).Value = status;
                sqlComm.Parameters.Add("@REMARK", SqlDbType.VarChar, 500).Value = remark;
                sqlComm.Parameters.Add("@BEGIN_TIME", SqlDbType.DateTime).Value = StartTime;
                sqlComm.Parameters.Add("@END_TIME", SqlDbType.DateTime).Value = DateTime.Now;

                sqlComm.CommandText = "UPDATE DBO.EOD_JOB SET STATUS = @STATUS,REMARK = @REMARK, BEGIN_TIME = @BEGIN_TIME, END_TIME = @END_TIME " +
                    "WHERE JOB_ID = @JOB_ID AND DATEDIFF(D, PROCESS_DATE, GETDATE()) = 0";
                sqlComm.ExecuteNonQuery();
            }

            sqlComm.Dispose();
            sqlConn.Close();
            sqlConn.Dispose();
        }
    }
}
