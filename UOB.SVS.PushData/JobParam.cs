﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UOB.SVS.PushData
{
    public class JobParam
    {
        public string FileLocation { get; set; }
        public string FileSuccess { get; set; }
        public string FileError { get; set; }
    }
}
