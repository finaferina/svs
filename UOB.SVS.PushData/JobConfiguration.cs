﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UOB.SVS.PushData
{
    public class JobConfiguration
    {
        public string JobID { get; set; }
        public string ConfigName { get; set; }
        public string ConfigValue { get; set; }
    }
}
