﻿using System.Drawing;
using System.Runtime.InteropServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Xml;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Security;
using System.Net.Security;
using System.Security.Authentication;

namespace SVSViewer
{
    public partial class frmSvsViewer : Form
    {
        public bool frmActivated = false ;

        [DllImport("user32.dll")]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SetClipboardViewer(IntPtr hWndNewViewer);

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern bool ChangeClipboardChain(IntPtr hWndRemove, IntPtr hWndNewNext);

        [DllImport("user32.dll")]
        static extern bool ClipCursor(ref RECT lpRect);

        static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
        static readonly IntPtr HWND_TOP = new IntPtr(0);
        static readonly IntPtr HWND_BOTTOM = new IntPtr(1);
        const UInt32 SWP_NOSIZE = 0x0001;
        const UInt32 SWP_NOMOVE = 0x0002;
        const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);


        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        private const int WM_DRAWCLIPBOARD = 0x0308;        // WM_DRAWCLIPBOARD message
        private IntPtr _clipboardViewerNext;


        public bool BolTrx;
        public bool BolAccept;
        public bool BolReject;
        public bool BolClose;

        public string CommsPort;
        public string CommsName;
        public string BranchId;
        public string AppId;
        public string WorkStnId;
        public string UserID;

        public string Reply = "";
        private int AcctNoIndex = 0;
        public static int LogLevel = -1;
        public static string LogFile = "";
        public static string LogPath = "";
        public const int cnsError = 0;
        public const int cnsEvent = 1;
        public const int cnsData = 2;

        private string Token;

        public List<string> lstAcctNo = new List<string>();
        public List<string> lstSubAcctNo = new List<string>();
        List<Acct> lstRcvdAcct = new List<Acct>();

        public string WebSvrUrl;


        static int FormHeight = 0;
        static int FormWidth = 0;
        static int SignHeight = 0;
        static int SignWidth = 0;
        static int IDHeight = 0;
        static int IDWidth = 0;

        System.Net.Sockets.TcpClient clientSocket;

        public frmSvsViewer()
        {
            InitializeComponent();

            _clipboardViewerNext = SetClipboardViewer(this.Handle); 
        }


        private void RestrictMouse()
        {
            RECT cursorLimits;

            cursorLimits.Left = this.Left;
            cursorLimits.Top = this.Top;
            cursorLimits.Right = this.Right;
            cursorLimits.Bottom = this.Bottom;
            ClipCursor(ref cursorLimits);

        }

        public static string ChkArchiveLog(string strFileName)
        {

            var files = new DirectoryInfo(LogPath).GetFiles("*.log").OrderByDescending(f => f.Name).Skip(0).ToList();

            if (files.Count == 0)
            {
                LogFile = LogPath + "\\SvsViewer_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".log";
                strFileName = LogFile;
            }
            else
            {

                FileInfo latestfi = new FileInfo(files[0].FullName);
                long s1 = latestfi.Length;

                if (s1 > 50000000)
                {
                    LogFile = LogPath + "\\SvsViewer_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".log";
                    strFileName = LogFile;
                }

                if (strFileName == "")
                {
                    LogFile = files[0].FullName;
                    strFileName = LogFile;

                }

                if (files.Count > 10)
                {
                    for (int i = 10; i < files.Count; i++)
                    {
                        FileInfo fi = new FileInfo(files[i].FullName);
                        fi.Delete();
                    }
                }
            }

            return strFileName;

        }

        private void frmView_Load(object sender, EventArgs e)
        {
             RestrictMouse() ;

            // _hook.Hook();

            String strConFileName = AppPath() + "\\" + "SvsViewer.ini";

            if (ReadFile(strConFileName) == true)
            {
                if (LogPath == "")
                {
                    LogPath = AppPath();
                }

                LogFile = ChkArchiveLog("");

            }


            // if (ReadFile(strConFileName) == true)
            // {
            //     if (LogPath == "")
            //     {
            //         LogFile =   AppPath() + "\\SvsViewer_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".log";
            //     }
            //     else
            //     {
            //         LogFile = LogPath + "\\SvsViewer_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".log";
            //     }

            //}

            WriteLog(LogFile, "SvsViewer Started @ " + DateTime.Now.ToString("yyyyMMdd hh:mm:ss"), cnsEvent);

            if (FormHeight != 0 | FormWidth != 0)
            {
                this.Size = new Size(FormWidth, FormHeight);
                // Re-center the form after adjustment of form size
                int boundWidth = Screen.PrimaryScreen.Bounds.Width;
                int boundHeight = Screen.PrimaryScreen.Bounds.Height;
                int x = boundWidth - this.Width;
                int y = boundHeight - this.Height;
                // Position the form
                this.Location = new Point(x / 2, y / 2);
                
            }


            if (SignHeight != 0 | SignWidth != 0)
            {

                this.pnlLeft.Size = new Size(SignWidth +35, pnlLeft.Size.Height);
                this.pctSign.Size = new Size(SignWidth, SignHeight);
                this.pnlSignZoomBtn.Size = new Size(this.pnlLeft.Size.Width, pnlSignZoomBtn.Size.Height);
                
            }


            if (IDHeight != 0 | IDWidth != 0)
            {
                this.pnlRight.Size = new Size(IDWidth + 35, pnlRight.Size.Height);
                this.pctID.Size = new Size(IDWidth, IDHeight);
                this.pnlIDZoomBtn.Size = new Size(this.pnlRight.Size.Width,pnlIDZoomBtn.Size.Height) ;
            }

            pctSign.Image = pctUOB.Image;
            pctID.Image = pctUOB.Image;

            //   ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback((sender, certificate, chain, policyErrors) => { return true; });

            WriteLog(LogFile, "Loaded", cnsEvent);

            SetWindowPos(this.Handle, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS);


           // WinFormUtils.DoPaintEvents();

            //Application.OpenForms["frmSvsViewer"].BringToFront();

            //RestrictMouse();

            //MessageBox.Show("Test");

            //Application.DoEvents(); 

            //System.Threading.Thread.Sleep(5000);
         
            //if (BolTrx == true)
            //{
            //    WriteLog(LogFile, "GeTOken", cnsEvent);
            //    GetToken();
            //}

        }

        public void GetToken()
        {
            String strConnect = "";

            strConnect = Connect();


            Process z = Process.GetCurrentProcess();
            SetForegroundWindow(z.MainWindowHandle);
            RestrictMouse();

            if (strConnect == "OK")
            {

                String strXMLSendData;
                String strXMLRcvdData;
                String strAuthorized;

                WriteLog(LogFile, "Creating Request for Token", cnsEvent);

                strXMLSendData = WriteRequestToken(AppId, BranchId, WorkStnId, UserID, lstAcctNo);

                WriteLog(LogFile, strXMLSendData, cnsData);

                strXMLRcvdData = SendData(strXMLSendData);

                clientSocket.Close();

                strXMLRcvdData = SanitizeXmlString(strXMLRcvdData);


                if (strXMLRcvdData == "")
                {
                    WriteLog(LogFile, "Time Out", cnsError);
                    MessageBox.Show("Time Out");
                }

                WriteLog(LogFile, "Receive Reply for Token", cnsData);

                                
              
                
                strAuthorized = ReadTokenReply(strXMLRcvdData);

                WriteLog(LogFile, strXMLRcvdData, cnsData);



                if (strAuthorized == "Y")
                {
                    ShowData(0);
                }
                else
                {

                    Reply += strAuthorized + ";";
                    BolAccept = false;
                    this.Close();
                }
            }
            else
            {

                               
                Reply += "9002: Error connecting to SVS Application Server (" + strConnect + ");";
                WriteLog(LogFile, Reply, cnsError);

                if (BolTrx == true)
                {
                    BolAccept = false;
                    this.Close();
                }
                else
                {
                    MessageBox.Show(Reply);
                    Reply = "";
                }
            }



        }

        private string Connect()
        {

            try
            {
                clientSocket = new System.Net.Sockets.TcpClient();                
                WriteLog(LogFile, "Conecting to " + CommsName + "@ Port " + CommsPort, cnsEvent);
                clientSocket.Connect(CommsName, Convert.ToInt32(CommsPort));
                //SslStream clientstream = new SslStream(clientSocket.GetStream());
                //clientstream.AuthenticateAsClient(CommsName);
                //clientstream.AuthenticateAsClient(CommsName, null, SslProtocols.Tls12, false);
            }
            catch (Exception e)
            {
                return e.Message;
            }


            return "OK";

        }

        private string SendData(string strXMLData)
        {
            NetworkStream serverStream = clientSocket.GetStream();
            byte[] outStream = System.Text.Encoding.ASCII.GetBytes(strXMLData);
            serverStream.Write(outStream, 0, outStream.Length);
            serverStream.Flush();


            byte[] inStream = new byte[65536];
            serverStream.Read(inStream, 0, (int)clientSocket.ReceiveBufferSize);
            string returndata = System.Text.Encoding.ASCII.GetString(inStream);

            serverStream.Close();
            serverStream = null;
            return returndata;


        }

        public static void WriteLog(string strFileName, string strMsg)
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter(strFileName, true);
            file.WriteLine(DateTime.Now.ToString("dd MM yyyy hh:mm:ss") + " - " + strMsg);
            file.Close();
            return;

        }

        public static string MaskMsg(string strMsg)
        {

            int ix = 0;
            int ixLen = strMsg.Length - 1;
            int iStart;
            int iEnd;

            List<string> lstName = new List<string>();

            while (ix < ixLen)
            {
                iStart = strMsg.IndexOf("<SubAcctName>", ix);
                if (iStart == -1)
                {
                    ix = ixLen;
                }
                else
                {
                    iStart += 13;
                    iEnd = strMsg.IndexOf("</SubAcctName>", iStart);
                    lstName.Add(strMsg.Substring(iStart, iEnd - iStart));
                    ix = iEnd + 14;
                }
            }

            for (int i = 0; i < lstName.Count; i++)
            {
                strMsg = strMsg.Replace(lstName[i], "****");
            }

            return strMsg;
        }


        public static void WriteLog(string strFileName, string strMsg, int intLogLevel)
        {
            if (LogLevel >= intLogLevel)
            {
                //if (intLogLevel == cnsData)
                //{
                strMsg = MaskMsg(strMsg);
                //}

                System.IO.StreamWriter file = new System.IO.StreamWriter(strFileName, true);
                file.WriteLine(DateTime.Now.ToString("dd MM yyyy hh:mm:ss") + " - " + strMsg);
                file.Close();
            }

            return;

        }


        public string AppPath()
        {
            String strPath;
            strPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            return strPath.Substring(6);
        }

        public string AppName()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
        }

        public static bool ReadFile(string strFileName)
        {
            string[] strConfigs;

            if (System.IO.File.Exists(strFileName) == false)
            {
                //WriteLog(LogFile, "File " + strFileName + " Required");
                return false;
            }



            string[] lines = System.IO.File.ReadAllLines(@strFileName);
            string strConfigItem;
            string strConfigData;


            foreach (string line in lines)
            {

                strConfigs = line.Split('=');

                if ((strConfigs.Length == 2) == true)
                {

                    strConfigItem = strConfigs[0];
                    strConfigData = strConfigs[1];
                    switch (strConfigItem.Trim())
                    {
                        case "LogLevel":
                            LogLevel = Int32.Parse(strConfigData.Trim());
                            break;
                        case "LogPath":
                            LogPath = strConfigData.Trim();
                            break;
                        case "FormHeight":
                            FormHeight = Int32.Parse(strConfigData.Trim());
                            break;
                        case "FormWidth":
                            FormWidth = Int32.Parse(strConfigData.Trim());
                            break;
                        case "SignHeight":
                            SignHeight = Int32.Parse(strConfigData.Trim());
                            break;
                        case "SignWidth":
                            SignWidth = Int32.Parse(strConfigData.Trim());
                            break;
                        case "IDHeight":
                            IDHeight = Int32.Parse(strConfigData.Trim());
                            break;
                        case "IDWidth":
                            IDWidth = Int32.Parse(strConfigData.Trim());
                            break;

                    }


                }

            }


            return true;
        }

        private void cboSignList_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSign();
        }

        private void btnShowID_Click(object sender, EventArgs e)
        {
            if (txtAcctNo.Text == "")
            {
                MessageBox.Show("Account Number Required");
                return;
            }


            if (cboSignList.Items.Count == 0)
            {

                MessageBox.Show("Signatory Required");
                return;
            }


            LoadID();
        }

        private void frmView_Deactivate(object sender, EventArgs e)
        {
            pctID.Visible = false;
            pctSign.Visible = false;
        }

        private void frmView_Activated(object sender, EventArgs e)
        {

         
            pctID.Visible = true;
            pctSign.Visible = true;
        }

        private void frmView_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (clientSocket != null)
            {
                if (clientSocket.Connected == true)
                {
                    clientSocket.Close();
                }
            }

            ChangeClipboardChain(this.Handle, _clipboardViewerNext);   
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);    // Process the message 

            if (m.Msg == WM_DRAWCLIPBOARD)
            {

                IDataObject iData = Clipboard.GetDataObject();      // Clipboard's data

                if (iData.GetDataPresent(DataFormats.Text))
                {
                    WriteLog(LogFile, "Cleared Text from ClipBoard", cnsEvent);
                    Clipboard.Clear();
                }
                else if (iData.GetDataPresent(DataFormats.Bitmap))
                {
                    WriteLog(LogFile, "Cleared BitMap from ClipBoard", cnsEvent);
                    Clipboard.Clear();
                }
                //else
                //{
                //    WriteLog(LogFile, "Cleared from ClipBoard", cnsEvent);
                //    Clipboard.Clear();
                //}

            }
        }

        private void txtAcctNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                if (this.GetNextControl(ActiveControl, true) != null)
                {
                    e.Handled = true;
                    this.GetNextControl(ActiveControl, true).Focus();

                }
            }
        }

        private void txtAcctNo_Validated(object sender, EventArgs e)
        {

            if (txtAcctNo.Text != "" & BolTrx == false)
            {
                lstAcctNo.Clear();
                lstAcctNo.Add(txtAcctNo.Text);
                AcctNoIndex = 0;
                GetToken();
            }
        }

        private void btnSignZoomIn_Click(object sender, EventArgs e)
        {
            pctSign.Width += Convert.ToInt32(pctSign.Width * 0.10);
            pctSign.Height += Convert.ToInt32(pctSign.Height * 0.10);
        }

        private void btnSignZoomOut_Click(object sender, EventArgs e)
        {
            pctSign.Width -= Convert.ToInt32(pctSign.Width * 0.10);
            pctSign.Height -= Convert.ToInt32(pctSign.Height * 0.10);
        }

        private void btnIdZoomIn_Click(object sender, EventArgs e)
        {
            pctID.Width += Convert.ToInt32(pctID.Width * 0.10);
            pctID.Height += Convert.ToInt32(pctID.Height * 0.10);
        }

        private void btnIdZoomOut_Click(object sender, EventArgs e)
        {
            pctID.Width -= Convert.ToInt32(pctID.Width * 0.10);
            pctID.Height -= Convert.ToInt32(pctID.Height * 0.10);
  
        }

        public static string WriteRequestToken(String strAppId, String strBranch, String strWorkStnId, String strUserId, List<string> lstAcctNo)
        {


            string strReturn;
            var sb = new StringBuilder();
            using (XmlWriter writer = XmlWriter.Create(new StringWriterUtf8(sb)))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("CESS");

                writer.WriteStartElement("SvcRq");
                writer.WriteElementString("ChannelId", strAppId);
                writer.WriteEndElement();

                writer.WriteStartElement("CESSRq");
                writer.WriteElementString("Branch", strBranch);
                writer.WriteElementString("UserId", strUserId);
                writer.WriteElementString("WorkStnId", strWorkStnId);


                foreach (string strAcctNo in lstAcctNo)
                {
                    writer.WriteElementString("AcctNo", strAcctNo);
                }

                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndDocument();

                writer.Close();


            }


            strReturn = sb.ToString();
            sb = null;
            return strReturn;




        }

        public void ShowData(int intAcct)
        {

            cboSignList.Items.Clear();

            if (lstRcvdAcct[intAcct].lstSignatory.Count > 0)
            {
                for (int i = 0; i < lstRcvdAcct[intAcct].lstSignatory.Count; i++)
                {
                    cboSignList.Items.Add(lstRcvdAcct[intAcct].lstSignatory[i].SubAcctNo);
                }


                txtName.Text = lstRcvdAcct[intAcct].lstSignatory[0].SubAcctName;
                txtSpecialInstr.Text = lstRcvdAcct[intAcct].lstSignatory[0].SubAcctInstr;
                cboSignList.SelectedIndex = 0;

            }
            else
            {
                txtName.Text = "";
                txtSpecialInstr.Text = "";
                //lstRcvdAcct.Clear();

                cboSignList.Text = "";
                cboSignList.Items.Clear();
                cboSignList.SelectedIndex = -1;

                pctID.Image = null;
                pctSign.Image = null;

                WriteLog(LogFile, txtAcctNo.Text + " Signatory List Is Not Available", cnsEvent);
                MessageBox.Show(txtAcctNo.Text + " Signatory List Is Not Available");

                if (BolTrx == true)
                {

                    Reply += "OK;";
                    WriteLog(LogFile, "Accepted ", cnsEvent);
                    WriteLog(LogFile, Reply, cnsData);

                    AcctNoIndex += 1;
                    if (AcctNoIndex < lstAcctNo.Count)
                    {
                        txtAcctNo.Text = lstAcctNo[AcctNoIndex];
                        pctID.Image = null;
                        pctSign.Image = null;
                        ShowData(AcctNoIndex);

                    }
                    else
                    {
                        BolAccept = true;
                        this.Close();
                    }
                }


            }


        }

        public string SanitizeXmlString(string xml)
        {
            if (xml == null)
            {
                throw new ArgumentNullException("xml");
            }

            StringBuilder buffer = new StringBuilder(xml.Length);

            foreach (char c in xml)
            {
                if (IsLegalXmlChar(c))
                {
                    buffer.Append(c);
                }
            }

            return buffer.ToString();
        }


        public bool IsLegalXmlChar(int character)
        {
            return
            (
                 (character >= 0x20 && character <= 0xD7FF) ||
                (character >= 0xE000 && character <= 0xFFFD) ||
                (character >= 0x10000 && character <= 0x10FFFF)
            );
        }


        private string ReadTokenReply(string strTokenReply)
        {

            lstRcvdAcct.Clear();

            String strReturn = "";
            String strAcctNo = "";
            String strSubAcctNo = "";
            String strSubAcctName = "";
            String strSubAcctInstr = "";

            Token = "";


            string strCleanXML = SanitizeXmlString(strTokenReply);


            using (XmlReader reader = XmlReader.Create(new System.IO.StringReader(strCleanXML)))
            {
                while (reader.Read())
                {

                    // Only detect start elements.
                    if (reader.IsStartElement())
                    {
                        switch (reader.Name)
                        {
                            //case "Accounts":
                            //    // lstRcvdAcct = new List<Acct>();
                            //    break;


                            case "Return":
                                if (reader.Read())
                                {
                                    strReturn = reader.Value;
                                }
                                break;


                            case "Tx":
                                if (reader.Read())
                                {
                                    Token = reader.Value;
                                }
                                break;


                            case "AcctNo":
                                if (reader.Read())
                                {
                                    strAcctNo = reader.Value;
                                }

                                lstRcvdAcct.Add(new Acct(strAcctNo));


                                break;

                            case "SubAcctNo":
                                if (reader.Read())
                                {
                                    strSubAcctNo = reader.Value;
                                }
                                break;

                            case "SubAcctName":

                                if (reader.Read())
                                {
                                    strSubAcctName = reader.Value;
                                }
                                break;

                            case "SubAcctInstr":
                                if (reader.Read())
                                {
                                    strSubAcctInstr = reader.Value;
                                }

                                if (strSubAcctInstr.Length > 0)
                                {
                                    lstRcvdAcct[lstRcvdAcct.Count - 1].lstSignatory.Add(new Signatory(strSubAcctNo, strSubAcctName, strSubAcctInstr));
                                }

                                break;

                            case "WebSvrUrl":
                                if (reader.Read())
                                {

                                    WebSvrUrl = reader.Value;
                                }

                                break;

                            //case "ErrMsg":
                            // if (reader.Read())
                            // {
                            //     strAuthorized = reader.Value;
                            // }

                            // break ;


                        }



                    }


                    if (reader.NodeType == XmlNodeType.EndElement)
                    {


                        if (reader.Name == "Accounts")
                        {


                            break;
                        }

                    }
                }

                reader.Close();


            }



            return strReturn;

        }

        public class StringWriterUtf8 : StringWriter
        {
            public StringWriterUtf8(StringBuilder sb)
                : base(sb)
            {
            }

            public override Encoding Encoding
            {
                get { return Encoding.UTF8; }
            }
        }

        private void LoadSignList()
        {
            cboSignList.Items.Clear();

            for (int i = 0; i < lstSubAcctNo.Count - 1; i++)
            {
                if (lstSubAcctNo[i].StartsWith(txtAcctNo.Text) == true)
                {
                    cboSignList.Items.Add(lstSubAcctNo[i]);
                }
            }

            if (cboSignList.Items.Count != 0)
            {
                cboSignList.SelectedIndex = 0;
                cboSignList.Refresh();
            }

        }

        public void LoadSign()
        {

            pctID.Image = pctUOB.Image;
            pctSign.Image = pctUOB.Image;

            if (lstRcvdAcct.Count > 0)
            {
                if (lstRcvdAcct[AcctNoIndex].lstSignatory[cboSignList.SelectedIndex].SignLoaded == true)
                {

                    if (lstRcvdAcct[AcctNoIndex].lstSignatory[cboSignList.SelectedIndex].SignImage == null)
                    {
                        MessageBox.Show("Signature not available");
                        pctSign.Image = pctUOB.Image;
                    }
                    else
                    {
                        pctSign.Image = lstRcvdAcct[AcctNoIndex].lstSignatory[cboSignList.SelectedIndex].SignImage;
                    }

                    return;
                }
            }


            string strSubAcctNo = Convert.ToString(cboSignList.Items[cboSignList.SelectedIndex]);
            string strSendLoadSign = Token + "0" + strSubAcctNo;
            Image ImageRvcd;

            try
            {

                //MessageBox.Show(WebSvrUrl);
                ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback((sender, certificate, chain, policyErrors) => { return true; });
                var request = WebRequest.Create(WebSvrUrl + "?tix=" + strSendLoadSign);

                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    if (response.ContentType == "image/png")
                    {
                        ImageRvcd = Bitmap.FromStream(stream);
                        lstRcvdAcct[AcctNoIndex].lstSignatory[cboSignList.SelectedIndex].SignImage = ImageRvcd;
                        pctSign.Image = ImageRvcd;
                    }
                    else
                    {
                        MessageBox.Show("Signature not available");
                        pctSign.Image = pctUOB.Image;
                    }

                    lstRcvdAcct[AcctNoIndex].lstSignatory[cboSignList.SelectedIndex].SignLoaded = true;

                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error connecting to Web Server (" + e.Message + ")");
            }

        }

        public void LoadID()
        {


            pctID.Image = pctUOB.Image;

            if (lstRcvdAcct.Count > 0)
            {
                if (lstRcvdAcct[AcctNoIndex].lstSignatory[cboSignList.SelectedIndex].IDLoaded == true)
                {

                    if (lstRcvdAcct[AcctNoIndex].lstSignatory[cboSignList.SelectedIndex].IDImage == null)
                    {
                        MessageBox.Show("ID not available");
                        pctID.Image = pctUOB.Image;
                    }
                    else
                    {
                        pctID.Image = lstRcvdAcct[AcctNoIndex].lstSignatory[cboSignList.SelectedIndex].IDImage;
                    }

                    return;
                }
            }


            string strSubAcctNo = Convert.ToString(cboSignList.Items[cboSignList.SelectedIndex]);
            string strSendLoadSign = Token + "1" + strSubAcctNo;
            Image ImageRvcd;


            //var request = WebRequest.Create("http://localhost:6229/login2.aspx?tix=" + strSendLoadSign);
            var request = WebRequest.Create(WebSvrUrl + "?tix=" + strSendLoadSign);
            try
            {

                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    if (response.ContentType == "image/png")
                    {
                        ImageRvcd = Bitmap.FromStream(stream);
                        lstRcvdAcct[AcctNoIndex].lstSignatory[cboSignList.SelectedIndex].IDImage = ImageRvcd;
                        lstRcvdAcct[AcctNoIndex].lstSignatory[cboSignList.SelectedIndex].IDLoaded = true;
                        pctID.Image = ImageRvcd;

                    }
                    else
                    {
                        MessageBox.Show("ID not available");
                        pctID.Image = pctUOB.Image;

                    }

                    lstRcvdAcct[AcctNoIndex].lstSignatory[cboSignList.SelectedIndex].IDLoaded = true;


                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error connecting to Web Server (" + e.Message + ")");
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            BolAccept = true;
            BolClose = true;
            this.Close();
        }

        private void btnReject_Click(object sender, EventArgs e)
        {
            Reply += "Reject;";
            WriteLog(LogFile, "Rejected", cnsEvent);
            WriteLog(LogFile, Reply, cnsData);

            BolAccept = true;
            BolReject = true;
            this.Close();
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            Reply += "OK;";
            WriteLog(LogFile, "Accepted ", cnsEvent);
            WriteLog(LogFile, Reply, cnsData);


            AcctNoIndex += 1;
            if (AcctNoIndex < lstAcctNo.Count)
            {
                txtAcctNo.Text = lstAcctNo[AcctNoIndex];
                pctID.Image = null;
                pctSign.Image = null;
                ShowData(AcctNoIndex);

            }
            else
            {
                BolAccept = true;
                this.Close();
            }
        }

        private void frmSvsViewer_ResizeEnd(object sender, EventArgs e)
        {
            RestrictMouse();
        }

        private void frmSvsViewer_Shown(object sender, EventArgs e)
        {
            if (frmActivated == false)
            {

                frmActivated = true;

                RestrictMouse();
                
                Application.DoEvents();

                SetWindowPos(this.Handle, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS);


                if (BolTrx == true)
                {
                    WriteLog(LogFile, "GetToken", cnsEvent);

                    Cursor.Current = Cursors.WaitCursor;

                    GetToken();

                    Cursor.Current = Cursors.Default;
                }
                else
                {

                    Process z = Process.GetCurrentProcess();
                    SetForegroundWindow(z.MainWindowHandle);
                    RestrictMouse();

                }


            }


        }
     
    }
}
