﻿namespace SVSViewer
{
    partial class frmSvsViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSvsViewer));
            this.pnlResponse = new System.Windows.Forms.Panel();
            this.btnAccept = new System.Windows.Forms.Button();
            this.btnReject = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.pnlZoom = new System.Windows.Forms.Panel();
            this.pnlIDZoomBtn = new System.Windows.Forms.Panel();
            this.btnIdZoomIn = new System.Windows.Forms.Button();
            this.btnIdZoomOut = new System.Windows.Forms.Button();
            this.pnlSignZoomBtn = new System.Windows.Forms.Panel();
            this.btnSignZoomIn = new System.Windows.Forms.Button();
            this.btnSignZoomOut = new System.Windows.Forms.Button();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.lblName = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtAcctNo = new System.Windows.Forms.TextBox();
            this.lblAcctNo = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtSpecialInstr = new System.Windows.Forms.TextBox();
            this.lblSpecialInstr = new System.Windows.Forms.Label();
            this.pnlSignList = new System.Windows.Forms.Panel();
            this.cboSignList = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnShowID = new System.Windows.Forms.Button();
            this.pctSign = new System.Windows.Forms.PictureBox();
            this.pctUOB = new System.Windows.Forms.PictureBox();
            this.pnlLeft = new System.Windows.Forms.Panel();
            this.pnlRight = new System.Windows.Forms.Panel();
            this.pctID = new System.Windows.Forms.PictureBox();
            this.pnlResponse.SuspendLayout();
            this.pnlZoom.SuspendLayout();
            this.pnlIDZoomBtn.SuspendLayout();
            this.pnlSignZoomBtn.SuspendLayout();
            this.pnlTop.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlSignList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctSign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctUOB)).BeginInit();
            this.pnlLeft.SuspendLayout();
            this.pnlRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctID)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlResponse
            // 
            this.pnlResponse.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnlResponse.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlResponse.Controls.Add(this.btnAccept);
            this.pnlResponse.Controls.Add(this.btnReject);
            this.pnlResponse.Controls.Add(this.btnClose);
            this.pnlResponse.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlResponse.Location = new System.Drawing.Point(0, 513);
            this.pnlResponse.Margin = new System.Windows.Forms.Padding(2);
            this.pnlResponse.Name = "pnlResponse";
            this.pnlResponse.Size = new System.Drawing.Size(746, 27);
            this.pnlResponse.TabIndex = 4;
            // 
            // btnAccept
            // 
            this.btnAccept.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnAccept.Location = new System.Drawing.Point(550, 0);
            this.btnAccept.Margin = new System.Windows.Forms.Padding(2);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(64, 23);
            this.btnAccept.TabIndex = 1;
            this.btnAccept.Text = "Accept";
            this.btnAccept.UseVisualStyleBackColor = true;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // btnReject
            // 
            this.btnReject.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnReject.Location = new System.Drawing.Point(614, 0);
            this.btnReject.Margin = new System.Windows.Forms.Padding(2);
            this.btnReject.Name = "btnReject";
            this.btnReject.Size = new System.Drawing.Size(64, 23);
            this.btnReject.TabIndex = 2;
            this.btnReject.Text = "Reject";
            this.btnReject.UseVisualStyleBackColor = true;
            this.btnReject.Click += new System.EventHandler(this.btnReject_Click);
            // 
            // btnClose
            // 
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnClose.Location = new System.Drawing.Point(678, 0);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(64, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pnlZoom
            // 
            this.pnlZoom.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnlZoom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlZoom.Controls.Add(this.pnlIDZoomBtn);
            this.pnlZoom.Controls.Add(this.pnlSignZoomBtn);
            this.pnlZoom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlZoom.Location = new System.Drawing.Point(0, 486);
            this.pnlZoom.Margin = new System.Windows.Forms.Padding(2);
            this.pnlZoom.Name = "pnlZoom";
            this.pnlZoom.Size = new System.Drawing.Size(746, 27);
            this.pnlZoom.TabIndex = 6;
            // 
            // pnlIDZoomBtn
            // 
            this.pnlIDZoomBtn.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnlIDZoomBtn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlIDZoomBtn.Controls.Add(this.btnIdZoomIn);
            this.pnlIDZoomBtn.Controls.Add(this.btnIdZoomOut);
            this.pnlIDZoomBtn.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlIDZoomBtn.Location = new System.Drawing.Point(370, 0);
            this.pnlIDZoomBtn.Margin = new System.Windows.Forms.Padding(2);
            this.pnlIDZoomBtn.Name = "pnlIDZoomBtn";
            this.pnlIDZoomBtn.Size = new System.Drawing.Size(372, 23);
            this.pnlIDZoomBtn.TabIndex = 4;
            // 
            // btnIdZoomIn
            // 
            this.btnIdZoomIn.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnIdZoomIn.Location = new System.Drawing.Point(304, 0);
            this.btnIdZoomIn.Margin = new System.Windows.Forms.Padding(2);
            this.btnIdZoomIn.Name = "btnIdZoomIn";
            this.btnIdZoomIn.Size = new System.Drawing.Size(32, 19);
            this.btnIdZoomIn.TabIndex = 5;
            this.btnIdZoomIn.Text = "+";
            this.btnIdZoomIn.UseVisualStyleBackColor = true;
            this.btnIdZoomIn.Click += new System.EventHandler(this.btnIdZoomIn_Click);
            // 
            // btnIdZoomOut
            // 
            this.btnIdZoomOut.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnIdZoomOut.Location = new System.Drawing.Point(336, 0);
            this.btnIdZoomOut.Margin = new System.Windows.Forms.Padding(2);
            this.btnIdZoomOut.Name = "btnIdZoomOut";
            this.btnIdZoomOut.Size = new System.Drawing.Size(32, 19);
            this.btnIdZoomOut.TabIndex = 4;
            this.btnIdZoomOut.Text = "-";
            this.btnIdZoomOut.UseVisualStyleBackColor = true;
            this.btnIdZoomOut.Click += new System.EventHandler(this.btnIdZoomOut_Click);
            // 
            // pnlSignZoomBtn
            // 
            this.pnlSignZoomBtn.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnlSignZoomBtn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlSignZoomBtn.Controls.Add(this.btnSignZoomIn);
            this.pnlSignZoomBtn.Controls.Add(this.btnSignZoomOut);
            this.pnlSignZoomBtn.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlSignZoomBtn.Location = new System.Drawing.Point(0, 0);
            this.pnlSignZoomBtn.Margin = new System.Windows.Forms.Padding(2);
            this.pnlSignZoomBtn.Name = "pnlSignZoomBtn";
            this.pnlSignZoomBtn.Size = new System.Drawing.Size(372, 23);
            this.pnlSignZoomBtn.TabIndex = 3;
            // 
            // btnSignZoomIn
            // 
            this.btnSignZoomIn.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSignZoomIn.Location = new System.Drawing.Point(304, 0);
            this.btnSignZoomIn.Margin = new System.Windows.Forms.Padding(2);
            this.btnSignZoomIn.Name = "btnSignZoomIn";
            this.btnSignZoomIn.Size = new System.Drawing.Size(32, 19);
            this.btnSignZoomIn.TabIndex = 5;
            this.btnSignZoomIn.Text = "+";
            this.btnSignZoomIn.UseVisualStyleBackColor = true;
            this.btnSignZoomIn.Click += new System.EventHandler(this.btnSignZoomIn_Click);
            // 
            // btnSignZoomOut
            // 
            this.btnSignZoomOut.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSignZoomOut.Location = new System.Drawing.Point(336, 0);
            this.btnSignZoomOut.Margin = new System.Windows.Forms.Padding(2);
            this.btnSignZoomOut.Name = "btnSignZoomOut";
            this.btnSignZoomOut.Size = new System.Drawing.Size(32, 19);
            this.btnSignZoomOut.TabIndex = 4;
            this.btnSignZoomOut.Text = "-";
            this.btnSignZoomOut.UseVisualStyleBackColor = true;
            this.btnSignZoomOut.Click += new System.EventHandler(this.btnSignZoomOut_Click);
            // 
            // pnlTop
            // 
            this.pnlTop.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnlTop.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlTop.Controls.Add(this.lblName);
            this.pnlTop.Controls.Add(this.txtName);
            this.pnlTop.Controls.Add(this.txtAcctNo);
            this.pnlTop.Controls.Add(this.lblAcctNo);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Margin = new System.Windows.Forms.Padding(2);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(746, 27);
            this.pnlTop.TabIndex = 7;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.Black;
            this.lblName.Location = new System.Drawing.Point(176, 0);
            this.lblName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(45, 17);
            this.lblName.TabIndex = 4;
            this.lblName.Text = "Name";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtName
            // 
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.Location = new System.Drawing.Point(220, 0);
            this.txtName.Margin = new System.Windows.Forms.Padding(2);
            this.txtName.Name = "txtName";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(512, 23);
            this.txtName.TabIndex = 10;
            this.txtName.TabStop = false;
            // 
            // txtAcctNo
            // 
            this.txtAcctNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAcctNo.Location = new System.Drawing.Point(55, 0);
            this.txtAcctNo.Margin = new System.Windows.Forms.Padding(2);
            this.txtAcctNo.Name = "txtAcctNo";
            this.txtAcctNo.Size = new System.Drawing.Size(110, 23);
            this.txtAcctNo.TabIndex = 20;
            this.txtAcctNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAcctNo_KeyDown);
            this.txtAcctNo.Validated += new System.EventHandler(this.txtAcctNo_Validated);
            // 
            // lblAcctNo
            // 
            this.lblAcctNo.AutoSize = true;
            this.lblAcctNo.BackColor = System.Drawing.Color.Transparent;
            this.lblAcctNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAcctNo.ForeColor = System.Drawing.Color.Black;
            this.lblAcctNo.Location = new System.Drawing.Point(-2, 2);
            this.lblAcctNo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAcctNo.Name = "lblAcctNo";
            this.lblAcctNo.Size = new System.Drawing.Size(56, 17);
            this.lblAcctNo.TabIndex = 0;
            this.lblAcctNo.Text = "A/C No.";
            this.lblAcctNo.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.txtSpecialInstr);
            this.panel1.Controls.Add(this.lblSpecialInstr);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 27);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(746, 41);
            this.panel1.TabIndex = 8;
            // 
            // txtSpecialInstr
            // 
            this.txtSpecialInstr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSpecialInstr.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSpecialInstr.Location = new System.Drawing.Point(0, 13);
            this.txtSpecialInstr.Margin = new System.Windows.Forms.Padding(2);
            this.txtSpecialInstr.Multiline = true;
            this.txtSpecialInstr.Name = "txtSpecialInstr";
            this.txtSpecialInstr.ReadOnly = true;
            this.txtSpecialInstr.Size = new System.Drawing.Size(742, 24);
            this.txtSpecialInstr.TabIndex = 6;
            this.txtSpecialInstr.TabStop = false;
            // 
            // lblSpecialInstr
            // 
            this.lblSpecialInstr.AutoSize = true;
            this.lblSpecialInstr.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblSpecialInstr.ForeColor = System.Drawing.Color.White;
            this.lblSpecialInstr.Location = new System.Drawing.Point(0, 0);
            this.lblSpecialInstr.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSpecialInstr.Name = "lblSpecialInstr";
            this.lblSpecialInstr.Size = new System.Drawing.Size(99, 13);
            this.lblSpecialInstr.TabIndex = 5;
            this.lblSpecialInstr.Text = "Special Instructions";
            // 
            // pnlSignList
            // 
            this.pnlSignList.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnlSignList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlSignList.Controls.Add(this.cboSignList);
            this.pnlSignList.Controls.Add(this.label1);
            this.pnlSignList.Controls.Add(this.btnShowID);
            this.pnlSignList.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSignList.Location = new System.Drawing.Point(0, 68);
            this.pnlSignList.Margin = new System.Windows.Forms.Padding(2);
            this.pnlSignList.Name = "pnlSignList";
            this.pnlSignList.Size = new System.Drawing.Size(746, 30);
            this.pnlSignList.TabIndex = 9;
            // 
            // cboSignList
            // 
            this.cboSignList.Dock = System.Windows.Forms.DockStyle.Left;
            this.cboSignList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSignList.FormattingEnabled = true;
            this.cboSignList.Location = new System.Drawing.Point(94, 0);
            this.cboSignList.Margin = new System.Windows.Forms.Padding(2);
            this.cboSignList.Name = "cboSignList";
            this.cboSignList.Size = new System.Drawing.Size(501, 25);
            this.cboSignList.TabIndex = 5;
            this.cboSignList.SelectedIndexChanged += new System.EventHandler(this.cboSignList_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Signatory List";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // btnShowID
            // 
            this.btnShowID.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnShowID.Location = new System.Drawing.Point(678, 0);
            this.btnShowID.Margin = new System.Windows.Forms.Padding(2);
            this.btnShowID.Name = "btnShowID";
            this.btnShowID.Size = new System.Drawing.Size(64, 26);
            this.btnShowID.TabIndex = 3;
            this.btnShowID.Text = "Show ID";
            this.btnShowID.UseVisualStyleBackColor = true;
            this.btnShowID.Click += new System.EventHandler(this.btnShowID_Click);
            // 
            // pctSign
            // 
            this.pctSign.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pctSign.Location = new System.Drawing.Point(9, 4);
            this.pctSign.Margin = new System.Windows.Forms.Padding(2);
            this.pctSign.Name = "pctSign";
            this.pctSign.Size = new System.Drawing.Size(359, 384);
            this.pctSign.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pctSign.TabIndex = 3;
            this.pctSign.TabStop = false;
            // 
            // pctUOB
            // 
            this.pctUOB.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pctUOB.Image = ((System.Drawing.Image)(resources.GetObject("pctUOB.Image")));
            this.pctUOB.Location = new System.Drawing.Point(2, 1);
            this.pctUOB.Margin = new System.Windows.Forms.Padding(2);
            this.pctUOB.Name = "pctUOB";
            this.pctUOB.Size = new System.Drawing.Size(8, 21);
            this.pctUOB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pctUOB.TabIndex = 6;
            this.pctUOB.TabStop = false;
            this.pctUOB.Visible = false;
            // 
            // pnlLeft
            // 
            this.pnlLeft.AutoScroll = true;
            this.pnlLeft.BackColor = System.Drawing.SystemColors.Control;
            this.pnlLeft.Controls.Add(this.pctSign);
            this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeft.Location = new System.Drawing.Point(0, 98);
            this.pnlLeft.Margin = new System.Windows.Forms.Padding(2);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.Size = new System.Drawing.Size(370, 388);
            this.pnlLeft.TabIndex = 11;
            // 
            // pnlRight
            // 
            this.pnlRight.AutoScroll = true;
            this.pnlRight.BackColor = System.Drawing.SystemColors.Control;
            this.pnlRight.Controls.Add(this.pctID);
            this.pnlRight.Controls.Add(this.pctUOB);
            this.pnlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlRight.Location = new System.Drawing.Point(376, 98);
            this.pnlRight.Margin = new System.Windows.Forms.Padding(2);
            this.pnlRight.Name = "pnlRight";
            this.pnlRight.Size = new System.Drawing.Size(370, 388);
            this.pnlRight.TabIndex = 12;
            // 
            // pctID
            // 
            this.pctID.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pctID.Location = new System.Drawing.Point(2, 6);
            this.pctID.Margin = new System.Windows.Forms.Padding(2);
            this.pctID.Name = "pctID";
            this.pctID.Size = new System.Drawing.Size(359, 384);
            this.pctID.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pctID.TabIndex = 4;
            this.pctID.TabStop = false;
            // 
            // frmSvsViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(746, 540);
            this.ControlBox = false;
            this.Controls.Add(this.pnlRight);
            this.Controls.Add(this.pnlLeft);
            this.Controls.Add(this.pnlSignList);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlTop);
            this.Controls.Add(this.pnlZoom);
            this.Controls.Add(this.pnlResponse);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSvsViewer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "SVS Viewer Version 2.0.1.1";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.frmView_Activated);
            this.Deactivate += new System.EventHandler(this.frmView_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmView_FormClosing);
            this.Load += new System.EventHandler(this.frmView_Load);
            this.Shown += new System.EventHandler(this.frmSvsViewer_Shown);
            this.ResizeEnd += new System.EventHandler(this.frmSvsViewer_ResizeEnd);
            this.pnlResponse.ResumeLayout(false);
            this.pnlZoom.ResumeLayout(false);
            this.pnlIDZoomBtn.ResumeLayout(false);
            this.pnlSignZoomBtn.ResumeLayout(false);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlSignList.ResumeLayout(false);
            this.pnlSignList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctSign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctUOB)).EndInit();
            this.pnlLeft.ResumeLayout(false);
            this.pnlRight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pctID)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlResponse;
        public System.Windows.Forms.Button btnAccept;
        public System.Windows.Forms.Button btnReject;
        public System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel pnlZoom;
        private System.Windows.Forms.Panel pnlIDZoomBtn;
        private System.Windows.Forms.Button btnIdZoomIn;
        private System.Windows.Forms.Button btnIdZoomOut;
        private System.Windows.Forms.Panel pnlSignZoomBtn;
        private System.Windows.Forms.Button btnSignZoomIn;
        private System.Windows.Forms.Button btnSignZoomOut;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Label lblName;
        public System.Windows.Forms.TextBox txtName;
        public System.Windows.Forms.TextBox txtAcctNo;
        private System.Windows.Forms.Label lblAcctNo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtSpecialInstr;
        private System.Windows.Forms.Label lblSpecialInstr;
        private System.Windows.Forms.Panel pnlSignList;
        private System.Windows.Forms.ComboBox cboSignList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnShowID;
        private System.Windows.Forms.PictureBox pctSign;
        private System.Windows.Forms.PictureBox pctUOB;
        private System.Windows.Forms.Panel pnlLeft;
        private System.Windows.Forms.Panel pnlRight;
        private System.Windows.Forms.PictureBox pctID;
    }
}