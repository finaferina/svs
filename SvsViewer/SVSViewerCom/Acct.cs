﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SVSViewer
{
    public class Acct
    {
        public string AcctNo { get; set; }
        public List<Signatory> lstSignatory { get; set; }
       

        public Acct()
        {
            AcctNo = "";
            lstSignatory = new List<Signatory>();

        }


        public Acct(string strAcctNo)
        {
            AcctNo = strAcctNo;
            lstSignatory = new List<Signatory>();

        }

        public void AddSignatory(string strSubAcctName, string strSubAcctNo, string strSubInstr)
        {
            lstSignatory.Add(new Signatory(strSubAcctName, strSubAcctNo, strSubInstr));
        }

    }
}
