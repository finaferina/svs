﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Runtime.InteropServices;

//Error Code	Error Description	Remarks
//-9000	Cannot resolve Comm. Server host name	
//-9001	Error connecting to Comm. Server (%d).	Detailed system error code is within bracket
//-9002	Error connecting to SVS Application Server (%d).	Detailed system error code is within bracket
//-9003	Error connecting to Authentication Server	
//-8000	Error connecting to SVS Comm. Servlet	
//-8001	Error connecting to SVS Web Service	
//-1000	Failed to authenticate iBranch user ID	iBranch user ID in authentication server is invalid
//-1001	Error resolving ESS branch ID	SVS application failed to map iBranch user ID to a ESS generic branch ID
//-3001	Invalid parameter passed from Host	




namespace SVSViewer
{
    public class Class1
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        static extern short GetKeyState(int vKey);

        public static bool _wasTabKeyHandled = false;
        const byte VK_LCONTROL = 0xA2;
        const byte VK_RCONTROL = 0x3;

        //void Hook_KeyUp(object sender, KeyEventArgs e)
        //{
         
        //    //if we previously suppressed the tab keydown, 
        //    // then we must also suppress the tab keyup for consistency.
        //    if (_wasTabKeyHandled)
        //    {
        //        _wasTabKeyHandled = false;
        //        e.Handled = true;
        //    }
        //}


        static void hook_KeyDown(object sender, KeyEventArgs e)
        {

            bool bControl = ((GetKeyState(VK_LCONTROL) & 0x80) != 0) || ((GetKeyState(VK_RCONTROL) & 0x80) != 0);

            if (e.KeyCode == Keys.PrintScreen)
            {
                //suppress the tab keydown. 
                //and set a flag to suppress keyup later for consistency
                _wasTabKeyHandled = true;

                //mark the keydown event as Handled. this will stop it from propagating.
                e.Handled = true;
                return;
            }
            
            //if the user presses TAB while ALT is down:
            if (e.KeyCode == Keys.Tab & e.Alt)
            {
                //suppress the tab keydown. 
                //and set a flag to suppress keyup later for consistency
                _wasTabKeyHandled = true;

                //mark the keydown event as Handled. this will stop it from propagating.
                e.Handled = true;
                return;
            }
            if (e.KeyCode == Keys.F4 & e.Alt)
            {
                //suppress the F4 keydown. 
                //and set a flag to suppress keyup later for consistency
                _wasTabKeyHandled = true;

                //mark the keydown event as Handled. this will stop it from propagating.
                e.Handled = true;
                return;
            }
            if (e.KeyCode == Keys.Escape & e.Alt)
            {
                //suppress the F4 keydown. 
                //and set a flag to suppress keyup later for consistency
                _wasTabKeyHandled = true;

                //mark the keydown event as Handled. this will stop it from propagating.
                e.Handled = true;
                return;
            }
            if (bControl & e.KeyValue == 67)
            {
                //suppress the F4 keydown. 
                //and set a flag to suppress keyup later for consistency
                _wasTabKeyHandled = true;

                //mark the keydown event as Handled. this will stop it from propagating.
                e.Handled = true;
                return;
            }


            if (e.KeyValue == 91)
            {
                //suppress the Left Windows keydown. 
                //and set a flag to suppress keyup later for consistency
                _wasTabKeyHandled = true;

                //mark the keydown event as Handled. this will stop it from propagating.
                e.Handled = true;
                return;
            }
            if (e.KeyValue == 92)
            {
                //suppress the Right Windows keydown. 
                //and set a flag to suppress keyup later for consistency
                _wasTabKeyHandled = true;

                //mark the keydown event as Handled. this will stop it from propagating.
                e.Handled = true;
                return;
            }
            
            //e.SuppressKeyPress = true;
            //e.Handled = true;
        }

        public interface ICalculator
        {
            String Inquiry(string strcommsport,string strcommsname, string strBranchID, string strAppID, string strWorkStnID, string strUserID) ;
            String Transaction(string strcommsport, string strcommsname, string strBranchID, string strAppID, string strWorkStnID, string strUserID, string strAcctNo1, string strAcctNo2, string strAcctNo3, string strAcctNo4, string strAcctNo5, string strAcctNo6, string strAcctNo7, string strAcctNo8, string strAcctNo9, string strAcctNo10);
        }

        // Interface implementation.
        public class ManagedClass : ICalculator
        {


          
      public string Inquiry(string strcommsport, string strcommsname, string strBranchID, string strAppID, string strWorkStnID, string strUserID)
      {

         
          bool bolResponse = false ;
          string Reply;

          frmSvsViewer f = new frmSvsViewer();


          f.Click += new EventHandler(delegate(object sender, EventArgs e)
          {
              f.Close();
          });

          GlobalKeyboardHook hook = new GlobalKeyboardHook();
          hook.KeyDown += new KeyEventHandler(hook_KeyDown);
          hook.Hook();

          

        

          f.BolTrx = false;
          f.CommsPort = strcommsport;
          f.CommsName = strcommsname;
          f.BranchId = strBranchID;
          f.AppId = strAppID;
          f.WorkStnId = strWorkStnID;
          f.UserID = strUserID;


          f.btnAccept.Visible = false;
          f.btnReject.Visible = false;
          f.SendToBack();
          f.ShowDialog();
          bolResponse = f.BolClose;
          Reply = f.Reply;
          f.Dispose();

          hook.Unhook();



          if (bolResponse)
          {
              return "0|OK;";
          }
          else
          {
              
              return "-1|" + Reply;
          }

      }


       public string Transaction(string strcommsport, string strcommsname, string strBranchID, string strAppID, string strWorkStnID, string strUserID, string strAcctNo1, string strAcctNo2, string strAcctNo3, string strAcctNo4, string strAcctNo5, string strAcctNo6, string strAcctNo7, string strAcctNo8, string strAcctNo9, string strAcctNo10)
        {

        
         
          bool bolResponse = false;
          string Reply = "" ;

          //frmSVSViewerOld f = new frmSVSViewerOld();
          frmSvsViewer f = new frmSvsViewer();


          f.Click += new EventHandler(delegate(object sender, EventArgs e)
          {
              f.Close();
          });

          GlobalKeyboardHook hook = new GlobalKeyboardHook();
          hook.KeyDown += new KeyEventHandler(hook_KeyDown);
          hook.Hook();
          

          f.BolTrx = true;
          f.CommsPort = strcommsport;
          f.CommsName = strcommsname;
          f.BranchId = strBranchID;
          f.AppId = strAppID;
          f.WorkStnId = strWorkStnID;
          f.UserID = strUserID;

          if (String.IsNullOrEmpty(strAcctNo1) == false)
          {
              f.lstAcctNo.Add(strAcctNo1.Substring(3));
          }

          if (String.IsNullOrEmpty(strAcctNo2) == false)
          {
              f.lstAcctNo.Add(strAcctNo2.Substring(3));
          }

          if (String.IsNullOrEmpty(strAcctNo3) == false)
          {
              f.lstAcctNo.Add(strAcctNo3.Substring(3));
          }

          if (String.IsNullOrEmpty(strAcctNo4) == false)
          {
              f.lstAcctNo.Add(strAcctNo4.Substring(3));
          }

          if (String.IsNullOrEmpty(strAcctNo5) == false)
          {
              f.lstAcctNo.Add(strAcctNo5.Substring(3));
          }

          if (String.IsNullOrEmpty(strAcctNo6) == false)
          {
              f.lstAcctNo.Add(strAcctNo6.Substring(3));
          }

          if (String.IsNullOrEmpty(strAcctNo7) == false)
          {
              f.lstAcctNo.Add(strAcctNo7.Substring(3));
          }

          if (String.IsNullOrEmpty(strAcctNo8) == false)
          {
              f.lstAcctNo.Add(strAcctNo8.Substring(3));
          }

          if (String.IsNullOrEmpty(strAcctNo9) == false)
          {
              f.lstAcctNo.Add(strAcctNo9.Substring(3));
          }

          if (String.IsNullOrEmpty(strAcctNo10) == false)
          {
              f.lstAcctNo.Add(strAcctNo10.Substring(3));
          }

           
          f.btnClose.Visible = false;
          f.txtAcctNo.ReadOnly = true;
          f.txtAcctNo.TabStop = false ;
          f.txtName.ReadOnly = true;
          f.txtAcctNo.Text = f.lstAcctNo[0];

          f.SendToBack();
          f.ShowDialog();
          bolResponse = f.BolAccept;
          Reply = f.Reply;
          f.Dispose();
          //f = null;

          hook.Unhook();

     
          if (bolResponse == true)
          {
              return "0|" + Reply;
          }
          else
          {
             
              return "-1|" + Reply;
          }

     
      }





      //public string Inquiry(string strcommsport, string strcommsname, string strBranchID, string strAppID, string strWorkStnID, string strUserID)
      //      {

      //          int intOk;
      //          Form1 f = new Form1();
      //          f.listBox1.Items.Add("INQUIRY");
      //          f.listBox1.Items.Add("CommsPort     " + strcommsport);
      //          f.listBox1.Items.Add("CommsName     " + strcommsname);
      //          f.listBox1.Items.Add("Branch      ID " + strBranchID);
      //          f.listBox1.Items.Add("Application ID " + strAppID);
      //          f.listBox1.Items.Add("WorkStation ID " + strWorkStnID);
      //          f.listBox1.Items.Add("User        ID " + strUserID);

          

      //          f.ShowDialog();
      //          intOk = f.intOk;
      //          f.Dispose();

      //          if (intOk == 1)
      //          {
      //              return "0|";
      //          }
      //          else
      //          {
      //              return "-1|9003 Query Error";
      //          }

      //      }




      //public string Transaction(string strcommsport, string strcommsname, string strBranchID, string strAppID, string strWorkStnID, string strUserID, string strAcctNo1, string strAcctNo2, string strAcctNo3, string strAcctNo4, string strAcctNo5, string strAcctNo6, string strAcctNo7, string strAcctNo8, string strAcctNo9, string strAcctNo10)
      //{

      //    strcommsport = "5";

      //    int intOk;
      //    Form1 f = new Form1();
      //    f.listBox1.Items.Add("TRANSACTION");
      //    f.listBox1.Items.Add("CommsPort     " + strcommsport);
      //    f.listBox1.Items.Add("CommsName     " + strcommsname);
      //    f.listBox1.Items.Add("Branch      ID " + strBranchID);
      //    f.listBox1.Items.Add("Branch      ID " + strBranchID);
      //    f.listBox1.Items.Add("Application ID " + strAppID);
      //    f.listBox1.Items.Add("WorkStation ID " + strWorkStnID);
      //    f.listBox1.Items.Add("User        ID " + strUserID);

      //    if (String.IsNullOrEmpty(strAcctNo1) == false)
      //    {
      //        f.listBox1.Items.Add("AcctNo 1 = " + strAcctNo1);
      //    }

      //    if (String.IsNullOrEmpty(strAcctNo2) == false)
      //    {
      //        f.listBox1.Items.Add("AcctNo 2 = " + strAcctNo2);
      //    }

      //    if (String.IsNullOrEmpty(strAcctNo3) == false)
      //    {
      //        f.listBox1.Items.Add("AcctNo 3 = " + strAcctNo3);
      //    }

      //    if (String.IsNullOrEmpty(strAcctNo4) == false)
      //    {
      //        f.listBox1.Items.Add("AcctNo 4 = " + strAcctNo4);
      //    }

      //    if (String.IsNullOrEmpty(strAcctNo5) == false)
      //    {
      //        f.listBox1.Items.Add("AcctNo 5 = " + strAcctNo5);
      //    }

      //    if (String.IsNullOrEmpty(strAcctNo6) == false)
      //    {
      //        f.listBox1.Items.Add("AcctNo 6 = " + strAcctNo6);
      //    }

      //    if (String.IsNullOrEmpty(strAcctNo7) == false)
      //    {
      //        f.listBox1.Items.Add("AcctNo 7 = " + strAcctNo7);
      //    }

      //    if (String.IsNullOrEmpty(strAcctNo8) == false)
      //    {
      //        f.listBox1.Items.Add("AcctNo 8 = " + strAcctNo8);
      //    }

      //    if (String.IsNullOrEmpty(strAcctNo9) == false)
      //    {
      //        f.listBox1.Items.Add("AcctNo 9 = " + strAcctNo9);
      //    }

      //    if (String.IsNullOrEmpty(strAcctNo10) == false)
      //    {
      //        f.listBox1.Items.Add("AcctNo 10 = " + strAcctNo10);
      //    }



      //    f.ShowDialog();
      //    intOk = f.intOk;
      //    f.Dispose();

      //    if (intOk == 1)
      //    {
      //        return "0|OK;OK;OK";
      //    }
      //    else
      //    {
      //        return "-1|OK;-9003 Trancaction Error 1;  9004 Trancaction Error 2";
        
      //    }

      //}



        }

      



    }
}
