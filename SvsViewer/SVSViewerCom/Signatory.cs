﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
namespace SVSViewer
{
    public class Signatory
    {


        public string SubAcctName { get; set; }
        public string SubAcctNo { get; set; }
        public string SubAcctInstr { get; set; }
        public Image SignImage;
        public Image IDImage;
        public bool SignLoaded ;
        public bool IDLoaded;


        public Signatory(string strSubAcctNo, string strSubAcctName, string strSubAcctInstr)
        {
            SubAcctName = strSubAcctName;
            SubAcctNo = strSubAcctNo;
            SubAcctInstr = strSubAcctInstr;
        }
    }
}
