﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Windows.Forms
{
    public static class WinFormUtils
    {

        /// <summary> Processes all Paint events only </summary>
       public static void DoPaintEvents()
       {
           //MessageFilter registration
           Application.AddMessageFilter(PaintMessageFilter.Instance);
           //Process messages in the queue
           Application.DoEvents();
           //MessageFilter desregistration
           Application.RemoveMessageFilter(PaintMessageFilter.Instance);
       }
 
       /// <summary> Custom message filter </summary>
       private class PaintMessageFilter : IMessageFilter
       {
           static public IMessageFilter Instance = new PaintMessageFilter();
 
           #region IMessageFilter Members
 
           /// <summary> Message filter function </summary>
           public bool PreFilterMessage(ref System.Windows.Forms.Message m)
           {
                   return (m.Msg != 0x000F); //WM_PAINT -> we only let WM_PAINT messages through
           }
 
           #endregion
       }
    }
}

