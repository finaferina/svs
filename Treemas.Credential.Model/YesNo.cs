﻿using Treemas.Base.Utilities;

namespace Treemas.Credential.Model
{
    public enum YesNo : int
    {
        [StringValue("Yes")]
        Y = 1,
        [StringValue("No")]
        N = 2
    }
}
