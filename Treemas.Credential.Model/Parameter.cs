﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.Credential.Model
{
    [Serializable]
    public class Parameter : EntityString
    {
        protected Parameter()
        {

        }
        public Parameter(string id) : base(id)
        {
            this.ParamId = id;
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string UserId { get; set; }
        public virtual string ParamId { get; set; }
        public virtual string Password { get; set; }
        public virtual string StaggingUrl { get; set; }
        public virtual string DomainServer { get; set; }
        
        public virtual int SessionTimeout { get; set; }
        public virtual int LoginAttempt { get; set; }
        public virtual string PasswordExp { get; set; }

        public virtual int SizeKTP { get; set; }
        public virtual int SizeSignature { get; set; }
        public virtual int SizeDocument { get; set; }
        public virtual int KeepHistory { get; set; }
        public virtual int KeepAuditTrail { get; set; }
        public virtual int KeepPendingAppr { get; set; }
        public virtual decimal MaxDiffAccount { get; set; }
        public virtual int KeepCloseAccount { get; set; }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }

        public virtual int GetSizeKTP
        {
            get
            {
                if (SizeKTP == 0) return 500 * 1024;
                else return SizeKTP * 1024;
            }
        }

        public virtual int GetSizeSignature
        {
            get
            {
                if (SizeSignature == 0) return 500 * 1024;
                else return SizeSignature * 1024;
            }
        }
        public virtual int GetSizeDocument
        {
            get
            {
                if (SizeDocument == 0) return 2000 * 1365; //1340;
                else return SizeDocument * 1365; //1340;
            }
        }
        public virtual int GetSizeDocumentJS
        {
            get
            {
                if (SizeDocument == 0) return 2000 * 1024;
                else return SizeDocument * 1024;
            }
        }
    }
}
