﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;

namespace Treemas.Credential.Model
{
    public class Authorization : Entity
    {
        protected Authorization()
        { }
        public Authorization(long id) : base(id)
        {

        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string Application { get; set; }
        public virtual string Feature { get; set; }
        public virtual string Function { get; set; }
        public virtual string QualifierKey { get; set; }
        public virtual string QualifierValue  { get; set; }
        public virtual string Role  { get; set; }
        public virtual string Username { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }
    }
}
