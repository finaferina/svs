﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treemas.Credential.Model
{
    public class ApplicationCRFilter
    {
        public string ApprovalFuntion { get; set; }
        public string ApprovalType { get; set; }
        public string ApprovalStatus { get; set; }
    }
}
