﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;

namespace Treemas.Credential.Model
{
    public class DivisionCr : Entity
    {
        public DivisionCr()
        {
        }
        public DivisionCr(long id) : base(id)
        {
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual ApplicationInfo Application { get; set; }
        public virtual string _Application { get; set; }
        public virtual string DivisonId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Alias { get; set; }
        public virtual string Region { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }
        public virtual bool Approved { get; set; }
        public virtual string ApprovedBy { get; set; }
        public virtual DateTime? ApprovedDate { get; set; }
        public virtual string ApprovalType { get; set; }
        public virtual string ApprovalStatus { get; set; }
        public virtual IList<AuthorizationFunction> Functions { get; set; }
        public virtual ApprovalType ApprovalTypeEnum
        {
            get
            {
                ApprovalType currentState = (ApprovalType)Enum.Parse(typeof(ApprovalType), this.ApprovalType);
                return currentState;
            }
        }
    }
}
