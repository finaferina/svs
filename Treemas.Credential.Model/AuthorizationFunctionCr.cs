﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.Credential.Model
{
    public class AuthorizationFunctionCr: Entity
    {
        protected AuthorizationFunctionCr()
        {
            this.Features = new List<AuthorizationFeatureCr>();
        }

        public AuthorizationFunctionCr(long id) : base(id)
        {

        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual IList<AuthorizationFeatureCr> Features { get; set; }
        public virtual string Application { get; set; }
        public virtual string FunctionId { get; set; }
        public virtual string Description { get; set; }
        public virtual string Name { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }
        public virtual bool Approved { get; set; }
        public virtual string ApprovedBy { get; set; }
        public virtual string ApprovalType { get; set; }
        public virtual ApprovalType ApprovalTypeEnum
        {
            get
            {
                ApprovalType currentState = (ApprovalType)Enum.Parse(typeof(ApprovalType), this.ApprovalType);
                return currentState;
            }
        }

        public virtual DateTime? ApprovedDate { get; set; }
        public virtual string ApprovalStatus { get; set; }
    }
}
