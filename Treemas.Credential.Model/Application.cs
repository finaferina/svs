﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;

namespace Treemas.Credential.Model
{
    [Serializable]
    public class Application : Entity
    {
        protected Application()
        { }
        public Application(long id) : base(id)
        {

        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string Description { get; set; }
        public virtual string ApplicationId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Runtime { get; set; }
        public virtual string Type { get; set; }
        public virtual string Icon { get; set; }
        public virtual string CSSColor { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }
    }
}
