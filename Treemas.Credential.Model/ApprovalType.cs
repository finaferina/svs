﻿using Treemas.Base.Utilities;

namespace Treemas.Credential.Model
{
    public enum ApprovalType : int
    {
        [StringValue("Create")]
        C = 1,
        [StringValue("Reset")]
        R = 2,
        [StringValue("Update")]
        U = 3,
            [StringValue("Delete")]
        D = 4,
        [StringValue("Activate")]
        A = 5,
        [StringValue("Add")]
        N = 6
    }
}
