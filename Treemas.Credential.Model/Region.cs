﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;

namespace Treemas.Credential.Model
{
    [Serializable]
    public class Region : Entity
    {
        protected Region()
        {
            //this.SessionTimeout = 30;
            //this.LockTimeout = 10;
            //this.Functions = new List<AuthorizationFunction>();
        }
        public Region(long id) : base(id)
        {
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual ApplicationInfo Application { get; set; }
        public virtual string _Application { get; set; }
        public virtual string RegionId { get; set; }
        public virtual IList<AuthorizationFunction> Functions { get; set; }
        public virtual string TransId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Alias { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }
    }

    public class RegionAudit
    {
        public virtual string RegionId { get; set; }
        public virtual string TransId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Alias { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }
    }
}
