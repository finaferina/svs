﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;
using Treemas.Base.Globals;

namespace Treemas.Credential.Model
{
    [Serializable]
    public class RoleFunctionFeature : Entity
    {
        protected RoleFunctionFeature()
        {
        }
        public RoleFunctionFeature(long id) : base(id)
        {
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }
        public virtual long RoleFunctionId { get; set; }
        public virtual long RoleId { get; set; }
        public virtual long FunctionId { get; set; }
        public virtual long FeatureId { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }
    }

    public class RoleFunctionFeatureAudit
    {
        public virtual string RoleName { get; set; }
        public virtual string FunctionName { get; set; }
        public virtual string FeatureName { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }
    }
}
