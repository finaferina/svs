﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.Credential.Model
{
    public class ParameterHistoryCentralUnit : Entity
    {
        protected ParameterHistoryCentralUnit()
        {

        }
        public ParameterHistoryCentralUnit(long id) : base(id)
        {
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual string ParamId { get; set; }
        public virtual string UserId { get; set; }
        public virtual string NewUserId { get; set; }
        public virtual string Password { get; set; }
        public virtual string NewPassword { get; set; }
        public virtual int SizeSignature { get; set; }
        public virtual int NewSizeSignature { get; set; }
        public virtual string ApprovalStatus { get; set; }
        public virtual ApprovalStatusState ApprovalStatusStateEnum
        {
            get
            {
                ApprovalStatusState currentState = (ApprovalStatusState)Enum.Parse(typeof(ApprovalStatusState), this.ApprovalStatus);
                return currentState;
            }
        }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }
        public virtual string ApprovedBy { get; set; }
        public virtual DateTime? ApprovedDate { get; set; }
        public virtual int KeepPendingAppr { get; set; }
        public virtual int NewKeepPendingAppr { get; set; }
        public virtual int SizeDocument { get; set; }
        public virtual int NewSizeDocument { get; set; }
        public virtual int KeepAuditTrail { get; set; }
        public virtual int NewKeepAuditTrail { get; set; }
    }
}
