﻿using System;

namespace Treemas.Credential.Model
{
    [Serializable]
    public class AuthorizationFeatureQualifier
    {
        public string Key { get; set; }
        public string Qualifier { get; set; }
    }
}
