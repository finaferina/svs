﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;

namespace Treemas.Credential.Model
{
    [Serializable]
    public class BusinessDate : Entity
    {
        protected BusinessDate()
        { }
        public BusinessDate(long id) : base(id)
        {

        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual DateTime BeginningOfDay { get; protected set; }
        public virtual DateTime CurrentDate { get; protected set; }
        public virtual DateTime EndOfDay { get; protected set; }
        public virtual DateTime BeginningOfDayPrevious { get; protected set; }
        public virtual DateTime BeginningOfDayNext { get; protected set; }
        public virtual DateTime EndOfMonthPrevious { get; protected set; }
        public virtual DateTime EndOfMonthNext { get; protected set; }
        public virtual bool IsBranchClosed { get; protected set; }
        public virtual bool IsCashierClosed { get; protected set; }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }
    }
}
