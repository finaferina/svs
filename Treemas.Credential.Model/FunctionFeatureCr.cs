﻿using System;
using System.Collections.Generic;
using Treemas.Base.Utilities;


namespace Treemas.Credential.Model
{
    public class FunctionFeatureCr : Entity
    {
        protected FunctionFeatureCr()
        {
            
        }
        public FunctionFeatureCr(long id) : base(id)
        {
            
        }
        protected override void validate()
        {
            throw new NotImplementedException();
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new List<Object>() { this.Id };
        }

        public virtual Boolean Selected { get; set; }

        public virtual long FunctionId { get; set; }
        public virtual long FeatureId { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
        public virtual string ChangedBy { get; set; }
        public virtual DateTime? ChangedDate { get; set; }
        public virtual bool Approved { get; set; }
        public virtual string ApprovedBy { get; set; }
        public virtual string ApprovalType { get; set; }
        public virtual ApprovalType ApprovalTypeEnum
        {
            get
            {
                ApprovalType currentState = (ApprovalType)Enum.Parse(typeof(ApprovalType), this.ApprovalType);
                return currentState;
            }
        }

        public virtual DateTime? ApprovedDate { get; set; }
        public virtual string ApprovalStatus { get; set; }
    }
}
