﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Data.SqlClient;
using System.Globalization;
using System.Timers;

namespace UOB.SVS.Push.Data.Svc
{
    public partial class MainService : ServiceBase
    {
        private static IConfiguration _config = null;
        private static string strConnectionString = "";
        private static string strFileLocation { get; set; }
        private static string SuccessFileDest { get; set; }
        private static string ErrorFileDest { get; set; }
        private string FileHistoryName { get; set; }

        private Timer mainTimer = new Timer();

        private static IConfiguration JobConfiguration
        {
            get
            {
                if (_config == null)
                {
                    _config = new SystemConfiguration();
                }
                return _config;
            }
        }

        private static void GetJobConfiguration()
        {
            var config = JobConfiguration.GetJobParam();
            strFileLocation = config.FileLocation;
            SuccessFileDest = config.FileSuccess;
            ErrorFileDest = config.FileError;
        }

        public enum ServiceState
        {
            SERVICE_STOPPED = 0x00000001,
            SERVICE_START_PENDING = 0x00000002,
            SERVICE_STOP_PENDING = 0x00000003,
            SERVICE_RUNNING = 0x00000004,
            SERVICE_CONTINUE_PENDING = 0x00000005,
            SERVICE_PAUSE_PENDING = 0x00000006,
            SERVICE_PAUSED = 0x00000007,
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct ServiceStatus
        {
            public int dwServiceType;
            public ServiceState dwCurrentState;
            public int dwControlsAccepted;
            public int dwWin32ExitCode;
            public int dwServiceSpecificExitCode;
            public int dwCheckPoint;
            public int dwWaitHint;
        };

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(System.IntPtr handle, ref ServiceStatus serviceStatus);

        public MainService()
        {
            InitializeComponent();
            GetJobConfiguration();
            strConnectionString = GetConnectionString();
        }

        private static void RunPushJob()
        {
            DirectoryInfo di = new DirectoryInfo(strFileLocation);
            foreach (FileInfo fi in di.GetFiles())
            {
                if (File.Exists(fi.FullName))
                {
                    if (!IsFileLocked(fi.FullName))
                    {
                        string[] strArgs = new string[1];
                        strArgs[0] = fi.FullName;

                        string pushResult = ProcessPushData(fi.FullName);

                        string strFileName = fi.Name + "_" + DateTime.Now.ToString("yyyyMMddhhmmss");

                        if (pushResult == "Success")
                        {
                            MoveToFolder(SuccessFileDest, fi.FullName, strFileName);
                        }
                        else
                        {
                            MoveToFolder(ErrorFileDest, fi.FullName, strFileName);
                        }
                    }
                }
            }
        }

        private static string ProcessPushData(string FileName)
        {
            strConnectionString = GetConnectionString();
            DateTime beginTime = DateTime.Now;

            // clear all file attributes
            File.SetAttributes(FileName, FileAttributes.Normal);

            // set just only archive and read only attributes (no other attribute will set)
            File.SetAttributes(FileName, FileAttributes.Archive | FileAttributes.ReadOnly);

            CultureInfo CI = CultureInfo.CreateSpecificCulture("id-ID");

            string[] Data = (File.ReadAllLines(FileName, Encoding.UTF8)).Distinct().ToArray();

            double CurrentDataCount = GetCurrentData();
            double MaximumDiff = GetaMaxDiff();
            double LessData = CurrentDataCount - Data.Length;

            if (LessData > 0)
            {
                double DiffResult = (LessData / CurrentDataCount) * 100;
                if (DiffResult >= MaximumDiff)
                {
                    WriteJobLog("Failed", "Data count difference with current Account Master exceeded (" + decimal.Round((decimal)DiffResult, 2) + "%)", beginTime);
                    return "Failed";
                }
            }

            SqlConnection sqlConn = new SqlConnection(strConnectionString);
            SqlTransaction sqlTrans = null;
            SqlCommand sqlComm = new SqlCommand();

            sqlConn.Open();
            sqlTrans = sqlConn.BeginTransaction();
            sqlComm.Connection = sqlConn;
            sqlComm.Transaction = sqlTrans;

            sqlComm.CommandText = "TRUNCATE TABLE ACCOUNT_MASTER";
            sqlComm.ExecuteNonQuery();
            try
            {
                foreach (string data in Data)
                {
                    string[] accData = data.Split(';');

                    sqlComm.Parameters.Clear();
                    sqlComm.Parameters.Add("@ACC_NO", SqlDbType.VarChar, 20).Value = accData[0].Trim();
                    sqlComm.Parameters.Add("@ACC_NAME", SqlDbType.VarChar, 100).Value = accData[1].Trim();
                    sqlComm.Parameters.Add("@ACC_TYPE", SqlDbType.Char, 10).Value = accData[3].Trim();
                    sqlComm.Parameters.Add("@CIF_NO", SqlDbType.VarChar, 20).Value = accData[4].Trim();
                    sqlComm.Parameters.Add("@BRANCH_CODE", SqlDbType.VarChar, 3).Value = accData[5].Trim().PadLeft(3, '0');
                    sqlComm.Parameters.Add("@STATUS", SqlDbType.VarChar, 50).Value = accData[6].Trim();
                    sqlComm.Parameters.Add("@DATE_OPENED", SqlDbType.Date).Value = DateTime.Parse(accData[7].Trim(), CI);
                    sqlComm.CommandText = "INSERT INTO dbo.ACCOUNT_MASTER (ACC_NO,	ACC_NAME, ACC_TYPE, CIF_NO, BRANCH_CODE, STATUS, DATE_OPENED) " +
                        "VALUES(@ACC_NO, @ACC_NAME, @ACC_TYPE, @CIF_NO, @BRANCH_CODE, @STATUS, @DATE_OPENED)";
                    sqlComm.ExecuteNonQuery();
                }

                WriteJobLog("Success", Data.Length.ToString() + " Inserted to Account Master", beginTime);

                sqlTrans.Commit();
                sqlComm.Dispose();
                sqlTrans.Dispose();
                sqlConn.Close();
                sqlConn.Dispose();

                return "Success";
            }
            catch (Exception ex)
            {
                WriteJobLog("Failed", ex.Message.ToString(), beginTime);
                sqlTrans.Rollback();
                sqlComm.Dispose();
                sqlTrans.Dispose();
                sqlConn.Close();
                sqlConn.Dispose();

                return "Failed";
            }
        }
        public void OnTimer(object sender, ElapsedEventArgs args)
        {
            RunPushJob();
        }

        protected override void OnStart(string[] args)
        {
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = ServiceState.SERVICE_START_PENDING;
            serviceStatus.dwWaitHint = 100000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            //mainTimer.Interval = 10000;
            mainTimer.Interval = 60000;// 10 seconds
            mainTimer.Elapsed += new ElapsedEventHandler(this.OnTimer);
            mainTimer.Start();

            serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }

        protected override void OnStop()
        {
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = ServiceState.SERVICE_STOP_PENDING;
            serviceStatus.dwWaitHint = 100000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            mainTimer.Stop();

            serviceStatus.dwCurrentState = ServiceState.SERVICE_STOPPED;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }

        static string GetConnectionString()
        {
            var conn = JobConfiguration.GetConnectionSetting();
            return "Data Source=" + conn.DBInstance + ";Initial Catalog=" + conn.DBName + ";Persist Security Info=True;User ID=" + conn.DBUser + ";Password=" + conn.DBPassword + ";";
        }

        static void WriteJobLog(string status, string remark, DateTime StartTime)
        {
            SqlConnection sqlConn = new SqlConnection(strConnectionString);
            SqlCommand sqlComm = new SqlCommand();

            sqlConn.Open();
            sqlComm.Connection = sqlConn;

            sqlComm.Parameters.Clear();
            sqlComm.Parameters.Add("@JOB_ID", SqlDbType.VarChar, 20).Value = "PUSH_DATA";
            sqlComm.Parameters.Add("@JOB_NAME", SqlDbType.VarChar, 50).Value = "Push Data Account Master";
            sqlComm.Parameters.Add("@STATUS", SqlDbType.Char, 20).Value = status;
            sqlComm.Parameters.Add("@PROCESS_DATE", SqlDbType.DateTime).Value = DateTime.Now.ToShortDateString();
            sqlComm.Parameters.Add("@REMARK", SqlDbType.VarChar, 500).Value = remark;
            sqlComm.Parameters.Add("@BEGIN_TIME", SqlDbType.DateTime).Value = StartTime;
            sqlComm.Parameters.Add("@END_TIME", SqlDbType.DateTime).Value = DateTime.Now;
            sqlComm.CommandText = "INSERT INTO DBO.EOD_JOB(JOB_ID, JOB_NAME, STATUS, REMARK, PROCESS_DATE, BEGIN_TIME, END_TIME) " +
                "VALUES(@JOB_ID, @JOB_NAME, @STATUS, @REMARK, @PROCESS_DATE, @BEGIN_TIME, @END_TIME)";
            sqlComm.ExecuteNonQuery();

            sqlComm.Dispose();
            sqlConn.Close();
            sqlConn.Dispose();
        }

        static double GetCurrentData()
        {
            SqlConnection sqlConn = new SqlConnection(strConnectionString);
            SqlCommand sqlComm = new SqlCommand();
            int retValue = 0;

            sqlConn.Open();
            sqlComm.Connection = sqlConn;

            sqlComm.CommandText = "SELECT COUNT(1) AS DTCOUNT FROM ACCOUNT_MASTER";
            SqlDataReader sqlReader = sqlComm.ExecuteReader();

            if (sqlReader.HasRows)
            {
                sqlReader.Read();
                retValue = Convert.ToInt32(sqlReader["DTCOUNT"].ToString());
            }

            sqlReader.Close();
            sqlComm.Dispose();
            sqlConn.Close();
            sqlConn.Dispose();

            return retValue;
        }

        static double GetaMaxDiff()
        {
            SqlConnection sqlConn = new SqlConnection(strConnectionString);
            SqlCommand sqlComm = new SqlCommand();
            double retValue = 0;

            sqlConn.Open();
            sqlComm.Connection = sqlConn;

            sqlComm.CommandText = "SELECT MAX_DIFF_ACCOUNT FROM PARAMETER";
            SqlDataReader sqlReader = sqlComm.ExecuteReader();

            if (sqlReader.HasRows)
            {
                sqlReader.Read();
                retValue = Convert.ToInt32(sqlReader["MAX_DIFF_ACCOUNT"].ToString());
            }

            sqlReader.Close();
            sqlComm.Dispose();
            sqlConn.Close();
            sqlConn.Dispose();

            return retValue;
        }

        private static bool IsFileLocked(string filePath)
        {
            // check whether file is read only
            bool isReadOnly = ((File.GetAttributes(filePath) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly);
            return isReadOnly;
        }

        private static void CreateNewFolder(string Path)
        {
            if (!Directory.Exists(Path))
            {
                Directory.CreateDirectory(Path);
            }
        }

        private static void MoveToFolder(string DestPath, string SourcePath, string FileName)
        {
            if (File.Exists(SourcePath))
            {
                if (!Directory.Exists(DestPath))
                {
                    CreateNewFolder(DestPath);
                }
                Directory.Move(SourcePath, DestPath + FileName);
            }
        }
    }
}
