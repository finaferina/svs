﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UOB.SVS.Push.Data.Svc
{
    public interface IConfiguration
    {
        ConnectionParam GetConnectionSetting();
        JobParam GetJobParam();
    }
}
