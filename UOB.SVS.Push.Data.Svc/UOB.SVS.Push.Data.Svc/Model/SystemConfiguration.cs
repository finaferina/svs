﻿using System.Data;
using System.IO;
using UOBISecurity;

namespace UOB.SVS.Push.Data.Svc
{
   public class SystemConfiguration : IConfiguration
    {
        private ConnectionParam connectionSetting;
        private JobParam jobParam;
        private string confifPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location.ToString()) + @"\setting.cfg";

        public ConnectionParam GetConnectionSetting()
        {
            return connectionSetting;
        }

        public JobParam GetJobParam()
        {
            return jobParam;
        }

        public SystemConfiguration()
        {

            DataSet MyDs = new DataSet();
            FileStream MyFS = new FileStream(confifPath, FileMode.Open);
            MyDs.ReadXml(MyFS);

            connectionSetting = new ConnectionParam();
            jobParam = new JobParam();

            ApplicationRegistryHandler appReg = new ApplicationRegistryHandler();
            Encryptor enc = new Encryptor();

            string appName = MyDs.Tables[0].Rows[0]["AppName"].ToString();
            string uobikey = MyDs.Tables[0].Rows[0]["UOBIKey"].ToString();
            string dbnstance = MyDs.Tables[0].Rows[0]["DBInstance"].ToString();
            string dbname = MyDs.Tables[0].Rows[0]["DBName"].ToString();
            string dbuser = MyDs.Tables[0].Rows[0]["DBUser"].ToString();
            string dbpassword = MyDs.Tables[0].Rows[0]["DBPassword"].ToString();
            string filelocation = MyDs.Tables[0].Rows[0]["FileLocation"].ToString();
            string filesuccess = MyDs.Tables[0].Rows[0]["FileSuccess"].ToString();
            string fileerror = MyDs.Tables[0].Rows[0]["FileError"].ToString();

            appName = appName.Trim().Replace(" ", "");

            string keyReg = appReg.ReadFromRegistry(@"Software\UOBSVS", "Key");

            connectionSetting.DBInstance = dbnstance;
            connectionSetting.DBName = dbname;
            connectionSetting.DBUser = enc.Decrypt(dbuser, keyReg, uobikey);
            connectionSetting.DBPassword = enc.Decrypt(dbpassword, keyReg, uobikey);

            jobParam.FileLocation = filelocation;
            jobParam.FileSuccess = filesuccess;
            jobParam.FileError = fileerror;

            MyFS.Flush();
            MyFS.Close();
            MyDs.Dispose();
        }
    }
}
