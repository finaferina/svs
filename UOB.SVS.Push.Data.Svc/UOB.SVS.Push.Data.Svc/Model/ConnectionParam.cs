﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UOB.SVS.Push.Data.Svc
{
    public class ConnectionParam
    {
        public string DBInstance { get; set; }
        public string DBName { get; set; }
        public string DBUser { get; set; }
        public string DBPassword { get; set; }
    }
}
