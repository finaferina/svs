﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UOB.SVS.Push.Data.Svc
{
    public class JobConfiguration
    {
        public string JobID { get; set; }
        public string ConfigName { get; set; }
        public string ConfigValue { get; set; }
    }
}
