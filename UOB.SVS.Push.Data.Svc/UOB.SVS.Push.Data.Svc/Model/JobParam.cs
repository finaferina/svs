﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UOB.SVS.Push.Data.Svc
{
    public class JobParam
    {
        public string FileLocation { get; set; }
        public string FileSuccess { get; set; }
        public string FileError { get; set; }
    }
}
