﻿using System.IO;
using System.Reflection;

namespace Treemas.Base.Configuration.Binder
{
    public class AssemblyTextFileConfigurationBinder : BaseTextFileConfigurationBinder
    {
        private Assembly assembly;
        private string namespacePath;

        public AssemblyTextFileConfigurationBinder(string label, string rootNamespace, Assembly assembly) : base(label)
        {
            this.namespacePath = rootNamespace;
            this.assembly = assembly;
        }

        public override void Load()
        {
            if (!string.IsNullOrEmpty(this.namespacePath) && (this.assembly != null))
            {
                string name = this.namespacePath + "." + base.GetLabel() + ".config";
                Stream manifestResourceStream = this.assembly.GetManifestResourceStream(name);
                base.Load(manifestResourceStream);
            }
        }

        public override void Save()
        {
        }
    }
}

