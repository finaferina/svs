﻿using System.IO;

namespace Treemas.Base.Configuration.Binder
{
    public class DifferentialTextFileConfigurationBinder : BaseTextFileConfigurationBinder
    {
        private string marker;
        private string path;

        public DifferentialTextFileConfigurationBinder(string label, string marker, string path) : base(label)
        {
            this.marker = marker;
            this.path = path;
        }

        public override void Load()
        {
            if (!string.IsNullOrEmpty(this.path))
            {
                string path = this.path + @"\" + base.GetLabel() + ".config";
                if (!string.IsNullOrEmpty(this.marker))
                {
                    path = this.path + @"\" + base.GetLabel() + "-" + this.marker + ".config";
                }
                if (File.Exists(path))
                {
                    Stream stream = File.OpenRead(path);
                    base.Load(stream);
                }
            }
        }

        public override void Save()
        {
            if (!string.IsNullOrEmpty(this.path))
            {
                string path = this.path + @"\" + base.GetLabel() + ".config";
                if (!string.IsNullOrEmpty(this.marker))
                {
                    path = this.path + @"\" + base.GetLabel() + "-" + this.marker + ".config";
                }
                File.WriteAllText(path, string.Empty);
                Stream stream = File.OpenWrite(path);
                base.Save(stream);
            }
        }
    }
}

