﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Treemas.Base.Configuration
{
    public abstract class ConfigurationBinder : IConfigurationBinder
    {
        private IDictionary<string, ConfigurationItem> configurationMap;
        protected const string EXTENSION = "config";
        private string label;

        public ConfigurationBinder(string label)
        {
            this.label = label;
            this.configurationMap = new Dictionary<string, ConfigurationItem>();
        }

        public void AddConfiguration(ConfigurationItem configuration)
        {
            string key = configuration.Key;
            if (this.configurationMap.ContainsKey(key))
            {
                this.configurationMap[key] = configuration;
            }
            else
            {
                this.configurationMap.Add(key, configuration);
            }
        }

        public ConfigurationItem GetConfiguration(string key)
        {
            if (!string.IsNullOrEmpty(key) && this.configurationMap.ContainsKey(key))
            {
                return this.configurationMap[key];
            }
            return null;
        }

        public ConfigurationItem[] GetConfigurations()
        {
            if (this.configurationMap.Count > 0)
            {
                return this.configurationMap.Values.ToArray<ConfigurationItem>();
            }
            return null;
        }

        public IList<string> GetKeys()
        {
            if (this.configurationMap.Count > 0)
            {
                return this.configurationMap.Keys.ToList<string>().AsReadOnly();
            }
            return null;
        }

        public string GetLabel() => 
            this.label;

        public abstract void Load();
        public void MarkAllAsPersisted()
        {
            foreach (ConfigurationItem item in this.configurationMap.Values)
            {
                item.Transient = false;
            }
        }

        public void MarkAllAsTransient()
        {
            foreach (ConfigurationItem item in this.configurationMap.Values)
            {
                item.Transient = true;
            }
        }

        public void MarkAsTransient(string key)
        {
            if (this.configurationMap.ContainsKey(key))
            {
                this.configurationMap[key].Transient = true;
            }
        }

        public void RemoveConfiguration(string key)
        {
            if (!string.IsNullOrEmpty(key))
            {
                this.configurationMap.Remove(key);
            }
        }

        public void RemoveNonPersistedConfigurations()
        {
            IList<string> list = new List<string>();
            foreach (ConfigurationItem item in this.configurationMap.Values)
            {
                if (item.Transient)
                {
                    list.Add(item.Key);
                }
            }
            foreach (string str in list)
            {
                this.configurationMap.Remove(str);
            }
        }

        public abstract void Save();
        public void UnmarkAsTransient(string key)
        {
            if (this.configurationMap.ContainsKey(key))
            {
                this.configurationMap[key].Transient = false;
            }
        }
    }
}

