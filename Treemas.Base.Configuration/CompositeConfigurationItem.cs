﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Treemas.Base.Configuration
{
    public class CompositeConfigurationItem : ConfigurationItem
    {
        private const string CONCATENATION_KEY_VALUE_SEPARATOR = ":";
        private const string CONCATENATION_SEPARATOR = "|";

        public CompositeConfigurationItem() : this(null)
        {
        }

        public CompositeConfigurationItem(string key) : this(key, null)
        {
        }

        public CompositeConfigurationItem(string key, string description) : this(key, description, null)
        {
        }

        public CompositeConfigurationItem(string key, string description, params ConfigurationItem[] configItems)
        {
            this.Items = new Dictionary<string, ConfigurationItem>();
            base.Key = key;
            base.Description = description;
            if ((configItems != null) && (configItems.Length > 0))
            {
                foreach (ConfigurationItem item in configItems)
                {
                    this.Items.Add(item.Key, item);
                }
            }
        }

        public void AddItem(ConfigurationItem config)
        {
            if (config != null)
            {
                if (this.Items.ContainsKey(config.Key))
                {
                    this.Items[config.Key] = config;
                }
                else
                {
                    this.Items.Add(config.Key, config);
                }
            }
        }

        public ConfigurationItem GetItem(string key)
        {
            if (this.Items.ContainsKey(key))
            {
                return this.Items[key];
            }
            return null;
        }

        public ConfigurationItem[] GetItems()
        {
            if (this.Items.Count > 0)
            {
                return this.Items.Values.ToArray<ConfigurationItem>();
            }
            return null;
        }

        public bool HasItem(string key) => 
            (this.GetItem(key) != null);

        public void RemoveItem(string key)
        {
            this.Items.Remove(key);
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            if (!string.IsNullOrEmpty(base.Description))
            {
                builder.AppendLine($"# {base.Key} ({base.Description})");
            }
            else
            {
                builder.AppendLine($"# {base.Key}");
            }
            if (this.Items.Count > 0)
            {
                foreach (ConfigurationItem item in this.Items.Values)
                {
                    builder.AppendLine(item.ToString());
                }
            }
            return builder.ToString();
        }

        public bool HasItems =>
            (this.Items.Count > 0);

        private IDictionary<string, ConfigurationItem> Items { get; set; }

        public override string Value
        {
            get
            {
                if (this.Items.Count <= 0)
                {
                    return base.Value;
                }
                StringBuilder builder = new StringBuilder();
                foreach (ConfigurationItem item in this.Items.Values)
                {
                    builder.Append("| " + $"{item.Key}{":"} {item.Value}");
                }
                string str = builder.ToString().Trim();
                return str.Substring(1, str.Length);
            }
            set
            {
                base.Value = value;
            }
        }
    }
}

