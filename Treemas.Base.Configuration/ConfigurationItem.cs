﻿namespace Treemas.Base.Configuration
{
    public class ConfigurationItem
    {
        public ConfigurationItem()
        {
        }

        public ConfigurationItem(string key, string value) : this(null, key, value)
        {
        }

        public ConfigurationItem(string description, string key, string value)
        {
            this.Key = key;
            this.Value = value;
            this.Description = description;
            this.Transient = false;
        }

        public override string ToString() => 
            $"{this.Key} = {this.Value} ({this.Description})";

        public string Description { get; set; }

        public string Key { get; set; }

        public bool Transient { get; set; }

        public virtual string Value { get; set; }
    }
}

