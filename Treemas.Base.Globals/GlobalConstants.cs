﻿namespace Treemas.Base.Globals
{
    public class GlobalConstants
    {
        private static readonly GlobalConstants instance = new GlobalConstants();
        private GlobalConstants()
        {
            
        }
        public string HASH_KEY => "K3yCrypt0";
        public static GlobalConstants Instance =>
           instance;
    }
}
