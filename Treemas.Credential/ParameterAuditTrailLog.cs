﻿using System;
using System.Globalization;
using System.Linq;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;
using Treemas.Base.Utilities;
using Newtonsoft.Json;
using Treemas.Base.Repository;
using NHibernate.Criterion;
using System.Collections.Generic;

namespace Treemas.Credential
{
    public class ParameterAuditTrailLog : RepositoryController<ParameterAuditTrail>, IParameterAuditTrail
    {
        private IParameterAuditTrailRepository _auditRepository;
        public ParameterAuditTrailLog(IParameterAuditTrailRepository auditRepository)
        {
            _auditRepository = auditRepository;
        }

        public void SaveAuditTrail(string functionName, object auditedObjectBefore, object auditedObjectAfter, string maker, DateTime? requestDate, string actionby, string action)
        {
            string objectBefore= JsonConvert.SerializeObject(auditedObjectBefore);
            string objectAfter = JsonConvert.SerializeObject(auditedObjectAfter);

            ParameterAuditTrail audit = new ParameterAuditTrail(0L);
            audit.TimeStamp = DateTime.Now;
            audit.FunctionName = functionName;
            audit.ObjectName = "Parameter";// auditedObjectBefore.GetType().Name;
            audit.ObjectValueBefore = AuditTrailJsonBefore(objectBefore);
            audit.ObjectValueAfter = AuditTrailJsonAfter(objectAfter);
            audit.UserMaker = maker;
            audit.RequestDate = requestDate;
            audit.Action = action;
            audit.UserChecker = actionby;
            audit.ApproveDate = audit.TimeStamp;
            _auditRepository.Add(audit);
        }

        public void SaveAuditTrail(string functionName, object auditedObject, string actionby, string action)
        {
            ParameterAuditTrail audit = new ParameterAuditTrail(0L);
            audit.FunctionName = functionName;
            audit.ObjectName = auditedObject.GetType().Name;
            audit.ObjectValue = JsonConvert.SerializeObject(auditedObject);
            audit.Action = action;
            audit.ActionBy = actionby;
            audit.ActionDateTime = DateTime.Now;
            _auditRepository.Add(audit);
        }

        public PagedResult<ParameterAuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<ParameterAuditTrail> paged = new PagedResult<ParameterAuditTrail>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<ParameterAuditTrail>()
                                                    .OrderBy(Projections.Property(orderColumn)).Asc
                                                    .Skip((pageNumber) * itemsPerPage)
                                                    .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<ParameterAuditTrail>()
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<ParameterAuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            ICriterion specification;

            if (searchColumn == "ActionDateTime")
            {
                DateTime reqDate;
                DateTime.TryParseExact(searchValue, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out reqDate);
                DateTime tempDate = reqDate.AddDays(1);
                specification = Restrictions.Between(searchColumn, reqDate, tempDate);
            }
            else
            {
                specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
            }

            PagedResult<ParameterAuditTrail> paged = new PagedResult<ParameterAuditTrail>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<ParameterAuditTrail>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<ParameterAuditTrail>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }


            paged.TotalItems = transact(() =>
                         session.QueryOver<ParameterAuditTrail>()
                                .Where(specification).RowCount());
            return paged;
        }

        public PagedResult<ParameterAuditTrail> FindAllPagedDate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue)
        {

            PagedResult<ParameterAuditTrail> paged = new PagedResult<ParameterAuditTrail>(pageNumber, itemsPerPage);
            if (searchColumn.StringEqualsIgnoreCase("ActionDateTime"))
            {
                if (orderKey.ToLower() == "asc")
                {
                    paged.Items = transact(() =>
                              session.QueryOver<ParameterAuditTrail>()
                                     .Where(f => f.ActionDateTime == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() =>
                              session.QueryOver<ParameterAuditTrail>()
                                     .Where(f => f.ActionDateTime == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                paged.TotalItems = transact(() =>
                        session.QueryOver<ParameterAuditTrail>()
                               .Where(f => f.ActionDateTime == searchValue).RowCount());
            }

            return paged;
        }

        //untuk menghilangkan data model yang tidak ingin ditampilkan
        public string AuditTrailJsonBefore(string objectBefore)
        {

            //Dibuat kamus map dictionary
            Dictionary<string, string> mapObjectValueBefore = JsonConvert.DeserializeObject<Dictionary<string, string>>(objectBefore);

            //Temp 
            Dictionary<string, string>.KeyCollection tempMapObjectValueBefore = new Dictionary<string, string>.KeyCollection(mapObjectValueBefore);

            string[] KeymapObjectValueBefore = new string[mapObjectValueBefore.Count];

            tempMapObjectValueBefore.CopyTo(KeymapObjectValueBefore, 0);

            //Hapuss mapObjectValueBefore
            foreach (var itemmapObjectValueBefore in KeymapObjectValueBefore)
            {
                if (itemmapObjectValueBefore == "UserId" || itemmapObjectValueBefore == "ParamId" || itemmapObjectValueBefore == "Password"
                    || itemmapObjectValueBefore == "NewUserId" || itemmapObjectValueBefore == "NewPassword" || itemmapObjectValueBefore == "NewStaggingUrl"
                    || itemmapObjectValueBefore == "NewSessionTimeout" || itemmapObjectValueBefore == "NewLoginAttempt" || itemmapObjectValueBefore == "NewPasswordExp"
                    || itemmapObjectValueBefore == "NewDomainServer" || itemmapObjectValueBefore == "DomainServer" || itemmapObjectValueBefore == "ApprovalStatus"
                    || itemmapObjectValueBefore == "ApprovalStatusStateEnum" || itemmapObjectValueBefore == "ChangedBy" || itemmapObjectValueBefore == "ApprovedBy"
                    || itemmapObjectValueBefore == "NewSizeKTP" || itemmapObjectValueBefore == "NewSizeSignature" || itemmapObjectValueBefore == "NewSizeDocument"
                    || itemmapObjectValueBefore == "NewKeepHistory" || itemmapObjectValueBefore == "NewKeepAuditTrail" || itemmapObjectValueBefore == "NewKeepPendingAppr"
                    || itemmapObjectValueBefore == "NewMaxDiffAccount" || itemmapObjectValueBefore == "StaggingUrl" || itemmapObjectValueBefore == "RecordVersion"
                    || itemmapObjectValueBefore == "GetSizeKTP" || itemmapObjectValueBefore == "GetSizeSignature" || itemmapObjectValueBefore == "GetSizeDocument"
                    || itemmapObjectValueBefore == "GetSizeDocumentJS" || itemmapObjectValueBefore == "Id" || itemmapObjectValueBefore == "GetSizeSignatureJS"
                    || itemmapObjectValueBefore == "GetSizeKTPJS" || itemmapObjectValueBefore == "PasswordExp"
                    )
                {
                    mapObjectValueBefore.Remove(itemmapObjectValueBefore);
                }

            }
            

            //susun kembali
            var newObjectValueBefore = JsonConvert.SerializeObject(mapObjectValueBefore, Formatting.None);

            return newObjectValueBefore;
        }

        //untuk menghilangkan data model yang tidak ingin ditampilkan
        public string AuditTrailJsonAfter(string objectAfter)
        {

            //Dibuat kamus map dictionary
            Dictionary<string, string> mapObjectValueAfter = JsonConvert.DeserializeObject<Dictionary<string, string>>(objectAfter);

            //Temp 
            Dictionary<string, string>.KeyCollection tempMapObjectValueAfter = new Dictionary<string, string>.KeyCollection(mapObjectValueAfter);

            string[] KeymapObjectValueAfter = new string[mapObjectValueAfter.Count];

            tempMapObjectValueAfter.CopyTo(KeymapObjectValueAfter, 0);
            
            //Hapuss mapitemObjectValueAfter
            foreach (var itemmapObjectValueAfter in KeymapObjectValueAfter)
            {
                if (itemmapObjectValueAfter == "UserId" || itemmapObjectValueAfter == "ParamId" || itemmapObjectValueAfter == "Password"
                    || itemmapObjectValueAfter == "DomainServer" || itemmapObjectValueAfter == "ChangedBy" || itemmapObjectValueAfter == "ChangedDate"
                    || itemmapObjectValueAfter == "GetSizeKTP" || itemmapObjectValueAfter == "GetSizeSignature" || itemmapObjectValueAfter == "GetSizeDocument"
                    || itemmapObjectValueAfter == "GetSizeDocumentJS" || itemmapObjectValueAfter == "StaggingUrl" || itemmapObjectValueAfter == "NewStaggingUrl"
                    || itemmapObjectValueAfter == "NewDomainServer" || itemmapObjectValueAfter == "NewUserId" || itemmapObjectValueAfter == "NewPassword"
                    || itemmapObjectValueAfter == "RecordVersion" || itemmapObjectValueAfter == "ApprovalStatusStateEnum" || itemmapObjectValueAfter == "Id"
                    || itemmapObjectValueAfter == "ApprovalStatus" || itemmapObjectValueAfter == "ID" || itemmapObjectValueAfter == "SizeKTP" || itemmapObjectValueAfter == "SizeSignature"
                    || itemmapObjectValueAfter == "SizeDocument" || itemmapObjectValueAfter == "KeepHistory" || itemmapObjectValueAfter == "KeepAuditTrail" || itemmapObjectValueAfter == "KeepPendingAppr"
                    || itemmapObjectValueAfter == "MaxDiffAccount" || itemmapObjectValueAfter == "SessionTimeout" || itemmapObjectValueAfter == "LoginAttempt" || itemmapObjectValueAfter == "PasswordExp"
                    || itemmapObjectValueAfter == "KeepCloseAccount" || itemmapObjectValueAfter == "NewPasswordExp"
                    )
                {
                    mapObjectValueAfter.Remove(itemmapObjectValueAfter);
                }

            }

            //susun kembali
            var newObjectValueAfter = JsonConvert.SerializeObject(mapObjectValueAfter, Formatting.None);

            return newObjectValueAfter;
        }
    }
}
