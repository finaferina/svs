﻿using System;
using System.Globalization;
using System.Linq;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;
using Treemas.Base.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Treemas.Base.Repository;
using NHibernate.Criterion;
using System.Collections.Generic;
using System.IO;

namespace Treemas.Credential
{
    public class UserAuditTrailLog : RepositoryController<UserAuditTrail>, IUserAuditTrailLog
    {
        private IUserAuditTrailRepository _auditRepository;
        public UserAuditTrailLog(IUserAuditTrailRepository auditRepository)
        {
            this._auditRepository = auditRepository;
        }
        public void SaveAuditTrail(string functionName, object auditedObjectBefore, object auditedObjectAfter, string maker, DateTime? requestDate, string actionby, string action)
        {
            User bUser = (User)auditedObjectBefore;
            User nUser = (User)auditedObjectAfter;

            JsonTextReader joReader = new JsonTextReader(new StringReader(JsonConvert.SerializeObject(auditedObjectBefore)));
            JsonTextReader jnReader = new JsonTextReader(new StringReader(JsonConvert.SerializeObject(auditedObjectAfter)));
            JObject joBefore = JObject.Load(joReader);
            JObject joAfter = JObject.Load(jnReader);

            int i = 0;

            //before
            string[] Applist = new string[bUser.Applications.IsNull() ? 0 : bUser.Applications.Count];
            string[] RoleList = new string[bUser.Roles.IsNull() ? 0 : bUser.Roles.Count];

            if (!bUser.Applications.IsNull())
            {
                foreach (ApplicationInfo app in bUser.Applications)
                {
                    if (!app.IsNull())
                    {
                        Applist[i] = app.Name;
                        i++;
                    }
                }
            }

            i = 0;
            if (!bUser.Roles.IsNull())
            {
                foreach (Role role in bUser.Roles)
                {
                    if (!role.IsNull())
                    {
                        RoleList[i] = role.Name;
                        i++;
                    }
                }
            }

            joBefore.Remove("Applications");
            joBefore.Remove("Roles");

            joBefore.Add("Applications", JToken.FromObject(Applist));
            joBefore.Add("Roles", JToken.FromObject(RoleList));

            //auditedObjectBefore = JsonConvert.SerializeObject(joBefore);

            //after
            Applist = new string[nUser.Applications.IsNull() ? 0 : nUser.Applications.Count];
            RoleList = new string[nUser.Roles.IsNull() ? 0 : nUser.Roles.Count];

            i = 0;
            if (!nUser.Applications.IsNull())
            {
                foreach (ApplicationInfo app in nUser.Applications)
                {
                    if (!app.IsNull())
                    {
                        Applist[i] = app.Name;
                        i++;
                    }
                }
            }

            i = 0;
            if (!nUser.Roles.IsNull())
            {
                foreach (Role role in nUser.Roles)
                {
                    if (!role.IsNull())
                    {
                        RoleList[i] = role.Name;
                        i++;
                    }
                }
            }
            joAfter.Remove("Applications");
            joAfter.Remove("Roles");
            joAfter.Add("Applications", JToken.FromObject(Applist));
            joAfter.Add("Roles", JToken.FromObject(RoleList));

            //Remove certain no-used properties
            joBefore.Remove("Applications");
            joBefore.Remove("AccountValidityDate");
            joBefore.Remove("PasswordExpirationDate");
            joBefore.Remove("Application");
            joBefore.Remove("CurrentApplication");
            joBefore.Remove("LoginInfos");
            joBefore.Remove("BusinessDate");
            joBefore.Remove("IsPasswordNeedToBeReset");
            joBefore.Remove("SessionTimeout");
            joBefore.Remove("Password");
            joBefore.Remove("UpdateMenu");
            joBefore.Remove("MaximumConcurrentLogin");
            joBefore.Remove("IsMaximumConcurrentLoginReached");
            joBefore.Remove("LockTimeout");
            joBefore.Remove("RegNo");
            joBefore.Remove("IsPrinted");
            joBefore.Remove("IsSuperUser");
            joBefore.Remove("Approved");
            joBefore.Remove("Id");
            joBefore.Remove("RecordVersion");
            joBefore.Remove("Applications");

            joAfter.Remove("AccountValidityDate");
            joAfter.Remove("PasswordExpirationDate");
            joAfter.Remove("Application");
            joAfter.Remove("CurrentApplication");
            joAfter.Remove("LoginInfos");
            joAfter.Remove("BusinessDate");
            joAfter.Remove("IsPasswordNeedToBeReset");
            joAfter.Remove("SessionTimeout");
            joAfter.Remove("Password");
            joAfter.Remove("UpdateMenu");
            joAfter.Remove("MaximumConcurrentLogin");
            joAfter.Remove("IsMaximumConcurrentLoginReached");
            joAfter.Remove("LockTimeout");
            joAfter.Remove("RegNo");
            joAfter.Remove("IsPrinted");
            joAfter.Remove("IsSuperUser");
            joAfter.Remove("Approved");
            joAfter.Remove("Id");
            joAfter.Remove("RecordVersion");

            UserAuditTrail audit = new UserAuditTrail(0L);
            audit.TimeStamp = DateTime.Now;
            audit.FunctionName = functionName;
            audit.ObjectName = auditedObjectBefore.GetType().Name;
            audit.ObjectValueBefore = JsonConvert.SerializeObject(joBefore); 
            audit.ObjectValueAfter = JsonConvert.SerializeObject(joAfter); 
            audit.UserMaker = maker;
            audit.RequestDate = requestDate;
            audit.Action = action;
            audit.UserChecker = actionby;
            audit.ApproveDate = DateTime.Now;
            _auditRepository.Add(audit);
        }

        public void SaveAuditTrail(string functionName, string objectName, object auditedObjectBefore, object auditedObjectAfter, string maker, DateTime? requestDate, string actionby, string action)
        {
            UserAuditTrail audit = new UserAuditTrail(0L);
            audit.TimeStamp = DateTime.Now;
            audit.FunctionName = functionName;
            audit.ObjectName = objectName;
            audit.ObjectValueBefore = JsonConvert.SerializeObject(auditedObjectBefore);
            audit.ObjectValueAfter = JsonConvert.SerializeObject(auditedObjectAfter);
            audit.UserMaker = maker;
            audit.RequestDate = requestDate;
            audit.Action = action;
            audit.UserChecker = actionby;
            audit.ApproveDate = DateTime.Now;
            _auditRepository.Add(audit);
        }
        public PagedResult<UserAuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<UserAuditTrail> paged = new PagedResult<UserAuditTrail>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<UserAuditTrail>()
                                                    .OrderBy(Projections.Property(orderColumn)).Asc
                                                    .Skip((pageNumber) * itemsPerPage)
                                                    .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<UserAuditTrail>()
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<UserAuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            ICriterion specification;

            if (searchColumn == "ActionDateTime")
            {
                DateTime reqDate;
                DateTime.TryParseExact(searchValue, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out reqDate);
                DateTime tempDate = reqDate.AddDays(1);
                specification = Restrictions.Between(searchColumn, reqDate, tempDate);
            }
            else
            {
                specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
            }

            PagedResult<UserAuditTrail> paged = new PagedResult<UserAuditTrail>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<UserAuditTrail>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<UserAuditTrail>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }


            paged.TotalItems = transact(() =>
                         session.QueryOver<UserAuditTrail>()
                                .Where(specification).RowCount());
            return paged;
        }

        public PagedResult<UserAuditTrail> FindAllPagedDate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue)
        {

            PagedResult<UserAuditTrail> paged = new PagedResult<UserAuditTrail>(pageNumber, itemsPerPage);
            if (searchColumn.StringEqualsIgnoreCase("ActionDateTime"))
            {
                if (orderKey.ToLower() == "asc")
                {
                    paged.Items = transact(() =>
                              session.QueryOver<UserAuditTrail>()
                                     .Where(f => f.TimeStamp == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() =>
                              session.QueryOver<UserAuditTrail>()
                                     .Where(f => f.TimeStamp == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                paged.TotalItems = transact(() =>
                        session.QueryOver<UserAuditTrail>()
                               .Where(f => f.TimeStamp == searchValue).RowCount());
            }

            return paged;
        }

    }
}
