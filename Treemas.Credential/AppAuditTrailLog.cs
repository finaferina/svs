﻿using System;
using System.Globalization;
using System.Linq;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;
using Treemas.Base.Utilities;
using Newtonsoft.Json;
using Treemas.Base.Repository;
using NHibernate.Criterion;


namespace Treemas.Credential
{
    public class AppAuditTrailLog : RepositoryController<AppAuditTrail>, IAppAuditTrailLog
    {
        private IAppAuditTrailRepository _auditRepository;
        public AppAuditTrailLog(IAppAuditTrailRepository auditRepository)
        {
            _auditRepository = auditRepository;
        }

        public void SaveAuditTrail(string functionName, object auditedObjectBefore, object auditedObjectAfter, string maker, DateTime? requestDate, string actionby, string action)
        {
            AppAuditTrail audit = new AppAuditTrail(0L);
            audit.TimeStamp = DateTime.Now;
            audit.FunctionName = functionName;
            audit.ObjectName = auditedObjectBefore.GetType().Name;
            audit.ObjectValueBefore = JsonConvert.SerializeObject(auditedObjectBefore);
            audit.ObjectValueAfter = JsonConvert.SerializeObject(auditedObjectAfter);
            audit.UserMaker = maker;
            audit.RequestDate = requestDate;
            audit.Action = action;
            audit.UserChecker = actionby;
            audit.ApproveDate = DateTime.Now;
            _auditRepository.Add(audit);
        }

        public void SaveAuditTrail(string functionName, string objectName, object auditedObjectBefore, object auditedObjectAfter, string maker, DateTime? requestDate, string actionby, string action)
        {
            AppAuditTrail audit = new AppAuditTrail(0L);
            audit.TimeStamp = DateTime.Now;
            audit.FunctionName = functionName;
            audit.ObjectName = objectName;
            audit.ObjectValueBefore = JsonConvert.SerializeObject(auditedObjectBefore);
            audit.ObjectValueAfter = JsonConvert.SerializeObject(auditedObjectAfter);
            audit.UserMaker = maker;
            audit.RequestDate = requestDate;
            audit.Action = action;
            audit.UserChecker = actionby;
            audit.ApproveDate = DateTime.Now;
            _auditRepository.Add(audit);
        }

        public PagedResult<AppAuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<AppAuditTrail> paged = new PagedResult<AppAuditTrail>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<AppAuditTrail>()
                                                    .OrderBy(Projections.Property(orderColumn)).Asc
                                                    .Skip((pageNumber) * itemsPerPage)
                                                    .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<AppAuditTrail>()
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<AppAuditTrail> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            ICriterion specification;

            if (searchColumn == "ActionDateTime")
            {
                DateTime reqDate;
                DateTime.TryParseExact(searchValue, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out reqDate);
                DateTime tempDate = reqDate.AddDays(1);
                specification = Restrictions.Between(searchColumn, reqDate, tempDate);
            }
            else
            {
                specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
            }

            PagedResult<AppAuditTrail> paged = new PagedResult<AppAuditTrail>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<AppAuditTrail>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<AppAuditTrail>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }


            paged.TotalItems = transact(() =>
                         session.QueryOver<AppAuditTrail>()
                                .Where(specification).RowCount());
            return paged;
        }

        public PagedResult<AppAuditTrail> FindAllPagedDate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue)
        {

            PagedResult<AppAuditTrail> paged = new PagedResult<AppAuditTrail>(pageNumber, itemsPerPage);
            if (searchColumn.StringEqualsIgnoreCase("ActionDateTime"))
            {
                if (orderKey.ToLower() == "asc")
                {
                    paged.Items = transact(() =>
                              session.QueryOver<AppAuditTrail>()
                                     .Where(f => f.ActionDateTime == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() =>
                              session.QueryOver<AppAuditTrail>()
                                     .Where(f => f.ActionDateTime == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                paged.TotalItems = transact(() =>
                        session.QueryOver<AppAuditTrail>()
                               .Where(f => f.ActionDateTime == searchValue).RowCount());
            }

            return paged;
        }
    }
}
