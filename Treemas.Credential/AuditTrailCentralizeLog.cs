﻿using System;
using System.Globalization;
using System.Linq;
using Treemas.Credential.Model;
using Treemas.Credential.Interface;
using Treemas.Base.Utilities.Queries;
using Treemas.Base.Utilities;
using Newtonsoft.Json;
using Treemas.Base.Repository;
using NHibernate.Criterion;
using UOB.SVS.Model;

namespace Treemas.Credential
{
    public class AuditTrailCentralizeLog : RepositoryController<AuditTrailCentralUnit>, IAuditTrailCentalizeLog
    {
        //private IAuditTrailRepository _auditRepository;
        private IAuditTrailCentralUnitRepository _auditRepository;
        public AuditTrailCentralizeLog(IAuditTrailCentralUnitRepository auditRepository)
        {
            this._auditRepository = auditRepository;
        }

        public void SaveAuditTrail(string functionName, object auditedObjectBefore, object auditedObjectAfter, string maker, DateTime? requestDate, string actionby, string action)
        {
            AuditTrailCentralUnit audit = new AuditTrailCentralUnit(0L);
            audit.TimeStamp = DateTime.Now;
            audit.FunctionName = functionName;
            audit.ObjectName = "SVS";
            audit.UserMaker = maker;
            audit.RequestDate = requestDate;

            if (action == "C")
            {
                audit.Action = "Create";
                ////SignatureHeaderAudit ObjectBefore = new SignatureHeaderAudit();
                //CentralizeSignature ObjectBefore = new CentralizeSignature()
                audit.ObjectValueBefore = JsonConvert.SerializeObject(auditedObjectBefore);
                audit.ObjectValueAfter = JsonConvert.SerializeObject(auditedObjectAfter);
            }
            else if (action == "U")
            {
                audit.Action = "Update";
                audit.ObjectValueBefore = JsonConvert.SerializeObject(auditedObjectBefore);
                audit.ObjectValueAfter = JsonConvert.SerializeObject(auditedObjectAfter);

            }
            else if (action == "D")
            {
                audit.Action = "Delete";
                //SignatureHeaderAudit ObjectAfter = new SignatureHeaderAudit();
                audit.ObjectValueBefore = JsonConvert.SerializeObject(auditedObjectBefore);
                audit.ObjectValueAfter = JsonConvert.SerializeObject(auditedObjectAfter);
            }
            else
            {
                audit.Action = action;
                audit.ObjectValueBefore = JsonConvert.SerializeObject(auditedObjectBefore);
                audit.ObjectValueAfter = JsonConvert.SerializeObject(auditedObjectAfter);
            }
            audit.UserChecker = actionby;
            audit.ApproveDate = DateTime.Now;
            _auditRepository.Add(audit);
        }

        public void SaveAuditTrail(string functionName, object auditedObject, string actionby, string action)
        {
            AuditTrailCentralUnit audit = new AuditTrailCentralUnit(0L);
            audit.FunctionName = functionName;
            audit.ObjectName = auditedObject.GetType().Name;
            audit.ObjectValue = JsonConvert.SerializeObject(auditedObject);
            audit.Action = action;
            audit.ActionBy = actionby;
            audit.ActionDateTime = DateTime.Now;
            _auditRepository.Add(audit);
        }

        public PagedResult<AuditTrailCentralUnit> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey)
        {
            PagedResult<AuditTrailCentralUnit> paged = new PagedResult<AuditTrailCentralUnit>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() => session.QueryOver<AuditTrailCentralUnit>()
                                                    .OrderBy(Projections.Property(orderColumn)).Asc
                                                    .Skip((pageNumber) * itemsPerPage)
                                                    .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() => session.QueryOver<AuditTrailCentralUnit>()
                                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                                     .Skip((pageNumber) * itemsPerPage)
                                                     .Take(itemsPerPage).List());
            }

            paged.TotalItems = Count;
            return paged;
        }

        public PagedResult<AuditTrailCentralUnit> FindAllPaged(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, string searchValue)
        {
            ICriterion specification;

            if (searchColumn == "ActionDateTime")
            {
                DateTime reqDate;
                DateTime.TryParseExact(searchValue, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out reqDate);
                DateTime tempDate = reqDate.AddDays(1);
                specification = Restrictions.Between(searchColumn, reqDate, tempDate);
            }
            else
            {
                specification = Restrictions.Like(searchColumn, searchValue, MatchMode.Anywhere);
            }

            PagedResult<AuditTrailCentralUnit> paged = new PagedResult<AuditTrailCentralUnit>(pageNumber, itemsPerPage);
            if (orderKey.ToLower() == "asc")
            {
                paged.Items = transact(() =>
                          session.QueryOver<AuditTrailCentralUnit>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Asc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }
            else
            {
                paged.Items = transact(() =>
                          session.QueryOver<AuditTrailCentralUnit>()
                                 .Where(specification)
                                 .OrderBy(Projections.Property(orderColumn)).Desc
                                 .Skip((pageNumber) * itemsPerPage)
                                 .Take(itemsPerPage).List());
            }


            paged.TotalItems = transact(() =>
                         session.QueryOver<AuditTrailCentralUnit>()
                                .Where(specification).RowCount());
            return paged;
        }

        public PagedResult<AuditTrailCentralUnit> FindAllPagedDate(int pageNumber, int itemsPerPage, string orderColumn, string orderKey, string searchColumn, DateTime searchValue)
        {

            PagedResult<AuditTrailCentralUnit> paged = new PagedResult<AuditTrailCentralUnit>(pageNumber, itemsPerPage);
            if (searchColumn.StringEqualsIgnoreCase("ActionDateTime"))
            {
                if (orderKey.ToLower() == "asc")
                {
                    paged.Items = transact(() =>
                              session.QueryOver<AuditTrailCentralUnit>()
                                     .Where(f => f.ActionDateTime == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Asc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                else
                {
                    paged.Items = transact(() =>
                              session.QueryOver<AuditTrailCentralUnit>()
                                     .Where(f => f.ActionDateTime == searchValue)
                                     .OrderBy(Projections.Property(orderColumn)).Desc
                                     .Skip((pageNumber) * itemsPerPage)
                                     .Take(itemsPerPage).List());
                }
                paged.TotalItems = transact(() =>
                        session.QueryOver<AuditTrailCentralUnit>()
                               .Where(f => f.ActionDateTime == searchValue).RowCount());
            }

            return paged;
        }

    }
}
